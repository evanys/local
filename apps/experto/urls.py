#urls apps contable
app_name = "experto"

from django.urls import path
#from apps.dina.views import index
from apps.experto.views import *
from django.conf.urls import url
from django.contrib.auth.views import LogoutView
from django.contrib.auth import views as auth_views

#hojatra
urlpatterns = [
    path('iniciob/', index.as_view(),name='iniciob'),
    #json
    path('planes/', planes.as_view()),
    path('asicab/', asicab.as_view()),
    path('proceso/', proceso.as_view()),
    path('registrarGlosa/', registrarGlosa.as_view()),
    path('BuscarRuc/', BuscarRuc.as_view()),
    path('registrarPersona/', registrarPersona.as_view()),
    path('registrarEmpresa/', registrarEmpresa.as_view()),
    path('buscarMasCuentas/', buscarMasCuentas.as_view()),
    path('buscarDocumento/', buscarDocumento.as_view()),
    path('buscarFlujo/', buscarFlujo.as_view()),
    path('buscarmedio/', buscarmedio.as_view()),
    path('jsonPrueba/', jsonPrueba.as_view()),
    path('pruebaMontos/', pruebaMontos.as_view()),
    path('actualizarMontos/', actualizarMontos.as_view()),
    
    #json creacion de tablas
    path('crearpgActivo/', crearpgActivo.as_view()),
    path('crearPgAdicion/', crearPgAdicion.as_view()),
    path('crear_PgAsicab/', crear_PgAsicab.as_view()),
    path('crear_PgAsidet/', crear_PgAsidet.as_view()),
    path('crear_PgAsiento/', crear_PgAsiento.as_view()),
    path('crear_PgAsimodel/', crear_PgAsimodel.as_view()),
    path('crear_PgCampat1/', crear_PgCampat1.as_view()),
    path('crear_PgCampat2/', crear_PgCampat2.as_view()),
    path('crear_PgCatesfin/', crear_PgCatesfin.as_view()),
    path('crear_PgClabieser/', crear_PgClabieser.as_view()),
    path('crear_PgConveniodt/', crear_PgConveniodt.as_view()),
    path('crear_PgCostos/', crear_PgCostos.as_view()),
    path('crear_PgCostos2/', crear_PgCostos2.as_view()),
    path('crear_PgCtrlple/', crear_PgCtrlple.as_view()),
    path('crear_PgDoc/', crear_PgDoc.as_view()),
    path('crear_PgEnfinan/', crear_PgEnfinan.as_view()),
    path('crear_PgFactor/', crear_PgFactor.as_view()),
    path('crear_PgFlujo/', crear_PgFlujo.as_view()),
    path('crear_PgFormato/', crear_PgFormato.as_view()),
    path('crear_PgFormato2/', crear_PgFormato2.as_view()),
    path('crear_PgMoneda/', crear_PgMoneda.as_view()),
    path('crear_PgOrigenfi/', crear_PgOrigenfi.as_view()),
    path('crear_PgPagsunat/', crear_PgPagsunat.as_view()),
    path('crear_PgPais/', crear_PgPais.as_view()),
    path('crear_PgPath/', crear_PgPath.as_view()),
    path('crear_PgPorcen/', crear_PgPorcen.as_view()),
    path('crear_PgSunat05/', crear_PgSunat05.as_view()),
    path('crear_PgTabdetra/', crear_PgTabdetra.as_view()),
    path('crear_PgTipo/', crear_PgTipo.as_view()),
    path('crear_PgTipod/', crear_PgTipod.as_view()),
    path('crear_PgTiprenta/', crear_PgTiprenta.as_view()),
    
    url(r'^logout/$', LogoutView.as_view(), {'template_name': 'log/logout.html'}, name='logout'),
    
]

