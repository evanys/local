from django.shortcuts import render, render_to_response, HttpResponse
from django.contrib.auth.models import User
from django.views.generic import TemplateView,ListView, UpdateView
from apps.pacioli.models import *
from django.forms.models import  model_to_dict
from django.core import serializers
from django.core.serializers import serialize
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.db.models import Max, Avg, Count, Min, Sum
#import datetime
from datetime import datetime,  timedelta

from django.db import connection
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl import load_workbook
from openpyxl.styles import Font, Fill ,Border, Side, PatternFill, GradientFill, Alignment,Border

from django.db.models import Q

from books.replica import replicarDatos
import json


'''
class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)
'''

#@login_required
#class index(LoginRequiredMixin, TemplateView):
class index(TemplateView):
#class index(TemplateView):
    template_name = 'experto/index.html'
    #login_url = '/'
    #redirect_field_name = 'redirect_to'

    def get_context_data(self, **kwargs):
        context  =  super(index,self).get_context_data(**kwargs)
        context['actualizar'] = "NewContasic"

        variablesPorcentajes()
        return context
        
def jsonExample(self):
    ct = {
            'titulo':'mi titulo',
            'nombre':'mi minombre'
        }
    #print(ct['titulo'])
    return JsonResponse(ct)


class buscarlibroHojaTrabajo(TemplateView):
    def get(self, request, *args, **kwargs):
        #debe
        usuario_id = request.GET['debe']
        miconsulta = PgDiario.objects.all().filter(ccod_cli=usuario_id).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber'))
        #for n in miconsulta:
        #    print(n.ccod_cue, str(n.ndebe), str(n.nhaber))
        asientos =  PgDiario.objects.filter(ccod_cli=usuario_id).order_by('-nasiento')
        data = serializers.serialize('json',miconsulta,fields=('pgplan__ccod_cue','pgplan__cdsc'))
        return HttpResponse(data,content_type='application/json')

class planes(TemplateView):
    def get(self, request, *args, **kwargs):
        name = request.GET.get('dato')
        lista = []
        lista.append({'ccod_asi':'dato cero','cdsc':'dato uno'})
        return HttpResponse(json.dumps(lista),content_type="application/json")

class asicab(TemplateView):
    def get(self, request, *args, **kwargs):
        name = request.GET.get('dato')
        usuario = request.GET.get('usuario')
        #this line has been modified to 
        #planes = PgAsicab.objects.filter(ccod_asi=name).filter(usuario=usuario)
        lista = []
        lista = cuentas(name, usuario)
        
        return HttpResponse(json.dumps(lista), content_type="application/json")

class proceso(TemplateView):
    def get(self, request, *args, **kwargs):
        ccod_asi = request.GET.get('ccod_asi')
        monto = request.GET.get('monto')
        usuario = request.GET.get('usuario')
        lista = []
        #lista = datos_listos(ccod_asi, monto) 
        lista = datos_listos_acabar(ccod_asi, monto, usuario)        
        return HttpResponse(json.dumps(lista), content_type="application/json")

class registrarGlosa(TemplateView):
    def get(self, request, *args, **kwargs):
        ccod_asi = request.GET.get('ccod_asi')
        monto = request.GET.get('monto')
        glosa  = request.GET.get('glosa')
        usuario = request.GET.get('usuario')
        #print(ccod_asi, monto, 'ccodasi - monto')
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        lista = datos_listos_acabar(ccod_asi, monto)

        for l in lista:
            
            if l['tipo'] == 'd':
                p = PgDiario(cglosa2='5000_', ccod_cue= l['ccod_cue_debe'] , ndebe=l['monto'],nhaber=0,cglosa=glosa,ccod_user=usuario)
                p.save()
            if l['tipo'] == 'h':
                p = PgDiario(cglosa2='5000_',ccod_cue= l['ccod_cue_haber'] , ndebe=0,nhaber=l['monto'],cglosa=glosa,ccod_user=usuario)
                p.save()
        return HttpResponse(json.dumps(lista), content_type="application/json")

class BuscarRuc(TemplateView):
    def get(self, request, *args, **kwargs):
        ruc_dni = request.GET.get('ruc_dni')
        usuario = request.GET.get('usuario')
        #print(ccod_asi, monto, 'ccodasi - monto')
        #print(ruc_dni)
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        cliente = PgCliPro.objects.filter(ccod_cli__startswith=ruc_dni).filter(usuario__isnull=True).order_by('ccod_cli')
        tam = len(cliente)
        crazon = ''
        cruc = ''
        if tam == 1:
            for x in range(len(cliente)):
                crazon = cliente[x].crazon
                cruc = cliente[x].ccod_cli
        if tam == 0:
            crazon = ''
        #print(tam)
        lista.append({'tamaño':str(tam),'crazon':crazon,'ruc':cruc})

        return HttpResponse(json.dumps(lista), content_type="application/json")

'''
class buscarDocumento(TemplateView):
    def get(self, request, *args, **kwargs):
        documentos_buscar_valido = request.GET.get('documentos_buscar_valido').upper()
        lista = []
        documento = PgDoc.objects.filter(ccod_doc__startswith=documentos_buscar_valido).order_by('ccod_doc')
        tam = len(documento)
        cdocumento = ''
        if tam == 1:
            for x in range(len(documento)):
                cdocumento = documento[x].cdsc
        if tam == 0:
            cdocumento = ''
        lista.append({'tamaño':str(tam),'crazon':cdocumento})
        return HttpResponse(json.dumps(lista), content_type="application/json")
'''
class buscarDocumento(TemplateView):
    def get(self, request, *args, **kwargs):
        documentos_buscar_valido = request.GET.get('documentos_buscar_valido').upper()
        usuario = request.GET.get('usuario')
        lista = []
        if documentos_buscar_valido == 'NONE' or documentos_buscar_valido == '':
            #this line was modified to work only whit null useres
            #documento = PgDoc.objects.all().filter(usuario=usuario).order_by('ccod_doc')
            documento = PgDoc.objects.all().filter(usuario__isnull=True).order_by('ccod_doc')
            for x in documento:
                datos = {'ccod_doc':x.ccod_doc, 'cdsc':x.cdsc}
                lista.append(datos)
        else:
            #this query was working whit personalized userews but now is onlñy weorking whit null useres
            #documento = PgDoc.objects.filter(ccod_doc__startswith=documentos_buscar_valido).filter(usuario=usuario).order_by('ccod_doc')
            documento = PgDoc.objects.filter(ccod_doc__startswith=documentos_buscar_valido).filter(usuario__isnull=True).order_by('ccod_doc')
            tam = len(documento)
            cdocumento = ''
            if tam == 1:
                for x in range(len(documento)):
                    cdocumento = documento[x].cdsc
            if tam == 0:
                cdocumento = ''
            lista.append({'tamaño':str(tam),'crazon':cdocumento})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class buscarmedio(TemplateView):
    def get(self, request, *args, **kwargs):
        medPago_valido = request.GET.get('medPago_valido').upper()
        lista = []
        medio = PgPagsunat.objects.filter(ccod_pagsu__startswith=medPago_valido).order_by('ccod_pagsu')
        
        tam = len(medio)
        cdocumento = ''
        if tam == 1:
            for x in range(len(medio)):
                cdocumento = medio[x].cdsc
        if tam == 0:
            cdocumento = ''
        #print(tam)
        lista.append({'tamaño':str(tam),'crazon':cdocumento})

        return HttpResponse(json.dumps(lista), content_type="application/json")

class buscarFlujo(TemplateView):
    def get(self, request, *args, **kwargs):
        flujo_efectivo = request.GET.get('flujo_efectivo').upper()
        lista = []
        flujo = PgFlujo.objects.filter(ccod_flujo__startswith=flujo_efectivo).order_by('ccod_flujo')
        
        tam = len(flujo)
        cdocumento = ''
        if tam == 1:
            for x in range(len(flujo)):
                cdocumento = flujo[x].cdsc
        if tam == 0:
            cdocumento = ''
        lista.append({'tamaño':str(tam),'crazon':cdocumento})

        return HttpResponse(json.dumps(lista), content_type="application/json")

class buscarMasCuentas(TemplateView):
    def get(self, request, *args, **kwargs):
        cuenta = request.GET.get('cuenta')
        usuario = request.GET.get('usuario')
        lista = []
        #tis query has been modified to work only whit null users
        print('cuenta:', cuenta, 'usuario:', usuario, 'Y - Y - Y - Y')
        '''
        planes = PgPlan.objects.filter(usuario=usuario).filter(ccod_cue__startswith=cuenta.strip()).filter(nnivel=3).order_by('ccod_cue')
        '''
        planes = PgPlan.objects.filter(usuario__isnull=True).filter(ccod_cue__startswith=cuenta.strip()).filter(nnivel=3).order_by('ccod_cue')
        for pla in planes:
            lista.append({'ccod_cue':pla.ccod_cue.strip(),'cdsc': pla.cdsc.strip(),'nanalisis':str(pla.nanalisis)})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class registrarPersona(TemplateView):
    def get(self, request, *args, **kwargs):
        ruc_dni = request.GET.get('ruc_dni')
        apPaterno = request.GET.get('apPaterno')
        apMaterno= request.GET.get('apMaterno')
        PrimerNombre= request.GET.get('PrimerNombre')
        SegundoNombre= request.GET.get('SegundoNombre')
        DireccionNatural= request.GET.get('DireccionNatural')
        #print(ccod_asi, monto, 'ccodasi - monto')
        #print(ruc_dni)
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        crazon = apPaterno + ' ' + apMaterno +' '+PrimerNombre +' '+SegundoNombre
        p = PgCliPro(ccod_cli=ruc_dni, crazon= crazon,cdir=  DireccionNatural,cpaterno=apPaterno, cmaterno = apMaterno, cnombre1 = PrimerNombre, cnombre2=SegundoNombre, ctel='5000_')
        p.save()
        lista.append({'guardado':'guardaddo'})

        return HttpResponse(json.dumps(lista), content_type="application/json")

class registrarEmpresa(TemplateView):
    def get(self, request, *args, **kwargs):
        ruc_dni = request.GET.get('ruc_dni')
        RazonSocial = request.GET.get('RazonSocial')
        DireccionSocial= request.GET.get('DireccionSocial')
        

        #print(ccod_asi, monto, 'ccodasi - monto')
        #print(ruc_dni)
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        
        p = PgCliPro(ccod_cli=ruc_dni, crazon= RazonSocial,cdir=  DireccionSocial, ctel='5000_')
        p.save()
        lista.append({'guardado':'guardaddo'})

        return HttpResponse(json.dumps(lista), content_type="application/json")


cuenta_global=""
monto_global =0
array_global = []

class jsonPruebaB(TemplateView):
    def get(self, request, *args, **kwargs):

        npigv = 18.0
        false = False
        true = True
        null = "null"

        cuenta = request.GET.get('cuenta')
        m= eval(cuenta)


        ccod_asi = m[0]['ccod_asi']
        monto = m[0]['monto']
        usuario = m[0]['usuario']
        nombreCambio = m[0]['tipomoneda']
        valorCambio = float(m[0]['valorTipoCambio'])
        
        lista = []
        lista = datos_listos_acabar(ccod_asi, monto)

        origen = PgAsiento.objects.all().filter(ccod_asi=ccod_asi)

        currentMonth = datetime.now().month
        orig = 0
        glosa = ""
        cregis = ""
        t2008_8 = ""

        for ori in origen:
            orig = ori.ccod_ori
            glosa = ori.cdsc
            cregis=ori.cregis
            t2008_8 = ori.t2008_8

        aumentar = PgOrigen.objects.all().filter(ccod_ori = orig)
        aut = 1
        for at in aumentar:
            aut = at.nnum11

        inafecta = 0
        inafectad=0
        exonerada = 0
        exoneradad = 0
        baseImponible = 0
        baseImponibled = 0
        nbase2 =0.0
        nigv2 = 0.0
        nbase3 = 0.0
        nigv3 = 0.0
        nisc = 0.0
        nbase2d = 0.0
        nigv2d = 0.0
        nbase3d = 0.0
        nigv3d = 0.0
        niscd = 0.0

        igv = 0
        igvd = 0
        total = 0
        totald = 0

        ndebe = 0
        nhaber = 0
        ndebed = 0
        nhaberd = 0
      

        total = inafecta+exonerada+baseImponible+igv
        totald = inafectad+exoneradad+baseImponibled+igvd

        for l in lista:
            lcompra = False
            cmreg = ""
            if(nombreCambio == 'D'):
                if l['tipo']=='d':
                    ndebed = l['monto']
                    ndebe = round((l['monto']*valorCambio),2)
                    nhaberd = 0.00
                    nhaber = 0.00
                if l['tipo']=='h':
                    nhaberd = l['monto']
                    nhaber = round((l['monto']*valorCambio),2)
                    ndebed = 0.00
                    ndebe= 0.00

                if l['nubic'] == '1':
                    inafecta = round((l['monto'] * valorCambio),2) 
                    inafectad = l['monto'] 
                elif l['nubic'] == '2':
                    exonerada = round((l['monto'] * valorCambio),2) 
                    exoneradad = l['monto'] 
                elif l['nubic'] == '3':
                    baseImponible = round((l['monto'] * valorCambio),2)
                    baseImponibled = l['monto'] 
                elif l['cdsc_asicab'].strip() == 'IGV':
                    igv = round((l['monto'] * valorCambio),2)
                    igvd = l['monto']   


            elif (nombreCambio == 'S'):

                if l['tipo']=='d':
                    ndebed = round((l['monto']/valorCambio),2)
                    ndebe = l['monto']
                    nhaberd = 0.00
                    nhaber = 0.00
                if l['tipo']=='h':
                    nhaberd = round((l['monto']/valorCambio),2)
                    nhaber = l['monto']
                    ndebed = 0.00
                    ndebe= 0.00


                if l['nubic'] == '1':
                    inafecta = l['monto']
                    inafectad =  round((l['monto'] / valorCambio),2)
                elif l['nubic'] == '2':
                    exonerada = l['monto']
                    exoneradad = round((l['monto'] / valorCambio),2) 
                elif l['nubic'] == '3':
                    baseImponible = l['monto']
                    baseImponibled = round((l['monto'] / valorCambio),2)
                elif l['cdsc_asicab'].strip() == 'IGV':
                    igv = l['monto']
                    igvd = round((l['monto'] / valorCambio),2)

            total = inafecta+exonerada+baseImponible+igv
            totald = inafectad+exoneradad+baseImponibled+igvd
            #print(ndebe,'|',nhaber,'|',ndebed,'|',nhaberd,'| mis datos para guardar |',l['monto'],'|',valorCambio,nombreCambio)
            if l['tipo'] == 'd':
                fven = None
                for posi in range(1,len(m)):
                    if l['nitem'] == m[posi]['nitem']:
                        ccod_cue_aca = m[posi]['ccod_cue_debe']
                    formulario = m[posi]['formulario']
                    

                if l['nanalisis']=='2' or l['nanalisis']=='4':
                    fven = formulario['datepicker_v']
                else:
                    fven = None

                if formulario['nanalisis'] == '2':
                    lcompra = True
                    cmreg = nombreCambio
                else:
                    lcompra = False
                    cmreg = ""
                 

                if l['rangolos'] !='':
                    for r in l['rangolos']:
                        if l['nitem'] == r['nitem']:
                            p = PgDiario(cglosa2='5000_', ccod_cue= r['ccod_cue'] , cmes= str(currentMonth), nasiento = str(aut), ccod_ori=str(orig),cglosa=glosa,ndebe=r['monto2'],nhaber=0.00,ffecha=datetime.now(),ffechadoc=formulario['datepicker_d'],ffechaven=fven,ccod_doc=formulario['documentos_buscar_valido'],ccod_cli=formulario['ruc_dni'],ffechareg=datetime.now(),ccod_asi=ccod_asi,ccod_user=usuario,cregis=cregis,cnumero=formulario['serie_Numero'],nina=0.0,nexo=0.0,nnet=0.0,nimp=0.0,ntot=0.0,ninad=0.0,nexod=0.0,nnetd=0.0,nimpd=0.0,ntotd=0.0,cmoneda=nombreCambio,ntc=valorCambio,ndebed=0.00,nhaberd=0.00,t2008_8=t2008_8,lcomp=lcompra,cmreg=cmreg,npigv=0.00,nbase2=nbase2, nigv2=nigv2,nbase3=nbase3,nigv3=nigv3,nisc=nisc,nbase2d=nbase2d,nigv2d=nigv2d,nbase3d=nbase3d,nigv3d=nigv3d,niscd=niscd )
                            p.save()
                else:
                    if l['cdsc_asicab'].strip() == 'IGV':
                        p = PgDiario(cglosa2='5000_', ccod_cue= ccod_cue_aca , cmes= str(currentMonth), nasiento = str(aut), ccod_ori=str(orig),cglosa=glosa,ndebe=ndebe,nhaber=0.00,ffecha=datetime.now(),ffechadoc=formulario['datepicker_d'],ffechaven=fven,ccod_doc=formulario['documentos_buscar_valido'],ccod_cli=formulario['ruc_dni'],ffechareg=datetime.now(),ccod_asi=ccod_asi,ccod_user=usuario,cregis=cregis,cnumero=formulario['serie_Numero'],nina=inafecta,nexo=exonerada,nnet=baseImponible,nimp=igv,ntot=total,ninad=inafectad,nexod=exoneradad,nnetd=baseImponibled,nimpd=igvd,ntotd=totald,cmoneda=nombreCambio,ntc=valorCambio,ndebed=ndebed,nhaberd=0.00,t2008_8=t2008_8,lcomp=lcompra,cmreg=cmreg,npigv=npigv,nbase2=nbase2, nigv2=nigv2,nbase3=nbase3,nigv3=nigv3,nisc=nisc,nbase2d=nbase2d,nigv2d=nigv2d,nbase3d=nbase3d,nigv3d=nigv3d,niscd=niscd )
                        p.save()
                    else:
                        p = PgDiario(cglosa2='5000_', ccod_cue= ccod_cue_aca , cmes= str(currentMonth), nasiento = str(aut), ccod_ori=str(orig),cglosa=glosa,ndebe=ndebe,nhaber=0.00,ffecha=datetime.now(),ffechadoc=formulario['datepicker_d'],ffechaven=fven,ccod_doc=formulario['documentos_buscar_valido'],ccod_cli=formulario['ruc_dni'],ffechareg=datetime.now(),ccod_asi=ccod_asi,ccod_user=usuario,cregis='',cnumero=formulario['serie_Numero'],nina=0.0,nexo=0.0,nnet=0.0,nimp=0.0,ntot=0.0,ninad=0.0,nexod=0.0,nnetd=0.0,nimpd=0.0,ntotd=0.0,cmoneda=nombreCambio,ntc=valorCambio,ndebed=ndebed,nhaberd=0.00,t2008_8=t2008_8,lcomp=lcompra,cmreg=cmreg,npigv=0.00,nbase2=nbase2, nigv2=nigv2,nbase3=nbase3,nigv3=nigv3,nisc=nisc,nbase2d=nbase2d,nigv2d=nigv2d,nbase3d=nbase3d,nigv3d=nigv3d,niscd=niscd )
                        p.save()

            if l['tipo'] == 'h':
                for posi in range(1,len(m)):
                    if l['nitem'] == m[posi]['nitem']:
                        ccod_cue_aca = m[posi]['ccod_cue_haber']
                    #if m[posi]['formulario'] != 'null' and l['nitem'] == m[posi]['nitem']:
                    formulario = m[posi]['formulario']
                
                    #print(formulario)
                if l['nanalisis']=='2' or l['nanalisis']=='4':
                    fven = formulario['datepicker_v']
                else:
                    fven = None

                if formulario['nanalisis'] == '2':
                    lcompra = True
                    cmreg = nombreCambio
                else:
                    lcompra = False
                    cmreg = ""

                if l['cdsc_asicab'].strip() == 'IGV':
                    p = PgDiario(cglosa2='5000_',ccod_cue= ccod_cue_aca , cmes= str(currentMonth), nasiento = str(aut), ccod_ori=str(orig),cglosa=glosa,ndebe =0.00, nhaber=nhaber,ffecha=datetime.now(),ffechadoc=formulario['datepicker_d'],ffechaven=fven,ccod_doc=formulario['documentos_buscar_valido'],ccod_cli=formulario['ruc_dni'],ffechareg=datetime.now(),ccod_asi=ccod_asi,ccod_user=usuario,cregis=cregis,cnumero=formulario['serie_Numero'],nina=inafecta,nexo=exonerada,nnet=baseImponible,nimp=igv,ntot=total,ninad=inafectad,nexod=exoneradad,nnetd=baseImponibled,nimpd=igvd,ntotd=totald,cmoneda=nombreCambio,ntc=valorCambio,nhaberd=nhaberd,ndebed=0.00,t2008_8=t2008_8,lcomp=lcompra,cmreg=cmreg,npigv=npigv,nbase2=nbase2, nigv2=nigv2,nbase3=nbase3,nigv3=nigv3,nisc=nisc,nbase2d=nbase2d,nigv2d=nigv2d,nbase3d=nbase3d,nigv3d=nigv3d,niscd=niscd )
                    p.save()
                else:
                    p = PgDiario(cglosa2='5000_',ccod_cue= ccod_cue_aca , cmes= str(currentMonth), nasiento = str(aut), ccod_ori=str(orig),cglosa=glosa,ndebe =0.00, nhaber=nhaber,ffecha=datetime.now(),ffechadoc=formulario['datepicker_d'],ffechaven=fven,ccod_doc=formulario['documentos_buscar_valido'],ccod_cli=formulario['ruc_dni'],ffechareg=datetime.now(),ccod_asi=ccod_asi,ccod_user=usuario,cregis='',cnumero=formulario['serie_Numero'],nina=0.0,nexo=0.0,nnet=0.0,nimp=0.0,ntot=0.0,ninad=0.0,nexod=0.0,nnetd=0.0,nimpd=0.0,ntotd=0.0,cmoneda=nombreCambio,ntc=valorCambio,nhaberd=nhaberd,ndebed=0.00,t2008_8=t2008_8,lcomp=lcompra,cmreg=cmreg,npigv=0.00,nbase2=nbase2, nigv2=nigv2,nbase3=nbase3,nigv3=nigv3,nisc=nisc,nbase2d=nbase2d,nigv2d=nigv2d,nbase3d=nbase3d,nigv3d=nigv3d,niscd=niscd)
                    p.save()
                    
        nuevo_orogen  = int(aut) + 1
        PgOrigen.objects.filter(ccod_ori=orig).update(nnum11= nuevo_orogen)

        listab = []
        listab.append({'datos': '0'})
        return HttpResponse(json.dumps(listab), content_type="application/json")

#validate if the amount is equual to the amoutn to rest to pay
# if the amount is less create  anew row 
# if the amount is equal then save and change the cmesc to the current mont
# also be carefuly about lcomp allways be false if teh amount is not equal 
# to the amonut send by the user

class jsonPrueba(TemplateView):
    def get(self, request, *args, **kwargs):
        npigv = 18.0
        false = False
        true = True
        null = "null"

        cuenta = eval(request.GET.get('cuenta'))
        ccod_asi = cuenta[0]['ccod_asi']
        monto = cuenta[0]['monto']
        nombreCambio = cuenta[0]['tipomoneda']
        valorCambio = float(cuenta[0]['valorTipoCambio'])
        usuario = cuenta[0]['usuario']
        lista = datos_listos_acabar(ccod_asi, monto, usuario,cuenta[1:])
        juntos =  juntararrays(cuenta[1:], lista,nombreCambio, valorCambio, usuario)
        #origen solo trabaja en IGV
        origen = origenes(ccod_asi, usuario)
        #extra solo trabaja en IGV
        extra = datosExtra(juntos)
        #extra solo trabaja para rellenar dattos no  IGV
        extras = {'nina':0.00, 'nexo':0.00, 'nnet':0.00 ,'nimp':0.00 ,'ntot':0.00, 'ninad':0.00, 'nexod':0.00 ,'nnetd':0.00 ,'nimpd':0.00 ,'ntotd':0.00,'nbase2':0.00,'nigv2':0.00,'nbase3':0.00,'nigv3':0.00,'nisc':0.00,'nbase2d':0.00,'nigv2d':0.00,'nbase3d':0.00,'nigv3d':0.00,'niscd':0.00}
        #se trabaja con juntos y origen
        #ninad=extra['ninad'], nexod=extra['nexod'] ,nnetd=extra['nnetd'] ,nimpd=extra['nimpd'] ,ntotd=extra['ntotd'],nbase2=extra['nbase2'],nbase3=extra['nbase3'],nisc=extra['nisc'],nbase2d=extra['nbase2d'],nbase3d=extra['nbase3d'],niscd=extra['niscd']
        #for j in juntos:
        #    print(j)
        #    print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
        #print('##############################')

        #print(origen)
        #print(extra)

        #guardando datos listos
        for juno in juntos:
            pacoa = PgPlan(
                id = juno['idpgplan']
            )
            #print(juno['formulario']['cuenta'].strip(), '$-$-$')
            if juno['tipo'] == 'd':
                if juno['rangolos'] != '':
                    for r in juno['rangolos']:
                        if juno['nitem'] == r['nitem']:
                            if nombreCambio == 'S':
                                rangolos_soles_debe = r['monto2']
                                rangolos_soles_debe_d = float(r['monto2']) / float(valorCambio)
                            elif nombreCambio == 'D':
                                rangolos_soles_debe = float(r['monto2']) * float(valorCambio)
                                rangolos_soles_debe_d = r['monto2']
                            actualizarValoresPlan(usuario, juno['formulario']['cuenta'].strip(), origen['mes'], rangolos_soles_debe, float(0), 'N')                
                            p = PgDiario(
                                cglosa2='5001_',
                                ccod_cue= r['ccod_cue'],
                                cmes= origen['mes'],
                                nasiento=origen['currentMonth'],
                                ccod_ori=origen['orig'],
                                cglosa=origen['cdsc'],
                                #ndebe=r['monto2'],
                                ndebe = rangolos_soles_debe,
                                nhaber=0.00,
                                cregis=None,
                                ffecha=datetime.now(),
                                ffechadoc= juno['formulario']['datepicker_d'],
                                cnumero=juno['formulario']['serie_Numero'],
                                ffechaven=juno['formulario']['datepicker_v'],
                                ccod_doc=juno['formulario']['documentos_buscar_valido'],
                                ccod_cli=juno['formulario']['ruc_dni'],
                                nina=extras['nina'],
                                nexo=extras['nexo'],
                                nnet=extras['nnet'],
                                nimp=extras['nimp'],
                                ntot=extras['ntot'],
                                ccod_user=usuario,
                                ffechareg=datetime.now(),
                                cmoneda=nombreCambio,
                                ntc=valorCambio,
                                #ndebed=juno['montoD'],
                                ndebed = rangolos_soles_debe_d,
                                nhaberd=0.00,
                                ninad=extras['ninad'],
                                nexod=extras['nexod'],
                                nnetd=extras['nnetd'],
                                nimpd=extras['nimpd'],
                                ntotd=extras['ntotd'],
                                lcomp=juno['lcomp'],
                                ccod_asi=ccod_asi,
                                nbase2=extras['nbase2'],
                                nigv2=extras['nigv2'],
                                nbase3=extras['nbase3'],
                                nigv3=extras['nigv3'],
                                nisc=extras['nisc'],
                                nbase2d=extras['nbase2d'],
                                nigv2d=extras['nigv2d'],
                                nbase3d=extras['nbase3d'],
                                nigv3d=extras['nigv3d'],
                                niscd=extras['niscd'],
                                t2008_8=origen['t2008_8'],
                                npigv=0.00
                                #pgplan = pacoa
                                )
                            p.save()
                else:
                    if juno['cdsc_asicab'].strip() == 'IGV':
                        actualizarValoresPlan(usuario, juno['ccod_cue_debe'].strip(), origen['mes'], juno['montoS'], float(0), 'N')
                        p = PgDiario(
                            cglosa2='5001_',
                            #ccod_cue= juno['formulario']['cuenta'],
                            ccod_cue= juno['ccod_cue_debe'],
                            cmes= origen['mes'],
                            nasiento=origen['currentMonth'],
                            ccod_ori=origen['orig'],
                            cglosa=origen['cdsc'],
                            ndebe=juno['montoS'],
                            nhaber=0.00,
                            cregis=origen['cregis'],
                            ffecha=datetime.now(),
                            ffechadoc= juno['formulario']['datepicker_d'],
                            cnumero=juno['formulario']['serie_Numero'],
                            ffechaven=juno['formulario']['datepicker_v'],
                            ccod_doc=juno['formulario']['documentos_buscar_valido'],
                            ccod_cli=juno['formulario']['ruc_dni'],
                            nina=extra['nina'],
                            nexo=extra['nexo'],
                            nnet=extra['nnet'],
                            nimp=extra['nimp'],
                            ntot=extra['ntot'],
                            ccod_user=usuario,
                            ffechareg=datetime.now(),
                            cmoneda=nombreCambio,
                            ntc=valorCambio,
                            ndebed=juno['montoD'],
                            nhaberd=0.00,
                            ninad=extra['ninad'], 
                            nexod=extra['nexod'],
                            nnetd=extra['nnetd'],
                            nimpd=extra['nimpd'],
                            ntotd=extra['ntotd'],
                            lcomp=juno['lcomp'],
                            ccod_asi=ccod_asi,
                            nbase2=extra['nbase2'],
                            nigv2=extra['nigv2'],
                            nbase3=extra['nbase3'],
                            nigv3=extra['nigv3'],
                            nisc=extra['nisc'],
                            nbase2d=extra['nbase2d'],
                            nigv2d=extra['nigv2d'],
                            nbase3d=extra['nbase3d'],
                            nigv3d=extra['nigv3d'],
                            niscd=extra['niscd'],
                            t2008_8=origen['t2008_8'],
                            npigv=npigv
                            #pgplan = pacoa
                            )
                        p.save()
                    else:
                        actualizarValoresPlan(usuario, juno['ccod_cue_debe'].strip(), origen['mes'], juno['montoS'], float(0), 'N')
                        p = PgDiario(
                            cglosa2='5001_',
                            #ccod_cue= juno['formulario']['cuenta'],
                            ccod_cue= juno['ccod_cue_debe'],
                            cmes= origen['mes'],
                            nasiento=origen['currentMonth'],
                            ccod_ori=origen['orig'],
                            cglosa=origen['cdsc'],
                            ndebe=juno['montoS'],
                            nhaber=0.00,
                            cregis=None,
                            ffecha=datetime.now(),
                            ffechadoc= juno['formulario']['datepicker_d'],
                            cnumero=juno['formulario']['serie_Numero'],
                            ffechaven=juno['formulario']['datepicker_v'],
                            ccod_doc=juno['formulario']['documentos_buscar_valido'],
                            ccod_cli=juno['formulario']['ruc_dni'],
                            nina=extras['nina'],
                            nexo=extras['nexo'],
                            nnet=extras['nnet'],
                            nimp=extras['nimp'],
                            ntot=extras['ntot'],
                            ccod_user=usuario,
                            ffechareg=datetime.now(),
                            cmoneda=nombreCambio,
                            ntc=valorCambio,
                            ndebed=juno['montoD'],
                            nhaberd=0.00,
                            ninad=extras['ninad'],
                            nexod=extras['nexod'],
                            nnetd=extras['nnetd'],
                            nimpd=extras['nimpd'],
                            ntotd=extras['ntotd'],
                            lcomp=juno['lcomp'],
                            ccod_asi=ccod_asi,
                            nbase2=extras['nbase2'],
                            nigv2=extras['nigv2'],
                            nbase3=extras['nbase3'],
                            nigv3=extras['nigv3'],
                            nisc=extras['nisc'],
                            nbase2d=extras['nbase2d'],
                            nigv2d=extras['nigv2d'],
                            nbase3d=extras['nbase3d'],
                            nigv3d=extras['nigv3d'],
                            niscd=extras['niscd'],
                            t2008_8=origen['t2008_8'],
                            npigv=0.00
                            #pgplan = pacoa
                            )
                        p.save()
                #print(juno)
                #print('tipo debe------------')
            elif juno['tipo'] == 'h':
                if juno['rangolos'] != '':
                    for r in juno['rangolos']:
                        if juno['nitem'] == r['nitem']:
                            if nombreCambio == 'S':
                                rangolos_soles_haber = r['monto2']
                                rangolos_soles_haber_d = float(r['monto2']) / float(valorCambio)
                            elif nombreCambio == 'D':
                                rangolos_soles_haber = float(r['monto2']) * float(valorCambio)
                                rangolos_soles_haber_d = r['monto2']
                            actualizarValoresPlan(usuario, r['ccod_cue'].strip(), origen['mes'], float(0), rangolos_soles_haber, 'N')
                            p = PgDiario(
                                cglosa2='5001_', 
                                ccod_cue= r['ccod_cue'],
                                cmes= origen['mes'],
                                nasiento=origen['currentMonth'],
                                ccod_ori=origen['orig'],
                                cglosa=origen['cdsc'],
                                ndebe=0.00,
                                #nhaber=r['monto2'],
                                nhaber = rangolos_soles_haber,
                                cregis=None,
                                ffecha=datetime.now(),
                                ffechadoc= juno['formulario']['datepicker_d'],
                                cnumero=juno['formulario']['serie_Numero'],
                                ffechaven=juno['formulario']['datepicker_v'],
                                ccod_doc=juno['formulario']['documentos_buscar_valido'],
                                ccod_cli=juno['formulario']['ruc_dni'],
                                nina=extras['nina'],
                                nexo=extras['nexo'],
                                nnet=extras['nnet'],
                                nimp=extras['nimp'],
                                ntot=extras['ntot'],
                                ccod_user=usuario,
                                ffechareg=datetime.now(),
                                cmoneda=nombreCambio,
                                ntc=valorCambio,
                                ndebed=0.00,
                                #nhaberd=juno['montoD'],
                                nhaberd = rangolos_soles_haber_d,
                                ninad=extras['ninad'],
                                nexod=extras['nexod'],
                                nnetd=extras['nnetd'],
                                nimpd=extras['nimpd'],
                                ntotd=extras['ntotd'],
                                lcomp=juno['lcomp'],
                                ccod_asi=ccod_asi,
                                nbase2=extras['nbase2'],
                                nigv2=extras['nigv2'],
                                nbase3=extras['nbase3'],
                                nigv3=extras['nigv3'],
                                nisc=extras['nisc'],
                                nbase2d=extras['nbase2d'],
                                nigv2d=extras['nigv2d'],
                                nbase3d=extras['nbase3d'],
                                nigv3d=extras['nigv3d'],
                                niscd=extras['niscd'],
                                t2008_8=origen['t2008_8'],
                                npigv=0.00
                                #pgplan = pacoa
                                )
                            p.save()
                else:
                    if juno['cdsc_asicab'].strip() == 'IGV':
                        actualizarValoresPlan(usuario, juno['formulario']['cuenta'].strip(), origen['mes'], float(0), juno['montoS'], 'N')
                        p = PgDiario(cglosa2='5001_',
                            ccod_cue= juno['formulario']['cuenta'],
                            #ccod_cue= juno['ccod_cue_haber'],
                            cmes= origen['mes'],
                            nasiento=origen['currentMonth'],
                            ccod_ori=origen['orig'],
                            cglosa=origen['cdsc'],
                            ndebe=0.00,
                            nhaber=juno['montoS'],
                            cregis=origen['cregis'],
                            ffecha=datetime.now(),
                            ffechadoc= juno['formulario']['datepicker_d'],
                            cnumero=juno['formulario']['serie_Numero'],
                            ffechaven=juno['formulario']['datepicker_v'],
                            ccod_doc=juno['formulario']['documentos_buscar_valido'],
                            ccod_cli=juno['formulario']['ruc_dni'],
                            nina=extra['nina'],
                            nexo=extra['nexo'],
                            nnet=extra['nnet'],
                            nimp=extra['nimp'],
                            ntot=extra['ntot'],
                            ccod_user=usuario,
                            ffechareg=datetime.now(),
                            cmoneda=nombreCambio,
                            ntc=valorCambio,
                            ndebed=0.00,
                            nhaberd=juno['montoD'],
                            ninad=extra['ninad'],
                            nexod=extra['nexod'],
                            nnetd=extra['nnetd'],
                            nimpd=extra['nimpd'],
                            ntotd=extra['ntotd'],
                            lcomp=juno['lcomp'],
                            ccod_asi=ccod_asi,
                            nbase2=extra['nbase2'],
                            nigv2=extra['nigv2'],
                            nbase3=extra['nbase3'],
                            nigv3=extra['nigv3'],
                            nisc=extra['nisc'],
                            nbase2d=extra['nbase2d'],
                            nigv2d=extra['nigv2d'],
                            nbase3d=extra['nbase3d'],
                            nigv3d=extra['nigv3d'],
                            niscd=extra['niscd'],
                            t2008_8=origen['t2008_8'],
                            npigv=npigv
                            #pgplan = pacoa
                            )
                        p.save()
                    else:
                        actualizarValoresPlan(usuario, juno['formulario']['cuenta'].strip(), origen['mes'], float(0), juno['montoS'], 'N')
                        p = PgDiario(cglosa2='5001_',
                             ccod_cue= juno['formulario']['cuenta'],
                             #ccod_cue= juno['ccod_cue_haber'],
                             cmes= origen['mes'],
                             nasiento=origen['currentMonth'],
                             ccod_ori=origen['orig'],
                             cglosa=origen['cdsc'],
                             ndebe=0.00,
                             nhaber=juno['montoS'],
                             cregis=None,
                             ffecha=datetime.now(),
                             ffechadoc= juno['formulario']['datepicker_d'],
                             cnumero=juno['formulario']['serie_Numero'],
                             ffechaven=juno['formulario']['datepicker_v'],
                             ccod_doc=juno['formulario']['documentos_buscar_valido'],
                             ccod_cli=juno['formulario']['ruc_dni'],
                             nina=extras['nina'],
                             nexo=extras['nexo'],
                             nnet=extras['nnet'],
                             nimp=extras['nimp'],
                             ntot=extras['ntot'],
                             ccod_user=usuario,
                             ffechareg=datetime.now(),
                             cmoneda=nombreCambio,
                             ntc=valorCambio,
                             ndebed=0.00,
                             nhaberd=juno['montoD'],
                             ninad=extras['ninad'],
                             nexod=extras['nexod'],
                             nnetd=extras['nnetd'],
                             nimpd=extras['nimpd'],
                             ntotd=extras['ntotd'],
                             lcomp=juno['lcomp'],
                             ccod_asi=ccod_asi,
                             nbase2=extras['nbase2'],
                             nigv2=extras['nigv2'],
                             nbase3=extras['nbase3'],
                             nigv3=extras['nigv3'],
                             nisc=extras['nisc'],
                             nbase2d=extras['nbase2d'],
                             nigv2d=extras['nigv2d'],
                             nbase3d=extras['nbase3d'],
                             nigv3d=extras['nigv3d'],
                             niscd=extras['niscd'],
                             t2008_8=origen['t2008_8'],
                             npigv=0.00
                             #pgplan = pacoa
                             )
                        p.save()
                #print(juno)
                #print('tipo haber-----------')

        #nuevo_orogen  = float(origen['currentMonth']) + 1.0
        nuevo_orogen  = float(origen['currentMonth'])
        if origen['mes'][:1] == '0':
            el_mes = 'nnum'+ origen['mes'][1:]
        else:
            el_mes = 'nnum'+ origen['mes']
        #el_mes = 'nnum'+ origen['mes']
        espresion = "PgOrigen.objects.filter(ccod_ori=origen['orig']).update("+el_mes+" = nuevo_orogen)"
        eval(espresion)
        #print(espresion, "expresion actualizar")
        '''
        if len(origen['mes']) ==1:
            nn = '0'+origen['mes']
        else:
            nn = origen['mes']
        '''
        listab = []
        listab.append({'ccod_ori': str(origen['orig']) , 'asiento':origen['currentMonth'], 'cmes':origen['mes']})
        return HttpResponse(json.dumps(listab), content_type="application/json")

class actualizarMontos(TemplateView):
    def get(self, request, *args, **kwargs):
        montos = request.GET.get('netos')
        montosGuardar = eval(montos)
        currentMonth = datetime.now().month
        print(montosGuardar, '++++++')
        for x in range(len(montosGuardar)):
            if montosGuardar[x]['antMonto'] == montosGuardar[x]['nuevoMonto']:
                #logica para actulizar monto y cmes
                if montosGuardar[x]['tipo'] == 'd':
                    PgDiario.objects.filter(id = montosGuardar[x]['id']).update(ndebe = montosGuardar[x]['nuevoMonto'], cmesc = currentMonth) 
                else:
                    PgDiario.objects.filter(id = montosGuardar[x]['id']).update(nhaber = montosGuardar[x]['nuevoMonto'], cmesc = currentMonth) 
            else:
                #logica para actualizar solo monto restar
                nm = float(montosGuardar[x]['antMonto']) - float(montosGuardar[x]['nuevoMonto'])
                if montosGuardar[x]['tipo'] == 'd':
                    PgDiario.objects.filter(id = montosGuardar[x]['id']).update(ndebe = nm) 
                else:
                    PgDiario.objects.filter(id = montosGuardar[x]['id']).update(nhaber = nm) 
        listab = []
        listab.append({'datos': '0'})
        return HttpResponse(json.dumps(listab), content_type="application/json")
        #antMonto
##funcones

def datosExtra(lista):
    extras = {'nina':0.00, 'nexo':0.00, 'nnet':0.00 ,'nimp':0.00 ,'ntot':0.00, 'ninad':0.00, 'nexod':0.00 ,'nnetd':0.00 ,'nimpd':0.00 ,'ntotd':0.00,'nbase2':0.00,'nigv2':0.00,'nbase3':0.00,'nigv3':0.00,'nisc':0.00,'nbase2d':0.00,'nigv2d':0.00,'nbase3d':0.00,'nigv3d':0.00,'niscd':0.00}
    tt = 0.00
    ttd = 0.00
    for l in lista:
        if l['nubic'] == '1':
            extras['nina'] = l['montoS']
            extras['ninad'] = l['montoD']
            tt = tt + float(l['montoS'])
            ttd = ttd + float(l['montoD'])
        elif l['nubic'] == '2':
            extras['nexo'] = l['montoS']
            extras['nexod'] = l['montoD']
            tt = tt + float(l['montoS'])
            ttd = ttd + float(l['montoD'])
        elif l['nubic'] == '3':
            extras['nnet'] = l['montoS']
            extras['nnetd'] = l['montoD']
            tt = tt + float(l['montoS'])
            ttd = ttd + float(l['montoD'])
        elif l['nubic'] == '4':
            extras['nbase2'] = l['montoS']
            extras['nbase2d'] = l['montoD']
            tt = tt + float(l['montoS'])
            ttd = ttd + float(l['montoD'])
        elif l['nubic'] == '5':
            extras['nbase3'] = l['montoS']
            extras['nbase3d'] = l['montoD']
            tt = tt + float(l['montoS'])
            ttd = ttd + float(l['montoD'])
        elif l['nubic'] == '6':
            extras['nisc'] = l['montoS']
            extras['niscd'] = l['montoD']
            tt = tt + float(l['montoS'])
            ttd = ttd + float(l['montoD'])
        elif l['cdsc_asicab'].strip() == 'IGV':
            extras['nimp'] = l['montoS']
            extras['nimpd'] = l['montoD']
            tt = tt + float(l['montoS'])
            ttd = ttd + float(l['montoD'])

    extras['nigv2'] = extras['nbase2']*0.18
    extras['nigv3'] = extras['nbase3']*0.18
    extras['nigv2d'] = extras['nbase2d']*0.18
    extras['nigv3d'] = extras['nbase3d']*0.18
    
    extras['ntot'] = float(tt)
    extras['ntotd'] = float(ttd)

    return extras

def origenes(ccod_asi, usuario):
    #this line has been modified 'cose we are going to work only whit null user
    #origen = PgAsiento.objects.all().filter(ccod_asi=ccod_asi).filter(usuario= usuario)
    #origen = PgAsiento.objects.all().filter(ccod_asi=ccod_asi).filter(usuario__isnull=True)
    #origen = PgAsiento.objects.all().filter(ccod_asi=ccod_asi)
    origen = PgPasiento.objects.all().filter(ccod_asi = ccod_asi)
    orig = 0
    glosa = ""
    cregis = ""
    t2008_8 = ""
    mi_asiento = {'orig':'', 'cdsc':'', 'cregis':'','t2008_8':'','currentMonth':None,'mes':0}

    for ori in origen:
        mi_asiento['orig'] = ori.ccod_ori.strip()
        mi_asiento['cdsc'] = ori.cdsc.strip()
        mi_asiento['cregis'] = ori.cregis.strip()
        mi_asiento['t2008_8'] = ori.t2008_8.strip()
        #orig = ori.ccod_ori
        #glosa = ori.cdsc
        #cregis=ori.cregis
        #t2008_8 = ori.t2008_8
    currentMonth = datetime.now().month
    el_mes=''
    if mi_asiento['orig']=='00':
        el_mes = 'o.nnum0'
        currentMonth = '00'
    else:
        el_mes = 'o.nnum'+ str(currentMonth)
        currentMonth = datetime.now().month


    #aumentar = PgOrigen.objects.all().filter(ccod_ori = mi_asiento['orig']).filter(usuario=usuario)
    aumentar = PgOrigen.objects.all().filter(ccod_ori = mi_asiento['orig']).filter(usuario__isnull=True)
    #aumentar = PgOrigen.objects.all().filter(ccod_ori = mi_asiento['orig'])
    

    for o in aumentar:
        #print(o, eval(el_mes), 'origenes completos')
        #mi_asiento['currentMonth'] = float(eval(el_mes)) + 1
        mi_asiento['currentMonth'] = float(eval(el_mes)) + 1

    if len(str(currentMonth)) == 1:
        currentMonth = '0'+str(currentMonth)
    mi_asiento['mes'] = str(currentMonth)    
    #print(mi_asiento, 'datos de mi asiento 555555555')

    return mi_asiento

def juntararrays( cuenta, listat,nombreCambio,valorCambio, usuario):
    #cuenta datos del html
    #lista datos procesados del mismo python
    '''
    for cut in cuenta:
        for lit in listat:
            print(cut['cuenta'])
    '''
    
    
    #print(cuenta)

    for l in listat:
        elefante = {'cuenta':'','item':'','ruc_dni':'','documentos_buscar_valido':'','serie_Numero':'','datepicker_d':None,'datepicker_v':None,'flujo_efectivo':'','MedPago_valido':'','nanalisis':''};
        ncu = ''
        if l['tipo']=='d':
            ncu = l['ccod_cue_debe']
        else:
            ncu = l['ccod_cue_haber']

        for c in cuenta:
            if l['nitem']==c['nitem']:
                if c['formulario']=='null':
                    c['formulario'] = elefante
                    c['formulario']['cuenta'] = ncu
                l['formulario'] = c['formulario']
                #reemplaza la nueva cuenta de ldespliega a formulario['cuenta']
                if l['tipo']=='d':
                    l['formulario']['cuenta'] = c['ccod_cue_debe']
                elif l['tipo']=='h':
                    l['formulario']['cuenta'] = c['ccod_cue_haber']

            #print('list', l['nitem'],'cuenta',c['nitem'])
        #print(l['nitem'])
    #reemplaza la nueva cuenta de ldespliega a formulario['cuenta']
        #o['formulario']['cuenta'] = " "
        #if o['tipo']=='d':
        #    o['formulario']['cuenta'] = o['ccod_cue_debe']
        #    print('puesto debe ')
        #elif o['tipo']=='h':
        #    o['formulario']['cuenta'] = o['ccod_cue_haber']
        #    print('puesto haber')
    '''
    for tu in listat:
        print(tu)
        print('tu ==============')
    '''
    #reemplazar los nanalisis del formulario a la plantilla origilal
    for l in listat:
        #print(l['nanalisis'],'|',l['formulario']['nanalisis'], '|',l['ldespliega'])
        if l['ldespliega'] == True:
            if l['formulario']['nanalisis'] != '':
                l['nanalisis'] = l['formulario']['nanalisis']
        if l['nanalisis']=='2' or l['nanalisis']=='4':
            l['lcomp'] = False
        else:
            l['lcomp'] = False
            l['formulario']['datepicker_v'] =None
    #reemplaza la nueva cuenta de ldespliega a formulario['cuenta']
    '''
    for o in listat:
        if o['ldespliega']==True and int(o['nanalisis']) >=2:
            if o['tipo'] == 'd':
                o['ccod_cue_debe'] = o['formulario']['cuenta']
            elif o['tipo']=='h':
                o['ccod_cue_haber'] = o['formulario']['cuenta']
        else:
            if o['tipo'] == 'd':
                o['ccod_cue_debe'] = o['formulario']['cuenta']
            elif o['tipo']=='h':
                o['ccod_cue_haber'] = o['formulario']['cuenta']
    '''
    #manejos de los montos para soles y dolares
    for l in listat:
        if nombreCambio == 'S':
            l['montoS'] = l['monto']
            l['montoD'] = str(round( float(l['monto']) / float(valorCambio),2))
            #l['montoD'] = float()
        elif nombreCambio == 'D':
            l['montoS'] = str(round( float(l['monto']) * float(valorCambio),2))
            l['montoD'] = l['monto']

    #elefante = {'cuenta':'','item':'','ruc_dni':'','documentos_buscar_valido':'','serie_Numero':'','datepicker_d':None,'datepicker_v':None,'flujo_efectivo':'','MedPago_valido':'','nanalisis':''};

    for lok in listat:
        #this line has been commet 'cose we work only whit null users
        #idpgplan = PgPlan.objects.all().filter(usuario=usuario).filter(Q(ccod_cue=lok['ccod_cue_debe'])|Q(ccod_cue=lok['ccod_cue_haber']))
        idpgplan = PgPlan.objects.all().filter(usuario__isnull=True).filter(Q(ccod_cue=lok['ccod_cue_debe'])|Q(ccod_cue=lok['ccod_cue_haber']))
        #print(lok['ccod_cue_debe'], lok['ccod_cue_haber'], idpgplan[0].id, 'el id--------------------pppppppppppp')
        lok['idpgplan'] = idpgplan[0].id
    
    for gyu in listat:
        print(gyu)
        print('$-$-$-$-$-$')
    
    return listat

def cuentas(ccod_asi,usuario):
    #this line has been modified to work only whit null useres
    #planes = PgAsicab.objects.filter(ccod_asi=ccod_asi).filter(usuario=usuario).order_by('nitem')
    #planes = PgAsicab.objects.filter(ccod_asi=ccod_asi).filter(usuario__isnull=True).order_by('nitem')
    planes = PgPasicab.objects.filter(ccod_asi=ccod_asi).filter(usuario__isnull=True).order_by('nitem')
    lista = []
    for dato in planes:
        #lista.append({'id':smart_str(row.product_id), 'name':smart_str(row.product_name), 'price':smart_str(row.retail_price)})
        #nombred = PgPlan.objects.values_list('cdsc', flat=True).filter(ccod_cue=dato.cdebe)
        #nombreh = PgPlan.objects.values_list('cdsc', flat=True).filter(ccod_cue=dato.chaber) 
        newAnanlisis = ''
        #nombred = PgPlan.objects.all().filter(ccod_cue=dato.cdebe).filter(usuario=usuario)
        nombred = PgPlan.objects.all().filter(ccod_cue=dato.cdebe).filter(usuario__isnull=True)
        nombreh = PgPlan.objects.all().filter(ccod_cue=dato.chaber).filter(usuario__isnull=True)        
        for m in nombreh:
            if (dato.cdebe[:1]=='5' or dato.chaber[:1]=='5') and m.nanalisis <2:
                newAnanlisis='10'
            else:
                newAnanlisis = str(m.nanalisis)
            lista.append({
                'cdsc':m.cdsc,
                'nanalisis':str(m.nanalisis),
                'newAnanlisis':newAnanlisis,
                'cdsc_asicab':dato.cdsc,
                'nubic':str(dato.nubic),
                'cid':dato.cid,
                'ccod_cue_debe':dato.cdebe,
                'ccod_cue_haber':dato.chaber,
                'formula':dato.cformula,
                'nitem':str(dato.nitem),
                'tipo':'h',
                'monto':0,
                'montoT':0,
                'montoS':0.00,
                'montoD':0.00,
                'lrango':dato.lrango,
                'ldespliega':dato.ldespliega,
                'movimiento':0,
                'lcancela':dato.lcancela,
                'lcomp':'',
                'rangolos':'',
                'formulario':'null',
                'ccod_camp1':dato.ccod_camp1,
                'ccod_camp2':dato.ccod_camp2,
                'ccod_flujo':dato.ccod_flujo,
                'fformulario': dato.cformario,
                'lpedir':dato.lpedir,
                'lcancela':dato.lcancela,
                'cdsc_cuenta': dato.cdsc
                })
        for x in nombred:
            if dato.cdebe[:1]=='5' or dato.chaber[:1]=='5' and m.nanalisis <2:
                newAnanlisis='10'
            else:
                newAnanlisis = str(x.nanalisis)
            lista.append({
                'cdsc':x.cdsc,
                'nanalisis':str(x.nanalisis),
                'newAnanlisis':newAnanlisis,
                'cdsc_asicab':dato.cdsc,
                'nubic':str(dato.nubic).strip(),
                'cid':dato.cid,
                'ccod_cue_debe':dato.cdebe,
                'ccod_cue_haber':dato.chaber,
                'formula':dato.cformula,
                'nitem':str(dato.nitem),
                'tipo':'d',
                'monto':0,
                'montoT':0,
                'montoS':0.00,
                'montoD':0.00,
                'lrango':dato.lrango,
                'ldespliega':dato.ldespliega,
                'movimiento':0,
                'lcancela':dato.lcancela,
                'lcomp':'',
                'rangolos':'',
                'formulario':'null',
                'ccod_camp1':dato.ccod_camp1,
                'ccod_camp2':dato.ccod_camp2,
                'ccod_flujo':dato.ccod_flujo,
                'fformulario': dato.cformario,
                'lpedir':dato.lpedir,
                'lcancela':dato.lcancela,
                'cdsc_cuenta': dato.cdsc
                })
    
    for x in lista:
        if x['lrango'] == True:
            #print(x['cid'])
            #nuevacuenta = PgAsidet.objects.filter(cid=x['cid']).filter(usuario=usuario)
            nuevacuenta = PgPasidet.objects.filter(cid=x['cid'])
            li = []
            for dt in nuevacuenta:
                #print(dt.ccod_cue, dt.cformula)
                #this line has been modified to qork only whit null useres
                #nporcen = PgPorcen.objects.filter(usuario=usuario).filter(ccod_por=tratar_palabras(dt.cformula))
                nporcen = PgPorcen.objects.all().filter(ccod_por=tratar_palabras(dt.cformula))
                elporce = ''
                for pr in nporcen:
                    elporce = pr.npor
                    #print(pr.npor,'prrrrrrrrrrrrrrrrrr')

                li.append({'nitem':x['nitem'],'ccod_cue':dt.ccod_cue, 'cformula': dt.cformula.strip(),'npor': str(elporce),'monto2':0.0, 'cdsc':str(pr.cdsc)})

            #x['movimiento']='hola'
            x['rangolos']=li
    '''
    for h in lista:
        print(h, '--')
        print(h, 'RTRTRTRTRT')
    '''
    return lista

def asidet_unicos():
    #this line has been modified to work only whit null user
    #asidet = PgAsidet.objects.filter(usuario=usuario).order_by('cformula').distinct('cformula')
    asidet = PgAsidet.objects.filter(usuario__isnull=True).order_by('cformula').distinct('cformula')
    lista_asidet = []
    for formula in asidet:
        frm = []
        if formula.cformula:
            posi = formula.cformula.find('_')
            formu = formula.cformula[posi+1:]
            #montos = PgPorcen.objects.values_list('npor', flat=True).filter(ccod_por=formu.strip())
            #montos = PgPorcen.objects.all().filter(ccod_por=formu.strip()).filter(usuario=usuario)
            montos = PgPorcen.objects.all().filter(ccod_por=formu.strip()).filter(usuario__isnull=True)
            
        #lista_asidet.append(formula.cformula)
        frm.append(formula.cformula.strip())
        frm.append(formu.strip())
        for mon in montos:
            frm.append(mon.npor)
        lista_asidet.append(frm)
    return lista_asidet
        
def tratar_palabras(palabra):
    posi = palabra.find('_')
    formu = palabra[posi+1:]
    return formu

def datos_listos(ccod_asi,monto):
    asidet = []
    asidet =  asidet_unicos()
    mi_lista=[]
    SYS_IGV = 18.0
    SYS_IGVP = 18.0
    SYSP_IGVP = 0.0200
    SYSP_IGVR = 0.0600
    SYSP_RPS = 0.0900
    SYSP_AFPI = 0.1300
    SYSP_AFPH = 0.1300
    SYSP_AFPR = 0.1300
    SYSP_AFPU = 0.1100
    #SYSP_CTS = 0.0800
    SYSP_CTS = 0.0833
    SYS_CTS = 0.0833
    SYSP_SNP = 0.1300
    
    I1 = float(monto)
    i1 = float(monto)
    I2 = 0.0
    i2 = 0.0
    I3 = 0.0
    i3 = 0.0
    I4 = 0.0
    i4 = 0.0
    I5 = 0.0
    i5 = 0.0
    I6 = 0.0
    i6 = 0.0
    I7 = 0.0
    i7 = 0.0
    I8 = 0.0
    i8 = 0.0
    I9 = 0.0
    i9 = 0.0
    I10 = 0.0
    i10 = 0.0
    #planes = PgAsicab.objects.filter(ccod_asi=ccod_asi).filter(usuario=usuario)
    #planes = PgAsicab.objects.filter(ccod_asi=ccod_asi).filter(usuario__isnull=True)
    planes = PgPasicab.objects.filter(ccod_asi=ccod_asi).filter(usuario__isnull=True)
    lista = []
    lista = cuentas(ccod_asi)
    for l in lista:
        distancia  = l['formula'].strip()
        if len(distancia) == 0 :
            l['monto'] = I1
        if len(distancia) > 1:
            if l['nitem'] == '3':
                for x in lista:
                    if x['nitem'] =='2':
                        I2 = x['monto']
                        i2 = x['monto']
            if l['nitem'] == '4':
                for x in lista:
                    if x['nitem'] =='3':
                        I3 = x['monto']
                        i3 = x['monto']
            if l['nitem'] == '5':
                for x in lista:
                    if x['nitem'] =='4':
                        I4 = x['monto']
                        i4 = x['monto']
            if l['nitem'] == '6':
                for x in lista:
                    if x['nitem'] =='5':
                        I5 = x['monto']
                        i5 = x['monto']
            lanzar =l['formula']
            l['monto'] = round(eval(str(lanzar)),2)
        for li in l['rangolos']:
            lanza = li['npor']
            #li['monto2'] = round(eval(str(lanza)*l['monto']),2)
            #li['monto2'] = str(lanza)+'-'+ str(l['monto']) +'-'+ str(eval(str(lanza)+'*'+str(l['monto']))) 
            li['monto2'] = str(round(eval(str(lanza)+'*'+str(l['monto'])),2)) 

    #for li in lista:
    #    print(li,'lista')
    return lista

def variablesPorcentajes():
    vari =  tratar_palabras('SYSP_SNP')
    mont = PgPorcen.objects.filter(ccod_por=vari)
    for dt in mont:
        print(dt.ccod_por, dt.npor)

class pruebaMontos(TemplateView):
    def get(self, request, *args, **kwargs):
        cuenta = request.GET.get('cuenta')
        monto = request.GET.get('monto')
        usuario = request.GET.get('usuario')
        datos_listos_acabar(cuenta, monto, usuario)
        #print(ccod_asi, monto, 'ccodasi - monto')
        #print(ruc_dni)
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        
        #print(tam)
        lista.append({'tamaño':'tam','crazon':'cdocumento'})

        return HttpResponse(json.dumps(lista), content_type="application/json")

class crearpgActivo(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        #datos_listos_acabar(cuenta, monto, usuario);
        m = replicarDatos(usuario)
        m.pgActivo()

        lista = []
        #lista = datos_listos(ccod_asi, monto)
        #print(tam)
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crearPgAdicion(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        #datos_listos_acabar(cuenta, monto, usuario);
        m = replicarDatos(usuario)
        m.PgAdicion()
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        #print(tam)
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgAsicab(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        #datos_listos_acabar(cuenta, monto, usuario);
        m = replicarDatos(usuario)
        m.PgAsicab()
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        #print(tam)
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgAsidet(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        #datos_listos_acabar(cuenta, monto, usuario);
        m = replicarDatos(usuario)
        m.PgAsidet()
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        #print(tam)
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgAsiento(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        #datos_listos_acabar(cuenta, monto, usuario);
        m = replicarDatos(usuario)
        m.PgAsiento()
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        #print(tam)
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgAsimodel(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgAsimodel()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
        
class crear_PgCampat1(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgCampat1()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgCampat2(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgCampat2()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgCatesfin(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgCatesfin()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgClabieser(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgClabieser()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgConveniodt(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgConveniodt()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgCostos(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgCostos()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgCostos2(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgCostos2()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
        
class crear_PgCtrlple(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgCtrlple()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
        
class crear_PgDoc(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgDoc()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
        
class crear_PgEnfinan(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgEnfinan()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgFactor(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgFactor()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgFlujo(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgFlujo()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
        
class crear_PgFormato(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgFormato()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
        
class crear_PgFormato2(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgFormato2()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgMoneda(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgMoneda()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
        
class crear_PgOrigenfi(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgOrigenfi()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
        
class crear_PgPagsunat(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgPagsunat()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
        
class crear_PgPais(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgPais()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
    
class crear_PgPath(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgPath()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")
    
class crear_PgPorcen(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgPorcen()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgSunat05(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgSunat05()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgTabdetra(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgTabdetra()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgTipo(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgTipo()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgTipod(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgTipod()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crear_PgTiprenta(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        m = replicarDatos(usuario)
        m.PgTiprenta()
        lista = []
        lista.append({'creado':'creado'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

#send also the new list, find nitem+ identification in which account we put the amuunt, replace the amount 
#to the user send to the amount for the (formulation)
# also be carefuly about -- lcomp 
def datos_listos_acabar(ccod_asi, monto, usuario, template):

    for tem in template:
        print(tem, 'P P P')
        print('* * ** * ** * * * * *')

    lista = []
    lista = cuentas(ccod_asi, usuario)
    variables = []
    
    SYS_IGV = 18.0
    SYS_IGVP = 18.0
    SYSP_IGVP = 0.0200
    SYSP_IGVR = 0.0600
    SYSP_RPS = 0.0900
    SYSP_AFPI = 0.1300
    SYSP_AFPH = 0.1300
    SYSP_AFPR = 0.1300
    SYSP_AFPU = 0.1100
    #SYSP_CTS = 0.0800
    SYSP_CTS = 0.0833
    SYS_CTS = 0.0833
    SYSP_SNP = 0.1300

    I1 = float(monto)
    i1 = float(monto)

    I2 = float(0)
    i2 = float(0)
    si2 = float(0)

    I3 = float(0)
    i3 = float(0)
    si3 = float(0)

    I4 = float(0)
    i4 = float(0)
    si4 = float(0)

    I5 = float(0)
    i5 = float(0)
    si5 = float(0)

    I6 = float(0)
    i6 = float(0)
    si6 = float(0)

    I7 = float(0)
    i7 = float(0)
    si7 = float(0)

    I8 = float(0)
    i8 = float(0)
    si8 = float(0)

    I9 = float(0)
    i9 = float(0)
    si9 = float(0)

    I10 = float(0)
    i10 = float(0)
    si10 = float(0)

    for dd in template:
        if dd['nitem'] == '2' and float(dd['montoT'])>0:
            si2 = float(dd['montoT'])
        if dd['nitem'] == '3' and float(dd['montoT'])>0:
            si3 = float(dd['montoT'])
        if dd['nitem'] == '4' and float(dd['montoT'])>0:
            si4 = float(dd['montoT'])
        if dd['nitem'] == '5' and float(dd['montoT'])>0:
            si5 = float(dd['montoT'])
        if dd['nitem'] == '6' and float(dd['montoT'])>0:
            si6 = float(dd['montoT'])
        if dd['nitem'] == '7' and float(dd['montoT'])>0:
            si7 = float(dd['montoT'])
        if dd['nitem'] == '8' and float(dd['montoT'])>0:
            si8 = float(dd['montoT'])
        if dd['nitem'] == '9' and float(dd['montoT'])>0:
            si9 = float(dd['montoT'])
        if dd['nitem'] == '10' and float(dd['montoT'])>0:
            si10 = float(dd['montoT'])
    print('si2',si2,'si3',si3,'si4',si4,'si5',si5,'si6',si6,'si7',si7,'si8',si8,'si9',si9,'si10',si10)

    #print(lista)
    for li in lista:
        lanzar =li['formula']
        if li['nitem'] == '1':
            li['monto'] = format(i1, '.2f')
        if li['nitem'] == '2':
            if len(lanzar.strip())>=2:
                i2 = round(eval(str(lanzar)),2)
                I2 = round(eval(str(lanzar)),2)
            else:
                if si2 > 0:
                    i2 = si2
                    I2 = si2
                else:
                    i2 = i1
                    I2 = i1

            li['monto'] = i2 
        if li['nitem'] == '3':
            if len(lanzar.strip())>=2:
                i3 = round(eval(str(lanzar)),2)
                I3 = round(eval(str(lanzar)),2)
            else:
                if si3 >0:
                    i3 = si3
                    I3 = si3
                else:
                    i3 = i2
                    I3 = i2
            li['monto'] = i3
        if li['nitem'] == '4':
            if len(lanzar.strip())>=2:
                i4 = round(eval(str(lanzar)),2)
                I4 = round(eval(str(lanzar)),2)
            else:
                if si4 > 0:
                    i4 = si4
                    I4 = si4
                else:
                    i4 = i3
                    I4 = i3
            li['monto'] = i4
        if li['nitem'] == '5':
            if len(lanzar.strip())>=2:
                i5 = round(eval(str(lanzar)),2)
                I5 = round(eval(str(lanzar)),2)
            else:
                if si5 >0:
                    i5 = si5
                    I5 = si5
                else:
                    i5 = i4
                    I5 = i4
            li['monto'] = i5
        if li['nitem'] == '6':
            if len(lanzar.strip())>=2:
                i6 = round(eval(str(lanzar)),2)
                I6 = round(eval(str(lanzar)),2)
            else:
                if si6>0:
                    i6 = si6
                    I6 = si6
                else:
                    i6 = i5
                    I6 = i5
            li['monto'] = i6
        if li['nitem'] == '7':
            if len(lanzar.strip())>=2:
                i7 = round(eval(str(lanzar)),2)
                I7 = round(eval(str(lanzar)),2)
            else:
                if si7>0:
                    i7 = si7
                    I7 = si7
                else:
                    i7 = i6
                    I7 = i6
            li['monto'] = i7
        if li['nitem'] == '8':
            if len(lanzar.strip())>=2:
                i8 = round(eval(str(lanzar)),2)
                I8 = round(eval(str(lanzar)),2)
            else:
                if si8>0:
                    i8 = si8
                    I8 = si8
                else:
                    i8 = i7
                    I8 = i7
            li['monto'] = i8
        if li['nitem'] == '9':
            if len(lanzar.strip())>=2:
                i9 = round(eval(str(lanzar)),2)
                I9 = round(eval(str(lanzar)),2) 
            else:
                if si9>0:
                    i9 = si9
                    I9 = si9
                else:
                    i9 = i8
                    I9 = i8
            li['monto'] = i9
        if li['nitem'] == '10':
            if len(lanzar.strip())>=2:
                i10 = round(eval(str(lanzar)),2)
                I10 = round(eval(str(lanzar)),2)
            else:
                if si10 >0:
                    i10 = si10
                    I10 = si10
                else:
                    i10 = i9
                    I10 = i9
            li['monto'] = i10
        for lo in li['rangolos']:
            lanza = lo['npor']
            lo['monto2'] = str(round(eval(str(lanza)+'*'+str(li['monto'])),2))
    #print(li['formula'].strip(), li['nitem'], li['monto'])
    #for lou in lista:
    #    print(lou['nitem'],'|',lou['formula'].strip(),'|',lou['monto'],'|',lou['rangolos'])
    for li in lista:
        li['monto'] = format(float(li['monto']),'.2f')

    for ti in lista:
        for tem in template:
            if tem['montoT'] != 0 and tem['nitem'] == ti['nitem']:
                ti['monto'] = tem['montoT']
                
    for tr in lista:
        print(tr['nitem'], tr['monto'], tr['formula'].strip() , 'p p p p ')
        print('% %  % % $')

    return lista

def actualizarValoresPlanB(usuario, cuenta, cmes, ndebe, nhaber, condicion):
    #buscando el valñor en la tabla pg_plan
    #condicion = n si es nuevo  , e si es eliminar
    mi_consulta  = PgPlan.objects.all().filter(ccod_cue=cuenta.strip()).filter(usuario= usuario)
    ndebea = float(0)
    nhabera = float(0)
    valordebe = ''
    valorhaber = ''
    if cmes == '00' or cmes == '0':
        valordebe = 'x.ndebe0'
        valorhaber = 'x.nhaber0'
        elmesdebe = 'ndebe0'
        elmeshaber = 'nhaber0'
    elif cmes[:1]=='0':
        valordebe = 'x.ndebe'+ str(cmes[1:])
        valorhaber = 'x.nhaber'+str(cmes[1:])
        elmesdebe = 'ndebe'+str(cmes[1:])
        elmeshaber = 'nhaber'+str(cmes[1:])
        print(cmes,valordebe, valorhaber, elmesdebe, elmeshaber)
    else:
        valordebe= 'x.ndebe'+ str(cmes)
        valorhaber = 'x.nhaber' + str(cmes)
        elmesdebe = 'ndebe'+str(cmes)
        elmeshaber = 'nhaber'+str(cmes)

    for x in mi_consulta:
        debe = eval(valordebe)
        haber = eval(valorhaber)

    if condicion == 'N':
        ndebea = float(ndebe) + float(debe)
        nhabera = float(nhaber) + float(haber)
        print(debe, haber,'4444',ndebea, nhabera)
        
    elif condicion == 'e':
        #buscar los datos para eliminar y restar la cuentas
        ndebea = float(debe) - float(ndebe)
        nhabera = float(haber) - float(nhaber)
        print(debe, haber,'555',ndebea, nhabera)
    else:
        #para cunado sea modificar
        pass
    espresion = "PgPlan.objects.filter(usuario=usuario).filter(ccod_cue=cuenta).update("+elmesdebe+" = ndebea, "+elmeshaber+" = nhabera)"
    #print(espresion)
    eval(espresion)
    
def actualizarValoresPlan(usuario, cuenta, cmes, ndebe, nhaber, condicion):
    #mi_consulta_count = PgSaldosiniciales.objects.all().filter(usuario = usuario).filter(ccod_cue=cuenta).count()
    mi_consulta_count = PgPlan.objects.all().filter(ccod_cue=cuenta).count()
    #mi_consulta = PgSaldosiniciales.objects.all().filter(usuario = usuario).filter(ccod_cue=cuenta)
    mi_consulta  = PgPlan.objects.all().filter(ccod_cue=cuenta.strip())
    print('usuario:', usuario,'cmes:',cmes , 'cuenta:' ,cuenta, 'ndebe:', ndebe, 'nhaber:', nhaber, 'Condicion:', condicion, 'cantidad:', mi_consulta)

    ndebea = float(0)
    nhabera = float(0)
    debe= float(0)
    haber=float(0)
    valordebe = ''
    valorhaber = ''
    if cmes == '00' or cmes == '0':
        valordebe = 'x.ndebe0'
        valorhaber = 'x.nhaber0'
        elmesdebe = 'ndebe0'
        elmeshaber = 'nhaber0'
        
    elif cmes[:1]=='0':
        valordebe = 'x.ndebe'+ str(cmes[1:])
        valorhaber = 'x.nhaber'+str(cmes[1:])
        elmesdebe = 'ndebe'+str(cmes[1:])
        elmeshaber = 'nhaber'+str(cmes[1:])
        print(cmes,valordebe, valorhaber, elmesdebe, elmeshaber)

    else:
        valordebe= 'x.ndebe'+ str(cmes)
        valorhaber = 'x.nhaber' + str(cmes)
        elmesdebe = 'ndebe'+str(cmes)
        elmeshaber = 'nhaber'+str(cmes)

    for x in mi_consulta:
        debe = eval(valordebe)
        haber = eval(valorhaber)

    if condicion == 'N':
        #conditin if the 'cuenta' exist to be update 
        #condition if the 'cuenta' doesnt exist to to create
        if mi_consulta_count <= 0:
            #p = 'PgSaldosiniciales(usuario = usuario, ccod_cue = cuenta, '+elmesdebe+' = ndebe, '+elmeshaber+' = nhaber)'
            p = 'PgPlan(ccod_cue = cuenta, '+elmesdebe+' = ndebe, '+elmeshaber+' = nhaber)'
            m = eval(p)
            m.save()
            print('datos nuevos que no existen')
        else:
            ndebea = float(ndebe) + float(debe)
            nhabera = float(nhaber) + float(haber)
            #espresion = "PgSaldosiniciales.objects.filter(usuario=usuario).filter(ccod_cue=cuenta).update("+elmesdebe+" = ndebea, "+elmeshaber+" = nhabera)"
            espresion = "PgPlan.objects.filter(ccod_cue=cuenta).update("+elmesdebe+" = ndebea, "+elmeshaber+" = nhabera)"
            eval(espresion)
            print('datos nuevos que se actualizaran')

    elif condicion == 'e':
        ndebea = float(debe) - float(ndebe)
        nhabera = float(haber) - float(nhaber)
        espresion = "PgSaldosiniciales.objects.filter(usuario=usuario).filter(ccod_cue=cuenta).update("+elmesdebe+" = ndebea, "+elmeshaber+" = nhabera)"
        eval(espresion)
        print('datos eliminar')
    else:
        print('no determinado parab mantenimieto de saldos iniciakles')
    

    

