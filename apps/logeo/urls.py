#urls de la app logeo

from django.urls import path
from django.conf.urls import url
from apps.logeo.views import *
from django.contrib.auth.views import LoginView, LogoutView



urlpatterns = [
    
    path('adentro/',adentro.as_view(),name='adentro'),
    #
    path('registro/', signup, name='signup'),
    
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate, name='activate'),
    path('',LoginView.as_view(template_name='log/login.html'),name  = "login"),
    path('dashboard/',ControlPanelView.as_view(),name  = "panel-dashboard"),
    #path('logout/',LogoutView.as_view(),name  = "panel-logout"),

    

]

