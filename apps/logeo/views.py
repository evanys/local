#vistas de logeo
from django.shortcuts import render

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
#
from .forms import SignupForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
#
from  apps.logeo.tokens import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMessage


from django.views.generic import FormView, TemplateView, RedirectView
from django.urls import reverse, reverse_lazy
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
#
from apps.logeo.forms import EntradaForm
from django.db.models import Max
from apps.pacioli.models import *

class adentro(TemplateView):
    template_name = 'log/adentro.html'

def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)

        cantidad= AuthUser.objects.all().count()
        altoid = 1
        if (cantidad >= 1):
            ids =  AuthUser.objects.values_list('id', flat=True)
            altoid = max(ids)+1
        else:
            altoid = 1


        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.id = altoid
            user.save()
            print("Usuario dado de alta")
            print(altoid)
            
            current_site = get_current_site(request)
            mail_subject = 'Activa tu Cuenta.'
            message = render_to_string('log/acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token':account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                        mail_subject, message, to=[to_email]
            )
            email.send()
            #return HttpResponse('Please confirm your email address to complete the registration')
            return render( request,'log/confir_email.html' )
            #return render( request,'registration/password_reset_complete.html' )
    else:
        form = SignupForm()
    return render(request, 'log/signup.html', {'form': form})

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        #uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        # return redirect('home')
        #return HttpResponse('Thank you for your email confirmation. Now you can login your account.')

        return render( request,'log/login_redirect.html' )
    else:
        return render( request,'log/login_invalido.html' )
        #return HttpResponse('Activation link is invalid!')



class LoginView(FormView):
    form_class = AuthenticationForm
    template_name = "log/login.html"
    success_url =  reverse_lazy("panel-dashboard")

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return reverse_lazy(self.get_success_url())
        else:
            return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)

class LogoutView(RedirectView):
    pattern_name = 'panel-login'
    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class LoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return reverse('panel-login')
        else:
            return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)

class ControlPanelView(LoginRequiredMixin, TemplateView):
    template_name = 'log/panel.html'
    #template_name = 'adminis/index.html'

    def get_context_data(self, **kwargs):
        context = super(ControlPanelView, self).get_context_data(**kwargs)
        #context['pedidos_pendientes'] = Pedido.objects.pendientes()
        # ....
        # Recopilar resto de la informacion
        # ....

        return context

class LogoutView(RedirectView):
    #url = '/auth/login/' login
    url = 'login'
    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)



#def rellenarOrigen(usuario):
    