#urls apps contable
app_name = "expertoPlus"

from django.urls import path
from apps.expertoPlus.views import *
from django.conf.urls import url
from django.contrib.auth.views import LogoutView
from django.contrib.auth import views as auth_views

#hojatra
urlpatterns = [
    path('inicioc/', index.as_view(),name='inicioc'),

    #jsons
    path('buscarNetos/', buscarNetos.as_view()),
    url(r'^logout/$', LogoutView.as_view(), {'template_name': 'log/logout.html'}, name='logout'),
]


