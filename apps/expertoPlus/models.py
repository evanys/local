from django.db import models


class PcAccprod(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_accprod'


class PcActimarca(models.Model):
    ccod_marca = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_actimarca'


class PcActimov(models.Model):
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)
    ldisgrega = models.BooleanField(blank=True, null=True)
    lcv = models.BooleanField(blank=True, null=True)
    lexp = models.BooleanField(blank=True, null=True)
    ccontado = models.CharField(max_length=10, blank=True, null=True)
    ccredito = models.CharField(max_length=10, blank=True, null=True)
    nie = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lneg = models.BooleanField(blank=True, null=True)
    limp = models.BooleanField(blank=True, null=True)
    mpath = models.CharField(max_length=500, blank=True, null=True)
    lstock = models.BooleanField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    ctipo = models.CharField(max_length=1, blank=True, null=True)
    cdefault = models.CharField(max_length=10, blank=True, null=True)
    cdebe = models.CharField(max_length=10, blank=True, null=True)
    chaber = models.CharField(max_length=10, blank=True, null=True)
    nlineas = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    lnumero = models.BooleanField(blank=True, null=True)
    lalmacen = models.BooleanField(blank=True, null=True)
    lf11 = models.BooleanField(blank=True, null=True)
    lf12 = models.BooleanField(blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    lsolomone = models.BooleanField(blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    naumdis = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nmons = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lstockmsg = models.BooleanField(blank=True, null=True)
    liguales = models.BooleanField(blank=True, null=True)
    lnosalir = models.BooleanField(blank=True, null=True)
    lnoimp = models.BooleanField(blank=True, null=True)
    ningreso = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ndigprod = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ljalaresp = models.BooleanField(blank=True, null=True)
    lgenera = models.BooleanField(blank=True, null=True)
    cgenera = models.CharField(max_length=5, blank=True, null=True)
    lcalcigv = models.BooleanField(blank=True, null=True)
    lprecond = models.BooleanField(blank=True, null=True)
    lcamprec = models.BooleanField(blank=True, null=True)
    lrubro = models.BooleanField(blank=True, null=True)
    nrubro = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    lcontome = models.BooleanField(blank=True, null=True)
    lnoexp = models.BooleanField(blank=True, null=True)
    lhacerdet = models.BooleanField(blank=True, null=True)
    ccuedet = models.CharField(max_length=10, blank=True, null=True)
    lautonum = models.BooleanField(blank=True, null=True)
    lvalstk = models.BooleanField(blank=True, null=True)
    lblopre = models.BooleanField(blank=True, null=True)
    npreciob = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lvalpre = models.BooleanField(blank=True, null=True)
    lcostea = models.BooleanField(blank=True, null=True)
    lnumven = models.BooleanField(blank=True, null=True)
    lkardex = models.BooleanField(blank=True, null=True)
    nactcol = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lpago = models.BooleanField(blank=True, null=True)
    t2008_8 = models.CharField(max_length=2, blank=True, null=True)
    ntopsunat = models.CharField(max_length=2, blank=True, null=True)
    cuedebe = models.CharField(max_length=10, blank=True, null=True)
    cuehaber = models.CharField(max_length=10, blank=True, null=True)
    cmovtran = models.CharField(max_length=5, blank=True, null=True)
    nmovpresu = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lpreres = models.BooleanField(blank=True, null=True)
    lpreimp = models.BooleanField(blank=True, null=True)
    lpreact = models.BooleanField(blank=True, null=True)
    cprodresp = models.CharField(max_length=12, blank=True, null=True)
    lverexp = models.BooleanField(blank=True, null=True)
    msecuen = models.CharField(max_length=500, blank=True, null=True)
    lporuser = models.BooleanField(blank=True, null=True)
    cprodex = models.CharField(max_length=12, blank=True, null=True)
    ligvvalor = models.BooleanField(blank=True, null=True)
    lpcli = models.BooleanField(blank=True, null=True)
    lctactea = models.BooleanField(blank=True, null=True)
    lactalte = models.BooleanField(blank=True, null=True)
    lactalma = models.BooleanField(blank=True, null=True)
    lcoskar = models.BooleanField(blank=True, null=True)
    lvercalt = models.BooleanField(blank=True, null=True)
    cctaalt = models.CharField(max_length=10, blank=True, null=True)
    nporage = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    lusufin = models.BooleanField(blank=True, null=True)
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    cdebeini = models.CharField(max_length=10, blank=True, null=True)
    chaberini = models.CharField(max_length=10, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    lnomodi = models.BooleanField(blank=True, null=True)
    lfei = models.BooleanField(blank=True, null=True)
    lsolofei = models.BooleanField(blank=True, null=True)
    lfeb = models.BooleanField(blank=True, null=True)
    lvuelto = models.BooleanField(blank=True, null=True)
    lmjenum = models.BooleanField(blank=True, null=True)
    lreinicia = models.BooleanField(blank=True, null=True)
    mdraw = models.CharField(max_length=500, blank=True, null=True)
    nanticip = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_actimov'


class PcActiresp(models.Model):
    ccod_resp = models.CharField(max_length=5, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ffini = models.DateField(blank=True, null=True)
    fffin = models.DateField(blank=True, null=True)
    ccod_traba = models.CharField(max_length=5, blank=True, null=True)
    ccod_jefe = models.CharField(max_length=5, blank=True, null=True)
    ccod_area = models.CharField(max_length=3, blank=True, null=True)
    ccod_local = models.CharField(max_length=3, blank=True, null=True)
    mobser = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_actiresp'


class PcActivo(models.Model):
    ccod_acti = models.CharField(max_length=12, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ncatego = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ccod_marca = models.CharField(max_length=4, blank=True, null=True)
    cmodelo = models.CharField(max_length=50, blank=True, null=True)
    cplaca = models.CharField(max_length=30, blank=True, null=True)
    clocal = models.CharField(max_length=70, blank=True, null=True)
    ndepre = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    fadqui = models.DateField(blank=True, null=True)
    finicio = models.DateField(blank=True, null=True)
    nestado = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    festado = models.DateField(blank=True, null=True)
    nmetdep = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cdocaut = models.CharField(max_length=20, blank=True, null=True)
    nmeses = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    ntasa = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    ccod_com = models.CharField(max_length=5, blank=True, null=True)
    nsaldoini = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nsaldoini2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncompadic = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncompmejo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncompbaja = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncompajus = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncompvolu = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncompreor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncompotro = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvalohist = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvaloinfl = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvaloajus = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nresidu = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvidaño = models.DecimalField(db_column='nvidaÑo', max_digits=2, decimal_places=0, blank=True, null=True)  # Field name made lowercase.
    nvidmes = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvidupa = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvidund = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmedida = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndepracum = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvdepreva = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvdepbaja = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvdepotro = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvrevvolu = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvrevreor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvrevotro = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndepacuhis = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndepajuinf = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndepacuinf = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cccosto = models.CharField(max_length=9, blank=True, null=True)
    cctaact = models.CharField(max_length=10, blank=True, null=True)
    cctadep = models.CharField(max_length=10, blank=True, null=True)
    cctagas = models.CharField(max_length=10, blank=True, null=True)
    ncolcom = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ncolven = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes01 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes02 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes03 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes04 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes05 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes06 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes07 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes08 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes09 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmes12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro01 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro02 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro03 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro04 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro05 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro06 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro07 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro08 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro09 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npro12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cserie = models.CharField(max_length=4, blank=True, null=True)
    cnumdoc = models.CharField(max_length=7, blank=True, null=True)
    nmoneda = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ncambio = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    nmonto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cruc = models.CharField(max_length=11, blank=True, null=True)
    ntpmov = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ncate = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cobse = models.CharField(max_length=500, blank=True, null=True)
    cmedida = models.CharField(max_length=15, blank=True, null=True)
    ncondi = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ningsal = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_activo'


class PcAlmacen(models.Model):
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)
    ccod_almas = models.CharField(max_length=4, blank=True, null=True)
    cdscs = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_almacen'


class PcAlmasun(models.Model):
    ccod_alma = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_almasun'


class PcArea(models.Model):
    ccod_area = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_area'


class PcCierre(models.Model):
    ffecha = models.DateField(blank=True, null=True)
    lcierre = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_cierre'


class PcCliAux(models.Model):
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    nestado = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    fingre = models.DateField(blank=True, null=True)
    fsuspen = models.DateField(blank=True, null=True)
    freing = models.DateField(blank=True, null=True)
    fconst = models.DateField(blank=True, null=True)
    fultact = models.DateField(blank=True, null=True)
    frenun = models.DateField(blank=True, null=True)
    nactual = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nextra = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nregistro = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    ccod_cobra = models.CharField(max_length=4, blank=True, null=True)
    ccod_pos = models.CharField(max_length=4, blank=True, null=True)
    ccod_com = models.CharField(max_length=4, blank=True, null=True)
    ccod_sec = models.CharField(max_length=2, blank=True, null=True)
    ccod_aux = models.CharField(max_length=5, blank=True, null=True)
    nmonact = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmonext = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nrevis = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    clegal = models.CharField(max_length=50, blank=True, null=True)
    ctelefono = models.CharField(max_length=80, blank=True, null=True)
    ncapital = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    fcumple = models.DateField(blank=True, null=True)
    cpregunta = models.CharField(max_length=100, blank=True, null=True)
    ffecese = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_cli_aux'


class PcCliAuxb(models.Model):
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    cbanco = models.CharField(max_length=50, blank=True, null=True)
    ctipo = models.CharField(max_length=30, blank=True, null=True)
    cnumero = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_cli_auxb'


class PcCliAuxc(models.Model):
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccargo = models.CharField(max_length=40, blank=True, null=True)
    cnombre = models.CharField(max_length=60, blank=True, null=True)
    ctelefono = models.CharField(max_length=80, blank=True, null=True)
    cemail = models.CharField(max_length=80, blank=True, null=True)
    ccod_profe = models.CharField(max_length=3, blank=True, null=True)
    cdireccion = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_cli_auxc'


class PcCliAuxp(models.Model):
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ncontado = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ncredito = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_cli_auxp'


class PcCliPro(models.Model):
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    crazon = models.CharField(max_length=100, blank=True, null=True)
    cdir = models.CharField(max_length=150, blank=True, null=True)
    ctel = models.CharField(max_length=20, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nsaldo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_inter = models.CharField(max_length=11, blank=True, null=True)
    ccod_ruta = models.CharField(max_length=3, blank=True, null=True)
    ccredcom = models.CharField(max_length=10, blank=True, null=True)
    ccredven = models.CharField(max_length=10, blank=True, null=True)
    cdefcom = models.CharField(max_length=10, blank=True, null=True)
    cdefven = models.CharField(max_length=10, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    nmonto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_tip = models.CharField(max_length=3, blank=True, null=True)
    cpaterno = models.CharField(max_length=20, blank=True, null=True)
    cmaterno = models.CharField(max_length=20, blank=True, null=True)
    cnombre1 = models.CharField(max_length=20, blank=True, null=True)
    cnombre2 = models.CharField(max_length=20, blank=True, null=True)
    nnatjur = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    ntipodoc = models.CharField(max_length=1, blank=True, null=True)
    ccontacto1 = models.CharField(max_length=30, blank=True, null=True)
    ccontacto2 = models.CharField(max_length=30, blank=True, null=True)
    ctel2 = models.CharField(max_length=20, blank=True, null=True)
    lbuenc = models.BooleanField(blank=True, null=True)
    lagret = models.BooleanField(blank=True, null=True)
    lagper = models.BooleanField(blank=True, null=True)
    crubro = models.CharField(max_length=40, blank=True, null=True)
    cubigeo = models.CharField(max_length=8, blank=True, null=True)
    ndiavisita = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    ccod_ubig = models.CharField(max_length=6, blank=True, null=True)
    fcumple = models.DateField(blank=True, null=True)
    npregunta = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    cncomer = models.CharField(max_length=80, blank=True, null=True)
    ldeshab = models.BooleanField(blank=True, null=True)
    lmoroso = models.BooleanField(blank=True, null=True)
    lalterno = models.BooleanField(blank=True, null=True)
    l1 = models.BooleanField(blank=True, null=True)
    l2 = models.BooleanField(blank=True, null=True)
    l3 = models.BooleanField(blank=True, null=True)
    l4 = models.BooleanField(blank=True, null=True)
    l5 = models.BooleanField(blank=True, null=True)
    l6 = models.BooleanField(blank=True, null=True)
    l7 = models.BooleanField(blank=True, null=True)
    ccontcred = models.CharField(max_length=1, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    ccod_pago = models.CharField(max_length=2, blank=True, null=True)
    ccod_tran = models.CharField(max_length=4, blank=True, null=True)
    ccod_zona = models.CharField(max_length=10, blank=True, null=True)
    ccod_prom = models.CharField(max_length=500, blank=True, null=True)
    lanulado = models.BooleanField(blank=True, null=True)
    lhabi = models.BooleanField(blank=True, null=True)
    lnohabi = models.BooleanField(blank=True, null=True)
    lnohalla = models.BooleanField(blank=True, null=True)
    ccorreo = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_cli_pro'


class PcCliProno(models.Model):
    ccod_ite = models.CharField(max_length=10, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_con = models.CharField(max_length=2, blank=True, null=True)
    ffec_ini = models.DateField(blank=True, null=True)
    ffec_fin = models.DateField(blank=True, null=True)
    ccod_mes = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_cli_prono'


class PcClialt(models.Model):
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    calterno = models.CharField(max_length=11, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_clialt'


class PcCobrador(models.Model):
    ccod_cobra = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_cobrador'


class PcComisp(models.Model):
    ccod_ni = models.CharField(max_length=3, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ncontado = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ncredito = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ncuota = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmonto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_comisp'


class PcComist(models.Model):
    ccod_ni = models.CharField(max_length=3, blank=True, null=True)
    ccod_tip = models.CharField(max_length=3, blank=True, null=True)
    ncontado = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ncredito = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ncuota = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmonto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_comist'


class PcComisvol(models.Model):
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    nmontos = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmontod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_comisvol'


class PcComite(models.Model):
    ccod_com = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_comite'


class PcComncxml(models.Model):
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    crazsoc = models.CharField(max_length=100, blank=True, null=True)
    mobser = models.CharField(max_length=500, blank=True, null=True)
    carchivo = models.CharField(max_length=254, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_comncxml'


class PcCompras(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    nina = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnet = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimp = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lcc = models.BooleanField(blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)
    cguia = models.CharField(max_length=20, blank=True, null=True)
    mmem = models.CharField(max_length=500, blank=True, null=True)
    lconta = models.BooleanField(blank=True, null=True)
    lexp = models.BooleanField(blank=True, null=True)
    lreg = models.BooleanField(blank=True, null=True)
    lneg = models.BooleanField(blank=True, null=True)
    lstock = models.BooleanField(blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ninad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnetd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    lalmacen = models.BooleanField(blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    ccod_alma2 = models.CharField(max_length=3, blank=True, null=True)
    cid_canj = models.CharField(max_length=8, blank=True, null=True)
    lcanje = models.BooleanField(blank=True, null=True)
    lguest = models.BooleanField(blank=True, null=True)
    ncajas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncajad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ffechareg = models.DateField(blank=True, null=True)
    ccod_inter = models.CharField(max_length=11, blank=True, null=True)
    ccod_trans = models.CharField(max_length=11, blank=True, null=True)
    ccod_moti = models.CharField(max_length=3, blank=True, null=True)
    ccod_tran = models.CharField(max_length=4, blank=True, null=True)
    ccod_pago = models.CharField(max_length=2, blank=True, null=True)
    lsolomone = models.BooleanField(blank=True, null=True)
    nbase2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nisc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    niscd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lresp = models.BooleanField(blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cserresp = models.CharField(max_length=6, blank=True, null=True)
    cnumresp = models.CharField(max_length=15, blank=True, null=True)
    fguiaresp = models.DateField(blank=True, null=True)
    cserguia = models.CharField(max_length=6, blank=True, null=True)
    cnumguia = models.CharField(max_length=13, blank=True, null=True)
    nrets = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nretd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    fdocdet = models.DateField(blank=True, null=True)
    mmem2 = models.CharField(max_length=500, blank=True, null=True)
    ccod_conto = models.CharField(max_length=8, blank=True, null=True)
    cdoc1 = models.CharField(max_length=2, blank=True, null=True)
    cser1 = models.CharField(max_length=6, blank=True, null=True)
    cnum1 = models.CharField(max_length=13, blank=True, null=True)
    cruc1 = models.CharField(max_length=11, blank=True, null=True)
    cdoc2 = models.CharField(max_length=2, blank=True, null=True)
    cser2 = models.CharField(max_length=6, blank=True, null=True)
    cnum2 = models.CharField(max_length=13, blank=True, null=True)
    cruc2 = models.CharField(max_length=11, blank=True, null=True)
    cdoc3 = models.CharField(max_length=2, blank=True, null=True)
    cser3 = models.CharField(max_length=6, blank=True, null=True)
    cnum3 = models.CharField(max_length=13, blank=True, null=True)
    cruc3 = models.CharField(max_length=11, blank=True, null=True)
    ffec1 = models.DateField(blank=True, null=True)
    ffec2 = models.DateField(blank=True, null=True)
    ffec3 = models.DateField(blank=True, null=True)
    cplaca = models.CharField(max_length=10, blank=True, null=True)
    fcanje = models.DateField(blank=True, null=True)
    ccod_gar = models.CharField(max_length=11, blank=True, null=True)
    ccod_cons = models.CharField(max_length=11, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    lproceso = models.BooleanField(blank=True, null=True)
    lkardex = models.BooleanField(blank=True, null=True)
    ccod_alma3 = models.CharField(max_length=3, blank=True, null=True)
    cserext = models.CharField(max_length=20, blank=True, null=True)
    cnumext = models.CharField(max_length=20, blank=True, null=True)
    ncoddes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nnumdes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cnumdet = models.CharField(max_length=5, blank=True, null=True)
    cexttpo = models.CharField(max_length=2, blank=True, null=True)
    cextser = models.CharField(max_length=10, blank=True, null=True)
    cextnum = models.CharField(max_length=20, blank=True, null=True)
    fextfec = models.DateField(blank=True, null=True)
    nextbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nextigv = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cperser = models.CharField(max_length=4, blank=True, null=True)
    cpernum = models.CharField(max_length=10, blank=True, null=True)
    ntpcv = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nentregas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nentregad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltos = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    ncosteado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncosteadod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cref = models.CharField(max_length=1, blank=True, null=True)
    cmle = models.CharField(max_length=2, blank=True, null=True)
    cale = models.CharField(max_length=4, blank=True, null=True)
    ncorrecb = models.CharField(max_length=1, blank=True, null=True)
    cmle2 = models.CharField(max_length=2, blank=True, null=True)
    cale2 = models.CharField(max_length=4, blank=True, null=True)
    cdoc_ndom = models.CharField(max_length=20, blank=True, null=True)
    cdoc_refnc = models.CharField(max_length=20, blank=True, null=True)
    fpagimpret = models.DateField(blank=True, null=True)
    cdocmodi = models.CharField(max_length=2, blank=True, null=True)
    dua_num = models.CharField(max_length=6, blank=True, null=True)
    ffecdua = models.DateField(blank=True, null=True)
    t2008_11 = models.CharField(max_length=3, blank=True, null=True)
    dua_año = models.CharField(db_column='dua_aÑo', max_length=4, blank=True, null=True)  # Field name made lowercase.
    dua_fregu = models.DateField(blank=True, null=True)
    dua_valor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lperden = models.BooleanField(blank=True, null=True)
    nmonperbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    ccod_almas = models.CharField(max_length=4, blank=True, null=True)
    crefk = models.CharField(max_length=1, blank=True, null=True)
    cmlek = models.CharField(max_length=2, blank=True, null=True)
    calek = models.CharField(max_length=4, blank=True, null=True)
    crefb = models.CharField(max_length=1, blank=True, null=True)
    cmleb = models.CharField(max_length=2, blank=True, null=True)
    caleb = models.CharField(max_length=4, blank=True, null=True)
    ccod_oric = models.CharField(max_length=2, blank=True, null=True)
    nasientoc = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_clabs = models.CharField(max_length=1, blank=True, null=True)
    chash = models.CharField(max_length=40, blank=True, null=True)
    c_cat09 = models.CharField(max_length=2, blank=True, null=True)
    c_motinc = models.CharField(max_length=50, blank=True, null=True)
    ct_dcto = models.CharField(max_length=1, blank=True, null=True)
    c_mbaja = models.CharField(max_length=50, blank=True, null=True)
    ldrawback = models.BooleanField(blank=True, null=True)
    lanticipo = models.BooleanField(blank=True, null=True)
    nimppla = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpplad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_compras'


class PcComprasl(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccue = models.CharField(max_length=10, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npigv = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cmedida = models.CharField(max_length=15, blank=True, null=True)
    nuni2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu2 = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    nfactor = models.DecimalField(max_digits=20, decimal_places=10, blank=True, null=True)
    ncosto = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    npud = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    npu2d = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotald = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndesc = models.DecimalField(max_digits=15, decimal_places=3, blank=True, null=True)
    npordesc = models.DecimalField(max_digits=7, decimal_places=3, blank=True, null=True)
    limpuesto = models.BooleanField(blank=True, null=True)
    ccod_talla = models.CharField(max_length=3, blank=True, null=True)
    mdatos = models.CharField(max_length=500, blank=True, null=True)
    ncolreg = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    mdsc = models.CharField(max_length=500, blank=True, null=True)
    npreal = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npreald = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_med = models.CharField(max_length=3, blank=True, null=True)
    ccod_punit = models.CharField(max_length=3, blank=True, null=True)
    ncolact = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    numiadi = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_lote = models.CharField(max_length=20, blank=True, null=True)
    numiadi1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    cdctosuc = models.CharField(max_length=20, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    nvalorigv = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ncostod = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    nimpneto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_docp = models.CharField(max_length=2, blank=True, null=True)
    cserp = models.CharField(max_length=6, blank=True, null=True)
    cnump = models.CharField(max_length=13, blank=True, null=True)
    citem = models.CharField(max_length=2, blank=True, null=True)
    cid_dimen = models.CharField(max_length=8, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    nporig = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npreori = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    mcaptura = models.CharField(max_length=500, blank=True, null=True)
    asoc = models.CharField(max_length=12, blank=True, null=True)
    ncosteado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncosteadod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncostc = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccorre40 = models.CharField(max_length=6, blank=True, null=True)
    cctaini = models.CharField(max_length=10, blank=True, null=True)
    nrecalc = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ctipaigv = models.CharField(max_length=2, blank=True, null=True)
    ctiprec = models.CharField(max_length=2, blank=True, null=True)
    nmonref = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cotrotri = models.CharField(max_length=4, blank=True, null=True)
    cdato3 = models.CharField(max_length=25, blank=True, null=True)
    cdatanti = models.CharField(max_length=27, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_comprasl'


class PcComxml(models.Model):
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nestado = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cresusunat = models.CharField(max_length=254, blank=True, null=True)
    crazsoc = models.CharField(max_length=100, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ctot = models.CharField(max_length=20, blank=True, null=True)
    mobser = models.CharField(max_length=500, blank=True, null=True)
    carchivo = models.CharField(max_length=254, blank=True, null=True)
    nfase = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_comxml'


class PcContome(models.Model):
    ccod_conto = models.CharField(max_length=8, blank=True, null=True)
    ccod_surt = models.CharField(max_length=3, blank=True, null=True)
    dfechai = models.DateField(blank=True, null=True)
    dfechas = models.DateField(blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    mdsc = models.CharField(max_length=500, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    nturno = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_contome'


class PcContomel(models.Model):
    ccod_conto = models.CharField(max_length=8, blank=True, null=True)
    nmanguera = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ninicio = models.DecimalField(max_digits=18, decimal_places=4, blank=True, null=True)
    nfin = models.DecimalField(max_digits=18, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_contomel'


class PcContomelm(models.Model):
    ccod_conto = models.CharField(max_length=8, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ninicio = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ncompras = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nventas = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nfin = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_contomelm'


class PcCostos(models.Model):
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)
    cdebe = models.CharField(max_length=10, blank=True, null=True)
    chaber = models.CharField(max_length=10, blank=True, null=True)
    lbloquea = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_costos'


class PcCostos2(models.Model):
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)
    cdebe = models.CharField(max_length=10, blank=True, null=True)
    chaber = models.CharField(max_length=10, blank=True, null=True)
    lbloquea = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_costos2'


class PcDatafact(models.Model):
    fecha1 = models.CharField(max_length=8, blank=True, null=True)
    fecha2 = models.CharField(max_length=8, blank=True, null=True)
    correla = models.DecimalField(max_digits=6, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_datafact'


class PcDoc(models.Model):
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)
    ccodc = models.CharField(max_length=3, blank=True, null=True)
    ccodv = models.CharField(max_length=3, blank=True, null=True)
    ccodcruc = models.CharField(max_length=3, blank=True, null=True)
    ndocint = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_doc'


class PcEqui(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    cmedida = models.CharField(max_length=15, blank=True, null=True)
    nfactor = models.DecimalField(max_digits=20, decimal_places=10, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    cbarras = models.CharField(max_length=15, blank=True, null=True)
    npeso = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    npu2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuniequi = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_med = models.CharField(max_length=3, blank=True, null=True)
    nporutil = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg1 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg2 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg3 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg4 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg5 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    npu6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nmarg6 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg7 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg8 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg9 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg10 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg11 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg12 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    npu13 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu14 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu15 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nmarg13 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg14 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg15 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nsalalma = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_equi'


class PcEstado(models.Model):
    ccod_esta = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_estado'


class PcFami(models.Model):
    cfami = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_fami'


class PcFamilias(models.Model):
    ccod_fami = models.CharField(max_length=5, blank=True, null=True)
    cdsc = models.CharField(max_length=60, blank=True, null=True)
    cnombre = models.CharField(max_length=30, blank=True, null=True)
    nfila = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    ncolumna = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    nfiltop = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    nfilbot = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    ncolizq = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    ncolder = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    cdatos1 = models.CharField(max_length=60, blank=True, null=True)
    cdatos2 = models.CharField(max_length=60, blank=True, null=True)
    cdatos3 = models.CharField(max_length=60, blank=True, null=True)
    cdatos4 = models.CharField(max_length=60, blank=True, null=True)
    cdatos5 = models.CharField(max_length=60, blank=True, null=True)
    ccompras = models.CharField(max_length=10, blank=True, null=True)
    cventas = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_familias'


class PcFeiuser(models.Model):
    ccod_emp = models.CharField(max_length=5, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_feiuser'


class PcKarval(models.Model):
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=8, blank=True, null=True)
    npud = models.DecimalField(max_digits=15, decimal_places=8, blank=True, null=True)
    ntots = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_karval'


class PcKarvalma(models.Model):
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=8, blank=True, null=True)
    npud = models.DecimalField(max_digits=15, decimal_places=8, blank=True, null=True)
    ntots = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_karvalma'


class PcKarvalmas(models.Model):
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_almas = models.CharField(max_length=4, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=8, blank=True, null=True)
    npud = models.DecimalField(max_digits=15, decimal_places=8, blank=True, null=True)
    ntots = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_karvalmas'


class PcKit(models.Model):
    ccod_prod = models.CharField(max_length=12, blank=True, null=True)
    rccod_prod = models.CharField(max_length=12, blank=True, null=True)
    cant = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    np_u = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    np_uc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cbarras = models.CharField(max_length=15, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_kit'


class PcLetpag(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    cnumcota = models.CharField(max_length=2, blank=True, null=True)
    famorti = models.DateField(blank=True, null=True)
    nmonto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ninteres = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    notros = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv = models.DecimalField(max_digits=8, decimal_places=4, blank=True, null=True)
    nmonigv = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    fpago = models.DateField(blank=True, null=True)
    ntpcam = models.DecimalField(max_digits=8, decimal_places=4, blank=True, null=True)
    nmonprov = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndifcam = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndepre = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntpcadq = models.DecimalField(max_digits=8, decimal_places=4, blank=True, null=True)
    naños = models.DecimalField(db_column='naÑos', max_digits=2, decimal_places=0, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pc_letpag'


class PcLinea(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_linea'


class PcLocal(models.Model):
    ccod_local = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_local'


class PcLote(models.Model):
    ccod_lote = models.CharField(max_length=20, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ffechafab = models.DateField(blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    cdes1 = models.CharField(max_length=100, blank=True, null=True)
    cdes2 = models.CharField(max_length=100, blank=True, null=True)
    cdes3 = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_lote'


class PcLotel(models.Model):
    ccod_lote = models.CharField(max_length=20, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    ning = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsaldo = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_lotel'


class PcLoteld(models.Model):
    cid_dimen = models.CharField(max_length=8, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_lote = models.CharField(max_length=26, blank=True, null=True)
    citem = models.CharField(max_length=2, blank=True, null=True)
    nuniequi = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ncanti = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nmed1 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmed2 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmed3 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nporcen = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_loteld'


class PcMarca(models.Model):
    ccod_marca = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_marca'


class PcMinus(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    nina = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnet = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimp = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lcc = models.BooleanField(blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)
    cguia = models.CharField(max_length=20, blank=True, null=True)
    mmem = models.CharField(max_length=500, blank=True, null=True)
    lconta = models.BooleanField(blank=True, null=True)
    lexp = models.BooleanField(blank=True, null=True)
    lreg = models.BooleanField(blank=True, null=True)
    lneg = models.BooleanField(blank=True, null=True)
    lstock = models.BooleanField(blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ninad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnetd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    lalmacen = models.BooleanField(blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    ccod_alma2 = models.CharField(max_length=3, blank=True, null=True)
    cid_canj = models.CharField(max_length=8, blank=True, null=True)
    lcanje = models.BooleanField(blank=True, null=True)
    lguest = models.BooleanField(blank=True, null=True)
    ncajas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncajad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ffechareg = models.DateField(blank=True, null=True)
    ccod_inter = models.CharField(max_length=11, blank=True, null=True)
    ccod_trans = models.CharField(max_length=11, blank=True, null=True)
    ccod_moti = models.CharField(max_length=3, blank=True, null=True)
    ccod_tran = models.CharField(max_length=4, blank=True, null=True)
    ccod_pago = models.CharField(max_length=2, blank=True, null=True)
    lsolomone = models.BooleanField(blank=True, null=True)
    nbase2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nisc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    niscd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lresp = models.BooleanField(blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cserresp = models.CharField(max_length=6, blank=True, null=True)
    cnumresp = models.CharField(max_length=15, blank=True, null=True)
    fguiaresp = models.DateField(blank=True, null=True)
    cserguia = models.CharField(max_length=6, blank=True, null=True)
    cnumguia = models.CharField(max_length=13, blank=True, null=True)
    nrets = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nretd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    fdocdet = models.DateField(blank=True, null=True)
    mmem2 = models.CharField(max_length=500, blank=True, null=True)
    ccod_conto = models.CharField(max_length=8, blank=True, null=True)
    cdoc1 = models.CharField(max_length=2, blank=True, null=True)
    cser1 = models.CharField(max_length=6, blank=True, null=True)
    cnum1 = models.CharField(max_length=13, blank=True, null=True)
    cruc1 = models.CharField(max_length=11, blank=True, null=True)
    cdoc2 = models.CharField(max_length=2, blank=True, null=True)
    cser2 = models.CharField(max_length=6, blank=True, null=True)
    cnum2 = models.CharField(max_length=13, blank=True, null=True)
    cruc2 = models.CharField(max_length=11, blank=True, null=True)
    cdoc3 = models.CharField(max_length=2, blank=True, null=True)
    cser3 = models.CharField(max_length=6, blank=True, null=True)
    cnum3 = models.CharField(max_length=13, blank=True, null=True)
    cruc3 = models.CharField(max_length=11, blank=True, null=True)
    ffec1 = models.DateField(blank=True, null=True)
    ffec2 = models.DateField(blank=True, null=True)
    ffec3 = models.DateField(blank=True, null=True)
    cplaca = models.CharField(max_length=10, blank=True, null=True)
    fcanje = models.DateField(blank=True, null=True)
    ccod_gar = models.CharField(max_length=11, blank=True, null=True)
    ccod_cons = models.CharField(max_length=11, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    lproceso = models.BooleanField(blank=True, null=True)
    lkardex = models.BooleanField(blank=True, null=True)
    ccod_alma3 = models.CharField(max_length=3, blank=True, null=True)
    cserext = models.CharField(max_length=20, blank=True, null=True)
    cnumext = models.CharField(max_length=20, blank=True, null=True)
    ncoddes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nnumdes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cnumdet = models.CharField(max_length=5, blank=True, null=True)
    cexttpo = models.CharField(max_length=2, blank=True, null=True)
    cextser = models.CharField(max_length=10, blank=True, null=True)
    cextnum = models.CharField(max_length=20, blank=True, null=True)
    fextfec = models.DateField(blank=True, null=True)
    nextbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nextigv = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cperser = models.CharField(max_length=4, blank=True, null=True)
    cpernum = models.CharField(max_length=10, blank=True, null=True)
    ntpcv = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nentregas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nentregad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltos = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    ncosteado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncosteadod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cref = models.CharField(max_length=1, blank=True, null=True)
    cmle = models.CharField(max_length=2, blank=True, null=True)
    cale = models.CharField(max_length=4, blank=True, null=True)
    ncorrecb = models.CharField(max_length=1, blank=True, null=True)
    cmle2 = models.CharField(max_length=2, blank=True, null=True)
    cale2 = models.CharField(max_length=4, blank=True, null=True)
    cdoc_ndom = models.CharField(max_length=20, blank=True, null=True)
    cdoc_refnc = models.CharField(max_length=20, blank=True, null=True)
    fpagimpret = models.DateField(blank=True, null=True)
    cdocmodi = models.CharField(max_length=2, blank=True, null=True)
    dua_num = models.CharField(max_length=6, blank=True, null=True)
    ffecdua = models.DateField(blank=True, null=True)
    t2008_11 = models.CharField(max_length=3, blank=True, null=True)
    dua_año = models.CharField(db_column='dua_aÑo', max_length=4, blank=True, null=True)  # Field name made lowercase.
    dua_fregu = models.DateField(blank=True, null=True)
    dua_valor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lperden = models.BooleanField(blank=True, null=True)
    nmonperbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    ccod_almas = models.CharField(max_length=4, blank=True, null=True)
    crefk = models.CharField(max_length=1, blank=True, null=True)
    cmlek = models.CharField(max_length=2, blank=True, null=True)
    calek = models.CharField(max_length=4, blank=True, null=True)
    crefb = models.CharField(max_length=1, blank=True, null=True)
    cmleb = models.CharField(max_length=2, blank=True, null=True)
    caleb = models.CharField(max_length=4, blank=True, null=True)
    ccod_oric = models.CharField(max_length=2, blank=True, null=True)
    nasientoc = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_clabs = models.CharField(max_length=1, blank=True, null=True)
    chash = models.CharField(max_length=40, blank=True, null=True)
    c_cat09 = models.CharField(max_length=2, blank=True, null=True)
    c_motinc = models.CharField(max_length=50, blank=True, null=True)
    ct_dcto = models.CharField(max_length=1, blank=True, null=True)
    c_mbaja = models.CharField(max_length=50, blank=True, null=True)
    ldrawback = models.BooleanField(blank=True, null=True)
    lanticipo = models.BooleanField(blank=True, null=True)
    nimppla = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpplad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_minus'


class PcMinusd(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccue = models.CharField(max_length=10, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npigv = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cmedida = models.CharField(max_length=15, blank=True, null=True)
    nuni2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu2 = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    nfactor = models.DecimalField(max_digits=20, decimal_places=10, blank=True, null=True)
    ncosto = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    npud = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    npu2d = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotald = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndesc = models.DecimalField(max_digits=15, decimal_places=3, blank=True, null=True)
    npordesc = models.DecimalField(max_digits=7, decimal_places=3, blank=True, null=True)
    limpuesto = models.BooleanField(blank=True, null=True)
    ccod_talla = models.CharField(max_length=3, blank=True, null=True)
    mdatos = models.CharField(max_length=500, blank=True, null=True)
    ncolreg = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    mdsc = models.CharField(max_length=500, blank=True, null=True)
    npreal = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npreald = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_med = models.CharField(max_length=3, blank=True, null=True)
    ccod_punit = models.CharField(max_length=3, blank=True, null=True)
    ncolact = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    numiadi = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_lote = models.CharField(max_length=20, blank=True, null=True)
    numiadi1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    cdctosuc = models.CharField(max_length=20, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    nvalorigv = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ncostod = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    nimpneto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_docp = models.CharField(max_length=2, blank=True, null=True)
    cserp = models.CharField(max_length=6, blank=True, null=True)
    cnump = models.CharField(max_length=13, blank=True, null=True)
    citem = models.CharField(max_length=2, blank=True, null=True)
    cid_dimen = models.CharField(max_length=8, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    nporig = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npreori = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    mcaptura = models.CharField(max_length=500, blank=True, null=True)
    asoc = models.CharField(max_length=12, blank=True, null=True)
    ncosteado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncosteadod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncostc = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccorre40 = models.CharField(max_length=6, blank=True, null=True)
    cctaini = models.CharField(max_length=10, blank=True, null=True)
    nrecalc = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ctipaigv = models.CharField(max_length=2, blank=True, null=True)
    ctiprec = models.CharField(max_length=2, blank=True, null=True)
    nmonref = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cotrotri = models.CharField(max_length=4, blank=True, null=True)
    cdato3 = models.CharField(max_length=25, blank=True, null=True)
    cdatanti = models.CharField(max_length=27, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_minusd'


class PcMotivo(models.Model):
    ccod_moti = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_motivo'


class PcMovi(models.Model):
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)
    ldisgrega = models.BooleanField(blank=True, null=True)
    lcv = models.BooleanField(blank=True, null=True)
    lexp = models.BooleanField(blank=True, null=True)
    ccontado = models.CharField(max_length=10, blank=True, null=True)
    ccredito = models.CharField(max_length=10, blank=True, null=True)
    nie = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lneg = models.BooleanField(blank=True, null=True)
    limp = models.BooleanField(blank=True, null=True)
    mpath = models.CharField(max_length=500, blank=True, null=True)
    lstock = models.BooleanField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    ctipo = models.CharField(max_length=1, blank=True, null=True)
    cdefault = models.CharField(max_length=10, blank=True, null=True)
    cdebe = models.CharField(max_length=10, blank=True, null=True)
    chaber = models.CharField(max_length=10, blank=True, null=True)
    nlineas = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    lnumero = models.BooleanField(blank=True, null=True)
    lalmacen = models.BooleanField(blank=True, null=True)
    lf11 = models.BooleanField(blank=True, null=True)
    lf12 = models.BooleanField(blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    lsolomone = models.BooleanField(blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    naumdis = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nmons = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lstockmsg = models.BooleanField(blank=True, null=True)
    liguales = models.BooleanField(blank=True, null=True)
    lnosalir = models.BooleanField(blank=True, null=True)
    lnoimp = models.BooleanField(blank=True, null=True)
    ningreso = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ndigprod = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ljalaresp = models.BooleanField(blank=True, null=True)
    lgenera = models.BooleanField(blank=True, null=True)
    cgenera = models.CharField(max_length=5, blank=True, null=True)
    lcalcigv = models.BooleanField(blank=True, null=True)
    lprecond = models.BooleanField(blank=True, null=True)
    lcamprec = models.BooleanField(blank=True, null=True)
    lrubro = models.BooleanField(blank=True, null=True)
    nrubro = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    lcontome = models.BooleanField(blank=True, null=True)
    lnoexp = models.BooleanField(blank=True, null=True)
    lhacerdet = models.BooleanField(blank=True, null=True)
    ccuedet = models.CharField(max_length=10, blank=True, null=True)
    lautonum = models.BooleanField(blank=True, null=True)
    lvalstk = models.BooleanField(blank=True, null=True)
    lblopre = models.BooleanField(blank=True, null=True)
    npreciob = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lvalpre = models.BooleanField(blank=True, null=True)
    lcostea = models.BooleanField(blank=True, null=True)
    lnumven = models.BooleanField(blank=True, null=True)
    lkardex = models.BooleanField(blank=True, null=True)
    nactcol = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lpago = models.BooleanField(blank=True, null=True)
    t2008_8 = models.CharField(max_length=2, blank=True, null=True)
    ntopsunat = models.CharField(max_length=2, blank=True, null=True)
    cuedebe = models.CharField(max_length=10, blank=True, null=True)
    cuehaber = models.CharField(max_length=10, blank=True, null=True)
    cmovtran = models.CharField(max_length=5, blank=True, null=True)
    nmovpresu = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lpreres = models.BooleanField(blank=True, null=True)
    lpreimp = models.BooleanField(blank=True, null=True)
    lpreact = models.BooleanField(blank=True, null=True)
    cprodresp = models.CharField(max_length=12, blank=True, null=True)
    lverexp = models.BooleanField(blank=True, null=True)
    msecuen = models.CharField(max_length=500, blank=True, null=True)
    lporuser = models.BooleanField(blank=True, null=True)
    cprodex = models.CharField(max_length=12, blank=True, null=True)
    ligvvalor = models.BooleanField(blank=True, null=True)
    lpcli = models.BooleanField(blank=True, null=True)
    lctactea = models.BooleanField(blank=True, null=True)
    lactalte = models.BooleanField(blank=True, null=True)
    lactalma = models.BooleanField(blank=True, null=True)
    lcoskar = models.BooleanField(blank=True, null=True)
    lvercalt = models.BooleanField(blank=True, null=True)
    cctaalt = models.CharField(max_length=10, blank=True, null=True)
    nporage = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    lusufin = models.BooleanField(blank=True, null=True)
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    cdebeini = models.CharField(max_length=10, blank=True, null=True)
    chaberini = models.CharField(max_length=10, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    lnomodi = models.BooleanField(blank=True, null=True)
    lfei = models.BooleanField(blank=True, null=True)
    lsolofei = models.BooleanField(blank=True, null=True)
    lfeb = models.BooleanField(blank=True, null=True)
    lvuelto = models.BooleanField(blank=True, null=True)
    lmjenum = models.BooleanField(blank=True, null=True)
    lreinicia = models.BooleanField(blank=True, null=True)
    mdraw = models.CharField(max_length=500, blank=True, null=True)
    nanticip = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_movi'


class PcMovip(models.Model):
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)
    ldisgrega = models.BooleanField(blank=True, null=True)
    lcv = models.BooleanField(blank=True, null=True)
    lexp = models.BooleanField(blank=True, null=True)
    ccontado = models.CharField(max_length=10, blank=True, null=True)
    ccredito = models.CharField(max_length=10, blank=True, null=True)
    nie = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lneg = models.BooleanField(blank=True, null=True)
    limp = models.BooleanField(blank=True, null=True)
    mpath = models.CharField(max_length=500, blank=True, null=True)
    lstock = models.BooleanField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    ctipo = models.CharField(max_length=1, blank=True, null=True)
    cdefault = models.CharField(max_length=10, blank=True, null=True)
    cdebe = models.CharField(max_length=10, blank=True, null=True)
    chaber = models.CharField(max_length=10, blank=True, null=True)
    nlineas = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    lnumero = models.BooleanField(blank=True, null=True)
    lalmacen = models.BooleanField(blank=True, null=True)
    lf11 = models.BooleanField(blank=True, null=True)
    lf12 = models.BooleanField(blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    lsolomone = models.BooleanField(blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    naumdis = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nmons = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lstockmsg = models.BooleanField(blank=True, null=True)
    liguales = models.BooleanField(blank=True, null=True)
    lnosalir = models.BooleanField(blank=True, null=True)
    lnoimp = models.BooleanField(blank=True, null=True)
    ningreso = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ndigprod = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ljalaresp = models.BooleanField(blank=True, null=True)
    lgenera = models.BooleanField(blank=True, null=True)
    cgenera = models.CharField(max_length=5, blank=True, null=True)
    lcalcigv = models.BooleanField(blank=True, null=True)
    lprecond = models.BooleanField(blank=True, null=True)
    lcamprec = models.BooleanField(blank=True, null=True)
    lrubro = models.BooleanField(blank=True, null=True)
    nrubro = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    lcontome = models.BooleanField(blank=True, null=True)
    lnoexp = models.BooleanField(blank=True, null=True)
    lhacerdet = models.BooleanField(blank=True, null=True)
    ccuedet = models.CharField(max_length=10, blank=True, null=True)
    lautonum = models.BooleanField(blank=True, null=True)
    lvalstk = models.BooleanField(blank=True, null=True)
    lblopre = models.BooleanField(blank=True, null=True)
    npreciob = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lvalpre = models.BooleanField(blank=True, null=True)
    lcostea = models.BooleanField(blank=True, null=True)
    lnumven = models.BooleanField(blank=True, null=True)
    lkardex = models.BooleanField(blank=True, null=True)
    nactcol = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lpago = models.BooleanField(blank=True, null=True)
    t2008_8 = models.CharField(max_length=2, blank=True, null=True)
    ntopsunat = models.CharField(max_length=2, blank=True, null=True)
    cuedebe = models.CharField(max_length=10, blank=True, null=True)
    cuehaber = models.CharField(max_length=10, blank=True, null=True)
    cmovtran = models.CharField(max_length=5, blank=True, null=True)
    nmovpresu = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lpreres = models.BooleanField(blank=True, null=True)
    lpreimp = models.BooleanField(blank=True, null=True)
    lpreact = models.BooleanField(blank=True, null=True)
    cprodresp = models.CharField(max_length=12, blank=True, null=True)
    lverexp = models.BooleanField(blank=True, null=True)
    msecuen = models.CharField(max_length=500, blank=True, null=True)
    cmovsal = models.CharField(max_length=5, blank=True, null=True)
    cmovsal1 = models.CharField(max_length=5, blank=True, null=True)
    cprodex = models.CharField(max_length=12, blank=True, null=True)
    ligvvalor = models.BooleanField(blank=True, null=True)
    malmac = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_movip'


class PcMoviuser(models.Model):
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    lblopre = models.BooleanField(blank=True, null=True)
    lblopreme = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_moviuser'


class PcNivel(models.Model):
    ccod_ni = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)
    nmontos = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmontod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndias = models.DecimalField(max_digits=4, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_nivel'


class PcOcupa(models.Model):
    ccod_ocupa = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_ocupa'


class PcPago(models.Model):
    ccod_pago = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)
    ccompras = models.CharField(max_length=10, blank=True, null=True)
    lcomproc = models.BooleanField(blank=True, null=True)
    cventas = models.CharField(max_length=10, blank=True, null=True)
    lcomprov = models.BooleanField(blank=True, null=True)
    ndias = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_pago'


class PcPedidos(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    nina = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnet = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimp = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lcc = models.BooleanField(blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)
    cguia = models.CharField(max_length=20, blank=True, null=True)
    mmem = models.CharField(max_length=500, blank=True, null=True)
    lconta = models.BooleanField(blank=True, null=True)
    lexp = models.BooleanField(blank=True, null=True)
    lreg = models.BooleanField(blank=True, null=True)
    lneg = models.BooleanField(blank=True, null=True)
    lstock = models.BooleanField(blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ninad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnetd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    lalmacen = models.BooleanField(blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    ccod_alma2 = models.CharField(max_length=3, blank=True, null=True)
    cid_canj = models.CharField(max_length=8, blank=True, null=True)
    lcanje = models.BooleanField(blank=True, null=True)
    lguest = models.BooleanField(blank=True, null=True)
    ncajas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncajad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ffechareg = models.DateField(blank=True, null=True)
    ccod_inter = models.CharField(max_length=11, blank=True, null=True)
    ccod_trans = models.CharField(max_length=11, blank=True, null=True)
    ccod_moti = models.CharField(max_length=3, blank=True, null=True)
    ccod_tran = models.CharField(max_length=4, blank=True, null=True)
    ccod_pago = models.CharField(max_length=2, blank=True, null=True)
    lsolomone = models.BooleanField(blank=True, null=True)
    nbase2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nisc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    niscd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lresp = models.BooleanField(blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cserresp = models.CharField(max_length=6, blank=True, null=True)
    cnumresp = models.CharField(max_length=15, blank=True, null=True)
    fguiaresp = models.DateField(blank=True, null=True)
    cserguia = models.CharField(max_length=6, blank=True, null=True)
    cnumguia = models.CharField(max_length=13, blank=True, null=True)
    nrets = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nretd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    fdocdet = models.DateField(blank=True, null=True)
    mmem2 = models.CharField(max_length=500, blank=True, null=True)
    ccod_conto = models.CharField(max_length=8, blank=True, null=True)
    cdoc1 = models.CharField(max_length=2, blank=True, null=True)
    cser1 = models.CharField(max_length=6, blank=True, null=True)
    cnum1 = models.CharField(max_length=13, blank=True, null=True)
    cruc1 = models.CharField(max_length=11, blank=True, null=True)
    cdoc2 = models.CharField(max_length=2, blank=True, null=True)
    cser2 = models.CharField(max_length=6, blank=True, null=True)
    cnum2 = models.CharField(max_length=13, blank=True, null=True)
    cruc2 = models.CharField(max_length=11, blank=True, null=True)
    cdoc3 = models.CharField(max_length=2, blank=True, null=True)
    cser3 = models.CharField(max_length=6, blank=True, null=True)
    cnum3 = models.CharField(max_length=13, blank=True, null=True)
    cruc3 = models.CharField(max_length=11, blank=True, null=True)
    ffec1 = models.DateField(blank=True, null=True)
    ffec2 = models.DateField(blank=True, null=True)
    ffec3 = models.DateField(blank=True, null=True)
    cplaca = models.CharField(max_length=10, blank=True, null=True)
    fcanje = models.DateField(blank=True, null=True)
    ccod_gar = models.CharField(max_length=11, blank=True, null=True)
    ccod_cons = models.CharField(max_length=11, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    lproceso = models.BooleanField(blank=True, null=True)
    lkardex = models.BooleanField(blank=True, null=True)
    ccod_alma3 = models.CharField(max_length=3, blank=True, null=True)
    cserext = models.CharField(max_length=20, blank=True, null=True)
    cnumext = models.CharField(max_length=20, blank=True, null=True)
    ncoddes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nnumdes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cnumdet = models.CharField(max_length=5, blank=True, null=True)
    cexttpo = models.CharField(max_length=2, blank=True, null=True)
    cextser = models.CharField(max_length=10, blank=True, null=True)
    cextnum = models.CharField(max_length=20, blank=True, null=True)
    fextfec = models.DateField(blank=True, null=True)
    nextbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nextigv = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cperser = models.CharField(max_length=4, blank=True, null=True)
    cpernum = models.CharField(max_length=10, blank=True, null=True)
    ntpcv = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nentregas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nentregad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltos = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    cestado = models.CharField(max_length=1, blank=True, null=True)
    cmovsal = models.CharField(max_length=5, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    lmar = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_pedidos'


class PcPedidosl(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccue = models.CharField(max_length=10, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npigv = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cmedida = models.CharField(max_length=15, blank=True, null=True)
    nuni2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu2 = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    nfactor = models.DecimalField(max_digits=20, decimal_places=10, blank=True, null=True)
    ncosto = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    npud = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    npu2d = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotald = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndesc = models.DecimalField(max_digits=15, decimal_places=3, blank=True, null=True)
    npordesc = models.DecimalField(max_digits=7, decimal_places=3, blank=True, null=True)
    limpuesto = models.BooleanField(blank=True, null=True)
    ccod_talla = models.CharField(max_length=3, blank=True, null=True)
    mdatos = models.CharField(max_length=500, blank=True, null=True)
    ncolreg = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    mdsc = models.CharField(max_length=500, blank=True, null=True)
    npreal = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npreald = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_med = models.CharField(max_length=3, blank=True, null=True)
    ccod_punit = models.CharField(max_length=3, blank=True, null=True)
    ncolact = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    numiadi = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_lote = models.CharField(max_length=20, blank=True, null=True)
    numiadi1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    cdctosuc = models.CharField(max_length=20, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    nvalorigv = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ncostod = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nimpneto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    citem = models.CharField(max_length=2, blank=True, null=True)
    naprob = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nejecu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nstock = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    cid_dimen = models.CharField(max_length=8, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    ntotp = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotpd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nfacturar = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    asoc = models.CharField(max_length=12, blank=True, null=True)
    ncosteado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncosteadod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncostc = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_pedidosl'


class PcPlus(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    nina = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnet = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimp = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lcc = models.BooleanField(blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)
    cguia = models.CharField(max_length=20, blank=True, null=True)
    mmem = models.CharField(max_length=500, blank=True, null=True)
    lconta = models.BooleanField(blank=True, null=True)
    lexp = models.BooleanField(blank=True, null=True)
    lreg = models.BooleanField(blank=True, null=True)
    lneg = models.BooleanField(blank=True, null=True)
    lstock = models.BooleanField(blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ninad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnetd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    lalmacen = models.BooleanField(blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    ccod_alma2 = models.CharField(max_length=3, blank=True, null=True)
    cid_canj = models.CharField(max_length=8, blank=True, null=True)
    lcanje = models.BooleanField(blank=True, null=True)
    lguest = models.BooleanField(blank=True, null=True)
    ncajas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncajad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ffechareg = models.DateField(blank=True, null=True)
    ccod_inter = models.CharField(max_length=11, blank=True, null=True)
    ccod_trans = models.CharField(max_length=11, blank=True, null=True)
    ccod_moti = models.CharField(max_length=3, blank=True, null=True)
    ccod_tran = models.CharField(max_length=4, blank=True, null=True)
    ccod_pago = models.CharField(max_length=2, blank=True, null=True)
    lsolomone = models.BooleanField(blank=True, null=True)
    nbase2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nisc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    niscd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lresp = models.BooleanField(blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cserresp = models.CharField(max_length=6, blank=True, null=True)
    cnumresp = models.CharField(max_length=15, blank=True, null=True)
    fguiaresp = models.DateField(blank=True, null=True)
    cserguia = models.CharField(max_length=6, blank=True, null=True)
    cnumguia = models.CharField(max_length=13, blank=True, null=True)
    nrets = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nretd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    fdocdet = models.DateField(blank=True, null=True)
    mmem2 = models.CharField(max_length=500, blank=True, null=True)
    ccod_conto = models.CharField(max_length=8, blank=True, null=True)
    cdoc1 = models.CharField(max_length=2, blank=True, null=True)
    cser1 = models.CharField(max_length=6, blank=True, null=True)
    cnum1 = models.CharField(max_length=13, blank=True, null=True)
    cruc1 = models.CharField(max_length=11, blank=True, null=True)
    cdoc2 = models.CharField(max_length=2, blank=True, null=True)
    cser2 = models.CharField(max_length=6, blank=True, null=True)
    cnum2 = models.CharField(max_length=13, blank=True, null=True)
    cruc2 = models.CharField(max_length=11, blank=True, null=True)
    cdoc3 = models.CharField(max_length=2, blank=True, null=True)
    cser3 = models.CharField(max_length=6, blank=True, null=True)
    cnum3 = models.CharField(max_length=13, blank=True, null=True)
    cruc3 = models.CharField(max_length=11, blank=True, null=True)
    ffec1 = models.DateField(blank=True, null=True)
    ffec2 = models.DateField(blank=True, null=True)
    ffec3 = models.DateField(blank=True, null=True)
    cplaca = models.CharField(max_length=10, blank=True, null=True)
    fcanje = models.DateField(blank=True, null=True)
    ccod_gar = models.CharField(max_length=11, blank=True, null=True)
    ccod_cons = models.CharField(max_length=11, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    lproceso = models.BooleanField(blank=True, null=True)
    lkardex = models.BooleanField(blank=True, null=True)
    ccod_alma3 = models.CharField(max_length=3, blank=True, null=True)
    cserext = models.CharField(max_length=20, blank=True, null=True)
    cnumext = models.CharField(max_length=20, blank=True, null=True)
    ncoddes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nnumdes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cnumdet = models.CharField(max_length=5, blank=True, null=True)
    cexttpo = models.CharField(max_length=2, blank=True, null=True)
    cextser = models.CharField(max_length=10, blank=True, null=True)
    cextnum = models.CharField(max_length=20, blank=True, null=True)
    fextfec = models.DateField(blank=True, null=True)
    nextbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nextigv = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cperser = models.CharField(max_length=4, blank=True, null=True)
    cpernum = models.CharField(max_length=10, blank=True, null=True)
    ntpcv = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nentregas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nentregad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltos = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    ncosteado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncosteadod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cref = models.CharField(max_length=1, blank=True, null=True)
    cmle = models.CharField(max_length=2, blank=True, null=True)
    cale = models.CharField(max_length=4, blank=True, null=True)
    ncorrecb = models.CharField(max_length=1, blank=True, null=True)
    cmle2 = models.CharField(max_length=2, blank=True, null=True)
    cale2 = models.CharField(max_length=4, blank=True, null=True)
    cdoc_ndom = models.CharField(max_length=20, blank=True, null=True)
    cdoc_refnc = models.CharField(max_length=20, blank=True, null=True)
    fpagimpret = models.DateField(blank=True, null=True)
    cdocmodi = models.CharField(max_length=2, blank=True, null=True)
    dua_num = models.CharField(max_length=6, blank=True, null=True)
    ffecdua = models.DateField(blank=True, null=True)
    t2008_11 = models.CharField(max_length=3, blank=True, null=True)
    dua_año = models.CharField(db_column='dua_aÑo', max_length=4, blank=True, null=True)  # Field name made lowercase.
    dua_fregu = models.DateField(blank=True, null=True)
    dua_valor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lperden = models.BooleanField(blank=True, null=True)
    nmonperbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    ccod_almas = models.CharField(max_length=4, blank=True, null=True)
    crefk = models.CharField(max_length=1, blank=True, null=True)
    cmlek = models.CharField(max_length=2, blank=True, null=True)
    calek = models.CharField(max_length=4, blank=True, null=True)
    crefb = models.CharField(max_length=1, blank=True, null=True)
    cmleb = models.CharField(max_length=2, blank=True, null=True)
    caleb = models.CharField(max_length=4, blank=True, null=True)
    ccod_oric = models.CharField(max_length=2, blank=True, null=True)
    nasientoc = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_clabs = models.CharField(max_length=1, blank=True, null=True)
    chash = models.CharField(max_length=40, blank=True, null=True)
    c_cat09 = models.CharField(max_length=2, blank=True, null=True)
    c_motinc = models.CharField(max_length=50, blank=True, null=True)
    ct_dcto = models.CharField(max_length=1, blank=True, null=True)
    c_mbaja = models.CharField(max_length=50, blank=True, null=True)
    ldrawback = models.BooleanField(blank=True, null=True)
    lanticipo = models.BooleanField(blank=True, null=True)
    nimppla = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpplad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_plus'


class PcPlusd(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccue = models.CharField(max_length=10, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npigv = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cmedida = models.CharField(max_length=15, blank=True, null=True)
    nuni2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu2 = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    nfactor = models.DecimalField(max_digits=20, decimal_places=10, blank=True, null=True)
    ncosto = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    npud = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    npu2d = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotald = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndesc = models.DecimalField(max_digits=15, decimal_places=3, blank=True, null=True)
    npordesc = models.DecimalField(max_digits=7, decimal_places=3, blank=True, null=True)
    limpuesto = models.BooleanField(blank=True, null=True)
    ccod_talla = models.CharField(max_length=3, blank=True, null=True)
    mdatos = models.CharField(max_length=500, blank=True, null=True)
    ncolreg = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    mdsc = models.CharField(max_length=500, blank=True, null=True)
    npreal = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npreald = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_med = models.CharField(max_length=3, blank=True, null=True)
    ccod_punit = models.CharField(max_length=3, blank=True, null=True)
    ncolact = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    numiadi = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_lote = models.CharField(max_length=20, blank=True, null=True)
    numiadi1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    cdctosuc = models.CharField(max_length=20, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    nvalorigv = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ncostod = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    nimpneto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_docp = models.CharField(max_length=2, blank=True, null=True)
    cserp = models.CharField(max_length=6, blank=True, null=True)
    cnump = models.CharField(max_length=13, blank=True, null=True)
    citem = models.CharField(max_length=2, blank=True, null=True)
    cid_dimen = models.CharField(max_length=8, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    nporig = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npreori = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    mcaptura = models.CharField(max_length=500, blank=True, null=True)
    asoc = models.CharField(max_length=12, blank=True, null=True)
    ncosteado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncosteadod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncostc = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccorre40 = models.CharField(max_length=6, blank=True, null=True)
    cctaini = models.CharField(max_length=10, blank=True, null=True)
    nrecalc = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ctipaigv = models.CharField(max_length=2, blank=True, null=True)
    ctiprec = models.CharField(max_length=2, blank=True, null=True)
    nmonref = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cotrotri = models.CharField(max_length=4, blank=True, null=True)
    cdato3 = models.CharField(max_length=25, blank=True, null=True)
    cdatanti = models.CharField(max_length=27, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_plusd'


class PcPostal(models.Model):
    ccod_pos = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_postal'


class PcPrecio(models.Model):
    ccod_punit = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)
    mformula = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_precio'


class PcPreciol(models.Model):
    ccod_punit = models.CharField(max_length=3, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_med = models.CharField(max_length=3, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ncomis = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    npu_c = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud_c = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_preciol'


class PcProceh(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    ccod_proc = models.CharField(max_length=8, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    ccod_esta = models.CharField(max_length=2, blank=True, null=True)
    cprod = models.CharField(max_length=60, blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_proceh'


class PcProcehl(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    cprio = models.CharField(max_length=2, blank=True, null=True)
    ctipo = models.CharField(max_length=1, blank=True, null=True)
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_procehl'


class PcProceso(models.Model):
    ccod_proc = models.CharField(max_length=8, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)
    ctipo = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_proceso'


class PcProcesol(models.Model):
    ccod_proc = models.CharField(max_length=8, blank=True, null=True)
    cprio = models.CharField(max_length=2, blank=True, null=True)
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)
    mfile = models.CharField(max_length=500, blank=True, null=True)
    nx = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ny = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_procesol'


class PcProd(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)
    cmedida = models.CharField(max_length=15, blank=True, null=True)
    np_u = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nstock = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsaluni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsalpu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    notros_i = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    notros_s = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nlpu = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    nlu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccompras = models.CharField(max_length=10, blank=True, null=True)
    cventas = models.CharField(max_length=10, blank=True, null=True)
    ldetalle = models.BooleanField(blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    npud1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsalpud = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nminimo = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nmaximo = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    cbarras = models.CharField(max_length=15, blank=True, null=True)
    limpuesto = models.BooleanField(blank=True, null=True)
    npeso = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    np_u2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ltallas = models.BooleanField(blank=True, null=True)
    cdebe = models.CharField(max_length=10, blank=True, null=True)
    chaber = models.CharField(max_length=10, blank=True, null=True)
    mfile = models.CharField(max_length=500, blank=True, null=True)
    mdsc = models.CharField(max_length=500, blank=True, null=True)
    nvarini = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nvarfin = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    lgasrel = models.BooleanField(blank=True, null=True)
    lserie = models.BooleanField(blank=True, null=True)
    ncolcom = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    ncolven = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    ndetpor = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nperpor = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nretpor = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nrenpor = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ndetmon = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npermon = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nretmon = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nrenmon = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ldet = models.BooleanField(blank=True, null=True)
    lper = models.BooleanField(blank=True, null=True)
    lret = models.BooleanField(blank=True, null=True)
    lren = models.BooleanField(blank=True, null=True)
    nporcom = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    np_u5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    fultc = models.DateField(blank=True, null=True)
    fultv = models.DateField(blank=True, null=True)
    cdocc = models.CharField(max_length=2, blank=True, null=True)
    cserc = models.CharField(max_length=6, blank=True, null=True)
    cnumc = models.CharField(max_length=13, blank=True, null=True)
    cdocv = models.CharField(max_length=2, blank=True, null=True)
    cserv = models.CharField(max_length=6, blank=True, null=True)
    cnumv = models.CharField(max_length=13, blank=True, null=True)
    crucc = models.CharField(max_length=11, blank=True, null=True)
    crucv = models.CharField(max_length=11, blank=True, null=True)
    nmesgaran = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    ccod_marca = models.CharField(max_length=4, blank=True, null=True)
    lboni = models.BooleanField(blank=True, null=True)
    cnomgen = models.CharField(max_length=60, blank=True, null=True)
    lmanprec = models.BooleanField(blank=True, null=True)
    cfabrica = models.CharField(max_length=60, blank=True, null=True)
    nporutil = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    lprecdol = models.BooleanField(blank=True, null=True)
    nmarg1 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg2 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg3 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg4 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg5 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    ntexsunat = models.CharField(max_length=2, blank=True, null=True)
    ncumsunat = models.CharField(max_length=3, blank=True, null=True)
    llote = models.BooleanField(blank=True, null=True)
    npedido = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u13 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u14 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    np_u15 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nmarg6 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg7 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg8 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg9 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg10 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg11 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg12 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg13 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg14 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    nmarg15 = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    ldimen = models.BooleanField(blank=True, null=True)
    cformu = models.CharField(max_length=25, blank=True, null=True)
    linhab = models.BooleanField(blank=True, null=True)
    cdebec = models.CharField(max_length=10, blank=True, null=True)
    chaberc = models.CharField(max_length=10, blank=True, null=True)
    lver = models.BooleanField(blank=True, null=True)
    nlpud = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    nsalmac = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    lkest = models.BooleanField(blank=True, null=True)
    lktip = models.BooleanField(blank=True, null=True)
    lmetvalide = models.BooleanField(blank=True, null=True)
    ccod_proca = models.CharField(max_length=16, blank=True, null=True)
    ccod_busq = models.CharField(max_length=50, blank=True, null=True)
    lanticip = models.BooleanField(blank=True, null=True)
    ccod_unsp = models.CharField(max_length=8, blank=True, null=True)
    limpbolsa = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_prod'


class PcProd2(models.Model):
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    nsaluni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsalpu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ning12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsal12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    notros_i = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    notros_s = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nlpu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nlu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccompras = models.CharField(max_length=10, blank=True, null=True)
    cventas = models.CharField(max_length=10, blank=True, null=True)
    ldetalle = models.BooleanField(blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    npud1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npud12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsalpud = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nminimo = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nmaximo = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa3 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa4 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa5 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa6 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa7 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa8 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa9 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa10 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa11 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nia12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nsa12 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_prod2'


class PcProdPro(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccodigo = models.CharField(max_length=15, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_prod_pro'


class PcProdcat(models.Model):
    ccod_proca = models.CharField(max_length=16, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_prodcat'


class PcProdresu(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    npcosto = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    cdoc1 = models.CharField(max_length=2, blank=True, null=True)
    cser1 = models.CharField(max_length=6, blank=True, null=True)
    cnum1 = models.CharField(max_length=13, blank=True, null=True)
    ccod_cli1 = models.CharField(max_length=11, blank=True, null=True)
    ffecha1 = models.DateField(blank=True, null=True)
    ntc1 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_prodresu'


class PcProdsus(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_sus = models.CharField(max_length=12, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_prodsus'


class PcProfesion(models.Model):
    ccod_profe = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_profesion'


class PcRelacion(models.Model):
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ctipo = models.CharField(max_length=1, blank=True, null=True)
    cdoc1 = models.CharField(max_length=2, blank=True, null=True)
    cser1 = models.CharField(max_length=6, blank=True, null=True)
    cnum1 = models.CharField(max_length=13, blank=True, null=True)
    ccli1 = models.CharField(max_length=11, blank=True, null=True)
    cdoc2 = models.CharField(max_length=2, blank=True, null=True)
    cser2 = models.CharField(max_length=6, blank=True, null=True)
    cnum2 = models.CharField(max_length=13, blank=True, null=True)
    ccli2 = models.CharField(max_length=11, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_relacion'


class PcRuta(models.Model):
    ccod_ruta = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_ruta'


class PcSector(models.Model):
    ccod_sec = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_sector'


class PcSeries(models.Model):
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    nnum = models.DecimalField(max_digits=13, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_series'


class PcSerprod(models.Model):
    cid_com = models.CharField(max_length=8, blank=True, null=True)
    cid_ven = models.CharField(max_length=8, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    cserie = models.CharField(max_length=30, blank=True, null=True)
    lestado = models.BooleanField(blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    cnumero = models.CharField(max_length=15, blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    cid_comneg = models.CharField(max_length=8, blank=True, null=True)
    cid_ped = models.CharField(max_length=8, blank=True, null=True)
    mcaptura = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_serprod'


class PcSunat05(models.Model):
    ccod_t05 = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_sunat05'


class PcSunat06(models.Model):
    ccod_t06 = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_sunat06'


class PcSunat12(models.Model):
    ccodt12 = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=42, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_sunat12'


class PcSurtidor(models.Model):
    ccod_surt = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)
    ccod_conto = models.CharField(max_length=8, blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    lotros = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_surtidor'


class PcSurtidorl(models.Model):
    ccod_surt = models.CharField(max_length=3, blank=True, null=True)
    nmanguera = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_surtidorl'


class PcTabdetra(models.Model):
    ccod_detra = models.CharField(max_length=5, blank=True, null=True)
    nporcen = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cdsc = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_tabdetra'


class PcTallas(models.Model):
    ccod_talla = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_tallas'


class PcTipo(models.Model):
    ccod_tip = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_tipo'


class PcTipod(models.Model):
    ccod_tipd = models.CharField(max_length=1, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_tipod'


class PcTrabajo(models.Model):
    ccod_tra = models.CharField(max_length=5, blank=True, null=True)
    cnom_tra = models.CharField(max_length=80, blank=True, null=True)
    ccod_ocupa = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_trabajo'


class PcTrans(models.Model):
    ccod_tran = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)
    ctlic = models.CharField(max_length=20, blank=True, null=True)
    ctins = models.CharField(max_length=20, blank=True, null=True)
    ctmar = models.CharField(max_length=20, blank=True, null=True)
    ctplaca = models.CharField(max_length=20, blank=True, null=True)
    ctconf = models.CharField(max_length=20, blank=True, null=True)
    ctruc = models.CharField(max_length=11, blank=True, null=True)
    ctdsc = models.CharField(max_length=30, blank=True, null=True)
    cttipo = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_trans'


class PcUbicalma(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    cubicacion = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_ubicalma'


class PcUbigeo(models.Model):
    ccod_ubig = models.CharField(max_length=6, blank=True, null=True)
    cdsc = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_ubigeo'


class PcUserkey(models.Model):
    campo01 = models.CharField(max_length=4, blank=True, null=True)
    campo02 = models.CharField(max_length=10, blank=True, null=True)
    campo03 = models.CharField(max_length=500, blank=True, null=True)
    campo04 = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_userkey'


class PcUseruser(models.Model):
    campo01 = models.CharField(max_length=4, blank=True, null=True)
    campo02 = models.CharField(max_length=4, blank=True, null=True)
    campo03 = models.CharField(max_length=500, blank=True, null=True)
    campo04 = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_useruser'


class PcVendedor(models.Model):
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)
    nmonto1 = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    npor1 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nmonto2 = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    npor2 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nmonto3 = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    npor3 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nsueldo = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    nporc1 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nporc2 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nporc3 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_vendedor'


class PcVeninfo(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    fini = models.DateField(blank=True, null=True)
    cptipo = models.CharField(max_length=20, blank=True, null=True)
    cpnom = models.CharField(max_length=20, blank=True, null=True)
    cpnum = models.CharField(max_length=20, blank=True, null=True)
    cpint = models.CharField(max_length=20, blank=True, null=True)
    cpzona = models.CharField(max_length=40, blank=True, null=True)
    cpdis = models.CharField(max_length=40, blank=True, null=True)
    cpprov = models.CharField(max_length=40, blank=True, null=True)
    cpdep = models.CharField(max_length=20, blank=True, null=True)
    cltipo = models.CharField(max_length=20, blank=True, null=True)
    clnom = models.CharField(max_length=20, blank=True, null=True)
    clnum = models.CharField(max_length=20, blank=True, null=True)
    clint = models.CharField(max_length=20, blank=True, null=True)
    clzona = models.CharField(max_length=20, blank=True, null=True)
    cldis = models.CharField(max_length=20, blank=True, null=True)
    clprov = models.CharField(max_length=20, blank=True, null=True)
    cldep = models.CharField(max_length=20, blank=True, null=True)
    ctlic = models.CharField(max_length=20, blank=True, null=True)
    ctmar = models.CharField(max_length=20, blank=True, null=True)
    ctplaca = models.CharField(max_length=20, blank=True, null=True)
    ctconf = models.CharField(max_length=20, blank=True, null=True)
    ctins = models.CharField(max_length=20, blank=True, null=True)
    csruc = models.CharField(max_length=11, blank=True, null=True)
    csdsc = models.CharField(max_length=80, blank=True, null=True)
    ccomp = models.CharField(max_length=2, blank=True, null=True)
    ccnum = models.CharField(max_length=20, blank=True, null=True)
    ctruc = models.CharField(max_length=11, blank=True, null=True)
    ctdsc = models.CharField(max_length=80, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    cnomdesti = models.CharField(max_length=60, blank=True, null=True)
    ccod_ubi1 = models.CharField(max_length=6, blank=True, null=True)
    ccod_ubi2 = models.CharField(max_length=6, blank=True, null=True)
    npeso = models.DecimalField(max_digits=12, decimal_places=3, blank=True, null=True)
    cdnicond = models.CharField(max_length=8, blank=True, null=True)
    cplasecu = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_veninfo'


class PcVentas(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    nina = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnet = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimp = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lcc = models.BooleanField(blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)
    cguia = models.CharField(max_length=20, blank=True, null=True)
    mmem = models.CharField(max_length=500, blank=True, null=True)
    lconta = models.BooleanField(blank=True, null=True)
    lexp = models.BooleanField(blank=True, null=True)
    lreg = models.BooleanField(blank=True, null=True)
    lneg = models.BooleanField(blank=True, null=True)
    lstock = models.BooleanField(blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ninad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnetd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    ccod_movi = models.CharField(max_length=5, blank=True, null=True)
    lalmacen = models.BooleanField(blank=True, null=True)
    ccod_alma = models.CharField(max_length=3, blank=True, null=True)
    ccod_alma2 = models.CharField(max_length=3, blank=True, null=True)
    cid_canj = models.CharField(max_length=8, blank=True, null=True)
    lcanje = models.BooleanField(blank=True, null=True)
    lguest = models.BooleanField(blank=True, null=True)
    ncajas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncajad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ffechareg = models.DateField(blank=True, null=True)
    ccod_inter = models.CharField(max_length=11, blank=True, null=True)
    ccod_trans = models.CharField(max_length=11, blank=True, null=True)
    ccod_moti = models.CharField(max_length=3, blank=True, null=True)
    ccod_tran = models.CharField(max_length=4, blank=True, null=True)
    ccod_pago = models.CharField(max_length=2, blank=True, null=True)
    lsolomone = models.BooleanField(blank=True, null=True)
    nbase2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nisc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    niscd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lresp = models.BooleanField(blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cserresp = models.CharField(max_length=6, blank=True, null=True)
    cnumresp = models.CharField(max_length=15, blank=True, null=True)
    fguiaresp = models.DateField(blank=True, null=True)
    cserguia = models.CharField(max_length=6, blank=True, null=True)
    cnumguia = models.CharField(max_length=13, blank=True, null=True)
    nrets = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nretd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    fdocdet = models.DateField(blank=True, null=True)
    mmem2 = models.CharField(max_length=500, blank=True, null=True)
    ccod_conto = models.CharField(max_length=8, blank=True, null=True)
    cdoc1 = models.CharField(max_length=2, blank=True, null=True)
    cser1 = models.CharField(max_length=6, blank=True, null=True)
    cnum1 = models.CharField(max_length=13, blank=True, null=True)
    cruc1 = models.CharField(max_length=11, blank=True, null=True)
    cdoc2 = models.CharField(max_length=2, blank=True, null=True)
    cser2 = models.CharField(max_length=6, blank=True, null=True)
    cnum2 = models.CharField(max_length=13, blank=True, null=True)
    cruc2 = models.CharField(max_length=11, blank=True, null=True)
    cdoc3 = models.CharField(max_length=2, blank=True, null=True)
    cser3 = models.CharField(max_length=6, blank=True, null=True)
    cnum3 = models.CharField(max_length=13, blank=True, null=True)
    cruc3 = models.CharField(max_length=11, blank=True, null=True)
    ffec1 = models.DateField(blank=True, null=True)
    ffec2 = models.DateField(blank=True, null=True)
    ffec3 = models.DateField(blank=True, null=True)
    cplaca = models.CharField(max_length=10, blank=True, null=True)
    fcanje = models.DateField(blank=True, null=True)
    ccod_gar = models.CharField(max_length=11, blank=True, null=True)
    ccod_cons = models.CharField(max_length=11, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    lproceso = models.BooleanField(blank=True, null=True)
    lkardex = models.BooleanField(blank=True, null=True)
    ccod_alma3 = models.CharField(max_length=3, blank=True, null=True)
    cserext = models.CharField(max_length=20, blank=True, null=True)
    cnumext = models.CharField(max_length=20, blank=True, null=True)
    ncoddes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nnumdes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cnumdet = models.CharField(max_length=5, blank=True, null=True)
    cexttpo = models.CharField(max_length=2, blank=True, null=True)
    cextser = models.CharField(max_length=10, blank=True, null=True)
    cextnum = models.CharField(max_length=20, blank=True, null=True)
    fextfec = models.DateField(blank=True, null=True)
    nextbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nextigv = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cperser = models.CharField(max_length=4, blank=True, null=True)
    cpernum = models.CharField(max_length=10, blank=True, null=True)
    ntpcv = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nentregas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nentregad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltos = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvueltod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    ncosteado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncosteadod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cref = models.CharField(max_length=1, blank=True, null=True)
    cmle = models.CharField(max_length=2, blank=True, null=True)
    cale = models.CharField(max_length=4, blank=True, null=True)
    ncorrecb = models.CharField(max_length=1, blank=True, null=True)
    cmle2 = models.CharField(max_length=2, blank=True, null=True)
    cale2 = models.CharField(max_length=4, blank=True, null=True)
    cdoc_ndom = models.CharField(max_length=20, blank=True, null=True)
    cdoc_refnc = models.CharField(max_length=20, blank=True, null=True)
    fpagimpret = models.DateField(blank=True, null=True)
    cdocmodi = models.CharField(max_length=2, blank=True, null=True)
    dua_num = models.CharField(max_length=6, blank=True, null=True)
    ffecdua = models.DateField(blank=True, null=True)
    t2008_11 = models.CharField(max_length=3, blank=True, null=True)
    dua_año = models.CharField(db_column='dua_aÑo', max_length=4, blank=True, null=True)  # Field name made lowercase.
    dua_fregu = models.DateField(blank=True, null=True)
    dua_valor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lperden = models.BooleanField(blank=True, null=True)
    nmonperbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    ccod_almas = models.CharField(max_length=4, blank=True, null=True)
    crefk = models.CharField(max_length=1, blank=True, null=True)
    cmlek = models.CharField(max_length=2, blank=True, null=True)
    calek = models.CharField(max_length=4, blank=True, null=True)
    crefb = models.CharField(max_length=1, blank=True, null=True)
    cmleb = models.CharField(max_length=2, blank=True, null=True)
    caleb = models.CharField(max_length=4, blank=True, null=True)
    ccod_oric = models.CharField(max_length=2, blank=True, null=True)
    nasientoc = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_clabs = models.CharField(max_length=1, blank=True, null=True)
    chash = models.CharField(max_length=40, blank=True, null=True)
    c_cat09 = models.CharField(max_length=2, blank=True, null=True)
    c_motinc = models.CharField(max_length=50, blank=True, null=True)
    ct_dcto = models.CharField(max_length=1, blank=True, null=True)
    c_mbaja = models.CharField(max_length=50, blank=True, null=True)
    ldrawback = models.BooleanField(blank=True, null=True)
    lanticipo = models.BooleanField(blank=True, null=True)
    nimppla = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpplad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_ventas'


class PcVentasl(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccue = models.CharField(max_length=10, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npigv = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cmedida = models.CharField(max_length=15, blank=True, null=True)
    nuni2 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu2 = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    nfactor = models.DecimalField(max_digits=20, decimal_places=10, blank=True, null=True)
    ncosto = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    npud = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    npu2d = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ntotald = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndesc = models.DecimalField(max_digits=15, decimal_places=3, blank=True, null=True)
    npordesc = models.DecimalField(max_digits=7, decimal_places=3, blank=True, null=True)
    limpuesto = models.BooleanField(blank=True, null=True)
    ccod_talla = models.CharField(max_length=3, blank=True, null=True)
    mdatos = models.CharField(max_length=500, blank=True, null=True)
    ncolreg = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    mdsc = models.CharField(max_length=500, blank=True, null=True)
    npreal = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npreald = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_med = models.CharField(max_length=3, blank=True, null=True)
    ccod_punit = models.CharField(max_length=3, blank=True, null=True)
    ncolact = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    numiadi = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccod_lote = models.CharField(max_length=20, blank=True, null=True)
    numiadi1 = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    cdctosuc = models.CharField(max_length=20, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    nvalorigv = models.DecimalField(max_digits=20, decimal_places=8, blank=True, null=True)
    ncostod = models.DecimalField(max_digits=19, decimal_places=8, blank=True, null=True)
    nimpneto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_docp = models.CharField(max_length=2, blank=True, null=True)
    cserp = models.CharField(max_length=6, blank=True, null=True)
    cnump = models.CharField(max_length=13, blank=True, null=True)
    citem = models.CharField(max_length=2, blank=True, null=True)
    cid_dimen = models.CharField(max_length=8, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    nporig = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npreori = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    mcaptura = models.CharField(max_length=500, blank=True, null=True)
    asoc = models.CharField(max_length=12, blank=True, null=True)
    ncosteado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncosteadod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncostc = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ccorre40 = models.CharField(max_length=6, blank=True, null=True)
    cctaini = models.CharField(max_length=10, blank=True, null=True)
    nrecalc = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ctipaigv = models.CharField(max_length=2, blank=True, null=True)
    ctiprec = models.CharField(max_length=2, blank=True, null=True)
    nmonref = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cotrotri = models.CharField(max_length=4, blank=True, null=True)
    cdato3 = models.CharField(max_length=25, blank=True, null=True)
    cdatanti = models.CharField(max_length=27, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_ventasl'


class PcZonas(models.Model):
    ccod_ubig = models.CharField(max_length=6, blank=True, null=True)
    ccod_zona = models.CharField(max_length=10, blank=True, null=True)
    cdsc = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pc_zonas'


class PgActivo(models.Model):
    ccod_acti = models.CharField(max_length=10, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)
    ffecha_ad = models.DateField(blank=True, null=True)
    lestado = models.BooleanField(blank=True, null=True)
    ffecha_ba = models.DateField(blank=True, null=True)
    cubicacion = models.CharField(max_length=30, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    nmeses = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    ntasadep = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nvalor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nresidual = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndepre = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    ccue_dep = models.CharField(max_length=10, blank=True, null=True)
    ccue_gas = models.CharField(max_length=10, blank=True, null=True)
    ndep1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndep12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    najus1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    najus2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ldepre = models.BooleanField(blank=True, null=True)
    mdsc = models.CharField(max_length=500, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_activo'


class PgAdicion(models.Model):
    ncod = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    cdsc = models.CharField(max_length=200, blank=True, null=True)
    npot1 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cdetalle = models.CharField(max_length=2, blank=True, null=True)
    nextraer = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nfiltrado = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncontable = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncalculo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntributa = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nadicion = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npot2 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_adicion'


class PgAsicab(models.Model):
    ccod_asi = models.CharField(max_length=5, blank=True, null=True)
    cid = models.CharField(max_length=8, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)
    cdebe = models.CharField(max_length=10, blank=True, null=True)
    chaber = models.CharField(max_length=10, blank=True, null=True)
    lrango = models.BooleanField(blank=True, null=True)
    nubic = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lpedir = models.BooleanField(blank=True, null=True)
    nitem = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    cformula = models.CharField(max_length=250, blank=True, null=True)
    lcancela = models.BooleanField(blank=True, null=True)
    lcaja = models.BooleanField(blank=True, null=True)
    ntipocam = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ldifcam = models.BooleanField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cnumero = models.CharField(max_length=12, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cglosa = models.CharField(max_length=30, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    lautoinc = models.BooleanField(blank=True, null=True)
    cserinc = models.CharField(max_length=6, blank=True, null=True)
    cdocinc = models.CharField(max_length=2, blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    ldespliega = models.BooleanField(blank=True, null=True)
    cdatregesp = models.CharField(max_length=5, blank=True, null=True)
    cdatdoc = models.CharField(max_length=5, blank=True, null=True)
    lresp = models.BooleanField(blank=True, null=True)
    lcobesp = models.BooleanField(blank=True, null=True)
    cdatcosto = models.CharField(max_length=5, blank=True, null=True)
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    ccod_camp1 = models.CharField(max_length=3, blank=True, null=True)
    ccod_camp2 = models.CharField(max_length=3, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    lmostrar = models.BooleanField(blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    ccod_clabs = models.CharField(max_length=1, blank=True, null=True)
    fformulario = models.CharField(max_length=500, blank=True, null=True)
    usuario = models.CharField(max_length=11, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_asicab'


class PgAsidet(models.Model):
    cid = models.CharField(max_length=8, blank=True, null=True)
    npor = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    ccod_asi = models.CharField(max_length=5, blank=True, null=True)
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cformula = models.CharField(max_length=250, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_asidet'


class PgAsiento(models.Model):
    ccod_asi = models.CharField(max_length=5, blank=True, null=True)
    cdsc = models.CharField(max_length=60, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    cglosa = models.CharField(max_length=30, blank=True, null=True)
    cregis = models.CharField(max_length=1, blank=True, null=True)
    limp = models.BooleanField(blank=True, null=True)
    ccod_rep = models.CharField(max_length=5, blank=True, null=True)
    mdsc = models.CharField(max_length=500, blank=True, null=True)
    cdesenc = models.CharField(max_length=5, blank=True, null=True)
    t2008_8 = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_asiento'


class PgAsimodel(models.Model):
    cmes = models.CharField(max_length=2, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    ndebe = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cglosa = models.CharField(max_length=60, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    cregis = models.CharField(max_length=1, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    cnumero = models.CharField(max_length=20, blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    nina = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnet = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimp = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)
    ffechareg = models.DateField(blank=True, null=True)
    cmesc = models.CharField(max_length=2, blank=True, null=True)
    cdestino = models.CharField(max_length=3, blank=True, null=True)
    cdes = models.CharField(max_length=1, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ndebed = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ninad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnetd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cmreg = models.CharField(max_length=1, blank=True, null=True)
    lcomp = models.BooleanField(blank=True, null=True)
    ntcc = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    ncajas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncajad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lajusdif = models.BooleanField(blank=True, null=True)
    lcaja = models.BooleanField(blank=True, null=True)
    cid = models.CharField(max_length=10, blank=True, null=True)
    ccod_asi = models.CharField(max_length=5, blank=True, null=True)
    ccadpdt = models.CharField(max_length=60, blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    nbase2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nisc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    niscd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lresp = models.BooleanField(blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cserresp = models.CharField(max_length=6, blank=True, null=True)
    cnumresp = models.CharField(max_length=15, blank=True, null=True)
    fguiaresp = models.DateField(blank=True, null=True)
    cserguia = models.CharField(max_length=6, blank=True, null=True)
    cnumguia = models.CharField(max_length=13, blank=True, null=True)
    nrets = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nretd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cdocf = models.CharField(max_length=2, blank=True, null=True)
    cnumf = models.CharField(max_length=20, blank=True, null=True)
    ffecf = models.DateField(blank=True, null=True)
    fdocdet = models.DateField(blank=True, null=True)
    cdoc_ndom = models.CharField(max_length=20, blank=True, null=True)
    cdoc_refnc = models.CharField(max_length=20, blank=True, null=True)
    fpagimpret = models.DateField(blank=True, null=True)
    fpagimport = models.DateField(blank=True, null=True)
    ldiferido = models.BooleanField(blank=True, null=True)
    ffechadif = models.DateField(blank=True, null=True)
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    ccod_camp1 = models.CharField(max_length=3, blank=True, null=True)
    ccod_camp2 = models.CharField(max_length=3, blank=True, null=True)
    t2008_11 = models.CharField(max_length=3, blank=True, null=True)
    ffecdua = models.DateField(blank=True, null=True)
    t2008_8 = models.CharField(max_length=2, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    cdocmodi = models.CharField(max_length=2, blank=True, null=True)
    cserext = models.CharField(max_length=20, blank=True, null=True)
    cnumext = models.CharField(max_length=20, blank=True, null=True)
    ncoddes = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nnumdes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cnumdet = models.CharField(max_length=8, blank=True, null=True)
    cexttpo = models.CharField(max_length=2, blank=True, null=True)
    cextser = models.CharField(max_length=10, blank=True, null=True)
    cextnum = models.CharField(max_length=20, blank=True, null=True)
    fextfec = models.DateField(blank=True, null=True)
    nextbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nextigv = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cperser = models.CharField(max_length=4, blank=True, null=True)
    cpernum = models.CharField(max_length=15, blank=True, null=True)
    ntpcv = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    dua_año = models.CharField(db_column='dua_aÑo', max_length=4, blank=True, null=True)  # Field name made lowercase.
    dua_num = models.CharField(max_length=6, blank=True, null=True)
    dua_fregu = models.DateField(blank=True, null=True)
    dua_valor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    cdocc = models.CharField(max_length=2, blank=True, null=True)
    cnumc = models.CharField(max_length=20, blank=True, null=True)
    ccod_user1 = models.CharField(max_length=4, blank=True, null=True)
    ffechareg1 = models.DateField(blank=True, null=True)
    nestado = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    cmesple = models.CharField(max_length=1, blank=True, null=True)
    coriple = models.CharField(max_length=2, blank=True, null=True)
    nasiple = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccodruc = models.CharField(max_length=11, blank=True, null=True)
    npigv = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cglosa2 = models.CharField(max_length=60, blank=True, null=True)
    cregpdb = models.CharField(max_length=2, blank=True, null=True)
    ctpcv = models.CharField(max_length=1, blank=True, null=True)
    ccoddes = models.CharField(max_length=1, blank=True, null=True)
    cnumdes = models.CharField(max_length=1, blank=True, null=True)
    cinddet = models.CharField(max_length=1, blank=True, null=True)
    ccoddet = models.CharField(max_length=5, blank=True, null=True)
    cindret = models.CharField(max_length=1, blank=True, null=True)
    cperind = models.CharField(max_length=1, blank=True, null=True)
    cpercod = models.CharField(max_length=2, blank=True, null=True)
    cref = models.CharField(max_length=1, blank=True, null=True)
    cmle = models.CharField(max_length=2, blank=True, null=True)
    cale = models.CharField(max_length=4, blank=True, null=True)
    ncorrelat = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    cnumero2 = models.CharField(max_length=13, blank=True, null=True)
    ncorrecb = models.CharField(max_length=1, blank=True, null=True)
    cmle2 = models.CharField(max_length=2, blank=True, null=True)
    cale2 = models.CharField(max_length=4, blank=True, null=True)
    lperden = models.BooleanField(blank=True, null=True)
    nmonperbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccorre40 = models.CharField(max_length=6, blank=True, null=True)
    nrentanet = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpreten = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndeduc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntasreten = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    ccod_renta = models.CharField(max_length=2, blank=True, null=True)
    ccod_mone = models.CharField(max_length=3, blank=True, null=True)
    ntcamsnd = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_asimodel'


class PgAsiuser(models.Model):
    ccod_asi = models.CharField(max_length=5, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)
    lblotc = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_asiuser'


class PgCampat1(models.Model):
    ccod_camp = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)
    ccod_csev = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_campat1'


class PgCampat2(models.Model):
    ccod_camp = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)
    ccod_csev = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_campat2'


class PgCatesfin(models.Model):
    ccod_catef = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_catesfin'


class PgCieCaja(models.Model):
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    nmonto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmontod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_cie_caja'


class PgCierre(models.Model):
    ffecha = models.DateField(blank=True, null=True)
    lcierre = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_cierre'


class PgClabieser(models.Model):
    ccod_clabs = models.CharField(max_length=1, blank=True, null=True)
    cdsc = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_clabieser'


class PgCliPro(models.Model):
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    crazon = models.CharField(max_length=100, blank=True, null=True)
    cdir = models.CharField(max_length=150, blank=True, null=True)
    ctel = models.CharField(max_length=20, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nsaldo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nsaldod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_tip = models.CharField(max_length=3, blank=True, null=True)
    nlimites = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nlimited = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cpaterno = models.CharField(max_length=20, blank=True, null=True)
    cmaterno = models.CharField(max_length=20, blank=True, null=True)
    cnombre1 = models.CharField(max_length=20, blank=True, null=True)
    cnombre2 = models.CharField(max_length=20, blank=True, null=True)
    nnatjur = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    ntipodoc = models.CharField(max_length=1, blank=True, null=True)
    lbuenc = models.BooleanField(blank=True, null=True)
    lagret = models.BooleanField(blank=True, null=True)
    lagper = models.BooleanField(blank=True, null=True)
    lmoroso = models.BooleanField(blank=True, null=True)
    nlimiadi = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nlimiadid = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lanulado = models.BooleanField(blank=True, null=True)
    lhabi = models.BooleanField(blank=True, null=True)
    lnohabi = models.BooleanField(blank=True, null=True)
    lnohalla = models.BooleanField(blank=True, null=True)
    ccod_pais = models.CharField(max_length=4, blank=True, null=True)
    ccod_conve = models.CharField(max_length=2, blank=True, null=True)
    cncomer = models.CharField(max_length=80, blank=True, null=True)
    npenlims = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npenlimd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_cli_pro'


class PgCliProno(models.Model):
    ccod_ite = models.CharField(max_length=10, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_con = models.CharField(max_length=2, blank=True, null=True)
    ffec_ini = models.DateField(blank=True, null=True)
    ffec_fin = models.DateField(blank=True, null=True)
    ccod_mes = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_cli_prono'


class PgComncxml(models.Model):
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    crazsoc = models.CharField(max_length=100, blank=True, null=True)
    mobser = models.CharField(max_length=500, blank=True, null=True)
    carchivo = models.CharField(max_length=254, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_comncxml'


class PgComxml(models.Model):
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    cnum = models.CharField(max_length=13, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nestado = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cresusunat = models.CharField(max_length=254, blank=True, null=True)
    crazsoc = models.CharField(max_length=100, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ctot = models.CharField(max_length=20, blank=True, null=True)
    mobser = models.CharField(max_length=500, blank=True, null=True)
    carchivo = models.CharField(max_length=254, blank=True, null=True)
    nfase = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_comxml'


class PgConsisple(models.Model):
    canio = models.CharField(max_length=4, blank=True, null=True)
    ccod = models.CharField(max_length=6, blank=True, null=True)
    cdsc = models.CharField(max_length=254, blank=True, null=True)
    nmes01 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes02 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes03 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes04 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes05 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes06 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes07 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes08 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes09 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes10 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes11 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nmes12 = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    mobser = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_consisple'


class PgContacto(models.Model):
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    mobs_emp = models.CharField(max_length=500, blank=True, null=True)
    ccod_con = models.CharField(max_length=2, blank=True, null=True)
    cdes_con = models.CharField(max_length=50, blank=True, null=True)
    ccar_con = models.CharField(max_length=50, blank=True, null=True)
    ctel_con = models.CharField(max_length=80, blank=True, null=True)
    cmai_con = models.CharField(max_length=80, blank=True, null=True)
    mobs_con = models.CharField(max_length=500, blank=True, null=True)
    ldefecto = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_contacto'


class PgConveniodt(models.Model):
    ccod_conve = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_conveniodt'


class PgCostos(models.Model):
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)
    cdebe = models.CharField(max_length=10, blank=True, null=True)
    chaber = models.CharField(max_length=10, blank=True, null=True)
    lbloquea = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_costos'


class PgCostos2(models.Model):
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)
    cdebe = models.CharField(max_length=10, blank=True, null=True)
    chaber = models.CharField(max_length=10, blank=True, null=True)
    lbloquea = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_costos2'


class PgCtabanco(models.Model):
    citecod = models.CharField(max_length=3, blank=True, null=True)
    nempcli = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ccod_ac = models.CharField(max_length=2, blank=True, null=True)
    cctacod = models.CharField(max_length=3, blank=True, null=True)
    cctanum = models.CharField(max_length=30, blank=True, null=True)
    nctamon = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cempcod = models.CharField(max_length=10, blank=True, null=True)
    cempdt1 = models.CharField(max_length=10, blank=True, null=True)
    cempdt2 = models.CharField(max_length=10, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    cclidt1 = models.CharField(max_length=10, blank=True, null=True)
    ffecini = models.DateField(blank=True, null=True)
    ffecfin = models.DateField(blank=True, null=True)
    cbanage = models.CharField(max_length=10, blank=True, null=True)
    nbanoks = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nctaoks = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_ctabanco'


class PgCtrlple(models.Model):
    ncod = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    cdsc = models.CharField(max_length=200, blank=True, null=True)
    l1 = models.BooleanField(blank=True, null=True)
    l2 = models.BooleanField(blank=True, null=True)
    l3 = models.BooleanField(blank=True, null=True)
    l4 = models.BooleanField(blank=True, null=True)
    l5 = models.BooleanField(blank=True, null=True)
    l6 = models.BooleanField(blank=True, null=True)
    l7 = models.BooleanField(blank=True, null=True)
    l8 = models.BooleanField(blank=True, null=True)
    l9 = models.BooleanField(blank=True, null=True)
    l10 = models.BooleanField(blank=True, null=True)
    l11 = models.BooleanField(blank=True, null=True)
    l12 = models.BooleanField(blank=True, null=True)
    cmesple30 = models.CharField(max_length=2, blank=True, null=True)
    cperple30 = models.CharField(max_length=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_ctrlple'


class PgDiario(models.Model):
    cmes = models.CharField(max_length=2, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    ndebe = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cglosa = models.CharField(max_length=60, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    cregis = models.CharField(max_length=1, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    cnumero = models.CharField(max_length=20, blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    nina = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnet = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimp = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntot = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_user = models.CharField(max_length=4, blank=True, null=True)
    ffechareg = models.DateField(blank=True, null=True)
    cmesc = models.CharField(max_length=2, blank=True, null=True)
    cdestino = models.CharField(max_length=3, blank=True, null=True)
    cdes = models.CharField(max_length=1, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    ntc = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ndebed = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ninad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nexod = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nnetd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cmreg = models.CharField(max_length=1, blank=True, null=True)
    lcomp = models.BooleanField(blank=True, null=True)
    ntcc = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    ncajas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncajad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lajusdif = models.BooleanField(blank=True, null=True)
    lcaja = models.BooleanField(blank=True, null=True)
    cid = models.CharField(max_length=10, blank=True, null=True)
    ccod_asi = models.CharField(max_length=5, blank=True, null=True)
    ccadpdt = models.CharField(max_length=60, blank=True, null=True)
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    nbase2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nisc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nbase3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nigv3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    niscd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    lresp = models.BooleanField(blank=True, null=True)
    nresp = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nporresp = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cserresp = models.CharField(max_length=6, blank=True, null=True)
    cnumresp = models.CharField(max_length=15, blank=True, null=True)
    fguiaresp = models.DateField(blank=True, null=True)
    cserguia = models.CharField(max_length=6, blank=True, null=True)
    cnumguia = models.CharField(max_length=13, blank=True, null=True)
    nrets = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nretd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cdocf = models.CharField(max_length=2, blank=True, null=True)
    cnumf = models.CharField(max_length=20, blank=True, null=True)
    ffecf = models.DateField(blank=True, null=True)
    fdocdet = models.DateField(blank=True, null=True)
    cdoc_ndom = models.CharField(max_length=20, blank=True, null=True)
    cdoc_refnc = models.CharField(max_length=20, blank=True, null=True)
    fpagimpret = models.DateField(blank=True, null=True)
    fpagimport = models.DateField(blank=True, null=True)
    ldiferido = models.BooleanField(blank=True, null=True)
    ffechadif = models.DateField(blank=True, null=True)
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    ccod_camp1 = models.CharField(max_length=3, blank=True, null=True)
    ccod_camp2 = models.CharField(max_length=3, blank=True, null=True)
    t2008_11 = models.CharField(max_length=3, blank=True, null=True)
    ffecdua = models.DateField(blank=True, null=True)
    t2008_8 = models.CharField(max_length=2, blank=True, null=True)
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    cdocmodi = models.CharField(max_length=2, blank=True, null=True)
    cserext = models.CharField(max_length=20, blank=True, null=True)
    cnumext = models.CharField(max_length=20, blank=True, null=True)
    ncoddes = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    nnumdes = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cnumdet = models.CharField(max_length=8, blank=True, null=True)
    cexttpo = models.CharField(max_length=2, blank=True, null=True)
    cextser = models.CharField(max_length=10, blank=True, null=True)
    cextnum = models.CharField(max_length=20, blank=True, null=True)
    fextfec = models.DateField(blank=True, null=True)
    nextbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nextigv = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cperser = models.CharField(max_length=4, blank=True, null=True)
    cpernum = models.CharField(max_length=15, blank=True, null=True)
    ntpcv = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    dua_año = models.CharField(db_column='dua_aÑo', max_length=4, blank=True, null=True)  # Field name made lowercase.
    dua_num = models.CharField(max_length=6, blank=True, null=True)
    dua_fregu = models.DateField(blank=True, null=True)
    dua_valor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    cdocc = models.CharField(max_length=2, blank=True, null=True)
    cnumc = models.CharField(max_length=20, blank=True, null=True)
    ccod_user1 = models.CharField(max_length=4, blank=True, null=True)
    ffechareg1 = models.DateField(blank=True, null=True)
    nestado = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ccod_cos2 = models.CharField(max_length=9, blank=True, null=True)
    cmesple = models.CharField(max_length=1, blank=True, null=True)
    coriple = models.CharField(max_length=2, blank=True, null=True)
    nasiple = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccodruc = models.CharField(max_length=11, blank=True, null=True)
    npigv = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cglosa2 = models.CharField(max_length=60, blank=True, null=True)
    cregpdb = models.CharField(max_length=2, blank=True, null=True)
    ctpcv = models.CharField(max_length=1, blank=True, null=True)
    ccoddes = models.CharField(max_length=1, blank=True, null=True)
    cnumdes = models.CharField(max_length=1, blank=True, null=True)
    cinddet = models.CharField(max_length=1, blank=True, null=True)
    ccoddet = models.CharField(max_length=5, blank=True, null=True)
    cindret = models.CharField(max_length=1, blank=True, null=True)
    cperind = models.CharField(max_length=1, blank=True, null=True)
    cpercod = models.CharField(max_length=2, blank=True, null=True)
    cref = models.CharField(max_length=1, blank=True, null=True)
    cmle = models.CharField(max_length=2, blank=True, null=True)
    cale = models.CharField(max_length=4, blank=True, null=True)
    ncorrelat = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    cnumero2 = models.CharField(max_length=13, blank=True, null=True)
    ncorrecb = models.CharField(max_length=1, blank=True, null=True)
    cmle2 = models.CharField(max_length=2, blank=True, null=True)
    cale2 = models.CharField(max_length=4, blank=True, null=True)
    lperden = models.BooleanField(blank=True, null=True)
    nmonperbas = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccorre40 = models.CharField(max_length=6, blank=True, null=True)
    nrentanet = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimpreten = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndeduc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntasreten = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    ccod_renta = models.CharField(max_length=2, blank=True, null=True)
    ccod_mone = models.CharField(max_length=3, blank=True, null=True)
    ntcamsnd = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True)
    mrutafac = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_diario'


class PgDifdbf(models.Model):
    cdsc = models.CharField(max_length=200, blank=True, null=True)
    mes = models.CharField(max_length=3, blank=True, null=True)
    asiento = models.CharField(max_length=8, blank=True, null=True)
    origen = models.CharField(max_length=2, blank=True, null=True)
    moneda = models.CharField(max_length=10, blank=True, null=True)
    asiento2 = models.CharField(max_length=8, blank=True, null=True)
    tipo = models.CharField(max_length=1, blank=True, null=True)
    cuenta = models.CharField(max_length=10, blank=True, null=True)
    diferen = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    tipocam = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_difdbf'


class PgDoc(models.Model):
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=252, blank=True, null=True)
    ligvc = models.BooleanField(blank=True, null=True)
    ligvv = models.BooleanField(blank=True, null=True)
    lcv = models.BooleanField(blank=True, null=True)
    lexp = models.BooleanField(blank=True, null=True)
    ccom_habc = models.CharField(max_length=10, blank=True, null=True)
    ccom_habr = models.CharField(max_length=10, blank=True, null=True)
    cven_debc = models.CharField(max_length=10, blank=True, null=True)
    cven_debr = models.CharField(max_length=10, blank=True, null=True)
    cven_hab = models.CharField(max_length=10, blank=True, null=True)
    nie = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    lneg = models.BooleanField(blank=True, null=True)
    limp = models.BooleanField(blank=True, null=True)
    nser = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    nnum = models.DecimalField(max_digits=7, decimal_places=0, blank=True, null=True)
    mpath = models.CharField(max_length=500, blank=True, null=True)
    ndocint = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_doc'


class PgEnfinan(models.Model):
    ccod_enfin = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)
    mcuenta = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_enfinan'


class PgFactor(models.Model):
    nfac0 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac1 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac2 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac3 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac4 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac5 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac6 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac7 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac8 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac9 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac10 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac11 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    nfac12 = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    cori_dep = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_factor'


class PgFlujo(models.Model):
    ccod_flujo = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)
    ndebe0 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber0 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe13 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber13 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe14 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber14 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe15 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber15 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed0 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd0 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed13 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd13 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed14 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd14 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed15 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd15 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cformula = models.CharField(max_length=100, blank=True, null=True)
    ccod_csev = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_flujo'


class PgFormato(models.Model):
    ccod_bal = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=55, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cformula = models.CharField(max_length=100, blank=True, null=True)
    nimporte = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimported = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    notas = models.CharField(max_length=20, blank=True, null=True)
    cnotac1 = models.DecimalField(max_digits=20, decimal_places=0, blank=True, null=True)
    cnotac2 = models.DecimalField(max_digits=20, decimal_places=0, blank=True, null=True)
    ccod_csev = models.CharField(max_length=6, blank=True, null=True)
    lconfig = models.BooleanField(blank=True, null=True)
    ccodpdt = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_formato'


class PgFormato2(models.Model):
    ccod_bal = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=55, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    cformula = models.CharField(max_length=100, blank=True, null=True)
    nimporte = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nimported = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    notas = models.CharField(max_length=20, blank=True, null=True)
    cnotac1 = models.DecimalField(max_digits=20, decimal_places=0, blank=True, null=True)
    cnotac2 = models.DecimalField(max_digits=20, decimal_places=0, blank=True, null=True)
    ccod_csev = models.CharField(max_length=6, blank=True, null=True)
    lconfig = models.BooleanField(blank=True, null=True)
    ccodpdt = models.CharField(max_length=3, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_formato2'


class PgFormato34(models.Model):
    ffecha = models.DateField(blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)
    ctipo = models.CharField(max_length=30, blank=True, null=True)
    ncontable = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    namort = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nneto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    ncorrlela = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_formato34'


class PgFormato37(models.Model):
    ccod_pro = models.CharField(max_length=12, blank=True, null=True)
    ccodt5 = models.CharField(max_length=2, blank=True, null=True)
    cdsc5 = models.CharField(max_length=50, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)
    ccodt6 = models.CharField(max_length=3, blank=True, null=True)
    cdsc6 = models.CharField(max_length=50, blank=True, null=True)
    nuni = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=4, blank=True, null=True)
    ntotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_proca = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_formato37'


class PgFormato38(models.Model):
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cnumero = models.CharField(max_length=12, blank=True, null=True)
    crazon = models.CharField(max_length=60, blank=True, null=True)
    cdenomi = models.CharField(max_length=30, blank=True, null=True)
    nnominal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npu = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ncantidad = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nlibros = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nprovis = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nneto = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    ncorrlela = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_formato38'


class PgFormato50(models.Model):
    nubic = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ncapsoc = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvalnomi = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nacsus = models.DecimalField(max_digits=15, decimal_places=0, blank=True, null=True)
    nacpag = models.DecimalField(max_digits=15, decimal_places=0, blank=True, null=True)
    nsocios = models.DecimalField(max_digits=15, decimal_places=0, blank=True, null=True)
    ctipodoc = models.CharField(max_length=2, blank=True, null=True)
    cnumero = models.CharField(max_length=20, blank=True, null=True)
    crazon = models.CharField(max_length=60, blank=True, null=True)
    ctipo = models.CharField(max_length=40, blank=True, null=True)
    naccion = models.DecimalField(max_digits=15, decimal_places=0, blank=True, null=True)
    npor = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_formato50'


class PgFunciones(models.Model):
    ncodfun = models.DecimalField(max_digits=3, decimal_places=0, blank=True, null=True)
    cdetfun = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_funciones'


class PgHisCal(models.Model):
    ctpo_cal = models.CharField(max_length=3, blank=True, null=True)
    cmes = models.CharField(max_length=2, blank=True, null=True)
    nvalini = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nvalfin = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    najssal = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npagcta = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntotigv = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ffecha = models.DateField(blank=True, null=True)
    ntpoopc = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ntpopor = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_his_cal'


class PgHistorial(models.Model):
    cdeshist = models.CharField(max_length=50, blank=True, null=True)
    mdethist = models.CharField(max_length=500, blank=True, null=True)
    ncodhist = models.DecimalField(max_digits=2, decimal_places=0, blank=True, null=True)
    ccondi = models.CharField(max_length=500, blank=True, null=True)
    cselec = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_historial'


class PgHistvenc(models.Model):
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cnumero = models.CharField(max_length=20, blank=True, null=True)
    ffechareg = models.DateField(blank=True, null=True)
    ffecharep = models.DateField(blank=True, null=True)
    tipreg = models.CharField(max_length=1, blank=True, null=True)
    ccoment = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_histvenc'


class PgMoneda(models.Model):
    ccod_mone = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_moneda'


class PgNumtick(models.Model):
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    nasiento = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    cnumtick = models.CharField(max_length=41, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_numtick'


class PgOrigen(models.Model):
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=70, blank=True, null=True)
    nnum0 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum1 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum2 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum3 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum4 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum5 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum6 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum7 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum8 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum9 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum10 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum11 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum12 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum13 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum14 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    nnum15 = models.DecimalField(max_digits=5, decimal_places=0, blank=True, null=True)
    lmodi = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_origen'


class PgOrigenbc(models.Model):
    ccod_ori = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=70, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_origenbc'


class PgOrigenfi(models.Model):
    ccod_ac = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)
    ccuenta = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_origenfi'


class PgPagsunat(models.Model):
    ccod_pagsu = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=90, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_pagsunat'


class PgPais(models.Model):
    ccod_pais = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_pais'


class PgPlan(models.Model):
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)
    nnivel = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    nanalisis = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ccod_bal = models.CharField(max_length=4, blank=True, null=True)
    cdes_d = models.CharField(max_length=10, blank=True, null=True)
    cdes_h = models.CharField(max_length=10, blank=True, null=True)
    ndebe0 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber0 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe13 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber13 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe14 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber14 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebe15 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber15 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nganper = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ccue_acm = models.CharField(max_length=10, blank=True, null=True)
    ndebed0 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd0 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed13 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd13 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed14 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd14 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ndebed15 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd15 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ccod_baln = models.CharField(max_length=4, blank=True, null=True)
    ccod_bal2 = models.CharField(max_length=4, blank=True, null=True)
    ccod_baln2 = models.CharField(max_length=4, blank=True, null=True)
    lajusneg = models.BooleanField(blank=True, null=True)
    lexpsunat = models.BooleanField(blank=True, null=True)
    neducat = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    mdinamica = models.CharField(max_length=500, blank=True, null=True)
    ccod_enfin = models.CharField(max_length=2, blank=True, null=True)
    cnumcta = models.CharField(max_length=40, blank=True, null=True)
    cmoneda = models.CharField(max_length=1, blank=True, null=True)
    npresup = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ccod_balr = models.CharField(max_length=4, blank=True, null=True)
    ccod_cueci = models.CharField(max_length=10, blank=True, null=True)
    cnocorr1 = models.CharField(max_length=10, blank=True, null=True)
    cnocorr2 = models.CharField(max_length=10, blank=True, null=True)
    lccadest = models.BooleanField(blank=True, null=True)
    lsaldosme = models.BooleanField(blank=True, null=True)
    cref = models.CharField(max_length=1, blank=True, null=True)
    cale = models.CharField(max_length=4, blank=True, null=True)
    cmle = models.CharField(max_length=2, blank=True, null=True)
    mesmod = models.CharField(max_length=2, blank=True, null=True)
    anomod = models.CharField(max_length=4, blank=True, null=True)
    lafecret = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_plan'


class PgPlanrela(models.Model):
    ccod_tpla = models.CharField(max_length=3, blank=True, null=True)
    ccuenta = models.CharField(max_length=20, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)
    ccod_cue = models.CharField(max_length=20, blank=True, null=True)
    nnivel = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_planrela'


class PgPlantipo(models.Model):
    ccod_tpla = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_plantipo'


class PgPorcen(models.Model):
    ccod_por = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)
    npor = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_porcen'


class PgPresup(models.Model):
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    ngrupo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    npe1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cdes = models.CharField(max_length=80, blank=True, null=True)
    ncen_costo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)
    npe1d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo1d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop1d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe4d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo4d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop4d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe5d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo5d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop5d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe6d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo6d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop6d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe7d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo7d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop7d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe8d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo8d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop8d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe9d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo9d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop9d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe10d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo10d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop10d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe11d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo11d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop11d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe12d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo12d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop12d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntc1 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc2 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc3 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc4 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc5 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc6 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc7 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc8 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc9 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc10 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc11 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc12 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_presup'


class PgPresupb(models.Model):
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_presupb'


class PgPresupc(models.Model):
    ccod_presu = models.CharField(max_length=10, blank=True, null=True)
    ccod_costo = models.CharField(max_length=9, blank=True, null=True)
    npe1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop1 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop2 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop3 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop4 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop5 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop6 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop7 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop8 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop9 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop10 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop11 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop12 = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe1d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo1d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop1d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop2d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop3d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe4d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo4d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop4d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe5d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo5d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop5d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe6d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo6d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop6d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe7d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo7d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop7d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe8d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo8d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop8d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe9d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo9d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop9d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe10d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo10d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop10d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe11d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo11d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop11d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    npe12d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nmo12d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nop12d = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    ntc1 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc2 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc3 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc4 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc5 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc6 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc7 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc8 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc9 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc10 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc11 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)
    ntc12 = models.DecimalField(max_digits=10, decimal_places=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_presupc'


class PgProdcat(models.Model):
    ccod_proca = models.CharField(max_length=16, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)
    ntipo = models.DecimalField(max_digits=1, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_prodcat'


class PgRecladbf(models.Model):
    cdsc = models.CharField(max_length=200, blank=True, null=True)
    anno = models.CharField(max_length=4, blank=True, null=True)
    ccod_ori_1 = models.CharField(max_length=2, blank=True, null=True)
    ccod_ori_2 = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_recladbf'


class PgRepogen(models.Model):
    cgrupo = models.CharField(max_length=30, blank=True, null=True)
    mfiles = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_repogen'


class PgSeries(models.Model):
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    cser = models.CharField(max_length=6, blank=True, null=True)
    nnum = models.DecimalField(max_digits=13, decimal_places=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_series'


class PgSunat05(models.Model):
    ccod_t05 = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_sunat05'


class PgSunat06(models.Model):
    ccod_t06 = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_sunat06'


class PgTabdetra(models.Model):
    ccod_detra = models.CharField(max_length=5, blank=True, null=True)
    nporcen = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cdsc = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_tabdetra'


class PgTeleprov(models.Model):
    cmes = models.CharField(max_length=2, blank=True, null=True)
    ccod_cue = models.CharField(max_length=10, blank=True, null=True)
    ndebe = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaber = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cglosa = models.CharField(max_length=60, blank=True, null=True)
    ffechadoc = models.DateField(blank=True, null=True)
    cnumero = models.CharField(max_length=20, blank=True, null=True)
    ffechaven = models.DateField(blank=True, null=True)
    ccod_doc = models.CharField(max_length=2, blank=True, null=True)
    ccod_cli = models.CharField(max_length=11, blank=True, null=True)
    cmesc = models.CharField(max_length=2, blank=True, null=True)
    ndebed = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    nhaberd = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cmreg = models.CharField(max_length=1, blank=True, null=True)
    cid = models.CharField(max_length=10, blank=True, null=True)
    nsaldo = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    namor = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
    cdscruc = models.CharField(max_length=80, blank=True, null=True)
    labono = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_teleprov'


class PgTipo(models.Model):
    ccod_tip = models.CharField(max_length=3, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_tipo'


class PgTipod(models.Model):
    ccod_tipd = models.CharField(max_length=1, blank=True, null=True)
    cdsc = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_tipod'


class PgTiprenta(models.Model):
    ccod_renta = models.CharField(max_length=2, blank=True, null=True)
    cdsc = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_tiprenta'


class PgUserkey(models.Model):
    campo01 = models.CharField(max_length=4, blank=True, null=True)
    campo02 = models.CharField(max_length=10, blank=True, null=True)
    campo03 = models.CharField(max_length=500, blank=True, null=True)
    campo04 = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_userkey'


class PgUseruser(models.Model):
    campo01 = models.CharField(max_length=4, blank=True, null=True)
    campo02 = models.CharField(max_length=4, blank=True, null=True)
    campo03 = models.CharField(max_length=500, blank=True, null=True)
    campo04 = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_useruser'


class PgVendedor(models.Model):
    ccod_vend = models.CharField(max_length=4, blank=True, null=True)
    cdsc = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pg_vendedor'