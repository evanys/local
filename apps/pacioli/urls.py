#urls apps pacioli
from django.urls import path
#from apps.pacioli.views import index, search, busquedaPlan,ingresarAsiento,buscarDiario,libroDiario
from apps.pacioli.views import *
from django.conf.urls import url
from django.contrib.auth.views import LogoutView
from django.contrib.auth import views as auth_views

#hojatra
urlpatterns = [
    
    path('', index.as_view(),name='inicio'),
    path('inicio/<slug:dato>', index.as_view(),name='inicio'),
    #path('/<slug:dato>', index.as_view(),name='iniciob'),
    
    #path('^search/$',search,name = 'search'),
    #path('logout/',  LogoutView.as_view() , name='logout'),
    path('home/', TemplateView.as_view(template_name='home.html'), name='home'),
    
    path('actualizar/<slug:act>/<slug:asi>/<int:pk>', Update_Asiento.as_view(),name='actualizarDiario'),

    #reportes
    path('reporteDiario/<int:pk>/<slug:cmes>/<int:proposito>', ReporteDiarioExcel.as_view(),name='reporteDiario'),
    #path('reportehTrabajo/<int:pk>/<slug:cmes>/<int:proposito>', ReporteLibroHojaTrabajoExcel.as_view(),name='reportehTrabajo'),
    path('reportehTrabajo/<int:pk>/<slug:cmes>/<int:proposito>', ReporteLibroHojaTrabajoExcel.as_view(),name='reportehTrabajo'),
    #path('ReporteEstadoFinanciero/<int:pk>', ReporteEstadoFinanciero.as_view(),name='ReporteEstadoFinanciero'),

    path('ReporteEstadoFinanciero/<int:pk>/<slug:cmes>/<int:proposito>', reporteEstadoFinancieros.as_view(),name='ReporteEstadoFinanciero'),
    path('reporteEstadoNaturalesa/<int:pk>/<slug:cmes>/<int:proposito>', reporteEstadoNaturalesa.as_view(),name='reporteEstadoNaturalesa'),
    path('reporteEstadoFunciones/<int:pk>/<slug:cmes>/<int:proposito>', reporteEstadoFunciones.as_view(),name='reporteEstadoFunciones'),

    path('ReporteEstadoResultados/<int:pk>', ReporteEstadoResultados.as_view(),name='ReporteEstadoResultados'),
    path('reporteMayor/<int:pk>', ReporteLibroMayor.as_view(),name='reporteMayor'),
    path('reporteViewMayor/<int:pk>', ReporteViewLibroMayor.as_view(),name='reporteViewMayor'),


    #libros
    path('libroDiario/<int:pk>', libroDiario.as_view(),name='librodiario'),
    path('actualizar_asiento/<int:pk>/<slug:asi>', actualizar_asiento.as_view(),name='actualizar_asiento'),
    path('librohojaTrabajo/<int:pk>', librohojaTrabajo.as_view(),name='librohojatrabajo'),
    path('libroMayor/<int:pk>', libroMayor.as_view(),name='libromayor'),
    path('eeff/<int:pk>', libroEEFF.as_view(),name="estadoFinanciero"),
    path('eeResultados/<int:pk>', libroEERR.as_view(),name="EstadoResultados"),
    path('eeRRFF/<int:pk>', eeRRFF.as_view(),name="eeRRFF"),

    path('reporteF11/<int:pk>/<slug:cmes>/<int:proposito>', reporteF11.as_view(),name="reporteF11"),
    path('reporteF12/<int:pk>/<slug:cmes>/<int:proposito>', reporteF12.as_view(),name="reporteF12"),
    path('reporteF81/<int:pk>/<slug:cmes>/<int:proposito>', reporteF81.as_view(),name="reporteF81"),
    path('reporteF141/<int:pk>/<slug:cmes>/<int:proposito>', reporteF141.as_view(),name="reporteF141"),




    #mantenimiento
    path('mantenimientoAsientos/<int:pk>', mantenimientoAsientos.as_view(),name="mantenimientoAsientos"),
    path('mantenimientoCuentas/<int:pk>', mantenimientoCuentas.as_view(),name="mantenimientoCuentas"),

    #json
    path('busqueda/', busquedaPlan.as_view()),
    path('busquedaunplan/', busquedaUnPlan.as_view()),
    path('editar/', EditarAsiento.as_view()),
    path('diario/', buscarDiario.as_view()),
    path('ingresar/', ingresarAsiento.as_view()),
    path('estadofinanciero/', estadoFinancieroPLanes.as_view()),
    path('hojatrabajo/', buscarlibroHojaTrabajo.as_view()),

    
    path('hojatrabajo2/', hojatra),
    path('diarioEliminar/', Eliminar_Diario.as_view()),
    path('libroMayorBuscar/', buscarMayor.as_view()),
    path('existeAsiento/', existeAsiento.as_view()),
    path('onlyOneAsiento/', onlyOneAsiento.as_view()),
    path('LibroJsonEERR/', LibroJsonEERR.as_view()),
    path('LibroJsonEEFF/', LibroJsonEEFF.as_view()),
    path('buscarDiario/', buscarDiario.as_view()),
    path('buscarMayorLibro/', buscarMayorLibro.as_view()),

    path('buscarhtLibro/', buscarhtLibro.as_view()),
    


    #segundo pacioli
    path('buscarMasCuentas/', buscarMasCuentas.as_view()),
    path('buscarDocumento/', buscarDocumento.as_view()),
    path('BuscarRuc/', BuscarRuc.as_view()),
    path('crearOrigenes/', crearOrigenes.as_view()),
    path('crearPlanes/', crearPlanes.as_view()),

    path('buscarMasCuentasTabla/', buscarMasCuentasTabla.as_view()),
    path('buscarOrigenes/', buscarOrigenes.as_view()),
    path('buscarOrigenesTabla/', buscarOrigenesTabla.as_view()),
    path('buscarAsiento/', buscarAsiento.as_view()),
    path('jsonPrueba/', jsonPrueba.as_view()),
    path('buscarCuentaIGV/', buscarCuentaIGV.as_view()),
    path('registrarCuentaIGV/', registrarCuentaIGV.as_view()),
    path('buscarCuo/', buscarCuo.as_view()),
    path('eliminarCuenta/', eliminarCuenta.as_view()),
    path('ListarSubDiario/', ListarSubDiario.as_view()),
    path('buscarSubdiarioUsuario/', buscarSubdiarioUsuario.as_view()),
    path('buscarExisteSubDiario/', buscarExisteSubDiario.as_view()),
    path('buscarAsientoExiste/', buscarAsientoExiste.as_view()),
    path('eliminarasientosVarios/', eliminarasientosVarios.as_view()),
    path('eliminarasientosFull/', eliminarasientosFull.as_view()),

    path('registrarPersona/', registrarPersona.as_view()),
    path('registrarEmpresa/', registrarEmpresa.as_view()),
    
    path('buscarFlujo/', buscarFlujo.as_view()),
    path('buscarmedio/', buscarmedio.as_view()),
    path('buscarPatriTribu/', buscarPatriTribu.as_view()),
    path('buscarPatriCnv/', buscarPatriCnv.as_view()),
###########################################################################################################################################
##############################################################ESTEFANY#####################################################################
    path('reporteF32/<int:pk>/<int:cmes>/<int:proposito>', reporteF32.as_view(),name='reporteF32'),
    path('reporteF33/<int:pk>/<int:cmes>/<int:proposito>', reporteF33.as_view(),name='reporteF33'),
    path('reporteF34/<int:pk>/<int:cmes>/<int:proposito>', reporteF34.as_view(),name='reporteF34'),
    path('reporteF35/<int:pk>/<int:cmes>/<int:proposito>', reporteF35.as_view(),name='reporteF35'),
    path('reporteF36/<int:pk>/<int:cmes>/<int:proposito>', reporteF36.as_view(),name='reporteF36'),
    path('reporteF31/<int:pk>/<int:cmes>/<int:proposito>', reporteF31.as_view(),name='reporteF31'),
    path('reporteF311/<int:pk>/<int:cmes>/<int:proposito>', reporteF311.as_view(),name='reporteF311'),
    path('reporteF312/<int:pk>/<int:cmes>/<int:proposito>', reporteF312.as_view(),name='reporteF312'),
    path('reporteF313/<int:pk>/<int:cmes>/<int:proposito>', reporteF313.as_view(),name='reporteF313'),
    path('reporteF315/<int:pk>/<int:cmes>/<int:proposito>', reporteF315.as_view(),name='reporteF315'),

############################################################################################################################################
############################################################################################################################################  
    
    
    url(r'^logout/$', LogoutView.as_view(), {'template_name': 'log/logout.html'}, name='logout'),
    
]
