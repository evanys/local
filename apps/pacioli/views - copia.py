from django.shortcuts import render, render_to_response, HttpResponse
from django.contrib.auth.models import User
from django.views.generic import TemplateView,ListView, UpdateView
from apps.pacioli.models import *
from django.forms.models import  model_to_dict
from django.core import serializers
from django.core.serializers import serialize
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.db.models import Max, Avg, Count, Min, Sum
#import datetime
from datetime import datetime,  timedelta

from django.db import connection
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl import load_workbook
from openpyxl.styles import Font, Fill ,Border, Side, PatternFill, GradientFill, Alignment,Border

from django.db.models import Q

from books.replica import replicarDatos


class index(ListView):
    model = PgPlan
    template_name = 'adminis/index.html'

    def get_context_data(self, **kwargs):
        context  =  super(index,self).get_context_data(**kwargs)
        pk = self.kwargs.get('id')
        dt = self.kwargs.get('dato')
        #print("datoffffffffff")
        #print(dt)
        #contextb = PgPlan.objects.filter(nnivel = 3)
        #context['nnivel'] = True
        #planes = self.model.objects.filter(nnivel=3)
        context['actualizar'] = dt
        context['listado'] = PgPlan.objects.filter(nnivel=3).filter(neducat=5)
        #print(context['listado'])
        #print ("este es el id" + str(pk))
        
        return context
    
    def listado(self):
        datos = PgDiario.objects.all()
        

    def listadob(self):
        datos = PgDiario.objects.all()
        

    def pasar_datos(self, **kwargs):
        context  =  super(index,self).get_context_data(**kwargs)
        context['listado'] = PgPlan.objects.filter(nnivel=3).filter(neducat=5)
        #print(context['listado'])
        return context


class actualizar(ListView):
    model = PgPlan
    template_name = 'adminis/indexb.html'

    def get_context_data(self, **kwargs):
        context  =  super(index,self).get_context_data(**kwargs)
        pk = self.kwargs.get('id')
        dt = self.kwargs.get('dato')
        
        #print(dt)
        #contextb = PgPlan.objects.filter(nnivel = 3)
        #context['nnivel'] = True
        #planes = self.model.objects.filter(nnivel=3)
        context['actualizar'] = dt
        context['listado'] = PgPlan.objects.filter(nnivel=3).filter(neducat=5)
        #print(context['listado'])
        #print ("este es el id" + str(pk))
        return context
    

    def pasar_datos(self, **kwargs):
        context  =  super(index,self).get_context_data(**kwargs)
        context['listado'] = PgPlan.objects.filter(nnivel=3).filter(neducat=5)
        #print(context['listado'])
        return context


import json        
def search(request):
    plan = request.GET.get('username')
    planes = PgPlan.objects.filter(neducat=plan)
    planes = [plan_serializer(plan) for plan in planes]
    #print (plan)
    return HttpResponse(json.dumps(planes), content_type='application/json')

def plan_serializer(plan):
    return{'ccod_cue':str(plan.ccod_cue) ,'cdsc':str(plan.cdsc)}



class busquedaPlan(TemplateView):
    def get(self, request, *args, **kwargs):
        educat = request.GET['dato']
        planes = PgPlan.objects.filter(neducat=educat).filter(nnivel=3).order_by('ccod_cue')
        data = serializers.serialize('json',planes,fields=('ccod_cue'.strip(),'cdsc'.strip(), 'ccod_bal2'.strip(),'pk'))
        #print(educat)
        
        return HttpResponse(data,content_type='application/json')


class busquedaUnPlan(TemplateView):
    def get(self, request, *args, **kwargs):
        educat = request.GET['dato']
        planes = PgPlan.objects.filter(ccod_cue=educat).filter(nnivel=3).order_by('ccod_cue')
        data = serializers.serialize('json',planes,fields=('ccod_cue'.strip(),'cdsc'.strip(), 'ccod_bal2'.strip(),'pgplan'))
        #print(educat)
        return HttpResponse(data,content_type='application/json')


class ingresarAsiento(TemplateView):
    def get(self, request,*args, **kwargs):
        debe = request.GET['debe']
        haber = request.GET['haber']
        monto_p = request.GET['monto']
        cglosa_p = request.GET['cglosa']
        usua_p = request.GET['usua']
        cuenta_des_p = request.GET['cuenta_des']
        cuenta_has_p = request.GET['cuenta_has']
        bal_des_p = request.GET['bal_debe']
        bal_has_p = request.GET['bal_haber']
        id_des_p = int(request.GET['id_debe'])
        id_has_p = int(request.GET['id_haber'])
        
        pacoa = PgPlan(
            id = id_des_p
        )
        pacob = PgPlan(
            id = id_has_p
        )

        ffecha_p = datetime.datetime.now()

        cantidad= PgDiario.objects.all().count()
        if (cantidad >= 1):
            asientos =  PgDiario.objects.values_list('nasiento', flat=True).filter(ccod_user=usua_p)
            ids =  PgDiario.objects.values_list('id', flat=True)
            alto = 0
            if (len(asientos)) > 0:
                alto = max(asientos)+1
            else:
                alto = 1
            altoid = max(ids)
        else:
            alto = 0
            altoid = 0

        #alto_guardar  = alto + 1
        alto_guardar = alto
        altoid_guardar = altoid+1
        altoid_guardar_b = altoid+2
        p = PgDiario(nasiento =alto_guardar, ccod_cue=debe, ndebe=monto_p,nhaber=0,cglosa=cglosa_p,ffecha = ffecha_p,ccod_ori=6,id= altoid_guardar,ccod_user=usua_p,cglosa2=cuenta_des_p,cpernum=bal_des_p,pgplan=pacoa)
        p2 = PgDiario(nasiento =alto_guardar, ccod_cue=haber, ndebe=0,nhaber=monto_p,cglosa=cglosa_p,ffecha = ffecha_p,ccod_ori=6, id = altoid_guardar_b,ccod_user=usua_p,cglosa2=cuenta_has_p,cpernum=bal_has_p,pgplan=pacob)
        p.save()
        p2.save()
        diario = PgDiario.objects.all()
        data = serializers.serialize('json',diario,fields=('ccod_cue','cglosa'))
        return HttpResponse(data,content_type='application/json')

'''
class buscarDiario(TemplateView):
    def get(self, request, *args, **kwargs):
        #debe
        usuario_id = request.GET['debe']
        asientos =  PgDiario.objects.filter(ccod_user=usuario_id).order_by('-nasiento')
        
        data = serializers.serialize('json',asientos,fields=('nasiento','ffecha','cglosa','ccod_cue','ndebe','nhaber','cglosa2'))
        return HttpResponse(data,content_type='application/json')
'''
class buscarMayorLibro(TemplateView):
    def get(self, request, *args, **kwargs):
        #debe

        usuario = request.GET.get('usuario')
        cmes = request.GET.get('cmes')
        cuenta = request.GET.get('cuenta')
        lista =  functionlibroMayorxMes(usuario, cmes, cuenta)

        return HttpResponse(json.dumps(lista), content_type="application/json")

class buscarhtLibro(TemplateView):
    def get(self, request, *args, **kwargs):
        #debe

        usuario = request.GET.get('usuario')
        cmes = request.GET.get('cmes')
        #$cuenta = request.GET.get('cuenta')
        lista =  funcionLibroHojaTrabajo(usuario, cmes)

        return HttpResponse(json.dumps(lista), content_type="application/json")


class libroDiario(ListView):
    model = PgDiario
    template_name = 'adminis/diariolibro.html'

    def get_context_data(self, **kwargs):
        context  =  super(libroDiario,self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        #print(pk)
        asientos =  PgDiario.objects.filter(ccod_user=pk).order_by('nasiento','id')
        tDEBE =0
        tHABER =0
        for n in  asientos:
            tDEBE = tDEBE + n.ndebe
            tHABER = tHABER + n.nhaber
        #context['listado'] = PgDiario.objects.all()
        context['tDEBE'] = tDEBE
        context['tHABER'] = tHABER
        context['listado'] = asientos
        return context

#buscar libro Diario x mes  + Json
class buscarDiario(TemplateView):
    def get(self, request, *args, **kwargs):
        cmes = request.GET.get('cmes')
        usuario = request.GET.get('usuario')
    
        #consulta =  PgDiario.objects.filter(cmes=cmes).filter(ccod_user=usuario).order_by('cmes', 'ccod_ori','nasiento')
        consulta =  PgDiario.objects.filter(ffechareg__month=cmes).filter(ccod_user=usuario).order_by('cmes', 'ccod_ori','nasiento')
        tDEBE =0
        tHABER =0
        lista=[]
        for d in consulta:
            tDEBE = tDEBE + d.ndebe
            tHABER = tHABER + d.nhaber
        lista.append({'tdebe': str(tDEBE), 'tHABER':str(tHABER)})

        for x in consulta:
            m = {'cmes': x.cmes, 'ccod_ori': x.ccod_ori, 'nasiento': str(x.nasiento), 'ccod_cue': x.ccod_cue.strip(), 'ndebe': str(x.ndebe), 'nhaber': str(x.nhaber), 'plan.cdsc': x.pgplan.cdsc.strip(), 'cglosa': x.cglosa}
            lista.append(m)


        #print(longitud,'+++++++++++++')
        #lista=[{'lon': 'longitud'}]
        return HttpResponse(json.dumps(lista), content_type="application/json")


class libroMayor(ListView):
    model = PgDiario
    template_name = 'adminis/mayorlibro.html'
    context_object_name = 'search_stores'
    """docstring for libroMayor"""

    def get_queryset(self, **kwargs):
        search_text = self.request.GET.get('cuentaBuscar')
        #search_text = '411'
        pk = self.kwargs.get('pk')
        search_stores = ""
        if search_text is None or len(search_text) == 0:
            #print("nada")
            #asientos =  PgDiario.objects.filter(ccod_cli=pk).order_by('nasiento','id')  
            #consulta de hoja de trabajo
            miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc').filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')


            #conuslta para libro mayor
            consultaslista= []            
            for d in range(len(miconsulta)):
                cuentaList =  miconsulta[d][0]
                consulta =  PgDiario.objects.filter(ccod_user=pk).filter(pgplan__ccod_cue=cuentaList).order_by('nasiento','id')
                unaLista = []
                for x in range(len(consulta)):
                    nuevo = []
                    nuevo.append(consulta[x].pgplan.ccod_cue.strip())
                    nuevo.append(consulta[x].pgplan.cdsc.strip())
                    nuevo.append(consulta[x].nasiento)
                    nuevo.append(consulta[x].cglosa.strip())
                    nuevo.append(consulta[x].ndebe)
                    nuevo.append(consulta[x].nhaber)
                    nuevo.append(consulta[x].ccod_ori)
                    unaLista.append(nuevo)
                consultaslista.append(unaLista)
                
            
            listado = []
            listaDEBE = []
            listaHABER = []
            for t in consultaslista:
                r=[0]
                
                tDE = 0    
                tHA = 0
                for m in range(len(t)):
                    inicial = t[m][0][:1]
                    if inicial == '4' or inicial == '5' or inicial== '7':
                        h = (t[m][5] - t[m][4]) + r[m]
                        r.append(h)
                    else:
                        h = (t[m][4] - t[m][5]) + r[m]
                        r.append(h)
                    tDE = tDE + t[m][4]
                    tHA = tHA + t[m][5]
                
                listaDEBE.append(tDE)
                listaHABER.append(tHA)

                
                #r.append(tDE)
                    #r.append(tHA)
                r.pop(0)
                #r.append(tDE)
                #r.append(tHA)

                listado.append(r)
                #listado.append(listaDEBE)

            #print("########")
            #for dt in listaDEBE:
            #    print(dt)
            #print("########")
                #add saldos to listGENERAL
            
            
            for g in range(len(listado)):
                #consultaslista[g].append(listado[g])
                n=0
                for h in consultaslista[g]:
                    h.append(listado[g][n])
                    h.append(listaDEBE[g])
                    h.append(listaHABER[g])
                    n=n+1

               
            identificador = ["0"]
            
            consultaslista.insert(0,identificador)
            #functionlibroMayorxMes(pk,0);
            search_stores = consultaslista
        else:
            #print("todo")
            consulta =  PgDiario.objects.filter(ccod_user=pk).filter(pgplan__ccod_cue=search_text).order_by('nasiento','id')
            asientos = []
            for x in range(len(consulta)):
                nuevo= []
                nuevo.append(consulta[x].pgplan.ccod_cue.strip())
                nuevo.append(consulta[x].pgplan.cdsc.strip())
                nuevo.append(consulta[x].nasiento)
                nuevo.append(consulta[x].cglosa.strip())
                nuevo.append(consulta[x].ndebe)
                nuevo.append(consulta[x].nhaber)
                nuevo.append(consulta[x].ccod_ori)
                asientos.append(nuevo)
            
            inicial=""
            if search_text:
                inicial = search_text[:1]

            r = [0]
            d= [0]

            

            if inicial == '4' or inicial == '5' or inicial== '7':
                for x in range(len(asientos)):
                    #h = (asientos[x].nhaber -  asientos[x].ndebe) + r[x]
                    h=(asientos[x][5]-asientos[x][4]) + r[x]
                    r.append(h)
            else:
                for x in range(len(asientos)):
                    #h = (asientos[x].ndebe -  asientos[x].nhaber) + r[x]
                    h=(asientos[x][4]-asientos[x][5]) + r[x]
                    r.append(h)
        
            r.pop(0)
            tDEB = 0
            tHAB = 0
            for x in range(len(asientos)):
                #tDEB = tDEB + asientos[x].ndebe
                tDEB = tDEB + asientos[x][4]
                #tHAB = tHAB + asientos[x].nhaber
                tHAB = tHAB + asientos[x][5]

            gran = []
            for v in range(len(asientos)):
                hu = []
                #hu.append(asientos[v].ccod_cue.strip())
                hu.append(asientos[v][0].strip())
                #hu.append(asientos[v].cglosa2.strip())
                hu.append(asientos[v][1].strip())
                #hu.append(asientos[v].nasiento)
                hu.append(asientos[v][2])
                #hu.append(asientos[v].cglosa.strip())
                hu.append(asientos[v][3].strip())
                hu.append(asientos[v][4])
                hu.append(asientos[v][5])
                hu.append(r[v])
                hu.append(tDEB)
                hu.append(tHAB)
                hu.append(asientos[v][6])
                gran.append(hu)
        
            #for m in gran:
            #    print(m, 'hhhhhhhhhhhhh')

            search_stores = gran   
        return search_stores

        def get_context_data(self, **kwargs):
            context = super(libroMayor,self).get_context_data(**kwargs)
            pk = self.kwargs.get('pk')
            cuenta = self.kwargs.get('cuenta')
            asientos =  PgDiario.objects.filter(ccod_user=pk).filter(pgplan__ccod_cue=cuenta).order_by('nasiento','id')
            
            tDEB = 0
            tHAB = 0
            for x in range(len(asientos)):
                tDEB = tDEB + asientos[x].ndebe
                tHAB = tHAB + asientos[x].nhaber
            
            t = self.totales
            context['tDEBE'] = t
            context['tHABER'] = tHAB
            context['mayor'] = "Libro Mayor"
            context['listado'] = asientos
            return context
        
#buscar libro Mayor x mes  + Json
class libroMayorxMes(TemplateView):
    pass

def functionlibroMayorxMes(usuario, cmes, cuenta):
    print('usuario', usuario, 'cmes', cmes, 'cuenta', cuenta);
    consulta = None
    if cmes !='--' and len(cuenta)==0:
        #consulta = PgDiario.objects.filter(ccod_user = usuario).filter(cmes = cmes).order_by('pgplan__ccod_cue','nasiento')
        consulta = PgDiario.objects.filter(ccod_user = usuario).filter(ffechareg__month=cmes).order_by('pgplan__ccod_cue','nasiento')
    if cmes =='--' and len(cuenta)>=1:
        consulta = PgDiario.objects.filter(ccod_user = usuario).filter(pgplan__ccod_cue = cuenta).order_by('pgplan__ccod_cue','nasiento')
        
    if cmes !='--' and len(cuenta)>=1:
        #consulta = PgDiario.objects.filter(ccod_user = usuario).filter(cmes = cmes).filter(pgplan__ccod_cue = cuenta).order_by('pgplan__ccod_cue','nasiento')
        consulta = PgDiario.objects.filter(ccod_user = usuario).filter(ffechareg__month=cmes).filter(pgplan__ccod_cue = cuenta).order_by('pgplan__ccod_cue','nasiento')
    if cmes =='--' and len(cuenta)==0:
        #consulta = PgDiario.objects.filter(ccod_user = usuario).order_by('pgplan__ccod_cue','nasiento')
        consulta = PgDiario.objects.filter(ccod_user = usuario).order_by('pgplan__ccod_cue','nasiento')
    #    consulta = PgDiario.objects.filter(ccod_user = usuario).filter(cmes = cmes).order_by('pgplan__ccod_cue','nasiento')
    '''
    elif cmes!='--' and len(cuenta)<=0:
        consulta = PgDiario.objects.filter(ccod_user = usuario).filter(cmes = cmes).order_by('pgplan__ccod_cue','nasiento')
    elif cmes =='--' and len(cuenta)<=0:
        consulta = PgDiario.objects.filter(ccod_user = usuario).order_by('pgplan__ccod_cue','nasiento')
    '''
    #consulta = PgDiario.objects.filter(ccod_user = usuario).filter(cmes = cmes).order_by('pgplan__ccod_cue','nasiento')
    
    listaConpleta = []
    listacuentas = []
    for g in consulta:
        s = 0
        for j in listacuentas:
            if g.ccod_cue.strip() == j['ccod_cue']:
                s +=1
        if s == 0:
            listacuentas.append({'ccod_cue': g.ccod_cue.strip()})


    for consu in consulta:
        if consu.pgplan.ccod_cue[:1]=='1' or consu.pgplan.ccod_cue[:1]=='2' or consu.pgplan.ccod_cue[:1]=='3':
            nm = consu.ndebe - consu.nhaber
            n = {'cmes':consu.cmes, 'ccod_cue':consu.pgplan.ccod_cue.strip(),'ccod_ori': consu.ccod_ori,'nasiento':str(consu.nasiento),'cglosa': consu.cglosa.strip(), 'pgplan_cdsc':consu.pgplan.cdsc.strip() ,'ndebe':consu.ndebe,'nhaber':consu.nhaber, 'newMonto':nm, 'saldos': 0, 'tDEBE': 0.0, 'tHABER':0.0}
            listaConpleta.append(n)
        elif consu.pgplan.ccod_cue[:1]=='4' or consu.pgplan.ccod_cue[:1]=='5' or consu.pgplan.ccod_cue[:1]=='7':
            nm = consu.nhaber - consu.ndebe
            n = {'cmes':consu.cmes, 'ccod_cue':consu.pgplan.ccod_cue.strip(),'ccod_ori': consu.ccod_ori,'nasiento':str(consu.nasiento),'cglosa': consu.cglosa.strip(), 'pgplan_cdsc':consu.pgplan.cdsc.strip() ,'ndebe':consu.ndebe,'nhaber':consu.nhaber, 'newMonto':nm , 'saldos': 0 , 'tDEBE': 0.0, 'tHABER':0.0}
            listaConpleta.append(n)
        #n = {'ccod_cue':consu.pgplan.ccod_cue,'ccod_ori': consu.ccod_ori,'nasiento':consu.nasiento,'cglosa': consu.cglosa.strip(), 'pgplan_cdsc':consu.pgplan.cdsc.strip() ,'ndebe':consu.ndebe,'nhaber':consu.nhaber, 'newMonto':0.0}

    nuevalista = []
    for g in listacuentas:
        l= []
        for h in listaConpleta:
            if g['ccod_cue'] == h['ccod_cue']:
                l.append(h)
        nuevalista.append(l)

    for t in nuevalista:
        c = 0
        tDEBE = 0.0
        tHABER = 0.0
        for l in t:
            print(c , 'este es el c')
            l['saldos'] = l['newMonto']  + c
            c = l['newMonto'] + c
            tDEBE = tDEBE+ float(l['ndebe']) 
            tHABER = tHABER+ float(l['nhaber'])
        c = 0

        for h in t:
            h['tDEBE'] = str(tDEBE)
            h['tHABER'] = str(tHABER)
            h['ndebe'] = str(h['ndebe'])
            h['nhaber'] = str(h['nhaber'])
            h['newMonto'] = str(h['newMonto'])
            h['saldos'] = str(h['saldos'])
    '''
    for h in nuevalista:
        for j in h:
            print(j)
        print('ooooooooooo')
    '''
    return nuevalista
        

class buscarMayor(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario_id = request.GET['usuario']
        cuenta_id = request.GET['cuenta']
        asientos =  PgDiario.objects.filter(ccod_user=usuario_id).filter(pgplan__ccod_cue=cuenta_id).order_by('nasiento','id')               
        data = serializers.serialize('json',asientos,fields=('pgplan__ccod_cue','pgplan__cdsc','nasiento','cglosa','ndebe','nhaber'))
        return HttpResponse(data,content_type='application/json')    


class librohojaTrabajo(ListView):
    model = PgDiario
    template_name = 'adminis/hojatrabajo.html'
    
    def get_context_data(self, **kwargs):
        context  =  super(librohojaTrabajo,self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')

        
        miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc').filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        minuevaconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc','pgplan__ntipo').filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        #funcionLibroHojaTrabajo(pk)
        listaNueva=[]
        listaVieja=[]
        for x in minuevaconsulta:
            #print(x, 'mi nueva consulta')
            l = {'ccod_cue': x[0],'cdsc':x[1].strip(),'ntipo':x[2],'sum_debe':x[3],'sum_haber':x[4]}
            listaNueva.append(l)
        
        for m in miconsulta:
            ll = {'ccod_cue': m[0],'cdsc':m[1].strip(),'sum_debe':m[2],'sum_haber':m[3]}
            listaVieja.append(ll)
        
            
        tDEBITO =0
        tCREDITO=0
        for ln in listaNueva:
            tDEBITO = tDEBITO + ln['sum_debe']
            tCREDITO = tCREDITO + ln['sum_haber']
            #print(ln['ccod_cue'],ln['sum_debe'],ln['sum_haber'])
        #print('------------')
        for lv in listaVieja:
            pass
            #print(lv['ccod_cue'], lv['sum_debe'],lv['sum_haber'])
        #print('------------')
        '''
        tDEBITO = 0
        for x in miconsulta:
            tDEBITO = tDEBITO + x[2]
        tCREDITO =0
        for x in miconsulta:
            tCREDITO = tCREDITO + x[3]
        '''
        #print(tDEBITO ,tCREDITO,'resultados finales')

        deu = []
        acre =[]
        buenArray = []

        activos = []
        pasivos = []
        perdidas_a = []
        ganancias_a = []
        perdidas_f = []
        ganancias_f = []
        acti = 0
        per = 0

        for listado in range(len(listaNueva)):
            resta = listaNueva[listado]['sum_debe'] - listaNueva[listado]['sum_haber']
            #print(listaNueva[listado]['sum_debe'], listaNueva[listado]['sum_haber'], resta)
            if(resta > 0):
                #nuevoJson = { 'deudor' : str(abs(resta)), 'acreedor': str(0)}
                listaNueva[listado]['deudor'] = str(abs(resta))
                listaNueva[listado]['acreedor'] = str(0.00)
            else:
                #nuevoJson = {'deudor' : 0, 'acreedor': str(abs(resta))}
                listaNueva[listado]['deudor'] = str(0.00)
                listaNueva[listado]['acreedor'] = str(abs(resta))


            if listaNueva[listado]['ntipo'] == 1:
                listaNueva[listado]['activo'] = str(abs(resta))
                listaNueva[listado]['pasivo'] = str(0.00)
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0)
                listaNueva[listado]['GanFun'] = float(0)

            if listaNueva[listado]['ntipo'] == 2:
                listaNueva[listado]['activo'] = str(0.00)
                listaNueva[listado]['pasivo'] = str(abs(resta))
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0)
                listaNueva[listado]['GanFun'] = float(0)

            if listaNueva[listado]['ntipo'] == 3:
                if(float(listaNueva[listado]['deudor']) <= 0):
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(listaNueva[listado]['deudor'])
                    listaNueva[listado]['GanNat'] = float(listaNueva[listado]['acreedor'])
                    listaNueva[listado]['perFun'] = float(listaNueva[listado]['activo'])
                    listaNueva[listado]['GanFun'] = float(listaNueva[listado]['pasivo'])
                    
                else:
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(listaNueva[listado]['deudor'])
                    listaNueva[listado]['GanNat'] = float(listaNueva[listado]['acreedor'])
                    listaNueva[listado]['perFun'] = float(listaNueva[listado]['activo'])
                    listaNueva[listado]['GanFun'] = float(listaNueva[listado]['pasivo'])
                    
               
            if listaNueva[listado]['ntipo'] == 4:
                if(float(listaNueva[listado]['deudor']) <= 0):
                    #print(listaNueva[listado]['ccod_cue'], "4 esta en pasivo")
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(0)
                    listaNueva[listado]['GanNat'] = float(abs(resta))
                    listaNueva[listado]['perFun'] = float(0.00)
                    listaNueva[listado]['GanFun'] = float(0.00)
                else:
                    #print(listaNueva[listado]['ccod_cue'], "4 esta en activo")
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(abs(resta))
                    listaNueva[listado]['GanNat'] = float(0)
                    listaNueva[listado]['perFun'] = float(0.00)
                    listaNueva[listado]['GanFun'] = float(0.00)

            if listaNueva[listado]['ntipo'] == 5:
                if(float(listaNueva[listado]['deudor']) <= 0):
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(0.00)
                    listaNueva[listado]['GanNat'] = float(0.00)
                    listaNueva[listado]['perFun'] = float(0.00)
                    listaNueva[listado]['GanFun'] = float(abs(resta))
                else:
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(0.00)
                    listaNueva[listado]['GanNat'] = float(0.00)
                    listaNueva[listado]['perFun'] = float(abs(resta))
                    listaNueva[listado]['GanFun'] = float(0.00)

            if listaNueva[listado]['ntipo'] == 6:
                listaNueva[listado]['activo'] = str(0.00)
                listaNueva[listado]['pasivo'] = str(0.00)
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0.00)
                listaNueva[listado]['GanFun'] = float(0.00)

            if listaNueva[listado]['ntipo'] == 7:
                listaNueva[listado]['activo'] = str(0.00)
                listaNueva[listado]['pasivo'] = str(0.00)
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0.00)
                listaNueva[listado]['GanFun'] = float(0.00)
            
        #for listado in range(len(listaNueva)):
        #    print(listaNueva[listado])

        for n in miconsulta:
            r = n[2]-n[3]
            a = n[0][:1]
            if r > 0:
                deu.append(abs(r))
                acre.append(0)
            else:
                acre.append(abs(r))
                deu.append(0)
            if a=='1' or a == '2' or a=='3' or a=='4' or a=='5':
                acti= acti + 1
            else:
                per = per + 1

        for n in range(len(deu)):
            if acti > n:
                activos.append(deu[n])
                pasivos.append(acre[n])
                perdidas_a.append(0)
                ganancias_a.append(0)
            else:
                activos.append(0)
                pasivos.append(0)
                perdidas_a.append(deu[n])
                ganancias_a.append(acre[n])

        for fila in range(len(listaNueva)):
            #print(listaNueva[fila], 'mostramos----------------')
            #perdidas_f.append(float(listaNueva[fila]['perFun'])) 
            perdidas_f.append(listaNueva[fila]['perFun'])
            #ganancias_f.append(float(listaNueva[fila]['GanFun'])) 
            ganancias_f.append(listaNueva[fila]['GanFun'])
            
        #convirtiendo tuplas en arrays
        tod = []
        for x in miconsulta:
            r = []
            for n in x:
                r.append(n)
            tod.append(r)
        #todo ya es igual a miconsulta

        for val in range(len(tod)):
            tod[val].append(deu[val])
            tod[val].append(acre[val])
            tod[val].append(activos[val])
            tod[val].append(pasivos[val])
            #tod[val].append(perdidas_a[val])
            #tod[val].append(ganancias_a[val])
            tod[val].append(listaNueva[val]['perNat'])
            tod[val].append(listaNueva[val]['GanNat'])
            tod[val].append(listaNueva[val]['perFun'])
            tod[val].append(listaNueva[val]['GanFun'])
        #para manadara a las otras cclass
        #----------
        #------------
        #sumatorias de arrays
        tDEUDOR = sum(deu)
        tACREEDOR = sum(acre)

        tACTIVO = sum(activos)
        tPASIVO = sum(pasivos)

        tPERDIDAS = sum(perdidas_a)
        tGANANCIAS = sum(ganancias_a)

        tfPERDIDAS = sum(perdidas_f)
        tfGANANCIAS = sum(ganancias_f)

        #Ganancias Perdidas
        tACTIVtPASIVO = tACTIVO - tPASIVO
        tPERDIDAStGANANCIA = tPERDIDAS-tGANANCIAS

        tfPERDIDAStGANANCIA = tfPERDIDAS-tfGANANCIAS
        pActivo = 0
        pPassivo =0
        pPerdidas =0
        pGananciass =0

        #sumas iguales
        sIGUALESTOTALACTIVO = 0
        sIGUALESTOTALPASIVO = 0
        sIGUALESTOTALPERDIDAS = 0
        sIGUALESTOTALGANANCIAS = 0

        sFIGUALESTOTALPERDIDAS = 0
        sFIGUALESTOTALGANANCIAS = 0

        #mensajes
        mensaje = ""
        
        #colocacion de resultados + GP SI
        if tACTIVtPASIVO <0:
            mensaje = 'PERDIDA'
            pActivo = abs(tACTIVtPASIVO)
            pPassivo = 0
            sIGUALESTOTALACTIVO = abs(tACTIVO) + abs(tACTIVtPASIVO)
            sIGUALESTOTALPASIVO = tPASIVO + 0
        else:
            mensaje = 'GANANCIA'
            pActivo = 0
            pPassivo = abs(tACTIVtPASIVO)
            sIGUALESTOTALACTIVO = tACTIVO + 0
            sIGUALESTOTALPASIVO = abs(tPASIVO) + abs(tACTIVtPASIVO)
        
        if tPERDIDAStGANANCIA <0:
            pPerdidas =  abs(tPERDIDAStGANANCIA)
            pGananciass =  0

            fPerdidas =  abs(tfPERDIDAStGANANCIA)
            fGananciass =  0
            sIGUALESTOTALPERDIDAS = abs(tPERDIDAS) + abs(tPERDIDAStGANANCIA)
            sIGUALESTOTALGANANCIAS = tGANANCIAS + 0

            sFIGUALESTOTALPERDIDAS = abs(tfPERDIDAS) + abs(tfPERDIDAStGANANCIA)
            sFIGUALESTOTALGANANCIAS = tfGANANCIAS + 0
        else:
            pPerdidas =  0
            pGananciass =  abs(tPERDIDAStGANANCIA)
            fPerdidas =  0
            fGananciass =  abs(tfPERDIDAStGANANCIA)
            sIGUALESTOTALPERDIDAS = tPERDIDAS + 0
            sIGUALESTOTALGANANCIAS = abs(tGANANCIAS) + abs(tPERDIDAStGANANCIA)

            sFIGUALESTOTALPERDIDAS = tfPERDIDAS + 0
            sFIGUALESTOTALGANANCIAS = abs(tfGANANCIAS) + abs(tfPERDIDAStGANANCIA)
        #fin colocaionde resultados 



        #pk = self.kwargs.get('id')
        context['granarray'] = tod
        context['listado'] = miconsulta
        context['tDEBITO'] = tDEBITO
        context['tCREDITO'] = tCREDITO
        context['tDEUDOR'] = tDEUDOR
        context['tACREEDOR'] = tACREEDOR
        context['tACTIVO'] = tACTIVO
        context['tPASIVO'] = tPASIVO
        context['tPERDIDAS'] = tPERDIDAS
        context['tGANACIAS'] = tGANANCIAS
        context['tfPERDIDAS'] = round(tfPERDIDAS, 2)
        context['tfGANACIAS'] = round(tfGANANCIAS, 2)
        
        context['mensaje'] = mensaje
        context['pActivo'] = pActivo
        context['pPasivo'] = pPassivo
        context['pPerdidas'] = pPerdidas
        context['pGanancias'] = pGananciass
        context['fPerdidas'] = round(fPerdidas,2)
        context['fGanancias'] = round(fGananciass, 2)

        context['siActivo'] = sIGUALESTOTALACTIVO
        context['siPasivo'] = sIGUALESTOTALPASIVO
        context['siPerdidas'] = sIGUALESTOTALPERDIDAS
        context['siGanancias'] = sIGUALESTOTALGANANCIAS
        context['sifPerdidas'] = round(sFIGUALESTOTALPERDIDAS,2)
        context['sifGanancias'] =round(sFIGUALESTOTALGANANCIAS, 2)

        return context


def funcionLibroHojaTrabajo(usuario, cmes):
    #consulta = PgDiario.objects.filter(ccod_user=usuario).filter(cmes=cmes).order_by('pgplan__ccod_cue')
    now = datetime.now()
    datetime_str = ''+cmes+'/31/'+str(now.year)[2:]+' 13:55:26'
    datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
    last_month = datetime_object - timedelta(days=0)

    #consulta = PgDiario.objects.filter(ccod_user=usuario).filter(ffechareg__month=cmes).order_by('pgplan__ccod_cue')
    consulta = PgDiario.objects.filter(ccod_user=usuario).filter(ffechareg__lte=last_month).order_by('pgplan__ccod_cue')
    listacuentas = []
    for g in consulta:
        s = 0
        for j in listacuentas:
            if g.ccod_cue.strip() == j['ccod_cue']:
                s +=1
        if s == 0:
            listacuentas.append({'ccod_cue': g.ccod_cue.strip(),'pgplan__cdsc':g.pgplan.cdsc,'ntipo':g.pgplan.ntipo, 'debito':0.0, 'credito':0.0 ,'deudor':0.0, 'acreedor':0.0, 'activo':0.0, 'pasivo':0.0, 'perNat':0.0, 'ganNat':0.0, 'perFun':0.0,'ganFun':0.0})

    for lis in listacuentas:
        for con in consulta:
            if lis['ccod_cue'] == con.ccod_cue.strip():
                lis['debito'] = float(lis['debito'])+ float(con.ndebe)
                lis['credito'] =float(lis['credito'])+ float(con.nhaber)

    for hu in listacuentas:
        resta = round(hu['debito'] - hu['credito'], 2)
        if resta > 0:
            #nuevoJson = { 'deudor' : str(abs(resta)), 'acreedor': str(0)}
            hu['deudor'] = abs(float(resta))
            hu['acreedor'] = float(0)
        else:
            #nuevoJson = {'deudor' : 0, 'acreedor': str(abs(resta))}
            hu['deudor'] = float(0)
            hu['acreedor'] = abs(float(resta))

        if hu['ntipo'] == 1:
            if(float(hu['deudor']) <= 0):
                hu['activo'] =  float(0)
                hu['pasivo'] =  abs(float(resta))
                hu['perNat'] =  float(0)
                hu['ganNat'] =  float(0)
                hu['perFun'] =  float(0)
                hu['ganFun'] =  float(0)
            else:
                hu['activo'] =  abs(float(resta))
                hu['pasivo'] =  float(0)
                hu['perNat'] =  float(0)
                hu['ganNat'] =  float(0)
                hu['perFun'] =  float(0)
                hu['ganFun'] =  float(0)

        elif hu['ntipo'] == 2:
            if(float(hu['deudor']) <= 0):
                hu['activo'] =  float(0)
                hu['pasivo'] =  abs(float(resta))
                hu['perNat'] =  float(0)
                hu['ganNat'] =  float(0)
                hu['perFun'] =  float(0)
                hu['ganFun'] =  float(0)
            else:
                hu['activo'] =  abs(float(resta))
                hu['pasivo'] =  float(0)
                hu['perNat'] =  float(0)
                hu['ganNat'] =  float(0)
                hu['perFun'] =  float(0)
                hu['ganFun'] =  float(0)

        elif hu['ntipo'] == 3:
            if(float(hu['deudor']) <= 0):
                hu['activo'] = float(0)
                #hu['pasivo'] = abs(resta)
                hu['pasivo'] = float(0)
                hu['perNat'] = float(hu['deudor'])
                hu['ganNat'] = float(hu['acreedor'])
                hu['perFun'] = float(hu['deudor'])
                hu['ganFun'] = float(hu['acreedor'])
            else:
                #hu['activo'] = abs(resta)
                hu['activo'] = float(0)
                hu['pasivo'] = float(0)
                hu['perNat'] = float(hu['deudor'])
                hu['ganNat'] = float(hu['acreedor'])
                hu['perFun'] = float(hu['deudor'])
                hu['ganFun'] = float(hu['acreedor'])
        elif hu['ntipo'] == 4:
            if(float(hu['deudor']) <= 0):
                #hu['activo'] = float(abs(resta))
                hu['activo'] = float(0)
                hu['pasivo'] = float(0)
                hu['perNat'] = float(0)
                hu['ganNat'] = float(abs(resta))
                hu['perFun'] = float(0)
                hu['ganFun'] = float(0)
            else:
                #hu['activo'] = float(abs(resta))
                hu['activo'] = float(0)
                hu['pasivo'] = float(0)
                hu['perNat'] = float(abs(resta))
                hu['ganNat'] = float(0)
                hu['perFun'] = float(0)
                hu['ganFun'] = float(0)
        elif hu['ntipo'] == 5:
            if(float(hu['deudor']) <= 0):
                hu['activo'] = float(0)
                hu['pasivo'] = float(abs(resta))
                hu['perNat'] = float(0)
                hu['ganNat'] = float(0)
                hu['perFun'] = float(0)
                hu['ganFun'] = float(abs(resta))
            else:
                hu['activo'] = float(0)
                hu['pasivo'] = float(0)
                hu['perNat'] = float(0)
                hu['ganNat'] = float(0)
                hu['perFun'] = float(abs(resta))
                hu['ganFun'] = float(0)
        elif hu['ntipo'] == 6:
            hu['activo'] = float(0)
            hu['pasivo'] = float(0)
            hu['perNat'] = float(0)
            hu['ganNat'] = float(0)
            hu['perFun'] = float(0)
            hu['ganFun'] = float(0)
        elif hu['ntipo'] == 7:
            hu['activo'] = float(0)
            hu['pasivo'] = float(0)
            hu['perNat'] = float(0)
            hu['ganNat'] = float(0)
            hu['perFun'] = float(0)
            hu['ganFun'] = float(0)


    m= {'ccod_cue': 'TOTALES', 'pgplan__cdsc':'','ntipo':0, 'debito':0.0, 'credito':0.0 ,'deudor':0.0, 'acreedor':0.0, 'activo':0.0, 'pasivo':0.0, 'perNat':0.0, 'ganNat':0.0, 'perFun':0.0,'ganFun':0.0}
    men= {'ccod_cue': 'MENSA', 'pgplan__cdsc':'', 'ntipo':0, 'debito':0.0, 'credito':0.0 ,'deudor':0.0, 'acreedor':0.0, 'activo':0.0, 'pasivo':0.0, 'perNat':0.0, 'ganNat':0.0, 'perFun':0.0,'ganFun':0.0}
    sua= {'ccod_cue': 'SUMATORIAS', 'pgplan__cdsc':'', 'ntipo':0, 'debito':0.0, 'credito':0.0 ,'deudor':0.0, 'acreedor':0.0, 'activo':0.0, 'pasivo':0.0, 'perNat':0.0, 'ganNat':0.0, 'perFun':0.0,'ganFun':0.0}
    for tmonto in listacuentas:
        m['debito'] = round(m['debito'] + tmonto['debito'],2) 
        m['credito'] = round( m['credito'] + tmonto['credito'] , 2)
        m['deudor'] = round( m['deudor'] + tmonto['deudor'] , 2)
        m['acreedor'] = round( m['acreedor'] + tmonto['acreedor'] , 2)
        m['activo'] = round( m['activo'] + tmonto['activo'] , 2)
        m['pasivo'] = round( m['pasivo'] + tmonto['pasivo'] , 2)
        m['perNat'] = round( m['perNat'] + tmonto['perNat'] , 2)
        m['ganNat'] = round( m['ganNat'] + tmonto['ganNat'] , 2)
        m['perFun'] = round( m['perFun'] + tmonto['perFun'] , 2)
        m['ganFun'] = round( m['ganFun'] + tmonto['ganFun'] , 2)
    if  m['activo'] - m['pasivo'] < 0:
        men['ccod_cue'] = 'PERDIDA'
        men['activo'] = abs(round(float(m['activo'] - m['pasivo']),2))
        men['pasivo'] = float(0)

        sua['activo'] = m['activo'] + men['activo']
        sua['pasivo'] = m['pasivo'] + men['pasivo']

    else:
        men['ccod_cue'] = 'GANANCIA'
        men['activo'] = float(0)
        men['pasivo'] = abs(round(float(m['activo'] - m['pasivo']),2))

        sua['activo'] = m['activo'] + men['activo']
        sua['pasivo'] = m['pasivo'] + men['pasivo']


    if m['perNat'] - m['ganNat'] < 0:
        men['perNat'] = abs(round(float(m['perNat'] - m['ganNat']),2))
        men['ganNat'] = float(0)

        sua['perNat'] = m['perNat'] + men['perNat']
        sua['ganNat'] = m['ganNat'] + men['ganNat']

    else:
        men['perNat'] = float(0)
        men['ganNat'] = abs(round(float(m['perNat'] - m['ganNat']),2))

        sua['perNat'] = m['perNat'] + men['perNat']
        sua['ganNat'] = m['ganNat'] + men['ganNat']

    if m['perFun'] - m['ganFun'] < 0:
        men['perFun'] = abs(round(float(m['perFun'] - m['ganFun']),2))
        men['ganFun'] = float(0)

        sua['perFun'] = m['perFun'] + men['perFun']
        sua['ganFun'] = m['ganFun'] + men['ganFun']

    else:
        men['perFun'] = float(0)
        men['ganFun'] = abs(round(float(m['perFun'] - m['ganFun']),2))

        sua['perFun'] = m['perFun'] + men['perFun']
        sua['ganFun'] = m['ganFun'] + men['ganFun']



    #print(m)
    #print(men)
    #print(sua)

    listacuentas.append(m)
    listacuentas.append(men)
    listacuentas.append(sua)

    # 'acreedor':0.0, 'activo':0.0, 'pasivo':0.0, 'perNat':0.0, 'ganNat':0.0, 'perFun':0.0,'ganFun':0.0}
    for io in listacuentas:
        io['ntipo'] = str(io['ntipo'])
        io['debito'] = str(io['debito'])
        io['credito'] = str(io['credito'])
        io['deudor'] = str(io['deudor'])
        io['acreedor'] = str(io['acreedor'])
        io['activo'] = str(io['activo'])
        io['pasivo'] = str(io['pasivo'])
        io['perNat'] = str(io['perNat'])
        io['ganNat'] = str(io['ganNat'])
        io['perFun'] = str(io['perFun'])
        io['ganFun'] = str(io['ganFun'])

    #for io in listacuentas:
    #    print(io, '55555555555555555555')
        
    return listacuentas
    
class buscarlibroHojaTrabajo(TemplateView):
    def get(self, request, *args, **kwargs):
        #debe
        usuario_id = request.GET['debe']
        #miconsulta = PgDiario.objects.values_list('ccod_cue','cglosa2').filter(ccod_cli=usuario_id).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber'))
        miconsulta = PgDiario.objects.all().filter(ccod_user=usuario_id).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber'))
        #for n in miconsulta:
        #    print(n.ccod_cue, str(n.ndebe), str(n.nhaber))
        asientos =  PgDiario.objects.filter(ccod_user=usuario_id).order_by('-nasiento')
        data = serializers.serialize('json',miconsulta,fields=('pgplan__ccod_cue','pgplan__cdsc'))
        return HttpResponse(data,content_type='application/json')

#clase principal estado finaciero
class DatosEstado():

    def listaValidos(self):
        validos = ['A000', 'A100', 'A105', 'A110', 'A115', 'A125', 'A130', 'A135', 'A140', 'A200', 'A210', 'A220', 'A225', 'A230', 'A240', 'A250', 'A999', 'N005', 'N010', 'N099', 'N154', 'N205', 'N210', 'N305', 'N310', 'N350', 'N405', 'N410', 'N499', 'N999', 'P000', 'P100', 'P105', 'P107', 'P110', 'P120', 'P122', 'P125', 'P199', 'P500', 'P505', 'P520', 'P525', 'P535', 'P540', 'P599', 'P999']
        validosPlan = PgPlan.objects.filter(ccod_bal__in = validos)
        #print(validosPlan)
        #return validos

    def consulta(sef,miusuario):
        #miconsulta = PgDiario.objects.values_list('ccod_cue','cglosa2').filter(ccod_cli=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('ccod_cue')
        #miconsulta = PgDiario.objects.values_list('ccod_cue','cpernum').exclude(cpernum='P115').exclude(cpernum='P128').exclude(cpernum='A120').filter(ccod_cli=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('ccod_cue')
        miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__ccod_bal2').filter(ccod_user=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        tDEBITO = 0
        for x in miconsulta:
            tDEBITO = tDEBITO + x[2]
        tCREDITO =0
        for x in miconsulta:
            tCREDITO = tCREDITO + x[3]

        deu = []
        acre =[]
        activos = []
        pasivos = []
        perdidas_a = []
        ganancias_a = []
        acti = 0
        per = 0
        for n in miconsulta:
            r = n[2]-n[3]
            a = n[0][:1]
            if r > 0:
                deu.append(abs(r))
                acre.append(0)
            else:
                acre.append(abs(r))
                deu.append(0)
            if a=='1' or a == '2' or a=='3' or a=='4' or a=='5':
                acti= acti + 1
            else:
                per = per + 1

        for n in range(len(deu)):
            if acti > n:
                activos.append(deu[n])
                pasivos.append(acre[n])
                perdidas_a.append(0)
                ganancias_a.append(0)
            else:
                activos.append(0)
                pasivos.append(0)
                perdidas_a.append(deu[n])
                ganancias_a.append(acre[n])
        #convirtiendo tuplas en arrays
        tod = []
        for x in miconsulta:
            r = []
            for n in x:
                r.append(n)
            tod.append(r)
        #todo ya es igual a miconsulta
        for val in range(len(tod)):
            tod[val].append(deu[val])
            tod[val].append(acre[val])
            tod[val].append(activos[val])
            tod[val].append(pasivos[val])
            tod[val].append(perdidas_a[val])
            tod[val].append(ganancias_a[val])
        return tod

    def planconsulta(self,miusuario):
        qr = PgDiario.objects.values_list('pgplan__ccod_bal2').filter(ccod_user=miusuario).distinct()
        qr2 = PgPlan.objects.values_list('ccod_bal2').distinct()
        qr3 = PgFormato2.objects.values_list('ccod_bal').distinct()
        envio = []
        for g in range(len(qr3)):
            envio.append(str(qr3[g][0]).strip())
        return envio

    #funcon quie pasa los datos al estado finasciero
    def arrayDatos(self,pk):
        qr = PgFormato2.objects.values_list('ccod_bal', 'cdsc')
        m= self.consulta(pk)
        n = self.planconsulta(pk)

        

        nuevaarry= []
        nuevaarrN = []
        a1=[]
        a2=[]
        p1=[]
        p5=[]
        clasen=[]
        #cuentas no N
        for data in m:
            f =[]
            #print(data[0], data[2:])
            if len(data[1].strip()) >=3:
                f.append(data[1].strip())
                f.append(data[6]+data[7])
                nuevaarry.append(f)

        

        #sumatorias
        for suma in range(len(nuevaarry)):
            if nuevaarry[suma][0][:2]=="A1":
                a1.append(nuevaarry[suma][1])
            elif nuevaarry[suma][0][:2]=="A2":
                a2.append(nuevaarry[suma][1])
            elif nuevaarry[suma][0][:2]=="P1":
                p1.append(nuevaarry[suma][1])
            elif nuevaarry[suma][0][:2]=="P5":
                p5.append(nuevaarry[suma][1])
            
            
        sn = 0
        for datab in m:
            if datab[1][:1] == "N":
                sn = sn + (datab[8]+datab[9])



        #print(a1,"a1")
        #print(a2,"a2")
        #print(p1,"p1")
        #print(p5,"p5")
        
        #print(clasen)

        #for gh in nuevaarry:
        #    print(gh)


        data = {}
        ultimoarraY = []
        ultimoarraz = []
        for hu in n:
            s= 0
            for ma in range(len(nuevaarry)):
                if hu == nuevaarry[ma][0]:
                    #print("igual "+ str(hu)+" "+ str(nuevaarry[m][0]))
                    s= s+nuevaarry[ma][1]
                else:
                    s = s
            #data['id'] = hu
            #data.update( {'id' : hu} )
            ultimoarraY.append(hu)
            ultimoarraz.append(str(s))

        ultimoarraY.append("TAC")
        ultimoarraz.append(str(sum(a1)))

        ultimoarraY.append("TANC")
        ultimoarraz.append(str(sum(a2)))

        ultimoarraY.append("TP")
        ultimoarraz.append(str(sum(p1)))

        ultimoarraY.append("TPN")
        ultimoarraz.append( str(sum(p5)))

        ultimoarraY.append("TTA")
        ultimoarraz.append(str(sum(a1) + sum(a2)))        


        ultimoarraY.append("RE")
        ultimoarraz.append(str(sn))

        ultimoarraY.append("TTP")
        ultimoarraz.append(str(sum(p1) + sum(p5)+ sn))

        j = dict( zip( ultimoarraY, ultimoarraz))
        #print(j)
        rqs=[]
        for gt in range(len(qr)):
            g = []
            g.append(qr[gt][0].strip())
            g.append(qr[gt][1].strip())
            rqs.append(g)

        #return ultimoarraY
        
        return j

#segunda clase estado financiero
class datosEstadoFinanciero():
    def diarioPlanes(self,miusuario):
        qr3 = PgFormato2.objects.values_list('ccod_bal').distinct()
        envio = []
        #datos de la tabla pg_plan
        for g in range(len(qr3)):
            envio.append(str(qr3[g][0]).strip())
        

        con = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__ccod_balr','pgplan__ccod_bal2','pgplan__ccod_baln2').filter(ccod_user=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')


        #for dt in con:
        #    print(dt)

        granlista=[]
        for t in con:
            r=[]
            for c in t:
                r.append(c)
            if t[0][:1]=='1' or t[0][:1]=='2' or t[0][:1]=='3':
                if t[4] > t[5]:
                    r.append(t[2])
                    r.append(t[4])
                    r.append(abs(t[4]-t[5]))
                else:
                    if t[3].strip():
                        #condicon si ccod_baln2 no existe
                        r.append(t[3]) #cambie de 3 a 2
                        r.append(t[5])
                        r.append(abs(t[4]-t[5]))
                    else:
                        r.append(t[2])
                        r.append(t[5] * (-1))
                        r.append(abs(t[4]-t[5]) *(-1))
                #r.append('uno')
            elif t[0][:1]=='4' or t[0][:1]=='5':
                if t[5] > t[4]:
                    r.append(t[2])
                    r.append(t[5])
                    r.append(abs(t[4]-t[5]))
                else:
                    if t[3].strip():
                        #condicon si ccod_baln2 no existe
                        r.append(t[3]) # cambie de tre a dos
                        r.append(t[4])
                        r.append(abs(t[4]-t[5]))
                    else:
                        r.append(t[2])
                        r.append(t[4]*(-1))
                        r.append(abs(t[4]-t[5]) * (-1))
                #r.append('cua')
            elif t[0][:1]=='6' or t[0][:1]=='7':
                if t[4]> t[5]:
                    r.append(t[2])
                    r.append(t[4]*(-1))
                    r.append(abs(t[4]-t[5]) *(-1))
                else:
                    r.append(t[2])
                    r.append(t[5])
                    r.append(abs(t[4]-t[5]))
            elif t[0][:1]=='9':
                if t[4]> t[5]:
                    r.append(t[2])
                    r.append(t[4]*(-1))
                    r.append(abs(t[4]-t[5]) *(-1))
                else:
                    r.append(t[2])
                    r.append(t[5])
                    r.append(abs(t[4]-t[5]))
            granlista.append(r)
        
        '''
        for dat in granlista:
            
            if len(dat)==6:
                dat.append(dat[2])
                dat.append(dat[4])
                dat.append(dat[5])
        '''    

        #for dat in granlista:
        #    print(dat)

        ultimoarraY = []
        ultimoarraz = []
        for hu in envio:
            s= 0
            for ma in range(len(granlista)):
                if hu == granlista[ma][6]:
                    #print("igual "+ str(hu)+" "+ str(granlista[ma][6]))
                    s= s+granlista[ma][8]
                else:
                    s = s
            #data['id'] = hu
            #data.update( {'id' : hu} )
            ultimoarraY.append(hu)
            ultimoarraz.append(str(s))
        
        
        
        soloAs=[]
        '''
        a1=[]
        a2=[]
        p1=[]
        p5=[]
        n0=[]
        n1=[]
        '''
        sa=[]
        sp=[]
        sn=[]
        sf=[]

        a1=[]
        a2=[]

        p1=[]
        p2=[]
        p3=[]
        p4=[]
        p5=[]
        p6=[]
        p7=[]
        p8=[]
        p9=[]

        f1=[]
        f2=[]
        f3=[]
        f4=[]
        f5=[]
        f6=[]
        f7=[]
        f8=[]
        f9=[]

        n0=[]
        n1=[]

        for suma in range(len(granlista)):
            #print(granlista[suma], 'los datos printcipales')

            if granlista[suma][6][:1]=="A":
               sa.append(float(granlista[suma][8]))#se agrego floats
            if granlista[suma][6][:1]=="P":
               sp.append(float(granlista[suma][8]))#se agrego floats
            if granlista[suma][6][:1]=="N":
               sn.append(float(granlista[suma][8]))#se agrego floats
            if granlista[suma][6][:1]=="F":
               sf.append(float(granlista[suma][8]))#se agrego floats


            if granlista[suma][6][:2]=="A1":
                a1.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="A2":
                a2.append(granlista[suma][8])

            elif granlista[suma][6][:2]=="P1":
                p1.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P2":
                p2.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P3":
                p3.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P4":
                p4.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P5":
                p5.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P6":
                p6.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P7":
                p7.append(float(granlista[suma][8]))#se agrego floats
            elif granlista[suma][6][:2]=="P8":
                p8.append(float(granlista[suma][8]))#se agrego floats
            elif granlista[suma][6][:2]=="P9":
                p9.append(float(granlista[suma][8]))#se agrego floats

            elif granlista[suma][6][:2]=="F1" or granlista[suma][1][:1]=="F1":
                f1.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F2" or granlista[suma][1][:2]=="F2":
                f2.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F3" or granlista[suma][1][:2]=="F3":
                f3.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F4" or granlista[suma][1][:2]=="F4":
                f4.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F5" or granlista[suma][1][:2]=="F5":
                f5.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F6" or granlista[suma][1][:2]=="F6":
                f6.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F7" or granlista[suma][1][:2]=="F7":
                f7.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F8" or granlista[suma][1][:2]=="F8":
                f8.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F9" or granlista[suma][1][:2]=="F9":
                f9.append(float(granlista[suma][8]))

            elif granlista[suma][6][:2]=="N0":
                n0.append(granlista[suma][8])
            else:
                n1.append(granlista[suma][8])
        '''
        print(a1,"a1----diario planes")
        print(a2,"a2----diario planes")

        print(p1,"p1----diario planes")
        print(p2,"p2----diario planes")
        print(p3,"p3----diario planes")
        print(p4,"p4----diario planes")
        print(p5,"p5----diario planes")
        print(p6,"p6----diario planes")
        print(p7,"p7----diario planes")
        print(p8,"p8----diario planes")
        print(p9,"p9----diario planes")

        print(f1,"f1----diario planes")
        print(f2,"f2----diario planes")
        print(f3,"f3----diario planes")
        print(f4,"f4----diario planes")
        print(f5,"f5----diario planes")
        print(f6,"f6----diario planes")
        print(f7,"f7----diario planes")
        print(f8,"f8----diario planes")
        print(f9,"f9----diario planes")

        print(sa,"SAA----")
        print(sp,"SPP----")
        print(sn,"SNN----")
        print(sf,"SFF----")

        print(n0,"n0----diario planes")
        print(n1,"n1----diario planes")
        '''
        lasenens = sum(n0) + sum(n1)
        totaPatrimonoNeto = float(lasenens) + float(sum(p5)) + float(sum(p4)) + float(sum(p8)) + float(sum(p1))
        TPPN = float(sum(p1)) + float(totaPatrimonoNeto)
        TOTALPASIVOSCORRIENTES = float(sum(p1)) + float(sum(p2))
        TOTALPASIVO = float(sum(p4))
        TOTALPATRIMONIO = float(lasenens) + float(sum(p5)) + float(sum(p4)) + float(sum(p8)) # se esta agragado float
        TOTALPASIVOYPATRIMONIONETO   = TOTALPATRIMONIO + TOTALPASIVO + TOTALPASIVOSCORRIENTES

        ultimoarraY.append("TAC")
        ultimoarraz.append(str(sum(a1)))

        ultimoarraY.append("TANC")
        ultimoarraz.append(str(sum(a2)))

        ultimoarraY.append("TP")
        ultimoarraz.append(str(sum(p1) + sum(p2)))

        ultimoarraY.append("TPN")
        ultimoarraz.append( str(totaPatrimonoNeto))

        ultimoarraY.append("TTA")
        ultimoarraz.append(str(sum(a1) + sum(a2)))        


        ultimoarraY.append("RE")
        ultimoarraz.append(str(lasenens))

        ultimoarraY.append("TTP")
        ultimoarraz.append(str(TPPN))

        ultimoarraY.append("UB")
        ultimoarraz.append(str(sum(sn)))

        ultimoarraY.append("RO")
        ultimoarraz.append(str(sum(n1)))

        ultimoarraY.append("REI")
        ultimoarraz.append(str(sum(n1)+ sum(n0)))

        ultimoarraY.append("TOTALPASIVOSCORRIENTES")
        ultimoarraz.append(str(TOTALPASIVOSCORRIENTES))

        ultimoarraY.append("TOTALPASIVO")
        ultimoarraz.append(str(TOTALPASIVO))

        ultimoarraY.append("TOTALPATRIMONIO")
        ultimoarraz.append(str(TOTALPATRIMONIO))

        ultimoarraY.append("TOTALPASIVOYPATRIMONIONETO")
        ultimoarraz.append(str(TOTALPASIVOYPATRIMONIONETO))

        j = dict( zip( ultimoarraY, ultimoarraz))
        #print(j)
        
        return j

    
    def diarioPlanesb(self,miusuario):
        qr3 = PgFormato2.objects.values_list('ccod_bal').distinct()
        envio = []
        #datos de la tabla pg_plan
        for g in range(len(qr3)):
            envio.append(str(qr3[g][0]).strip())
        
        con = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__ccod_balr','pgplan__ccod_bal2','pgplan__ccod_baln2').filter(ccod_user=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')

        granlista=[]
        for t in con:
            r=[]
            for c in t:
                r.append(c)
            if t[0][:1]=='1' or t[0][:1]=='2' or t[0][:1]=='3':
                if t[4] > t[5]:
                    r.append(t[2])
                    r.append(t[4])
                    r.append(abs(t[4]-t[5]))
                else:
                    if t[3].strip():
                        #condicon si ccod_baln2 no existe
                        r.append(t[3]) # cambie de 3  a dos
                        r.append(t[5])
                        r.append(abs(t[4]-t[5]))
                    else:
                        r.append(t[2])
                        r.append(t[5] * (-1))
                        r.append(abs(t[4]-t[5]) *(-1))
                #r.append('uno')
            elif t[0][:1]=='4' or t[0][:1]=='5':
                if t[5] > t[4]:
                    r.append(t[2])
                    r.append(t[5])
                    r.append(abs(t[4]-t[5]))
                else:
                    if t[3].strip():
                        #condicon si ccod_baln2 no existe
                        r.append(t[3])
                        r.append(t[4])
                        r.append(abs(t[4]-t[5]))
                    else:
                        r.append(t[2])
                        r.append(t[4]*(-1))
                        r.append(abs(t[4]-t[5]) * (-1))
                #r.append('cua')
            elif t[0][:1]=='6' or t[0][:1]=='7':
                if t[4]> t[5]:
                    r.append(t[2])
                    r.append(t[4]*(-1))
                    r.append(abs(t[4]-t[5]) *(-1))
                else:
                    r.append(t[2])
                    r.append(t[5])
                    r.append(abs(t[4]-t[5]))
            elif t[0][:1]=='9':
                if t[4]> t[5]:
                    r.append(t[2])
                    r.append(t[4]*(-1))
                    r.append(abs(t[4]-t[5]) *(-1))
                else:
                    r.append(t[2])
                    r.append(t[5])
                    r.append(abs(t[4]-t[5]))


            granlista.append(r)
        
        '''
        for dat in granlista:
            
            if len(dat)==6:
                dat.append(dat[2])
                dat.append(dat[4])
                dat.append(dat[5])

            
            if len(dat) < 9:
                falta = 9 - len(dat)
                for x in range(falta):
                    dat.append("{:.{}f}".format( 0, 2 )) #se agraga formato decimal si falta espacios
        '''

        #for datu in granlista:
        #    print(datu,len(datu) ,'esta es la gran lista j')


        ultimoarraY = []
        ultimoarraz = []
        for hu in envio:
            s= 0
            for ma in range(len(granlista)):
                if hu == granlista[ma][6]:
                    #print("igual "+ str(hu)+" "+ str(granlista[ma][6]))
                    s= s+float(granlista[ma][8])#se cambio a decimal
                elif hu == granlista[ma][1]:
                    s= s+float(granlista[ma][8])
                else:
                    s = s
            #data['id'] = hu
            #data.update( {'id' : hu} )
            ultimoarraY.append(hu)
            ultimoarraz.append(str(s))
        
        
        
        sa=[]
        sp=[]
        sn=[]
        sf=[]

        a1=[]
        a2=[]

        p1=[]
        p2=[]
        p3=[]
        p4=[]
        p5=[]
        p6=[]
        p7=[]
        p8=[]
        p9=[]

        f1=[]
        f2=[]
        f3=[]
        f4=[]
        f5=[]
        f6=[]
        f7=[]
        f8=[]
        f9=[]


        n0=[]
        n1=[]

        for suma in range(len(granlista)):
            #print(granlista[suma][6],granlista[suma][8],'dentro de suma', suma)
            if granlista[suma][6][:1]=="A":
               sa.append(float(granlista[suma][8]))#se agrego floats
            if granlista[suma][6][:1]=="P":
               sp.append(float(granlista[suma][8]))#se agrego floats
            if granlista[suma][6][:1]=="N":
               sn.append(float(granlista[suma][8]))#se agrego floats
            if granlista[suma][6][:1]=="F":
               sf.append(float(granlista[suma][8]))#se agrego floats

            if granlista[suma][6][:2]=="A1":
                a1.append(float(granlista[suma][8]))#se agrego floats
            elif granlista[suma][6][:2]=="A2":
                a2.append(float(granlista[suma][8]))#se agrego floats

            elif granlista[suma][6][:2]=="P1":
                p1.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P2":
                p2.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P3":
                p3.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P4":
                p4.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P5":
                p5.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P6":
                p6.append(granlista[suma][8])
            elif granlista[suma][6][:2]=="P7":
                p7.append(float(granlista[suma][8]))#se agrego floats
            elif granlista[suma][6][:2]=="P8":
                p8.append(float(granlista[suma][8]))#se agrego floats
            elif granlista[suma][6][:2]=="P9":
                p9.append(float(granlista[suma][8]))#se agrego floats

            elif granlista[suma][6][:2]=="F1" or granlista[suma][1][:1]=="F1":
                f1.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F2" or granlista[suma][1][:2]=="F2":
                f2.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F3" or granlista[suma][1][:2]=="F3":
                f3.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F4" or granlista[suma][1][:2]=="F4":
                f4.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F5" or granlista[suma][1][:2]=="F5":
                f5.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F6" or granlista[suma][1][:2]=="F6":
                f6.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F7" or granlista[suma][1][:2]=="F7":
                f7.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F8" or granlista[suma][1][:2]=="F8":
                f8.append(float(granlista[suma][8]))
            elif granlista[suma][6][:2]=="F9" or granlista[suma][1][:2]=="F9":
                f9.append(float(granlista[suma][8]))

            elif granlista[suma][6][:1]=="N":
                n0.append(float(granlista[suma][8]))#se agrego floats
            else:
                n1.append(float(granlista[suma][8]))#se agrego floats
        '''
        print(a1,"a1----")
        print(a2,"a2----")

        print(p1,"p1----diario planesb")
        print(p2,"p2----diario planesb")
        print(p3,"p3----diario planesb")
        print(p4,"p4----diario planesb")
        print(p5,"p5----diario planesb")
        print(p6,"p6----diario planesb")
        print(p7,"p7----diario planesb")
        print(p8,"p8----diario planesb")
        print(p9,"p9----diario planesb")

        print(f1,"f1----diario planesb")
        print(f2,"f2----diario planesb")
        print(f3,"f3----diario planesb")
        print(f4,"f4----diario planesb")
        print(f5,"f5----diario planesb")
        print(f6,"f6----diario planesb")
        print(f7,"f7----diario planesb")
        print(f8,"f8----diario planesb")
        print(f9,"f9----diario planesb")

        print(sa,"SAA----")
        print(sp,"SPP----")
        print(sn,"SNN----")
        print(sf,"SFF----")


        print(n0,"n0------")
        print(n1,"n1------")
        '''
        #lasenens = float(sum(n0)) + float(sum(n1)) #se esta gargardo float
        lasenens = sum(sn)
        totaPatrimonoNeto = lasenens + float(sum(p5)) + float(sum(p4)) + float(sum(p8)) # se esta agragado float
        TPPN = float(sum(p1)) + float(totaPatrimonoNeto)  #se agrego floats
        TOTALPASIVOSCORRIENTES = float(sum(p1)) + float(sum(p2))
        TOTALPASIVO = float(sum(p4))
        TOTALPATRIMONIO = lasenens + float(sum(p5)) + float(sum(p4)) + float(sum(p8)) # se esta agragado float
        TOTALPASIVOYPATRIMONIONETO   = TOTALPATRIMONIO + TOTALPASIVO + TOTALPASIVOSCORRIENTES

        ultimoarraY.append("TAC")
        ultimoarraz.append(str(sum(a1)))

        ultimoarraY.append("TANC")
        ultimoarraz.append(str(sum(a2)))

        ultimoarraY.append("TP")
        ultimoarraz.append(str(sum(p1)))

        ultimoarraY.append("TP4")
        ultimoarraz.append(str(sum(p4)))

        ultimoarraY.append("TP8")
        ultimoarraz.append(str(sum(p8)))

        ultimoarraY.append("TPN")
        ultimoarraz.append( str(totaPatrimonoNeto))

        ultimoarraY.append("TTA")
        #ultimoarraz.append(str(sum(a1) + sum(a2)))        
        ultimoarraz.append(str(round(sum(sa),2)))


        ultimoarraY.append("RE")
        ultimoarraz.append(str(lasenens))

        ultimoarraY.append("TTP")
        ultimoarraz.append(str(TPPN))

        ultimoarraY.append("UB")
        ultimoarraz.append(str(sum(sn)))

        ultimoarraY.append("RO")
        ultimoarraz.append(str(sum(n1)))

        ultimoarraY.append("REI")
        ultimoarraz.append(str(sum(n1)+ sum(n0)))

        ultimoarraY.append("TOTALPASIVOSCORRIENTES")
        ultimoarraz.append(str(TOTALPASIVOSCORRIENTES))

        ultimoarraY.append("TOTALPASIVO")
        ultimoarraz.append(str(TOTALPASIVO))

        ultimoarraY.append("TOTALPATRIMONIO")
        ultimoarraz.append(str(TOTALPATRIMONIO))

        ultimoarraY.append("TOTALPASIVOYPATRIMONIONETO")
        ultimoarraz.append(str(TOTALPASIVOYPATRIMONIONETO))

        

        j = dict( zip( ultimoarraY, ultimoarraz))
        

        midictionario = []
        for dt in range(len(ultimoarraY)):
            midictionario.append({'ccod_cue':ultimoarraY[dt],'valor':ultimoarraz[dt]})

        #for f in midictionario:
        #    print(f)

        
        return midictionario


    def diarioPlanesc(self, miusuario,cmes, proposito):
        
        listaCcod_bal = PgFormato2.objects.all().filter(usuario=miusuario).order_by('ccod_bal')
        lista_ccod_bal= []
        for lista in listaCcod_bal:
            m = {'ccod_bal': lista.ccod_bal,'cdsc': lista.cdsc ,'monto': float(0.0)}
            lista_ccod_bal.append(m)
        
        #con = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__ccod_balr','pgplan__ccod_bal2','pgplan__ccod_baln2').filter(ccod_user=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        now = datetime.now()
        datetime_str = ''+cmes+'/28/'+str(now.year)[2:]+' 13:55:26'
        #datetime_str = '09/19/18 13:55:26'
        datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
        last_month = datetime_object - timedelta(days=0)

        if proposito == 0:
            consulta = PgDiario.objects.filter(ccod_user = miusuario).filter(ffechareg__month=cmes).order_by('pgplan__ccod_cue')
        else:
            consulta = PgDiario.objects.filter(ccod_user = miusuario).filter(ffechareg__lte=last_month).order_by('pgplan__ccod_cue')

        #consulta = PgDiario.objects.filter(ccod_user = miusuario).order_by('pgplan__ccod_cue')
        listaConpleta = []
       
        for consu in consulta:
            n = {'ccod_cue':consu.pgplan.ccod_cue,'ccod_balr':consu.pgplan.ccod_balr, 'ccod_bal2':consu.pgplan.ccod_bal2,'ccod_baln2':consu.pgplan.ccod_baln2,'ndebe':0.0,'nhaber':0.0, 'newCcod_bal':'', 'newMonto':0.0}
            if n not in listaConpleta:
                listaConpleta.append(n)

        j=0
        for cons in consulta:
            #print(cons.pgplan.ccod_cue,'|',cons.pgplan.ccod_balr,'|',cons.pgplan.ccod_bal2,'|',cons.pgplan.ccod_baln2,'|',cons.ndebe,'|',cons.nhaber,'|', 'consulta c ------------------', j)
            for consul in listaConpleta:
                if cons.pgplan.ccod_cue == consul['ccod_cue']:
                    consul['ndebe'] = consul['ndebe'] + float(cons.ndebe)
                    consul['nhaber'] = consul['nhaber'] + float(cons.nhaber)
            #j +=1 
            #print(lista.ccod_bal,'diarioPlanesc')
            #envio.append(str(qr3[g][0]).strip())
        ##
        ## lista que se obtine datos de los ccod_bal
        ##
        for listac in listaConpleta:
            if listac['ccod_cue'][:1] == '1' or listac['ccod_cue'][:1] == '2' or listac['ccod_cue'][:1] == '3':
                if float(listac['ndebe']) > float(listac['nhaber']):
                    listac['newCcod_bal'] = listac['ccod_bal2']
                    listac['newMonto'] = round(abs(float(listac['ndebe'])-float(listac['nhaber'])),2)
                else:
                    if len(listac['ccod_baln2'].strip()) >=1:
                        listac['newCcod_bal'] = listac['ccod_baln2']
                        listac['newMonto'] = round(abs(float(listac['ndebe'])-float(listac['nhaber'])),2)
                    else:
                        listac['newCcod_bal'] = listac['ccod_bal2']
                        listac['newMonto'] = round(abs(float(listac['ndebe'])-float(listac['nhaber'])) *(-1), 2)

            elif listac['ccod_cue'][:1] == '4' or listac['ccod_cue'][:1] == '5':
                if float(listac['nhaber']) > float(listac['ndebe']):
                    listac['newCcod_bal'] = listac['ccod_bal2']
                    listac['newMonto'] = round(abs(float(listac['ndebe'])-float(listac['nhaber'])), 2)
                else:
                    if len(listac['ccod_baln2'].strip()) >=1:
                        listac['newCcod_bal'] = listac['ccod_baln2']
                        listac['newMonto'] = round(abs(float(listac['ndebe'])-float(listac['nhaber'])), 2)
                    else:
                        listac['newCcod_bal'] = listac['ccod_bal2']
                        listac['newMonto'] = round(abs(float(listac['ndebe'])-float(listac['nhaber'])) *(-1),2)

            elif listac['ccod_cue'][:1] == '6' or listac['ccod_cue'][:1] == '7' or listac['ccod_cue'][:1] == '9':
                if float(listac['ndebe']) > float(listac['nhaber']):
                    listac['newCcod_bal'] = listac['ccod_bal2']
                    listac['newMonto'] = round(abs(float(listac['ndebe'])-float(listac['nhaber'])) *(-1) ,2)
                else:
                    listac['newCcod_bal'] = listac['ccod_bal2']
                    listac['newMonto'] = round(abs(float(listac['ndebe'])-float(listac['nhaber'])), 2)


        '''
        conta = 0
        for listau in listaConpleta:
            print(listau, '$', conta)
            conta +=1
        '''
        ## from this point to next is personalize 
        contab = 0
        sa123 = 0.0
        sa56 = 0.0
        n0 = 0.0 
        n11 = 0.0
        n22 = 0.0
        n33 = 0.0
        n44 = 0.0
        n55 = 0.0
        n88 = 0.0
        f01 = 0.0
        f22 = 0.0
        f34 = 0.0
        p0123 = 0.0
        p44 = 0.0
        p88 = 0.0
        for lista_ in lista_ccod_bal:
            for listab_ in listaConpleta:
                #print(lista_['ccod_bal'],lista_['monto'],'-',listab_['newCcod_bal'],listab_['newMonto'],'--')                
                if lista_['ccod_bal'] == listab_['newCcod_bal']:
                    lista_['monto'] =  float(lista_['monto']) + float(listab_['newMonto'])
                if lista_['ccod_bal'] == listab_['ccod_balr']:
                    lista_['monto'] = float(lista_['monto']) + float(listab_['newMonto'])
                    #print(lista_['ccod_bal'],float(lista_['monto']),'-',listab_['newCcod_bal'],float(listab_['newMonto']),'--')
            if lista_['ccod_bal'][:2] == "A1" or lista_['ccod_bal'][:2] == "A2":
                sa123 = round(sa123,2)+ float(lista_['monto'])
                #print(lista_['ccod_bal'][:2], sa123, 'pp')
            if lista_['ccod_bal'][:2] == "A4" or lista_['ccod_bal'][:2] == "A5"or lista_['ccod_bal'][:2] == "A6":
                sa56 = round(sa56,2)+ float(lista_['monto'])
                #print(lista_['ccod_bal'][:2], sa56, 'pp')
            if lista_['ccod_bal'][:2] == "N0":
                n0 = round(n0,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "N1":
                n11 = round(n11,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "N2":
                n22 = round(n22,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "N3":
                n33 = round(n33,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "N4":
                n44 = round(n44,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "N5":
                n55 = round(n55,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "N8":
                n88 = round(n88,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "F0" or lista_['ccod_bal'][:2] == "F1":
                f01 = round(f01,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "F2":
                f22 = round(f22,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "F3" or lista_['ccod_bal'][:2] == "F4":
                f34 = round(f34,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "P0" or lista_['ccod_bal'][:2] == "P1" or lista_['ccod_bal'][:2] == "P2" or lista_['ccod_bal'][:2] == "P3":
                p0123 = round(p0123,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "P4":
                p44 = round(p44,2)+ float(lista_['monto'])
            if lista_['ccod_bal'][:2] == "P8":
                p88 = round(p88,2)+ float(lista_['monto'])
            

        
        for listas_ in lista_ccod_bal:
            if listas_['ccod_bal'] == "A399":
                listas_['monto'] = sa123
            if listas_['ccod_bal'] == "A599":
                listas_['monto'] = sa56
            if listas_['ccod_bal'] == "A999":
                listas_['monto'] = sa123 + sa56
            if listas_['ccod_bal'] == "N099":
                listas_['monto'] = round(n0,2)
            if listas_['ccod_bal'] == "N199":
                listas_['monto'] = round(n11 + n0,2)
            if listas_['ccod_bal'] == "N299":
                listas_['monto'] = round(n22 + n11 + n0,2)
            if listas_['ccod_bal'] == "N399":
                listas_['monto'] = round(n33 + n22 + n11 + n0,2 )
            if listas_['ccod_bal'] == "N499":
                listas_['monto'] = round(n44 + n33 + n22 + n11 + n0,2 )
            if listas_['ccod_bal'] == "N599":
                listas_['monto'] = round(n55 +n44 + n33 + n22 + n11 + n0,2 )
            if listas_['ccod_bal'] == "N999":
                listas_['monto'] = round(n0 + n11 + n22 + n33 + n44 + n55 + n88,2)
            if listas_['ccod_bal'] == "P835":
                listas_['monto'] = round(n0 + n11 + n22 + n33 + n44 + n55 + n88,2)
            if listas_['ccod_bal'] == "F199":
                listas_['monto'] = round(f01,2)
            if listas_['ccod_bal'] == "F299":
                listas_['monto'] = round(f22,2) + round(f01,2)
            if listas_['ccod_bal'] == "F699":
                listas_['monto'] = round(f34,2) + round(f22,2) + round(f01,2)
            if listas_['ccod_bal'] == "F799":
                listas_['monto'] = round(f34,2) + round(f22,2) + round(f01,2)
            if listas_['ccod_bal'] == "F999":
                listas_['monto'] = round(f01 + f22+ f34,2)
            if listas_['ccod_bal'] == "P399":
                listas_['monto'] = round(p0123,2)
            if listas_['ccod_bal'] == "P499":
                listas_['monto'] = round(p44,2)
            if listas_['ccod_bal'] == "P699":
                listas_['monto'] = round(p0123+ p44,2)
            if listas_['ccod_bal'] == "P899":
                listas_['monto'] = round(p88+ n0 + n11 + n22 + n33 + n44 + n55 + n88 ,2)
            if listas_['ccod_bal'] == "P999":
                listas_['monto'] = round(p88+ n0 + n11 + n22 + n33 + n44 + n55 + n88 + p44 + p0123,2)

        
        for lit_ in lista_ccod_bal:
            print(lit_, '##', contab)
            contab += 1
    
        return lista_ccod_bal
     

class libroEEFF(TemplateView):
    model = PgFormato2
    template_name = 'adminis/eeff.html'

    def basicConsulta(self,miusuario):
        miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__ccod_bal2').filter(ccod_user=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        #miconsultab = PgDiario.objects.values_list('cpernum').filter(ccod_cli=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('cpernum')        
        

        return miconsulta     

    def consulta(sef,miusuario):
        #miconsulta = PgDiario.objects.values_list('ccod_cue','cglosa2').filter(ccod_cli=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('ccod_cue')
        miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__ccod_bal2').filter(ccod_user=miusuario).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        tDEBITO = 0
        for x in miconsulta:
            tDEBITO = tDEBITO + x[2]
        tCREDITO =0
        for x in miconsulta:
            tCREDITO = tCREDITO + x[3]


        deu = []
        acre =[]
        activos = []
        pasivos = []
        perdidas_a = []
        ganancias_a = []
        acti = 0
        per = 0
        for n in miconsulta:
            r = n[2]-n[3]
            a = n[0][:1]
            if r > 0:
                deu.append(abs(r))
                acre.append(0)
            else:
                acre.append(abs(r))
                deu.append(0)
            if a=='1' or a == '2' or a=='3' or a=='4' or a=='5':
                acti= acti + 1
            else:
                per = per + 1

        for n in range(len(deu)):
            if acti > n:
                activos.append(deu[n])
                pasivos.append(acre[n])
                perdidas_a.append(0)
                ganancias_a.append(0)
            else:
                activos.append(0)
                pasivos.append(0)
                perdidas_a.append(deu[n])
                ganancias_a.append(acre[n])
        #convirtiendo tuplas en arrays
        tod = []
        for x in miconsulta:
            r = []
            for n in x:
                r.append(n)
            tod.append(r)
        #todo ya es igual a miconsulta
        for val in range(len(tod)):
            tod[val].append(deu[val])
            tod[val].append(acre[val])
            tod[val].append(activos[val])
            tod[val].append(pasivos[val])
            tod[val].append(perdidas_a[val])
            tod[val].append(ganancias_a[val])
        return tod

    def planconsulta(self,miusuario):
        #qr = PgDiario.objects.values_list('cpernum').filter(ccod_cli=miusuario).exclude(cpernum="-").distinct()
        #qr2 = PgPlan.objects.values_list('ccod_bal').exclude(ccod_bal="-").distinct()
        qr3 = PgFormato2.objects.values_list('ccod_bal').distinct()

        envio = []
        for g in range(len(qr3)):
            #m= str(qr2[g][0]).strip()   
            #print(m)
            envio.append(str(qr3[g][0]).strip())
        
        return envio


    def get_context_data(self, **kwargs):
        context  =  super(libroEEFF,self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        qr = PgFormato2.objects.values_list('ccod_bal', 'cdsc')
        
        m = self.consulta(pk)
        n= self.planconsulta(pk)


        #m = self.basicConsulta(pk)
        nuevaarry= []
        for data in m:
            f =[]
            #print(data[0], data[2:])
            if len(data[1].strip()) >=3:
                f.append(data[1].strip())
                f.append(data[6]+data[7])
                nuevaarry.append(f)
        

        ultimoarraY = []
        for hu in n:
            s= 0
            for ma in range(len(nuevaarry)):
                if hu == nuevaarry[ma][0]:
                    #print("igual "+ str(hu)+" "+ str(nuevaarry[m][0]))
                    s= s+nuevaarry[ma][1]
                else:
                    s = s
            ultimoarraY.append(hu)
            ultimoarraY.append(s)
            #print(hu+" " + str(s))

        #########
        #convirtiendo qr a array
        rqs=[]
        for gt in range(len(qr)):
            g = []
            g.append(qr[gt][0].strip())
            g.append(qr[gt][1].strip())
            rqs.append(g)
        
        #ava vemos la consulta
        #for misdatosr in rqs:
        #    print("estos dats")
        #    print(misdatosr)

        #for ji in ultimoarraY:
        #    print("-----------")
        #    print(ji)


        

        context['listado'] = qr
        context['datos']= ultimoarraY
        return context


class libroEERR(TemplateView):
    model = PgFormato2
    template_name = 'adminis/eerr.html'
    def get_context_data(self, **kwargs):
        context  =  super(libroEERR,self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        context['nombre'] = "Estado por Naturaleza"
        return context

class eeRRFF(TemplateView):
    model = PgFormato2
    template_name = 'adminis/eerrff.html'

    def get_context_data(self, **kwargs):
        context  =  super(eeRRFF,self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        context['nombre'] = "Resultados por Función"
        return context


class LibroJsonEERR(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET['usuario']
        cmes = request.GET['cmes']
        
        Datoses_b = datosEstadoFinanciero()
        #h = Datoses_b.diarioPlanesb(usuario)
        hi = Datoses_b.diarioPlanesc(usuario, cmes, 0)
        '''
        N0 = PgFormato2.objects.filter(ccod_bal__startswith='N0')
        N1 = PgFormato2.objects.filter(ccod_bal__startswith='N1')
        N2 = PgFormato2.objects.filter(ccod_bal__startswith='N2')
        N3 = PgFormato2.objects.filter(ccod_bal__startswith='N3')
        N4 = PgFormato2.objects.filter(ccod_bal__startswith='N4').exclude(ccod_bal = 'N499')

        consulta = PgFormato2.objects.filter(Q(ccod_bal__startswith='N') | Q(ccod_bal__startswith='F')).order_by('ccod_bal')
        buenaLista = []
        lista=[]
        for fila in consulta:
            lista.append({'ccod_bal':str(fila.ccod_bal),'cdsc':fila.cdsc, 'monto':0})

        TAC= 0
        TANC=0
        TP=0
        TPN=0
        TTA=0
        RE=0
        TTP=0
        UB=0
        RO=0
        REI=0
        PASAR = 0
        for fg in h:
            if fg['ccod_cue'] == 'TAC':
                TAC = fg['valor']
            if fg['ccod_cue'] == 'TANC':
                TANC = fg['valor']
            if fg['ccod_cue'] == 'TP':
                TP = fg['valor']
            if fg['ccod_cue'] == 'TPN':
                TPN = fg['valor']
            if fg['ccod_cue'] == 'TTA':
                TTA = fg['valor']
            if fg['ccod_cue'] == 'RE':
                RE = fg['valor']
            if fg['ccod_cue'] == 'TTP':
                TTP = fg['valor']
            if fg['ccod_cue'] == 'UB':
                UB = fg['valor']
            if fg['ccod_cue'] == 'RO':
                RO = fg['valor']
            if fg['ccod_cue'] == 'REI':
                REI = fg['valor']
            if fg['ccod_cue'] == 'N005':
                PASAR = fg['valor']

            for hu in lista:
                if fg['ccod_cue'] == hu['ccod_bal']:
                    hu['monto'] = str(fg['valor'])
                if hu['ccod_bal'] == 'N999':
                    hu['monto'] = str( round(float(UB),2))
                if hu['ccod_bal'] == 'F005':
                    hu['monto'] = str( round(float(PASAR),2))
                if hu['ccod_bal'] == 'F999':
                    hu['monto'] = str( round(float(UB),2))
        '''
        mandar = []
        for tt in hi:
            if(tt['ccod_bal'][:1]=="N" or tt['ccod_bal'][:1]=="F"):
                mandar.append(tt)

        return HttpResponse(json.dumps(mandar), content_type="application/json")
        
class LibroJsonEEFF(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET['usuario']
        cmes = request.GET['cmes']
        Datoses_b = datosEstadoFinanciero()
        #h = Datoses_b.diarioPlanesb(usuario)
        hi = Datoses_b.diarioPlanesc(usuario, cmes, 0)

        mandar = []        
        for tt in hi:
            if(tt['ccod_bal'][:1]=="A" or tt['ccod_bal'][:1]=="P"):
                mandar.append(tt)
                #print(tt, '##', cm)
                #cm += 1
        return HttpResponse(json.dumps(mandar), content_type="application/json")


#devolver json estado financuiero
class estadoFinancieroPLanes(TemplateView):

    def llamarclass(self, miusuario):
        #datosEs = DatosEstado()
        datosEs_b = datosEstadoFinanciero()
        h = datosEs_b.diarioPlanes(miusuario)
        #j = datosEs.arrayDatos(miusuario)
        return h

    def get(self, request,*args, **kwargs):
        usua_p = request.GET['usua']
        k = self.llamarclass(usua_p)
        
        return HttpResponse(json.dumps(k), content_type="application/json")

        #return HttpResponse(data,content_type='application/json')

class estadoResultadosPlanes(TemplateView):
    def llamarclass(self, miusuario):
        datosEs = DatosEstado()
        datosEs_b = datosEstadoFinanciero()
        h = datosEs_b.diarioPlanes(miusuario)
        j = datosEs.arrayDatos(miusuario)
        return h
    def get(self, request,*args, **kwargs):
        usua_p = request.GET['usua']
        k = self.llamarclass(usua_p)
        
        return HttpResponse(json.dumps(k), content_type="application/json")


def hojatra(self):
    ct = {
            'titulo':'mi titulo',
            'nombre':'mi minombre'
        }
    #print(ct['titulo'])
    return JsonResponse(ct)


###########################
###########################
#reportes en excvel

#ruta_origen = "C:/Users/Evanys/Documents/Documentos"
#ruta_origen = "C:/Users/server/Documents/Documentos"
ruta_origen = "/home/hernancapcha/webapps/demo_static/files"
#ruta_origen = "C:/Users/Evanys/Documents/Documentos"
#ruta_origen = "C:/Users/server/Documents/Documentos"
#ruta_origen = "/home/hernancapcha/webapps/demo_static/files"
###########################
###########################
class ReporteViewLibroMayor(TemplateView):
    model = PgDiario
    template_name = 'adminis/mayordescargar.html'


def crearListaCous(usuario):
    asientos =  PgDiario.objects.filter(ccod_user=usuario).order_by('nasiento')
    listaAsiento=[]
    for asiento in asientos:
        #m = {'cmes':asiento.cmes,'ccod_ori':asiento.ccod_ori,'nasiento':asiento.nasiento}
        cuo = str(asiento.cmes).strip()+str(asiento.ccod_ori).strip()+str(asiento.nasiento).strip()
        res = next((sub for sub in listaAsiento if sub['cuo'] == cuo), None)
        if res == None:
            listaAsiento.append({'cuo': cuo,'datos':''})

        '''
        if listaAsiento['cuo'] not in listaAsiento['cuo']:
            listaAsiento.append({'cuo': cuo,'datos':''})
        '''
    for v in listaAsiento:
        print(v)

    return listaAsiento




class ReporteDiarioExcel(TemplateView):
    def get(self, request, *args, **kwargs):

            pk = self.kwargs.get('pk')
            cmes = self.kwargs.get('cmes')
            proposito = self.kwargs.get('proposito')

            if len(cmes)==1:
                cmes = '0'+str(cmes)
            else:
                cmes = cmes

            now = datetime.now()
            datetime_str = ''+cmes+'/28/'+str(now.year)[2:]+' 13:55:26'
            #datetime_str = '09/19/18 13:55:26'
            datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
            last_month = datetime_object - timedelta(days=0)            


            
            usuario_id = pk

            if proposito == 0:
                asientos =  PgDiario.objects.filter(ccod_user=usuario_id).filter(ffechareg__month=cmes).order_by('-nasiento')
            else:
                asientos =  PgDiario.objects.filter(ccod_user=usuario_id).filter(ffechareg__lte=last_month).order_by('-nasiento')
            #asientos =  PgDiario.objects.filter(ccod_user=usuario_id).order_by('nasiento')

            lista = []
            for asiento in asientos:
                cuo = str(asiento.cmes).strip()+str(asiento.ccod_ori).strip()+str(asiento.nasiento).strip()
                m = {'cuo':cuo, 'cmes':asiento.cmes,'ccod_ori':asiento.ccod_ori,'nasiento':str(asiento.nasiento).strip(), 'ffechareg': asiento.ffechareg,'cglosa':asiento.cglosa.strip(),'cnumero':asiento.cnumero,'ccod_cue':asiento.ccod_cue.strip(),'cdsc':asiento.pgplan.cdsc.strip(),'ndebe':asiento.ndebe,'nhaber':asiento.nhaber, 'ccorre40':asiento.ccorre40,'t2008_8': asiento.t2008_8}
                lista.append(m)
            listaCous= crearListaCous(pk)
            
            for x in listaCous:
                d = []
                for a in lista:
                    if x['cuo'] == a['cuo']:
                        datos = {'cmes':a['cmes'], 'ccod_ori': a['ccod_ori'],'nasiento':a['nasiento'],'ffechareg':a['ffechareg'],'cglosa':a['cglosa'],'cnumero':a['cnumero'],'ccod_cue':a['ccod_cue'],'cdsc':a['cdsc'],'ndebe':a['ndebe'],'nhaber':a['nhaber'],'ccorre40':a['ccorre40'],'t2008_8':a['t2008_8']}
                        d.append(datos)
                x['datos'] = d
            
            nameFile = '/LIBRO DIARIO.xlsx'
            miruta = ruta_origen+nameFile
            wb = load_workbook(miruta)
            ws = wb.active
           
            thin_border = Border(left=Side(style='thin'),
                     right=Side(style='thin'),
                     #top=Side(style='thin'),
                     bottom=Side(style='medium'))

            thin_lat = Border(left=Side(style='thin'),
                     right=Side(style='thin')
                     #top=Side(style='thin'),
                     #bottom=Side(style='medium')
                     )

            cont=11
            tDEBE = 11
            tHABER = 11

            ttdebe = float(0)
            tthaber = float(0)
            for dato in listaCous:
                tdebe = float(0)
                thaber = float(0)
                for d in dato['datos']:
                    tdebe = tdebe + float(d['ndebe'])
                    thaber = thaber + float(d['nhaber'])

                    ws['B'+str(cont)] = d['cmes']
                    ws['B'+str(cont)].border = thin_lat
                    ws['B'+str(cont+2)].border = thin_lat
                    ws['B'+str(cont+1)].border = thin_lat

                    ws['C'+str(cont)] = d['ccod_ori']
                    ws['C'+str(cont)].border = thin_lat
                    ws['C'+str(cont+2)].border = thin_lat
                    ws['C'+str(cont+1)].border = thin_lat

                    ws['D'+str(cont)] = d['nasiento']
                    ws['D'+str(cont)].border = thin_lat
                    ws['D'+str(cont+2)].border = thin_lat
                    ws['D'+str(cont+1)].border = thin_lat

                    #ws['E'+str(cont)] = d['ffechareg'].strftime('%d/%m/%y')
                    ws['E'+str(cont)] = funcionFecha(d['ffechareg'])
                    ws['E'+str(cont)].border = thin_lat
                    ws['E'+str(cont+2)].border = thin_lat
                    ws['E'+str(cont+1)].border = thin_lat

                    ws['F'+str(cont)] = d['cglosa']
                    ws['F'+str(cont)].border = thin_lat
                    ws['F'+str(cont+2)].border = thin_lat
                    ws['F'+str(cont+1)].border = thin_lat

                    ws['G'+str(cont)] = d['t2008_8']
                    ws['G'+str(cont)].border = thin_lat
                    ws['G'+str(cont+2)].border = thin_lat
                    ws['G'+str(cont+1)].border = thin_lat

                    ws['H'+str(cont)] = d['ccorre40']
                    ws['H'+str(cont)].border = thin_lat
                    ws['H'+str(cont+2)].border = thin_lat
                    ws['H'+str(cont+1)].border = thin_lat

                    ws['I'+str(cont)] = d['cnumero']
                    ws['I'+str(cont)].border = thin_lat
                    ws['I'+str(cont+2)].border = thin_lat
                    ws['I'+str(cont+1)].border = thin_lat

                    ws['J'+str(cont)] = d['ccod_cue']
                    ws['J'+str(cont)].border = thin_lat
                    ws['J'+str(cont+2)].border = thin_lat
                    ws['J'+str(cont+1)].border = thin_lat

                    ws['K'+str(cont)] = d['cdsc']
                    ws['K'+str(cont)].border = thin_lat
                    ws['K'+str(cont+2)].border = thin_lat
                    ws['K'+str(cont+1)].border = thin_lat

                    ws['L'+str(cont)] = d['ndebe']
                    ws['L'+str(cont)].border = thin_lat
                    ws['L'+str(cont+2)].border = thin_lat
                    ws['L'+str(cont+1)].border = thin_lat

                    ws['M'+str(cont)] = d['nhaber']
                    ws['M'+str(cont)].border = thin_lat
                    ws['M'+str(cont+2)].border = thin_lat
                    ws['M'+str(cont+1)].border = thin_lat

                    ws['L'+str(cont+1)] = tdebe
                    ws['L'+str(cont)].border = thin_lat
                    ws['L'+str(cont+2)].border = thin_lat
                    

                    ws['M'+str(cont+1)] = thaber
                    ws['M'+str(cont)].border = thin_lat
                    #ws.cell(row=3, column=2).style = my_style
                    
                    #ws['D'+str(cont)] = ''
                    
                    cont +=1 
                ws['L'+str(cont-1)].border = thin_border
                ws['M'+str(cont-1)].border = thin_border
                ttdebe = ttdebe + tdebe
                tthaber = tthaber + thaber
                cont += 2


            cont = cont
            li = ['B','C','D','E','F','G','H','I','J','K','L','M']

            for x in li:
                ws[x + str(cont-2)].border = thin_lat
                ws[x + str(cont-1)].border = thin_lat
                ws[x + str(cont)].border = thin_border
            '''    
            x = datetime.now()
            day = str(x.day)
            month = str(x.month)
            if len(day) == 1:
                day = '0' + day
            else:
                day =  day

            if len(month) == 1:
                month = '0' +  month
            else:
                month =  month

            ff = str(day) +'/' + str(month) +'/' + str(x.year)
            '''
            x = datetime.now()
            pie = 'Generado automáticamente por Gestión Contable Financiero Plus – Premium 20.00 - NewContaSis - Versión Educativa el ' + funcionFecha(x)
            ws['B' + str(cont+1)]  = pie
            ws['K' + str(cont)]  = 'TOTALES'
            ws['L' + str(cont)]  = ttdebe
            ws['M' + str(cont)]  = tthaber

            '''
            fl = len(asientos)+5
            ws['D'+str(fl)] = 'TOTALES'
            _cell = ws['D'+str(fl)]
            _cell.number_format = "@"
            
            ws['E'+str(fl)] = tDEBE
            _cell = ws['E'+str(fl)]
            _cell.number_format = '#,##0.00'
            
            ws['F'+str(fl)] = tHABER
            _cell = ws['F'+str(fl)]
            _cell.number_format = '#,##0.00'
            
            letras = ['B','C','D','E','F','G']
            for n in letras:
                m = n + str(fl)
                c= ws[m]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")
            '''
            #ws.cell(column)
            #Establecemos el nombre del archivo

            nombre_archivo ="LibroDiario.xlsx"
            #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
            response = HttpResponse(content_type="application/ms-excel")
            contenido = "attachment; filename={0}".format(nombre_archivo)
            response["Content-Disposition"] = contenido
            wb.save(response)
            return response


class ReporteLibroHojaTrabajoExcel(TemplateView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        cmes = self.kwargs.get('cmes')
        proposito = self.kwargs.get('proposito')

        if len(cmes)==1:
            cmes = '0'+str(cmes)
        else:
            cmes = cmes

        now = datetime.now()
        datetime_str = ''+cmes+'/28/'+str(now.year)[2:]+' 13:55:26'
        #datetime_str = '09/19/18 13:55:26'
        datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
        last_month = datetime_object - timedelta(days=0)

        #pgplan__ccod_cue
        
        #miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc').filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        if proposito == 0:
            minuevaconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc','pgplan__ntipo').filter(ffechareg__month=cmes).filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
            miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc').filter(ffechareg__month=cmes).filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        else:
            minuevaconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc','pgplan__ntipo').filter(ffechareg__lte=last_month).filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
            miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc').filter(ffechareg__lte=last_month).filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        #minuevaconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc','pgplan__ntipo').filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        listaNueva=[]
        listaVieja=[]
        for x in minuevaconsulta:
            #print(x, 'mi nueva consulta')
            l = {'ccod_cue': x[0],'cdsc':x[1].strip(),'ntipo':x[2],'sum_debe':x[3],'sum_haber':x[4]}
            listaNueva.append(l)

        for m in miconsulta:
            ll = {'ccod_cue': m[0],'cdsc':m[1].strip(),'sum_debe':m[2],'sum_haber':m[3]}
            listaVieja.append(ll)

        #Creamos el libro de trabajo
        wb = Workbook()
        
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        ws.column_dimensions['C'].width = 35
        ws.column_dimensions['D'].width = 14
        ws.column_dimensions['E'].width = 14
        ws.column_dimensions['F'].width = 14
        ws.column_dimensions['G'].width = 14
        ws.column_dimensions['H'].width = 14
        ws.column_dimensions['I'].width = 14
        ws.column_dimensions['J'].width = 20
        ws.column_dimensions['K'].width = 20
        ws.column_dimensions['L'].width = 20
        ws.column_dimensions['M'].width = 20
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'HOJA DE TRABAJO'
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:E1')
        #Creamos los encabezados desde la celda B3 hasta la E3
        ws['B3'] = 'CUENTA'
        c = ws['B3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['C3'] = 'NOMBRE'
        c = ws['C3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['D3'] = 'DEBITO'
        c = ws['D3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['E3'] = 'CREDITO'
        c = ws['E3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['F3'] = 'S. DEUDOR'
        c = ws['F3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['G3'] = 'S. ACREEDOR'
        c = ws['G3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['H3'] = 'ACTIVO'
        c = ws['H3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['I3'] = 'PASIVO'
        c = ws['I3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['J3'] = 'PERDIDAS NAT'
        c = ws['J3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['K3'] = 'GANANCIAS NAT'
        c = ws['K3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['L3'] = 'PERDIDAS FUN'
        c = ws['L3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['M3'] = 'GANANCIAS FUN'
        c = ws['M3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        
        cont=4
        conta=4
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        deu = []
        acre =[]
        activos = []
        pasivos = []
        perdidas_a = []
        ganancias_a = []
        perdidas_f = []
        ganancias_f = []
        acti = 0
        per = 0

        for listado in range(len(listaNueva)):
            resta = listaNueva[listado]['sum_debe'] - listaNueva[listado]['sum_haber']
            #print(listaNueva[listado]['sum_debe'], listaNueva[listado]['sum_haber'], resta)
            if(resta > 0):
                #nuevoJson = { 'deudor' : str(abs(resta)), 'acreedor': str(0)}
                listaNueva[listado]['deudor'] = str(abs(resta))
                listaNueva[listado]['acreedor'] = str(0.00)
            else:
                #nuevoJson = {'deudor' : 0, 'acreedor': str(abs(resta))}
                listaNueva[listado]['deudor'] = str(0.00)
                listaNueva[listado]['acreedor'] = str(abs(resta))


            if listaNueva[listado]['ntipo'] == 1:
                listaNueva[listado]['activo'] = str(abs(resta))
                listaNueva[listado]['pasivo'] = str(0.00)
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0)
                listaNueva[listado]['GanFun'] = float(0)

            if listaNueva[listado]['ntipo'] == 2:
                listaNueva[listado]['activo'] = str(0.00)
                listaNueva[listado]['pasivo'] = str(abs(resta))
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0)
                listaNueva[listado]['GanFun'] = float(0)

            if listaNueva[listado]['ntipo'] == 3:
                if(float(listaNueva[listado]['deudor']) <= 0):
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = abs(resta)
                    '''
                    listaNueva[listado]['perNat'] = float(5000)
                    listaNueva[listado]['GanNat'] = float(50001)
                    listaNueva[listado]['perFun'] = float(50002)
                    listaNueva[listado]['GanFun'] = float(50003)
                    '''
                    listaNueva[listado]['perNat'] = float(listaNueva[listado]['deudor'])
                    listaNueva[listado]['GanNat'] = float(listaNueva[listado]['acreedor'])
                    listaNueva[listado]['perFun'] = float(listaNueva[listado]['activo'])
                    listaNueva[listado]['GanFun'] = float(listaNueva[listado]['pasivo'])
                    
                else:
                    listaNueva[listado]['activo'] = abs(resta)
                    listaNueva[listado]['pasivo'] = str(0.00)
                    '''
                    listaNueva[listado]['perNat'] = float(5000)
                    listaNueva[listado]['GanNat'] = float(50001)
                    listaNueva[listado]['perFun'] = float(50002)
                    listaNueva[listado]['GanFun'] = float(50003)
                    '''
                    listaNueva[listado]['perNat'] = float(listaNueva[listado]['deudor'])
                    listaNueva[listado]['GanNat'] = float(listaNueva[listado]['acreedor'])
                    listaNueva[listado]['perFun'] = float(listaNueva[listado]['activo'])
                    listaNueva[listado]['GanFun'] = float(listaNueva[listado]['pasivo'])
                    
               
            if listaNueva[listado]['ntipo'] == 4:
                if(float(listaNueva[listado]['deudor']) <= 0):
                    #print(listaNueva[listado]['ccod_cue'], "4 esta en pasivo")
                    listaNueva[listado]['activo'] = str(abs(resta))
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(0)
                    listaNueva[listado]['GanNat'] = float(abs(resta))
                    listaNueva[listado]['perFun'] = float(0.00)
                    listaNueva[listado]['GanFun'] = float(0.00)
                else:
                    #print(listaNueva[listado]['ccod_cue'], "4 esta en activo")
                    listaNueva[listado]['activo'] = str(abs(resta))
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(abs(resta))
                    listaNueva[listado]['GanNat'] = float(0)
                    listaNueva[listado]['perFun'] = float(0.00)
                    listaNueva[listado]['GanFun'] = float(0.00)

            if listaNueva[listado]['ntipo'] == 5:
                if(float(listaNueva[listado]['deudor']) <= 0):
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(abs(resta))
                    listaNueva[listado]['perNat'] = float(0.00)
                    listaNueva[listado]['GanNat'] = float(0.00)
                    listaNueva[listado]['perFun'] = float(0.00)
                    listaNueva[listado]['GanFun'] = float(abs(resta))
                else:
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(abs(resta))
                    listaNueva[listado]['perNat'] = float(0.00)
                    listaNueva[listado]['GanNat'] = float(0.00)
                    listaNueva[listado]['perFun'] = float(abs(resta))
                    listaNueva[listado]['GanFun'] = float(0.00)

                
            
            if listaNueva[listado]['ntipo'] == 6:
                listaNueva[listado]['activo'] = str(0.00)
                listaNueva[listado]['pasivo'] = str(abs(resta))
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0.00)
                listaNueva[listado]['GanFun'] = float(0.00)

            if listaNueva[listado]['ntipo'] == 7:
                listaNueva[listado]['activo'] = str(0.00)
                listaNueva[listado]['pasivo'] = str(abs(resta))
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0.00)
                listaNueva[listado]['GanFun'] = float(0.00)


        for n in miconsulta:
            r = n[2]-n[3]
            a = n[0][:1]
            if r > 0:
                deu.append(abs(r))
                acre.append(0)
            else:
                acre.append(abs(r))
                deu.append(0)
            if a=='1' or a == '2' or a=='3' or a=='4' or a=='5':
                acti= acti + 1
            else:
                per = per + 1

        for n in range(len(deu)):
            if acti > n:
                activos.append(deu[n])
                pasivos.append(acre[n])
                perdidas_a.append(float(listaNueva[n]['perNat']))
                ganancias_a.append(float(listaNueva[n]['GanNat']))

                perdidas_f.append(float(listaNueva[n]['perFun']))
                ganancias_f.append(float(listaNueva[n]['GanFun']))
                #perdidas_a.append(0)
                #ganancias_a.append(0)
            else:
                activos.append(0)
                pasivos.append(0)
                perdidas_a.append(float(listaNueva[n]['perNat']))
                ganancias_a.append(float(listaNueva[n]['GanNat']))

                perdidas_f.append(float(listaNueva[n]['perFun']))
                ganancias_f.append(float(listaNueva[n]['GanFun']))
                #tod[val].append(listaNueva[val]['perFun'])
                #tod[val].append(listaNueva[val]['GanFun'])
                #perdidas_a.append(deu[n])
                #ganancias_a.append(acre[n])

        for asiento in miconsulta:
            ws.cell(row=cont,column=2).value = asiento[0]
            _cell = ws.cell(row=cont, column = 2)
            _cell.number_format = "@"
            ws.cell(row=cont,column=3).value = asiento[1]
            _cell = ws.cell(row=cont, column = 3)
            _cell.number_format = "@"
            
            ws.cell(row=cont,column=4).value = asiento[2]
            _cell = ws.cell(row=cont, column = 4)
            _cell.number_format = '#,##0.00'
            ws.cell(row=cont,column=5).value = asiento[3]
            _cell = ws.cell(row=cont, column = 5)
            _cell.number_format = '#,##0.00'
            cont = cont + 1

        for x in range(len(deu)):
            ws.cell(row=conta,column=6).value = deu[x]
            _cell = ws.cell(row=conta, column = 6)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=7).value = acre[x]
            _cell = ws.cell(row=conta, column = 7)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=8).value = activos[x]
            _cell = ws.cell(row=conta, column = 8)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=9).value = pasivos[x]
            _cell = ws.cell(row=conta, column = 9)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=10).value = perdidas_a[x]
            _cell = ws.cell(row=conta, column = 10)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=11).value = ganancias_a[x]
            _cell = ws.cell(row=conta, column = 11)
            _cell.number_format = '#,##0.00'

            ws.cell(row=conta,column=12).value = perdidas_f[x]
            _cell = ws.cell(row=conta, column = 12)
            _cell.number_format = '#,##0.00'

            ws.cell(row=conta,column=13).value = ganancias_f[x]
            _cell = ws.cell(row=conta, column = 13)
            _cell.number_format = '#,##0.00'

            conta = conta + 1
        #5 es la fila
        fl = len(deu)+5
        flb= fl+1
        flc=fl+2

        #sumas de columnas totales
        tDEBITO = 0
        for x in miconsulta:
            tDEBITO = tDEBITO + x[2]
        tCREDITO =0
        for x in miconsulta:
            tCREDITO = tCREDITO + x[3]
        tDEUDOR = sum(deu)
        tACREEDOR = sum(acre)
        tACTIVO = sum(activos)
        tPASIVO = sum(pasivos)
        tPERDIDAS = sum(perdidas_a)
        tGANANCIAS = sum(ganancias_a)

        tfPERDIDAS = sum(perdidas_f)
        tfGANANCIAS = sum(ganancias_f)

        tACTIVtPASIVO = tACTIVO - tPASIVO
        tPERDIDAStGANANCIA = tPERDIDAS-tGANANCIAS

        tfPERDIDAStGANANCIA = tfPERDIDAS-tfGANANCIAS
        
        sIGUALESTOTALACTIVO = 0
        sIGUALESTOTALPASIVO = 0
        sIGUALESTOTALPERDIDAS = 0
        sIGUALESTOTALGANANCIAS = 0

        sFIGUALESTOTALPERDIDAS = 0
        sFIGUALESTOTALGANANCIAS = 0

        ws['B'+str(fl)] = 'TOTALES'
        ws.merge_cells('B'+str(fl) +':'+'C'+str(fl))
        


        ws['D'+str(fl)] = tDEBITO
        _cell = ws['D'+str(fl)]
        #_cell.number_format = '#,##0.00'

        ws['E'+str(fl)] = tCREDITO
        _cell = ws['E'+str(fl)]
        #_cell.number_format = '#,##0.00'

        ws['F'+str(fl)] = tDEUDOR
        _cell = ws['F'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['G'+str(fl)] = tACREEDOR
        _cell = ws['G'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['H'+str(fl)] = tACTIVO
        _cell = ws['H'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['I'+str(fl)] = tPASIVO
        _cell = ws['I'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['J'+str(fl)] = tPERDIDAS
        _cell = ws['J'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['K'+str(fl)] = tGANANCIAS
        _cell = ws['K'+str(fl)]

        ws['L'+str(fl)] = tfPERDIDAS
        _cell = ws['L'+str(fl)]

        ws['M'+str(fl)] = tfGANANCIAS
        _cell = ws['M'+str(fl)]
        #_cell.number_format = '#,##0.00'

        letrasnumeros = ['D','E','F','G','H','I','J','K','L','M']
        for m in letrasnumeros:
            h = m+ str(fl)
            celda  = ws[h]
            celda.number_format = '#,##0.00'
        

        ws['B'+str(flb)] = 'MENSAJES'

        ws.merge_cells('B'+str(flb) +':'+'C'+str(flb))
        if tACTIVtPASIVO <0:
            ws['B'+str(flb)] = 'PERDIDA DEL EJERCICIO'
            ws.merge_cells('B'+str(flb) +':'+'C'+str(flb))

            ws['H'+str(flb)] =  abs(tACTIVtPASIVO)
            _cell = ws['H'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['I'+str(flb)] =  0
            _cell = ws['I'+str(flb)]
            _cell.number_format = '#,##0.00'

            sIGUALESTOTALACTIVO = abs(tACTIVO) + abs(tACTIVtPASIVO)
            sIGUALESTOTALPASIVO = tPASIVO + 0
        else:
            ws['B'+str(flb)] = 'GANANCIA DEL EJERCICIO'
            ws.merge_cells('B'+str(flb) +':'+'C'+str(flb))
            ws['H'+str(flb)] =  0
            _cell = ws['H'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['I'+str(flb)] =  abs(tACTIVtPASIVO)
            _cell = ws['I'+str(flb)]
            _cell.number_format = '#,##0.00'

            sIGUALESTOTALACTIVO = tACTIVO + 0
            sIGUALESTOTALPASIVO = abs(tPASIVO) + abs(tACTIVtPASIVO)
        
        if tPERDIDAStGANANCIA <0:
            ws['J'+str(flb)] =  abs(tPERDIDAStGANANCIA)
            _cell = ws['J'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['K'+str(flb)] =  0
            _cell = ws['K'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['L'+str(flb)] =  abs(tfPERDIDAStGANANCIA)
            _cell = ws['L'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['M'+str(flb)] =  0
            _cell = ws['M'+str(flb)]
            _cell.number_format = '#,##0.00'

            sIGUALESTOTALPERDIDAS = abs(tPERDIDAS) + abs(tPERDIDAStGANANCIA)
            sIGUALESTOTALGANANCIAS = tGANANCIAS + 0

            sFIGUALESTOTALPERDIDAS = abs(tfPERDIDAS) + abs(tfPERDIDAStGANANCIA)
            sFIGUALESTOTALGANANCIAS = tfGANANCIAS + 0
        else:
            ws['J'+str(flb)] =  0
            _cell = ws['J'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['K'+str(flb)] =  abs(tPERDIDAStGANANCIA)
            _cell = ws['K'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['L'+str(flb)] =  0
            _cell = ws['L'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['M'+str(flb)] =  abs(tfPERDIDAStGANANCIA)
            _cell = ws['M'+str(flb)]
            _cell.number_format = '#,##0.00'

            sIGUALESTOTALPERDIDAS = tPERDIDAS + 0
            sIGUALESTOTALGANANCIAS = abs(tGANANCIAS) + abs(tPERDIDAStGANANCIA)

            sFIGUALESTOTALPERDIDAS = tfPERDIDAS + 0
            sFIGUALESTOTALGANANCIAS = abs(tfGANANCIAS) + abs(tfPERDIDAStGANANCIA)


        ws['B'+str(flc)] = 'SUMAS IGUALES'
        ws.merge_cells('B'+str(flc) +':'+'C'+str(flc))
        ws['H'+str(flc)] = abs(sIGUALESTOTALACTIVO) 
        _cell = ws['H'+str(flc)]
        _cell.number_format = '#,##0.00'
        
        
        ws['I'+str(flc)] = abs(sIGUALESTOTALPASIVO)
        _cell = ws['I'+str(flc)]
        _cell.number_format = '#,##0.00'
        
        ws['J'+str(flc)] = abs(sIGUALESTOTALPERDIDAS)
        _cell = ws['J'+str(flc)]
        _cell.number_format = '#,##0.00'
        
        ws['K'+str(flc)] = abs(sIGUALESTOTALGANANCIAS)
        _cell = ws['K'+str(flc)]
        _cell.number_format = '#,##0.00'

        ws['L'+str(flc)] = abs(sFIGUALESTOTALGANANCIAS)
        _cell = ws['L'+str(flc)]
        _cell.number_format = '#,##0.00'

        ws['M'+str(flc)] = abs(sFIGUALESTOTALGANANCIAS)
        _cell = ws['M'+str(flc)]
        _cell.number_format = '#,##0.00'
        

        miarr = ['B','C', 'D','E','F','G','H','I','J','K','L','M']
        mitroarr = [fl,flb,flc]
        for n in miarr:
            for m in mitroarr:
                h=str(n)+str(m)
                c = ws[h]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

        
        #Establecemos el nombre del archivo
        nombre_archivo ="HojadeTrabajo.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response


class ReporteLibroMayor(TemplateView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        cuenta = self.request.GET.get('cuenta')
        ##el error es ccod_cue
        asientos = PgDiario.objects.filter(ccod_user=pk).filter(pgplan__ccod_cue=cuenta).order_by('nasiento','id')
        #asientos =  PgDiario.objects.filter(ccod_cli=pk).filter(ccod_cue=cuenta).order_by('nasiento','id')

        if len(asientos)<=0:
            miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc').filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
            consultaslista= []            
            for d in range(len(miconsulta)):
                cuentaList =  miconsulta[d][0]
                consulta =  PgDiario.objects.filter(ccod_user=pk).filter(pgplan__ccod_cue=cuentaList).order_by('nasiento','id')
                unaLista = []
                for x in range(len(consulta)):
                    nuevo = []
                    nuevo.append(consulta[x].pgplan.ccod_cue.strip())
                    nuevo.append(consulta[x].pgplan.cdsc.strip())
                    nuevo.append(consulta[x].nasiento)
                    nuevo.append(consulta[x].cglosa.strip())
                    nuevo.append(consulta[x].ndebe)
                    nuevo.append(consulta[x].nhaber)
                    unaLista.append(nuevo)
                consultaslista.append(unaLista)


            listado = []
            listaDEBE = []
            listaHABER = []
            for t in consultaslista:
                r=[0]
                
                tDE = 0    
                tHA = 0
                for m in range(len(t)):
                    inicial = t[m][0][:1]
                    if inicial == '4' or inicial == '5' or inicial== '7':
                        h = (t[m][5] - t[m][4]) + r[m]
                        r.append(h)
                    else:
                        h = (t[m][4] - t[m][5]) + r[m]
                        r.append(h)
                    tDE = tDE + t[m][4]
                    tHA = tHA + t[m][5]
                
                listaDEBE.append(tDE)
                listaHABER.append(tHA)

                r.pop(0)
                listado.append(r)
            
            for g in range(len(listado)):
                n=0
                for h in consultaslista[g]:
                    h.append(listado[g][n])
                    h.append(listaDEBE[g])
                    h.append(listaHABER[g])
                    n=n+1

            wb = Workbook()
            ws = wb.active
            ws.column_dimensions['C'].width = 70
            ws.column_dimensions['D'].width = 14
            ws.column_dimensions['E'].width = 35
            ws.column_dimensions['F'].width = 14
            ws.column_dimensions['G'].width = 14
            ws.column_dimensions['H'].width = 14
            ws.column_dimensions['I'].width = 14
            ws.column_dimensions['J'].width = 14
            ws.column_dimensions['K'].width = 14
            ws['B1'] = 'LIBRO MAYOR'
            ws.merge_cells('B1:E1')


            ws['B3'] = 'CUENTA'
            ws['C3'] = 'DESCRIPCION'
            ws['D3'] = 'ASIENTO'
            ws['E3'] = 'CONCEPTO'
            ws['F3'] = 'DEBITO'
            ws['G3'] = 'CREDITO'
            ws['H3'] = 'SALDO'
            
            
            titulos = ['B','C','D','E','F','G','H']
            for t in titulos:
                cel = str(t)+str(3)
                c= ws[cel]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

            cont=4
            conta=4
            tama = 0

            #for l in consultaslista:
            #    print(l)

            for lista in consultaslista:
                for dato in lista:
                    ws['B'+str(cont)] = dato[0]
                    _cell = ws['B'+str(cont)]
                    _cell.number_format = '#,##0.00'

                    ws['C'+str(cont)] = dato[1]
                    #_cell = ws['C'+str(cont)]
                    #_cell.number_format = '#,##0.00'
                    

                    ws['D'+str(cont)] = dato[2]
                    #_cell = ws['B'+str(cont)]
                    #_cell.number_format = '#,##0.00'
                    

                    ws['E'+str(cont)] = dato[3]
                    #_cell = ws['B'+str(cont)]
                    #_cell.number_format = '#,##0.00'

                    ws['F'+str(cont)] = dato[4]
                    _cell = ws['F'+str(cont)]
                    _cell.number_format = '#,##0.00'

                    ws['G'+str(cont)] = dato[5]
                    _cell = ws['G'+str(cont)]
                    _cell.number_format = '#,##0.00'

                    ws['H'+str(cont)] = dato[6]
                    _cell = ws['H'+str(cont)]
                    _cell.number_format = '#,##0.00'

                    cont = cont + 1

                ws['E'+str(cont)] = "TOTALES"
                cel = str('E')+str(cont)
                c= ws[cel]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

                ws['F'+str(cont)] = dato[7]
                _cell = ws['F'+str(cont)]
                _cell.number_format = '#,##0.00'
                cel = str('F')+str(cont)
                c= ws[cel]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")
                
                ws['G'+str(cont)] = dato[8]
                _cell = ws['G'+str(cont)]
                _cell.number_format = '#,##0.00'
                cel = str('G')+str(cont)
                c= ws[cel]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

                cont= cont+2





            nombre_archivo ="Libro Mayor.xlsx"
            response = HttpResponse(content_type="application/ms-excel")
            contenido = "attachment; filename={0}".format(nombre_archivo)
            response["Content-Disposition"] = contenido
            wb.save(response)

        else:
            inicial=""
            if cuenta:
                inicial = cuenta[:1]

            r = [0]
            d= [0]
            if inicial == '4' or inicial == '5' or inicial== '7':
                for x in range(len(asientos)):
                    h = (asientos[x].nhaber -  asientos[x].ndebe) + r[x]
                    r.append(h)
            else:
                for x in range(len(asientos)):
                    h = (asientos[x].ndebe -  asientos[x].nhaber) + r[x]
                    r.append(h)

            r.pop(0)

            tDEB = 0
            tHAB = 0
            for x in range(len(asientos)):
                tDEB = tDEB + asientos[x].ndebe
                tHAB = tHAB + asientos[x].nhaber

            #print(str(tDEB), str(tHAB))

            gran = []

            for v in range(len(asientos)):
                hu = []
                hu.append(asientos[v].pgplan.ccod_cue.strip())
                hu.append(asientos[v].pgplan.cdsc.strip())
                hu.append(asientos[v].nasiento)
                hu.append(asientos[v].cglosa.strip())
                hu.append(asientos[v].ndebe)
                hu.append(asientos[v].nhaber)
                hu.append(r[v])
                hu.append(tDEB)
                hu.append(tHAB)
                gran.append(hu)


            #Creamos el libro de trabajo
            wb = Workbook()
            ws = wb.active
            ws.column_dimensions['C'].width = 35
            ws.column_dimensions['D'].width = 14
            ws.column_dimensions['E'].width = 35
            ws.column_dimensions['F'].width = 14
            ws.column_dimensions['G'].width = 14
            ws.column_dimensions['H'].width = 14
            ws.column_dimensions['I'].width = 14
            ws.column_dimensions['J'].width = 14
            ws.column_dimensions['K'].width = 14
            ws['B1'] = 'LIBRO MAYOR'
            ws.merge_cells('B1:E1')


            ws['B3'] = 'CUENTA'
            ws['C3'] = 'DESCRIPCION'
            ws['D3'] = 'ASIENTO'
            ws['E3'] = 'CONCEPTO'
            ws['F3'] = 'DEBITO'
            ws['G3'] = 'CREDITO'
            ws['H3'] = 'SALDO'
            
            
            titulos = ['B','C','D','E','F','G','H']
            for t in titulos:
                cel = str(t)+str(3)
                c= ws[cel]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

            cont=4
            conta=4
            
            for x in range(len(gran)):
                ws.cell(row=conta,column=2).value = gran[x][0]
                _cell = ws.cell(row=conta, column = 2)
                _cell.number_format = '@'
                ws.cell(row=conta,column=3).value = gran[x][1]
                _cell = ws.cell(row=conta, column = 3)
                _cell.number_format = '@'
                ws.cell(row=conta,column=4).value = gran[x][2]
                _cell = ws.cell(row=conta, column = 4)
                _cell.number_format = '@'
                ws.cell(row=conta,column=5).value = gran[x][3]
                _cell = ws.cell(row=conta, column = 5)
                _cell.number_format = '@'
                ws.cell(row=conta,column=6).value = gran[x][4]
                _cell = ws.cell(row=conta, column = 6)
                _cell.number_format = '#,##0.00'
                ws.cell(row=conta,column=7).value = gran[x][5]
                _cell = ws.cell(row=conta, column = 7)
                _cell.number_format = '#,##0.00'
                ws.cell(row=conta,column=8).value = gran[x][6]
                _cell = ws.cell(row=conta, column = 8)
                _cell.number_format = '#,##0.00'
                conta = conta + 1

            fl = len(gran)+5
            ws['D'+str(fl)] = 'TOTALES'
            ws['F'+str(fl)] = gran[0][7]
            m = ws['F'+str(fl)]
            m.number_format = '#,##0.00'
            ws['G'+str(fl)] = gran[0][8]
            m = ws['G'+str(fl)]
            m.number_format = '#,##0.00'

            for t in titulos:
                cel = str(t)+str(fl)
                c= ws[cel]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

            '''
            for x in range(len(gran)):
                print(gran[x][0])
            '''
            nombre_archivo ="Libro Mayor cuenta "+cuenta+".xlsx"
            #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
            response = HttpResponse(content_type="application/ms-excel")
            contenido = "attachment; filename={0}".format(nombre_archivo)
            response["Content-Disposition"] = contenido
            wb.save(response)
        return response

class ReporteEstadoFinanciero(TemplateView):
    def get(self, request, *args, **kwargs):
            
            pk = self.kwargs.get('pk')
            Datoses_b = datosEstadoFinanciero()
            #recuperando los datos para el estado finaciero
            h = Datoses_b.diarioPlanesb(pk)#cambio de diarioPlanesB a diarioplanes
        
            wb = Workbook()
            ws = wb.active
            ws.column_dimensions['A'].width = 3
            ws.column_dimensions['B'].width = 45
            ws.column_dimensions['C'].width = 16
            ws.column_dimensions['D'].width = 3
            ws.column_dimensions['E'].width = 45
            ws.column_dimensions['F'].width = 16
            
            ws.merge_cells('B2:F2')
            ws['B2'].alignment = Alignment(horizontal="center", vertical="center")

            A1 = PgFormato2.objects.filter(ccod_bal__startswith='A1').order_by('ccod_bal') 
            A2 = PgFormato2.objects.filter(ccod_bal__startswith='A2').order_by('ccod_bal')
            P1 = PgFormato2.objects.filter(ccod_bal__startswith='P1').order_by('ccod_bal')
            P5 = PgFormato2.objects.filter(ccod_bal__startswith='P4').order_by('ccod_bal')

            cott = 5
            cottb = 5
            inicio = cott
            inicio2 = cott + len(A1) + 1
            ini = cottb
            ini2 = cottb + len(P1) + 1

            consulta = PgFormato2.objects.filter(Q(ccod_bal__startswith='A') | Q(ccod_bal__startswith='P')).order_by('ccod_bal')
            buenaLista = []
            lista=[]
            for fila in consulta:
                lista.append({'ccod_bal':str(fila.ccod_bal),'cdsc':fila.cdsc, 'monto':0})


            TAC= 0
            TANC=0
            TP=0
            TPN=0
            TTA=0
            RE=0
            TTP=0
            UB=0
            RO=0
            REI=0
            TOTALPASIVOSCORRIENTES= 0
            TOTALPASIVO=0
            TOTALPATRIMONIO=0
            TOTALPASIVOYPATRIMONIONETO=0
            for fg in h:
                if fg['ccod_cue'] == 'TAC':
                    TAC = fg['valor']
                if fg['ccod_cue'] == 'TANC':
                    TANC = fg['valor']
                if fg['ccod_cue'] == 'TP':
                    TP = fg['valor']
                if fg['ccod_cue'] == 'TPN':
                    TPN = fg['valor']
                if fg['ccod_cue'] == 'TTA':
                    TTA = fg['valor']
                if fg['ccod_cue'] == 'RE':
                    RE = fg['valor']
                if fg['ccod_cue'] == 'TTP':
                    TTP = fg['valor']
                if fg['ccod_cue'] == 'UB':
                    UB = fg['valor']
                if fg['ccod_cue'] == 'RO':
                    RO = fg['valor']
                if fg['ccod_cue'] == 'REI':
                    REI = fg['valor']
                if fg['ccod_cue'] == 'TOTALPASIVOSCORRIENTES':
                    TOTALPASIVOSCORRIENTES = fg['valor']
                if fg['ccod_cue'] == 'TOTALPASIVO':
                    TOTALPASIVO = fg['valor']
                if fg['ccod_cue'] == 'TOTALPATRIMONIO':
                    TOTALPATRIMONIO = fg['valor']
                if fg['ccod_cue'] == 'TOTALPASIVOYPATRIMONIONETO':
                    TOTALPASIVOYPATRIMONIONETO = fg['valor']

                for hu in lista:
                    if fg['ccod_cue'] == hu['ccod_bal']:
                        hu['monto'] = str(round(float( fg['valor']),2))
                    if hu['ccod_bal'] == 'A399':
                        hu['monto'] = str(round( float(TAC),2))
                    if hu['ccod_bal'] == 'A599':
                        hu['monto'] = str(round( float(TANC),2))
                    if hu['ccod_bal'] == 'A999':
                        hu['monto'] = str(round( float(TTA),2))
                    if hu['ccod_bal'] == 'P699':
                        hu['monto'] = str(round( float(TOTALPASIVO),2))
                    if hu['ccod_bal'] == 'P999':
                        hu['monto'] = str(round(float(TOTALPASIVOYPATRIMONIONETO), 2))
                    if hu['ccod_bal'] == 'P835':
                        hu['monto'] = str(round(float(UB), 2))
                    if hu['ccod_bal'] == 'P399':
                        hu['monto'] = str(round(float(TOTALPASIVOSCORRIENTES), 2))
                    if hu['ccod_bal'] == 'P899':
                        hu['monto'] = str(round(float(TOTALPATRIMONIO), 2))

            soloA = []
            soloB = []
            for t in range(len(lista)):
                if lista[t]['ccod_bal'][:1] == 'A':
                    soloA.append({'ccod_bal':str(lista[t]['ccod_bal']),'cdsc':lista[t]['cdsc'], 'monto':lista[t]['monto']})
                else:
                    soloB.append({'ccod_bal':str(lista[t]['ccod_bal']),'cdsc':lista[t]['cdsc'], 'monto':lista[t]['monto']})
                
            #print(soloA)
            #print(soloB)
            soloA.insert(35,{'ccod_bal':'','cdsc':'', 'monto':str(float(0.0))})

            for x in range(len(soloB)):
                cdsc = soloA[x]['cdsc']
                mont = soloA[x]['monto']
                cdscB = soloB[x]['cdsc']
                montB = soloB[x]['monto']
                na = ""

                ws['B'+str(inicio)] = cdsc
                ws['C'+str(inicio)] = float(mont)

                ws['E'+str(inicio)] = cdscB
                ws['F'+str(inicio)] = float(montB)

                _cell = ws['C'+str(inicio)]
                _cell.number_format = '#,##0.00'
                _cell = ws['F'+str(inicio)]
                _cell.number_format = '#,##0.00'
                inicio = inicio + 1


            nuevoarray = ['B2','B4','B12','E4','E20','B20','B22','E22','C12','E12','F12','F20','C22','F22']
            for cell in nuevoarray:
                c= ws[cell]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                #c.fill = PatternFill("solid", fgColor="DDDDDD")

            ws['C5'] = ''
            ws['F5'] = ''
            ws['C14'] = ''
            ws['F14'] = ''
            
            nombre_archivo ="EstadoFinanciero.xlsx"
            #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
            response = HttpResponse(content_type="application/ms-excel")
            contenido = "attachment; filename={0}".format(nombre_archivo)
            response["Content-Disposition"] = contenido
            wb.save(response)
            return response


class ReporteEstadoResultados(TemplateView):
    def get(self, request, *args, **kwargs):
            
            pk = self.kwargs.get('pk')
            Datoses_b = datosEstadoFinanciero()
            #recuperando los datos para el estado finaciero
            h = Datoses_b.diarioPlanesb(pk)

            #for hd in h:
            #    print(hd,'resultados')

            wb = Workbook()
            ws = wb.active
            ws.column_dimensions['A'].width = 3
            ws.column_dimensions['B'].width = 45
            ws.column_dimensions['C'].width = 16
            ws.column_dimensions['D'].width = 3
            ws.column_dimensions['E'].width = 45
            ws.column_dimensions['F'].width = 16

            ws['B2'] = 'ESTADO DE RESULTADOS'
            ws['B33'] = 'RESULTADO DEL EJERCICIO ANTES DE IMPUEST'
            ws.merge_cells('B2:C2')
            ws['B2'].alignment = Alignment(horizontal="center", vertical="center")
            N0 = PgFormato2.objects.filter(ccod_bal__startswith='N0')
            N1 = PgFormato2.objects.filter(ccod_bal__startswith='N1')
            N2 = PgFormato2.objects.filter(ccod_bal__startswith='N2')
            N3 = PgFormato2.objects.filter(ccod_bal__startswith='N3')
            N4 = PgFormato2.objects.filter(ccod_bal__startswith='N4').exclude(ccod_bal = 'N499')



            cott = 5
            inicio = cott
            iniciob = cott
            inicio2 = cott + len(N0)
            inicio3 = cott + len(N0) + len(N1)
            inicio4 = cott + len(N0) + len(N1) + len(N2)
            inicio5 = cott + len(N0) + len(N1) + len(N2) + len(N3)
            
            consulta = PgFormato2.objects.filter(Q(ccod_bal__startswith='N') | Q(ccod_bal__startswith='F')).order_by('ccod_bal')

            #for lf in consulta:
            #    print(lf, 'efes')

            buenaLista = []
            lista=[]
            for fila in consulta:
                lista.append({'ccod_bal':str(fila.ccod_bal),'cdsc':fila.cdsc, 'monto':0})

            TAC= 0
            TANC=0
            TP=0
            TPN=0
            TTA=0
            RE=0
            TTP=0
            UB=0
            RO=0
            REI=0
            TOTALPASIVOSCORRIENTES= 0
            TOTALPASIVO=0
            TOTALPATRIMONIO=0
            TOTALPASIVOYPATRIMONIONETO=0

            for fg in h:
                if fg['ccod_cue'] == 'TAC':
                    TAC = fg['valor']
                if fg['ccod_cue'] == 'TANC':
                    TANC = fg['valor']
                if fg['ccod_cue'] == 'TP':
                    TP = fg['valor']
                if fg['ccod_cue'] == 'TPN':
                    TPN = fg['valor']
                if fg['ccod_cue'] == 'TTA':
                    TTA = fg['valor']
                if fg['ccod_cue'] == 'RE':
                    RE = fg['valor']
                if fg['ccod_cue'] == 'TTP':
                    TTP = fg['valor']
                if fg['ccod_cue'] == 'UB':
                    UB = fg['valor']
                if fg['ccod_cue'] == 'RO':
                    RO = fg['valor']
                if fg['ccod_cue'] == 'REI':
                    REI = fg['valor']


                for hu in lista:
                    if fg['ccod_cue'] == hu['ccod_bal']:
                        hu['monto'] = str(round(float( fg['valor']),2))
                    if hu['ccod_bal'] == 'A399':
                        hu['monto'] = str(round( float(TAC),2))
                    if hu['ccod_bal'] == 'A599':
                        hu['monto'] = str(round( float(TANC),2))
                    if hu['ccod_bal'] == 'A999':
                        hu['monto'] = str(round( float(TTA),2))
                    if hu['ccod_bal'] == 'P699':
                        hu['monto'] = str(round( float(TOTALPASIVO),2))
                    if hu['ccod_bal'] == 'P999':
                        hu['monto'] = str(round(float(TOTALPASIVOYPATRIMONIONETO), 2))
                    if hu['ccod_bal'] == 'N999':
                        hu['monto'] = str(round(float(UB), 2))
                    if hu['ccod_bal'] == 'P399':
                        hu['monto'] = str(round(float(TOTALPASIVOSCORRIENTES), 2))
                    if hu['ccod_bal'] == 'P899':
                        hu['monto'] = str(round(float(TOTALPATRIMONIO), 2))
                    if hu['ccod_bal'] == 'F999':
                        hu['monto'] = str( round(float(UB),2))

            soloN=[]
            soloF=[]
            for t in range(len(lista)):
                if lista[t]['ccod_bal'][:1] == 'N':
                    soloN.append({'ccod_bal':str(lista[t]['ccod_bal']),'cdsc':lista[t]['cdsc'], 'monto':lista[t]['monto']})
                else:
                    soloF.append({'ccod_bal':str(lista[t]['ccod_bal']),'cdsc':lista[t]['cdsc'], 'monto':lista[t]['monto']})

            for x in range(len(soloN)):
                cdsc = soloN[x]['cdsc']
                mont = soloN[x]['monto']
                ws['B'+str(inicio)] = cdsc
                ws['C'+str(inicio)] = float(mont)
                _cell = ws['C'+str(inicio)]
                _cell.number_format = '#,##0.00'
                _cell = ws['F'+str(inicio)]
                _cell.number_format = '#,##0.00'
                inicio = inicio + 1

            for xi in range(len(soloF)):
                cdsc = soloF[xi]['cdsc']
                mont = soloF[xi]['monto']
                ws['E'+str(iniciob)] = cdsc
                ws['F'+str(iniciob)] = float(mont)
                iniciob = iniciob + 1

            '''
            for field in h:
                if field['ccod_cue'] == 'REI':
                    ws['C33'] = float(field['valor'])
                    _cell = ws['C33']
                    _cell.number_format = '#,##0.00'
                if field['ccod_cue'] == 'UB':
                    ws['C7'] = float(field['valor'])
                    _cell = ws['C7']
                    _cell.number_format = '#,##0.00'
            '''
            nuevoarray = ['B33','C33','B2']
            for cell in nuevoarray:
                c= ws[cell]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

            nombre_archivo ="EstadoResultados.xlsx"
            #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
            response = HttpResponse(content_type="application/ms-excel")
            contenido = "attachment; filename={0}".format(nombre_archivo)
            response["Content-Disposition"] = contenido
            wb.save(response)
            return response


class reporteEstadoFinancieros(TemplateView):
    def get(self, request, *args, **kwargs):
        
        pk = self.kwargs.get('pk')
        cmes = self.kwargs.get('cmes')
        proposito = self.kwargs.get('proposito')

        if len(cmes)==1:
            cmes = '0'+str(cmes)
        else:
            cmes = cmes

        print(pk, cmes, proposito, "este es estado finaciero")
        wb = Workbook()
        ws = wb.active

        ws.column_dimensions['A'].width = 3
        ws.column_dimensions['B'].width = 45
        ws.column_dimensions['C'].width = 16
        ws.column_dimensions['D'].width = 3
        ws.column_dimensions['E'].width = 45
        ws.column_dimensions['F'].width = 16


        Datoses_b = datosEstadoFinanciero()
        hi = Datoses_b.diarioPlanesc(pk,cmes, proposito)            

        soloA = []
        soloP = []
        for tt in hi:
            if tt['ccod_bal'][:1] =="A":
                soloA.append(tt)
                #soloA.sort()
            elif tt['ccod_bal'][:1] =="P":
                soloP.append(tt)
                #soloP.sort()
        soloA.insert(35,{'ccod_bal':'','cdsc':'', 'monto':str(float(0.0))})

        inicio = 5
        for x in range(len(soloP)):
            cdsc = soloA[x]['cdsc']
            mont = soloA[x]['monto']
            cdscP = soloP[x]['cdsc']
            montP = soloP[x]['monto']

            ws['B'+str(inicio)] = cdsc
            ws['C'+str(inicio)] = float(mont)

            ws['E'+str(inicio)] = cdscP
            ws['F'+str(inicio)] = float(montP)

            if soloA[x]['ccod_bal'][1:] =="000":
                cel = str('B')+str(inicio)
                cel1 = str('E')+str(inicio)
                ws['C'+str(inicio)] = ""
                ws['F'+str(inicio)] = ""

                c= ws[cel]
                c1= ws[cel1]

                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

                c1.font = Font(size=12)
                c1.font= Font(bold=True)
                c1.fill = PatternFill("solid", fgColor="DDDDDD")
            else:
                #pass
                _cell = ws['C'+str(inicio)]
                _cell.number_format = '#,##0.00'
                _cell = ws['F'+str(inicio)]
                _cell.number_format = '#,##0.00'

            if soloA[x]['ccod_bal'][2:] =="00" or soloA[x]['ccod_bal'][1:] =="999":
                cel = str('B')+str(inicio)
                cel1 = str('E')+str(inicio)

                c= ws[cel]
                c1= ws[cel1]

                c.font = Font(size=12)
                c.font= Font(bold=True)
                
                c1.font = Font(size=12)
                c1.font= Font(bold=True)
                
            inicio = inicio + 1

        nombre_archivo ="EstadoFinanciero.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class reporteEstadoNaturalesa(TemplateView):
    def get(self, request, *args, **kwargs):
        print("este es estado finaciero")
        pk = self.kwargs.get('pk')
        cmes= self.kwargs.get('cmes')
        proposito = self.kwargs.get('proposito')

        if len(cmes) == 1:
            cmes = '0'+ str(cmes)
        else:
            cmes =  cmes

        wb = Workbook()
        ws = wb.active

        ws.column_dimensions['A'].width = 3
        ws.column_dimensions['B'].width = 45
        ws.column_dimensions['C'].width = 16
        ws.column_dimensions['D'].width = 3
        ws.column_dimensions['E'].width = 45
        ws.column_dimensions['F'].width = 16


        Datoses_b = datosEstadoFinanciero()
        hi = Datoses_b.diarioPlanesc(pk,cmes, proposito)            

        soloN = []
        
        for tt in hi:
            if tt['ccod_bal'][:1] =="N":
                soloN.append(tt)
                #soloA.sort()
            
        

        inicio = 5
        for x in range(len(soloN)):
            cdsc = soloN[x]['cdsc']
            mont = soloN[x]['monto']
            

            ws['B'+str(inicio)] = cdsc
            ws['C'+str(inicio)] = float(mont)

            

            if soloN[x]['ccod_bal'][1:] =="000":
                cel = str('B')+str(inicio)
                cel1 = str('E')+str(inicio)
                ws['C'+str(inicio)] = ""
                ws['F'+str(inicio)] = ""

                c= ws[cel]
                

                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

            else:
                #pass
                _cell = ws['C'+str(inicio)]
                _cell.number_format = '#,##0.00'
                _cell = ws['F'+str(inicio)]
                _cell.number_format = '#,##0.00'

            if soloN[x]['ccod_bal'][2:] =="00" or soloN[x]['ccod_bal'][1:] =="999":
                cel = str('B')+str(inicio)
                

                c= ws[cel]
                

                c.font = Font(size=12)
                c.font= Font(bold=True)
                
                
                
            inicio = inicio + 1

        nombre_archivo ="ReporteNaturaleza.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class reporteEstadoFunciones(TemplateView):
    def get(self, request, *args, **kwargs):
        print("este es estado finaciero")
        pk = self.kwargs.get('pk')
        cmes = self.kwargs.get('cmes')
        proposito =  self.kwargs.get('proposito')

        if len(cmes)==1:
            cmes = '0'+ cmes
        else:
            cmes = cmes

        wb = Workbook()
        ws = wb.active

        ws.column_dimensions['A'].width = 3
        ws.column_dimensions['B'].width = 45
        ws.column_dimensions['C'].width = 16
        ws.column_dimensions['D'].width = 3
        ws.column_dimensions['E'].width = 45
        ws.column_dimensions['F'].width = 16


        Datoses_b = datosEstadoFinanciero()
        hi = Datoses_b.diarioPlanesc(pk,cmes, proposito)            

        soloF = []
        
        for tt in hi:
            if tt['ccod_bal'][:1] =="F":
                soloF.append(tt)
                #soloA.sort()
            
        #soloN.insert(35,{'ccod_bal':'','cdsc':'', 'monto':str(float(0.0))})

        inicio = 5
        for x in range(len(soloF)):
            cdsc = soloF[x]['cdsc']
            mont = soloF[x]['monto']
            

            ws['B'+str(inicio)] = cdsc
            ws['C'+str(inicio)] = float(mont)


            if soloF[x]['ccod_bal'][1:] =="000":
                cel = str('B')+str(inicio)
                
                ws['C'+str(inicio)] = ""
                ws['F'+str(inicio)] = ""

                c= ws[cel]
                

                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

            else:
                #pass
                _cell = ws['C'+str(inicio)]
                _cell.number_format = '#,##0.00'

            if soloF[x]['ccod_bal'][2:] =="00" or soloF[x]['ccod_bal'][1:] =="999":
                cel = str('B')+str(inicio)
                

                c= ws[cel]
                

                c.font = Font(size=12)
                c.font= Font(bold=True)
                
                
                
            inicio = inicio + 1

        nombre_archivo ="Reportefuncion.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class reporteF11(TemplateView):
    def get(self, request, *args, **kwargs):

        pk = self.kwargs.get('pk')
        cmes = self.kwargs.get('cmes')
        proposito = self.kwargs.get('proposito')
        #print(pk,cmes,'TTTTTTTTTTT')

        nameFile = '/F11.xlsx'
        miruta = ruta_origen+nameFile

        wb = load_workbook(miruta)
        ws = wb.active
        #cons = consultaBancos(pk, 'C',cmes)
        cons = listaCuos10(pk,'C', cmes, proposito)


        thin_border = Border(left=Side(style='thin'),
                     right=Side(style='thin'),
                     #top=Side(style='thin'),
                     bottom=Side(style='medium'))

        thin_lat = Border(#left=Side(style='thin'),
                 #right=Side(style='thin')
                 top=Side(style='medium')
                 #bottom=Side(style='medium')
                 )

        inicio = 9
        for cn in cons:
            ws['B'+str(inicio)] = cn['cmes']
            ws['C'+str(inicio)] = cn['sd']
            ws['D'+str(inicio)] = cn['nasiento']
            ws['E'+str(inicio)] = cn['ffechareg']
            ws['F'+str(inicio)] = cn['cglosa']
            ws['G'+str(inicio)] = cn['ccod_cue']
            ws['H'+str(inicio)] = cn['cdsc']
            ws['I'+str(inicio)] = cn['ndebe']
            ws['J'+str(inicio)] = cn['nhaber']

            inicio = inicio + 1

        ws['I'+str(inicio-3)].border = thin_lat
        c = ws['I'+str(inicio-3)]
        c.font = Font(size=12)
        c.font= Font(bold=True)

        ws['J'+str(inicio-3)].border = thin_lat
        c = ws['J'+str(inicio-3)]
        c.font = Font(size=12)
        c.font= Font(bold=True)

        ws['I'+str(inicio-1)].border = thin_lat
        c = ws['I'+str(inicio-1)]
        c.font = Font(size=12)
        c.font= Font(bold=True)

        ws['J'+str(inicio-1)].border = thin_lat
        c = ws['J'+str(inicio-1)]
        c.font = Font(size=12)
        c.font= Font(bold=True)

        x = datetime.now()
        pie = 'Generado automáticamente por Gestión Contable Financiero Plus – Premium 20.00 - NewContaSis - Versión Educativa el ' + funcionFecha(x)
        ws['B'+str(inicio+1)] = pie
        
        li = ['B','C','D','E','F','G','H','I','J']

        for x in li:
            ws[x + str(inicio+1)].border = thin_lat
            ws[x + str(inicio+1)].border = thin_lat
            #ws[x + str(cont)].border = thin_border

        cel = 'B'+str(inicio+1)
        c= ws[cel]
        c.font = Font(size=12)
        c.font= Font(bold=True)
        
        nombre_archivo ="F.1.1 - Libro Caja y Bancos - Detalle de los movimientos de Efectivo.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class reporteF12(TemplateView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        cmes = self.kwargs.get('cmes')
        proposito = self.kwargs.get('proposito')

        nameFile = '/F12.xlsx'
        miruta = ruta_origen+nameFile

        wb = load_workbook(miruta)
        ws = wb.active
        #cons = consultaBancos(pk, '104', cmes)
        cons = listaCuos10(pk, '104', cmes, proposito)
        

        thin_border = Border(left=Side(style='thin'),
                     right=Side(style='thin'),
                     #top=Side(style='thin'),
                     bottom=Side(style='medium'))

        thin_lat = Border(#left=Side(style='thin'),
                 #right=Side(style='thin')
                 top=Side(style='medium')
                 #bottom=Side(style='medium')
                 )
        inicio = 13
        for cn in cons:
            ws['B'+str(inicio)] = cn['cmes']
            ws['C'+str(inicio)] = cn['sd']
            ws['D'+str(inicio)] = cn['nasiento']
            ws['E'+str(inicio)] = cn['ffechareg']
            ws['F'+str(inicio)] = cn['ccod_pagsu']
            ws['G'+str(inicio)] = cn['cglosa']
            ws['H'+str(inicio)] = cn['ccod_cli']
            ws['I'+str(inicio)] = cn['ccod_doc']
            ws['J'+str(inicio)] = cn['cnumero']
            ws['K'+str(inicio)] = cn['ccod_cue']
            ws['L'+str(inicio)] = cn['cdsc']
            ws['M'+str(inicio)] = cn['ndebe']
            ws['N'+str(inicio)] = cn['nhaber']

            inicio = inicio + 1

        ws['M'+str(inicio-3)].border = thin_lat
        c = ws['M'+str(inicio-3)]
        c.font = Font(size=12)
        c.font= Font(bold=True)

        ws['N'+str(inicio-3)].border = thin_lat
        c = ws['N'+str(inicio-3)]
        c.font = Font(size=12)
        c.font= Font(bold=True)

        ws['M'+str(inicio-1)].border = thin_lat
        c = ws['M'+str(inicio-1)]
        c.font = Font(size=12)
        c.font= Font(bold=True)

        ws['N'+str(inicio-1)].border = thin_lat
        c = ws['N'+str(inicio-1)]
        c.font = Font(size=12)
        c.font= Font(bold=True)

        x = datetime.now()
        pie = 'Generado automáticamente por Gestión Contable Financiero Plus – Premium 20.00 - NewContaSis - Versión Educativa el ' + funcionFecha(x)
        ws['B'+str(inicio+1)] = pie
        
        li = ['B','C','D','E','F','G','H','I','J','K','L','M','N']
        for x in li:
            ws[x + str(inicio+1)].border = thin_lat
            ws[x + str(inicio+1)].border = thin_lat

        cel = 'B'+str(inicio+1)
        c= ws[cel]
        c.font = Font(size=12)
        c.font= Font(bold=True)


        nombre_archivo ="F.1.2 - Libro Caja y Bancos - Detalle de los movimientos de las Cuentas Corrientes.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class reporteF81(TemplateView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        cmes = self.kwargs.get('cmes')
        proposito = self.kwargs.get('proposito')

        nameFile = '/F81.xlsx'
        miruta = ruta_origen+nameFile

        wb = load_workbook(miruta)
        ws = wb.active
        cons = consultaGeneral(pk, 'C', cmes, proposito)

        thin_border = Border(left=Side(style='thin'),
                     right=Side(style='thin'),
                     #top=Side(style='thin'),
                     bottom=Side(style='medium'))

        thin_lat = Border(#left=Side(style='thin'),
                 #right=Side(style='thin')
                 top=Side(style='medium')
                 #bottom=Side(style='medium')
                 )

        inicio = 12
        for cn in cons:
            ws['B'+str(inicio)] = cn['cmes']
            c = ws['B'+str(inicio)]
            c.font = Font(size=7)

            ws['C'+str(inicio)] = cn['sd']
            c = ws['C'+str(inicio)]
            c.font = Font(size=7)

            ws['D'+str(inicio)] = cn['asiento']
            c = ws['D'+str(inicio)]
            c.font = Font(size=7)

            ws['E'+str(inicio)] = cn['ffechadoc']
            c = ws['E'+str(inicio)]
            c.font = Font(size=7)

            ws['F'+str(inicio)] = cn['ffechaven']
            c = ws['F'+str(inicio)]
            c.font = Font(size=7)

            ws['G'+str(inicio)] = cn['ccod_doc']
            c = ws['G'+str(inicio)]
            c.font = Font(size=7)

            ws['H'+str(inicio)] = cn['serie']
            c = ws['H'+str(inicio)]
            c.font = Font(size=7)

            ws['I'+str(inicio)] = cn['dua_ano']
            c = ws['I'+str(inicio)]
            c.font = Font(size=7)

            ws['J'+str(inicio)] = cn['numero']
            c = ws['J'+str(inicio)]
            c.font = Font(size=7)

            ws['K'+str(inicio)] = cn['T']
            c = ws['K'+str(inicio)]
            c.font = Font(size=7)

            ws['L'+str(inicio)] = cn['ccod_cli']
            c = ws['L'+str(inicio)]
            c.font = Font(size=7)

            ws['M'+str(inicio)] = cn['crazon']
            c = ws['M'+str(inicio)]
            c.font = Font(size=7)

            ws['N'+str(inicio)] = cn['nina']
            c = ws['N'+str(inicio)]
            c.font = Font(size=7)

            ws['O'+str(inicio)] = cn['nnet']
            c = ws['O'+str(inicio)]
            c.font = Font(size=7)

            ws['P'+str(inicio)] = cn['nbase2']
            c = ws['P'+str(inicio)]
            c.font = Font(size=7)

            ws['Q'+str(inicio)] = cn['nigv2']
            c = ws['Q'+str(inicio)]
            c.font = Font(size=7)

            ws['R'+str(inicio)] = cn['nbase3']
            c = ws['R'+str(inicio)]
            c.font = Font(size=7)

            ws['S'+str(inicio)] = cn['nigv3']
            c = ws['S'+str(inicio)]
            c.font = Font(size=7)

            ws['T'+str(inicio)] = cn['IGV']
            c = ws['T'+str(inicio)]
            c.font = Font(size=7)

            ws['U'+str(inicio)] = cn['nisc']
            c = ws['U'+str(inicio)]
            c.font = Font(size=7)

            ws['V'+str(inicio)] = cn['nexo']
            c = ws['V'+str(inicio)]
            c.font = Font(size=7)

            ws['W'+str(inicio)] = cn['ntot']
            c = ws['W'+str(inicio)]
            c.font = Font(size=7)

            ws['X'+str(inicio)] = cn['cdoc_ndom']
            c = ws['X'+str(inicio)]
            c.font = Font(size=7)

            ws['Y'+str(inicio)] = cn['cnumresp']
            c = ws['Y'+str(inicio)]
            c.font = Font(size=7)

            ws['Z'+str(inicio)] = cn['fdocdet']
            c = ws['Z'+str(inicio)]
            c.font = Font(size=7)

            ws['AA'+str(inicio)] = cn['ntc']
            c = ws['AA'+str(inicio)]
            c.font = Font(size=7)

            ws['AB'+str(inicio)] = cn['fpagimpret']
            c = ws['AB'+str(inicio)]
            c.font = Font(size=7)

            ws['AC'+str(inicio)] = cn['cdocmodi']
            c = ws['AC'+str(inicio)]
            c.font = Font(size=7)

            ws['AD'+str(inicio)] = cn['COD_REFNC_I']
            c = ws['AD'+str(inicio)]
            c.font = Font(size=7)

            ws['AE'+str(inicio)] = cn['COD_REFNC_D']
            c = ws['AE'+str(inicio)]
            c.font = Font(size=7)

    
            inicio =  inicio + 1


        

        li = ['B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE']
        for x in li:
            ws[x + str(inicio-1)].border = thin_lat
            c= ws[x + str(inicio-1)]
            c.font = Font(size=12)
            c.font= Font(bold=True)
            
        x = datetime.now()
        pie = 'Generado automáticamente por Gestión Contable Financiero Plus – Premium 20.00 - NewContaSis - Versión Educativa el ' +  funcionFecha(x)
        ws['B'+str(inicio)] = pie

        cel = 'B'+str(inicio)
        c= ws[cel]
        #c.font = Font(size=7)
        c.font= Font(bold=True)

        nombre_archivo ="F.8.1- Registro de Compras.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class reporteF141(TemplateView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        cmes = self.kwargs.get('cmes')
        proposito = self.kwargs.get('proposito')

        nameFile = '/F141.xlsx'
        miruta = ruta_origen+nameFile

        wb = load_workbook(miruta)
        ws = wb.active

        thin_border = Border(left=Side(style='thin'),
                     right=Side(style='thin'),
                     #top=Side(style='thin'),
                     bottom=Side(style='medium'))

        thin_lat = Border(#left=Side(style='thin'),
                 #right=Side(style='thin')
                 top=Side(style='medium')
                 #bottom=Side(style='medium')
                 )

        cons = consultaGeneral(pk, 'V', cmes, proposito)
        inicio = 12
        for cn in cons:
            ws['B'+str(inicio)] = cn['cmes']
            ws['C'+str(inicio)] = cn['sd']
            ws['D'+str(inicio)] = cn['asiento']
            ws['E'+str(inicio)] = cn['ffechadoc']
            ws['F'+str(inicio)] = cn['ffechaven']
            ws['G'+str(inicio)] = cn['ccod_doc']
            ws['H'+str(inicio)] = cn['serie']
            ws['I'+str(inicio)] = cn['numero']
            ws['J'+str(inicio)] = cn['T']
            ws['K'+str(inicio)] = cn['ccod_cli']
            ws['L'+str(inicio)] = cn['crazon']
            ws['M'+str(inicio)] = cn['nbase2']
            ws['N'+str(inicio)] = cn['nnet']
            ws['O'+str(inicio)] = cn['nexo']
            ws['P'+str(inicio)] = cn['nina']
            ws['Q'+str(inicio)] = cn['nisc']
            ws['R'+str(inicio)] = cn['nimp']
            ws['S'+str(inicio)] = cn['nbase3']
            ws['T'+str(inicio)] = cn['ntot']
            ws['U'+str(inicio)] = cn['ntc']
            ws['V'+str(inicio)] = cn['fpagimpret']
            ws['W'+str(inicio)] = cn['cdocmodi']
            ws['X'+str(inicio)] = cn['serie']
            ws['Y'+str(inicio)] = cn['numero']

            #ws['AE'+str(inicio)] = cn['COD_REFNC_D']

            inicio =  inicio + 1
        li = ['B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y']
        for x in li:
            ws[x + str(inicio-1)].border = thin_lat
            c= ws[x + str(inicio-1)]
            #c.font = Font(size=12)
            c.font= Font(bold=True)

        x = datetime.now()
        pie = 'Generado automáticamente por Gestión Contable Financiero Plus – Premium 20.00 - NewContaSis - Versión Educativa el ' + funcionFecha(x)
        ws['B'+str(inicio)] = pie
        cel = 'B'+str(inicio)
        c= ws[cel]
        #c.font = Font(size=7)
        c.font= Font(bold=True)

        nombre_archivo ="F.14.1-Registro de Ventas e Ingresos.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

###########################
###########################
# fin reportes en excvel
###########################
###########################
#funciones para los reportes

def consultaGeneral(usuario, tipo, cmes, proposito):
    if len(cmes) == 1:
        cmes = '0'+ cmes
    else:
        cmes = cmes

    now = datetime.now()
    datetime_str = ''+cmes+'/28/'+str(now.year)[2:]+' 13:55:26'
    datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
    last_month = datetime_object - timedelta(days=0)

    if tipo == 'C':
        #consulta = PgDiario.objects.all().filter(ccod_user= usuario).filter(cregis='C')    
        if proposito == 0:
            consulta = PgDiario.objects.all().filter(ccod_user= usuario).filter(ffechareg__lte=last_month).filter(cregis='C')  
        if  proposito==1:
            consulta = PgDiario.objects.all().filter(ccod_user= usuario).filter(ffechareg__month=cmes).filter(cregis='C')    
    elif tipo == 'V':
        if proposito == 0:
            consulta = PgDiario.objects.all().filter(ccod_user= usuario).filter(ffechareg__lte=last_month).filter(cregis='V')
        if  proposito==1:
            #consulta = PgDiario.objects.all().filter(ccod_user= usuario).filter(cregis='V')
            consulta = PgDiario.objects.all().filter(ccod_user= usuario).filter(ffechareg__month=cmes).filter(cregis='V')
    lista40=[]
    for con in consulta:
        fv = ''
        if con.ffechaven is None:
            fv = ''
        else:
            fv = con.ffechaven.strftime("%d/%m/%Y")

        guion = con.cnumero.find('-')
        iz = ''
        der = ''
        if guion == -1:
            iz = con.cnumero
            der = ''
        else:
            iz = con.cnumero[:guion]
            der = con.cnumero[guion+1:]
        ntipo = PgCliPro.objects.all().filter(usuario=usuario).filter(ccod_cli=con.ccod_cli)
        #print(ntipo[0].ntipo, ntipo[0].crazon ,'5555555555')
        data = {'cmes':con.cmes,
        'sd':con.ccod_ori,
        'asiento': con.nasiento,
        'ffechadoc': con.ffechadoc.strftime("%d/%m/%Y"),
        'ffechaven': fv,
        'ccod_doc': con.ccod_doc,
        'serie': iz.strip(),
        'dua_ano':con.dua_año,
        'numero': der.strip(),
        'T':ntipo[0].ntipo,
        'ccod_cli': con.ccod_cli,
        'crazon':ntipo[0].crazon.strip(),
        'nnet': con.nnet,
        'nimp':con.nimp,
        'IGV': con.nimp,
        'nbase2': con.nbase2,
        'nigv2': con.nigv2,
        'nbase3': con.nbase3,
        'nigv3': con.nigv3,
        'nina': con.nina,
        'nisc': con.nisc,
        'nexo': con.nexo,
        'ntot': con.ntot,
        'cdoc_ndom':con.cdoc_ndom,
        'cnumresp':con.cnumresp,
        'fdocdet':con.fdocdet,
        'ntc': con.ntc,
        'fpagimpret':con.fpagimpret,
        'cdocmodi': con.cdocmodi,
        'COD_REFNC_I':iz.strip(),
        'COD_REFNC_D':der.strip()
        }
        lista40.append(data)

    nnet = float(0)
    nimp = float(0)
    IGV = float(0)
    nbase2 = float(0)
    nigv2 = float(0)
    nbase3 = float(0)
    nigv3 = float(0)
    nina = float(0)
    nisc = float(0)
    nexo = float(0)
    ntot = float(0)
    
    for l in lista40:
        nnet = nnet + float(l['nnet'])
        nimp =nimp + float(l['nimp'])
        IGV =  IGV + float(l['IGV'])
        nbase2 = nbase2 + float(l['nbase2'])
        nigv2 = nigv2 + float(l['nigv2'])
        nbase3 = nbase3 + float(l['nbase3'])
        nigv3 = nigv3 + float(l['nigv3'])
        nina = nina + float(l['nina'])
        nisc = nisc + float(l['nisc'])
        nexo = nexo + float(l['nexo'])
        ntot = ntot + float(l['ntot'])
        

    totales = {
    'cmes':'',
    'sd':'',
    'asiento': '',
    'ffechadoc': '',
    'ffechaven': '',
    'ccod_doc': '',
    'serie': '',
    'dua_ano':'',
    'numero': '',
    'T':'',
    'ccod_cli': '',
    'crazon':'TOTALES:',
    'nnet': float(round(nnet,2)),
    'nimp':float(round(nimp,2)),
    'IGV': float(round(IGV,2)),
    'nbase2': float(round(nbase2,2)),
    'nigv2': float(round(nigv2,2)),
    'nbase3': float(round(nbase3,2)),
    'nigv3': float(round(nigv3,2)),
    'nina': float(round(nina,2)),
    'nisc': float(round(nisc,2)),
    'nexo': float(round(nexo,2)),
    'ntot': float(round(ntot,2)),
    'cdoc_ndom':'',
    'cnumresp':'',
    'fdocdet':'',
    'ntc': '',
    'fpagimpret':'',
    'cdocmodi': '',
    'COD_REFNC_I':'',
    'COD_REFNC_D':''
    }

    lista40.append(totales)

    #for u in lista40:
    #    print(u)
    return lista40


def listaCuos10(usuario,tipo,cmes, proposito):

    if len(cmes) == 1:
        cmes = '0'+ cmes
    else:
        cmes = cmes

    now = datetime.now()
    datetime_str = ''+cmes+'/28/'+str(now.year)[2:]+' 13:55:26'
    datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
    last_month = datetime_object - timedelta(days=0)


    #saldoinicail = saldoInicial(usuario, cmes, 'C')

    if tipo=='C':
        saldoinicail = saldoInicial(usuario, cmes, 'C')
        consultas = PgDiario.objects.all().filter(ccod_user=usuario).filter(Q(ccod_cue__startswith='101')|
            Q(ccod_cue__startswith='102')|
            Q(ccod_cue__startswith='103')|
            Q(ccod_cue__startswith='104')|
            Q(ccod_cue__startswith='105')|
            Q(ccod_cue__startswith='106')
            )
    if tipo=='104':
        saldoinicail = saldoInicial(usuario, cmes, '104')
        consultas = PgDiario.objects.all().filter(ccod_user=usuario).filter(ccod_cue__startswith='104')
        #consulta = PgDiario.objects.all().filter(ccod_user=usuario).filter(ffechareg__lte=last_month).filter(ccod_cue__startswith='104')

    #consulta = PgDiario.objects.all().filter(ccod_user=usuario).filter(ffechareg__lte=last_month).exclude(cdes='S')
    #consulta = PgDiario.objects.all().filter(ccod_user=usuario).filter(ffechareg__lte=last_month).exclude(cdes='S')
    if proposito == 0:
        consulta =  PgDiario.objects.all().filter(ffechareg__month=cmes).filter(ccod_user=usuario).exclude(cdes='S').exclude(cmes='00')
    if proposito == 1:
        consulta =  PgDiario.objects.all().filter(ffechareg__lte=last_month).filter(ccod_user=usuario).exclude(cdes='S').exclude(cmes='00')
    
    


    listaasientos10 = []
    #primero los cuos con las posiciones
    posi = None
    for asiento in consultas:
        
        cuo = str(asiento.cmes).strip()+str(asiento.ccod_ori).strip()+str(asiento.nasiento).strip()
        if asiento.ndebe > asiento.nhaber:
            posi = 'd'
        else:
            posi = 'h'
        m = {'cuo': cuo, 'posi': posi, 'ccod_cue': asiento.ccod_cue.strip()}
        listaasientos10.append(m)

    nuevo_listado=[]
    for nc in consulta:
        cuo = str(nc.cmes).strip()+str(nc.ccod_ori).strip()+str(nc.nasiento).strip()
        for cn in listaasientos10:
            razon = PgCliPro.objects.all().filter(ccod_cli=nc.ccod_cli)
            if cn['cuo'] == cuo:
                if cn['posi'] == 'd':
                    ma = {'cmes':nc.cmes,
                    'sd':nc.ccod_ori,
                    'nasiento': nc.nasiento,
                    'ccod_cue':nc.ccod_cue.strip(),
                    'ffechareg':nc.ffechareg.strftime("%d/%m/%Y"),
                    'cglosa':nc.cglosa.strip(),
                    'cdsc': nc.pgplan.cdsc.strip(),
                    'ccod_pagsu':nc.ccod_pagsu,
                    'ccod_cli':'',
                    'ccod_doc':nc.ccod_doc,
                    'cnumero':nc.cnumero,
                    #'debe': nc.ndebe,
                    #'haber': nc.nhaber,
                    'ndebe': nc.ndebe + nc.nhaber,
                    'nhaber': float(0),
                    }
                    nuevo_listado.append(ma)
                else:
                    #razon = PgCliPro.objects.all().filter(ccod_cli=nc.ccod_cli)
                    ma = {'cmes':nc.cmes,
                    'sd':nc.ccod_ori,
                    'nasiento': nc.nasiento,
                    'ccod_cue':nc.ccod_cue.strip(),
                    'ffechareg':nc.ffechareg.strftime("%d/%m/%Y"),
                    'cglosa':nc.cglosa.strip(),
                    'cdsc': nc.pgplan.cdsc.strip(),
                    'ccod_pagsu':nc.ccod_pagsu,
                    'ccod_cli':'',
                    'ccod_doc':nc.ccod_doc,
                    'cnumero':nc.cnumero,
                    #'debe': nc.ndebe,
                    #'haber': nc.nhaber,
                    'ndebe':  float(0),
                    'nhaber': nc.ndebe + nc.nhaber,
                    }
                    nuevo_listado.append(ma)
    ultimaLista=[]
    ultimaLista.append(saldoinicail)

    for ultima in ultimaLista:
        print(ultima, '$ $')

    if tipo == 'C':
        for ta in nuevo_listado:
            if ta['ccod_cue'][:2] != '10':
                ma = {
                'cmes':ta['cmes'],
                'sd':ta['sd'],
                'nasiento': ta['nasiento'],
                'ccod_cue':ta['ccod_cue'],
                'ffechareg':ta['ffechareg'],
                'cglosa':ta['cglosa'],
                'cdsc': ta['cdsc'],
                'ccod_pagsu':ta['ccod_pagsu'],
                'ccod_cli':ta['ccod_cli'],
                'ccod_doc':ta['ccod_doc'],
                'cnumero':ta['cnumero'],
                'ndebe':  ta['ndebe'],
                'nhaber': ta['nhaber']
                }
                ultimaLista.append(ma)
    else:
        for te in nuevo_listado:
            print(te, '85858585858585')
            if te['ccod_cue'][:3].strip() != '104':
                ma = {
                'cmes':te['cmes'],
                'sd':te['sd'],
                'nasiento': te['nasiento'],
                'ccod_cue':te['ccod_cue'],
                'ffechareg':te['ffechareg'],
                'cglosa':te['cglosa'],
                'cdsc': te['cdsc'],
                'ccod_pagsu':te['ccod_pagsu'],
                'ccod_cli':te['ccod_cli'],
                'ccod_doc':te['ccod_doc'],
                'cnumero':te['cnumero'],
                'ndebe':  te['ndebe'],
                'nhaber': te['nhaber']
                }
                ultimaLista.append(ma)

    tdebe = float(0)
    thaber = float(0)
    fdebe = float(0)
    fhaber = float(0)
    ffdebe = float(0)
    ffhaber = float(0)
    for t in ultimaLista:
        tdebe = tdebe + float(t['ndebe']) 
        thaber = thaber + float(t['nhaber'])
        #print(t)

    if tdebe > thaber:
        fdebe = float(0)
        fhaber = tdebe - thaber

        ffdebe = tdebe + fdebe  
        ffhaber = thaber + fhaber
    else:
        fdebe = thaber - tdebe
        fhaber =float(0)

        ffdebe = tdebe +  fdebe  
        ffhaber = thaber + fhaber

    mt  = {'cmes':'','sd':'', 'nasiento': '', 'ffechareg':'', 'cglosa':'', 'ccod_cue':'', 'cdsc': 'TOTAL:', 'ndebe':tdebe , 'nhaber': thaber, 'ccod_pagsu': '','ccod_cli':'','ccod_doc':'','cnumero':''}
    mf  = {'cmes':'','sd':'', 'nasiento': '', 'ffechareg':'', 'cglosa':'', 'ccod_cue':'', 'cdsc': 'SALDO FINAL:', 'ndebe':fdebe , 'nhaber': fhaber, 'ccod_pagsu': '','ccod_cli':'','ccod_doc':'','cnumero':''}
    mff = {'cmes':'','sd':'', 'nasiento': '', 'ffechareg':'', 'cglosa':'', 'ccod_cue':'', 'cdsc': '', 'ndebe':ffdebe , 'nhaber': ffhaber,'ccod_pagsu': '','ccod_cli':'','ccod_doc':'','cnumero':''}
    ultimaLista.append(mt)
    ultimaLista.append(mf)
    ultimaLista.append(mff)
        



    return ultimaLista

    
    
        


def consultaBancos(usuario, tipo, cmes):

    now = datetime.now()
    datetime_str = ''+cmes+'/28/'+str(now.year)[2:]+' 13:55:26'
    datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
    last_month = datetime_object - timedelta(days=0)
    #consultaSaldoInicial= PgDiario.objects.all().filter(ccod_user=usuario).filter(cmes='00').filter(ccod_ori='00')
    m = saldoInicial(usuario, cmes, 'C')
    #C para completo
    #V 104 para solo 104
    listaConpletac = []
    if tipo == '104':
        consulta = PgDiario.objects.all().filter(ccod_user=usuario).filter(ffechareg__lte=last_month).filter(ccod_cue__startswith='104').exclude(cmes='00')

    lista=[]
    lista.append(m)
    for x in consulta:
        razon = PgCliPro.objects.all().filter(ccod_cli=x.ccod_cli)
        m = {'cmes':x.cmes,
        'sd':x.ccod_ori,
        'nasiento': x.nasiento,
        'ffechareg':x.ffechareg.strftime("%d/%m/%Y"),
        'cglosa':x.cglosa.strip(),
        'ccod_cue': x.ccod_cue.strip(),
        'cdsc': x.pgplan.cdsc.strip(),
        'ndebe': x.ndebe,
        'nhaber': x.nhaber,
        'ccod_pagsu':x.ccod_pagsu,
        'ccod_cli':razon[0].crazon.strip(),
        'ccod_doc':x.ccod_doc,
        'cnumero':x.cnumero
        }
        lista.append(m)

    tdebe = float(0)
    thaber = float(0)
    fdebe = float(0)
    fhaber = float(0)
    ffdebe = float(0)
    ffhaber = float(0)
    for t in lista:
        tdebe = tdebe + float(t['ndebe']) 
        thaber = thaber + float(t['nhaber'])
        #print(t)

    if tdebe > thaber:
        fdebe = float(0)
        fhaber = tdebe - thaber

        ffdebe = tdebe + fdebe  
        ffhaber = thaber + fhaber
    else:
        fdebe = thaber - tdebe
        fhaber =float(0)

        ffdebe = tdebe +  fdebe  
        ffhaber = thaber + fhaber

    mt  = {'cmes':'','sd':'', 'nasiento': '', 'ffechareg':'', 'cglosa':'', 'ccod_cue':'', 'cdsc': 'TOTAL:', 'ndebe':tdebe , 'nhaber': thaber, 'ccod_pagsu': '','ccod_cli':'','ccod_doc':'','cnumero':''}
    mf  = {'cmes':'','sd':'', 'nasiento': '', 'ffechareg':'', 'cglosa':'', 'ccod_cue':'', 'cdsc': 'SALDO FINAL:', 'ndebe':fdebe , 'nhaber': fhaber, 'ccod_pagsu': '','ccod_cli':'','ccod_doc':'','cnumero':''}
    mff = {'cmes':'','sd':'', 'nasiento': '', 'ffechareg':'', 'cglosa':'', 'ccod_cue':'', 'cdsc': '', 'ndebe':ffdebe , 'nhaber': ffhaber,'ccod_pagsu': '','ccod_cli':'','ccod_doc':'','cnumero':''}
    lista.append(mt)
    lista.append(mf)
    lista.append(mff)

    for ui in lista:
        print(ui)
    #print(lista)

    return lista


def saldoInicial(usuario, cmes, tipo):
    
    if tipo=='C':
        print(usuario, cmes, tipo, '555555555555555555555555555555555555555555555555555555')
        consulta = PgPlan.objects.all().filter(usuario=usuario).filter(nnivel=3).filter(Q(ccod_cue__startswith='101')|
            Q(ccod_cue__startswith='102')|
            Q(ccod_cue__startswith='103')|
            Q(ccod_cue__startswith='104')|
            Q(ccod_cue__startswith='105')|
            Q(ccod_cue__startswith='106')
            )
    if tipo=='104':
        print(usuario, cmes, tipo, '555555555555555555555555555555555555555555555555555555')
        consulta = PgPlan.objects.all().filter(usuario=usuario).filter(nnivel=3).filter(ccod_cue__startswith='104')
        

    lista = []
    listat= []

    debet = float(0)
    habert = float(0)

    if cmes=='0':
        ff  = datetime.now()
        cmes = ff.month
    elif cmes[:1]=='0':
        cmes = cmes[1:]
    else:
        cmes = cmes

    print('usuario', usuario, 'cmes', cmes)
    for x in range(0, int(cmes)):
        for n in consulta:
            db = float(eval("n.ndebe"+str(x)))
            ha = float(eval("n.nhaber"+str(x)))
            m = {}
            if db > float(0):
                m = {'ndebe':db, 'nhaber': float(0), 'x': x}
                lista.append(m)
            if ha > float(0):
                m = {'ndebe':float(0), 'nhaber': ha, 'x': x}
                lista.append(m)

    for li in lista:
        debet = debet + li['ndebe']
        habert = habert + li['nhaber']
    #tt ={'debe': debet, 'haber': habert}
    tt = {
    'cmes':'',
    'sd':'',
    'nasiento':'',
    'ffechareg':'',
    'cglosa':'SALDO INICIAL',
    'ccod_cue':'',
    'cdsc': '',
    'ndebe':debet,
    'nhaber': habert,
    'ccod_pagsu':'',
    'ccod_cli':'',
    'ccod_doc':'',
    'cnumero':''
    }
    listat.append(tt)
    return tt
        
def funcionFecha(x):
    #x = datetime.now()
    day = str(x.day)
    month = str(x.month)
    if len(day) == 1:
        day = '0' + day
    else:
        day =  day
    if len(month) == 1:
        month = '0' +  month
    else:
        month =  month
    ff = str(day) +'/'+ str(month) +'/'+ str(x.year)
    return ff


###########################
###########################


class libroDiarioAsientos(ListView):
    model = PgDiario
    template_name = 'adminis/asientosLibroDiario.html'

    def get_context_data(self, **kwargs):
        context  =  super(libroDiario,self).get_context_data(**kwargs)
        pk = self.kwargs.get('id')
        context['listado'] = PgDiario.objects.all()
        return context

def deleteAsiento(request):
    query = PgDiario.objects.get(ccod_user=id)
    #print(query)
    #query.delete()
    return HttpResponse("Deleted!")



#--------mantenimiento Asientos

class mantenimientoAsientos(TemplateView):
    model = PgDiario
    template_name = 'adminis/mantenimientoAsientos.html'

    def get_context_data(self, **kwargs):
        context = super(mantenimientoAsientos, self).get_context_data(**kwargs)
        pk = self.kwargs.get('id')
        asiento = self.kwargs.get('asiento')
        #print(asiento)
        queryc = PgDiario.objects.all().filter(ccod_user=pk).filter(nasiento=asiento).order_by('id') 
        context['nombre'] = "Eliminacion de Asientos"
        context['consulta'] = queryc
        return context

#//---json devolver un asiento
class onlyOneAsiento(TemplateView):
    def get(self, request, *args, **kwargs):
        
        pk = request.GET['usuario']
        asiento = request.GET['asiento']
        
        queryc = PgDiario.objects.filter(ccod_user=pk).filter(nasiento=asiento).order_by('id') 
        miconsulta = PgDiario.objects.values_list('nasiento','ndebe','nhaber','cglosa','pgplan__ccod_cue','pgplan__cdsc').filter(ccod_user=pk).filter(nasiento=asiento)
        nombre = ['dato0','dato1']
        

        tod = []
        for x in miconsulta:
            r = []
            for n in x:
                r.append(str(n))
            tod.append(r)
        
        j = dict( zip( nombre, tod))


        #miconsulta = PgDiario.objects.values_list('nasiento','ndebe','nhaber','cglosa','pgplan','t2008_11').filter(ccod_cli=pk).filter(nasiento=asiento)
        #data = serializers.serialize('json',queryc,fields=('nasiento','ndebe','nhaber','cglosa','pgplan','pgplan__ccod_cue'))
        #return HttpResponse(data,content_type='application/json')
        return HttpResponse(json.dumps(j), content_type="application/json")


#//-----mantenimiento Asientos

#-------mantenimiento Cuentas

class mantenimientoCuentas(TemplateView):
    model = PgDiario
    template_name = 'adminis/mantenimientoCuentas.html'

    def get_context_data(self, **kwars):
        context  =  super(mantenimientoCuentas, self).get_context_data(**kwargs)
        pk  = self.kwargs.get('id')
        context['nombre'] = "Mantenimiento de cuentas"
        return context

#JSON
class existeAsiento(TemplateView):
    def get(self, request, *args, **kwargs):
        #debe
        asiento_id = request.GET['id']
        usuario = request.GET['usuario']
        #miconsulta = PgDiario.objects.values_list('ccod_cue','cglosa2').filter(ccod_cli=usuario_id).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber'))
        miconsulta = PgDiario.objects.all().filter(nasiento=asiento_id).filter(ccod_user= usuario)
        asientos =  PgDiario.objects.filter(nasiento=asiento_id)
        data = serializers.serialize('json',miconsulta,fields=('pgplan__ccod_cue','pgplan__cdsc'))
        return HttpResponse(data,content_type='application/json')


#//-----mantenimiento Asientos

class Eliminar_Diario(TemplateView):
    def get(self, request, *args, **kwargs):
        
        usuario_id = request.GET['usuario']
        asiento_inicial = request.GET['inicial']
        asiento_final = request.GET['final']
        
        #print(usuario_id, asiento_inicial, asiento_final)
        
        
        if not asiento_final:
            PgDiario.objects.filter(ccod_user=usuario_id).filter(nasiento=asiento_inicial).delete()
        else:
            for n in range(int(asiento_inicial),int(asiento_final)+1):
                PgDiario.objects.filter(ccod_user=usuario_id).filter(nasiento=n).delete()
                #diario = PgDiario.objects.filter(ccod_cli=usuario_id).filter(nasiento=asiento_id).exists()
                #if diario:
                    
         
        asientos =  PgDiario.objects.filter(ccod_user=usuario_id).order_by('-nasiento')
        data = serializers.serialize('json',asientos,fields=('nasiento','pgplan__cdsc'))
        return HttpResponse(data,content_type='application/json')


class Update_Asiento(TemplateView):
    model = PgPlan
    template_name = 'adminis/actualizar.html'

    def get_context_data(self, **kwargs):
        context  =  super(Update_Asiento,self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        act = self.kwargs.get('act')
        asien = self.kwargs.get('asi')
        

        context['actualizar'] = act
        context['asiento'] = asien
        context['pk'] = pk
        #query = PgDiario.objects.filter(ccod_cli=pk).filter(nasiento=asien)
        query = PgDiario.objects.values_list('nasiento','ccod_cue','ndebe','nhaber','cglosa2','cglosa','id','cpernum','pgplan').filter(ccod_user=pk).filter(nasiento=asien).order_by('id')
        #for n in query:
        #    print(n)
        context['consulta'] = query

        context['listado'] = PgPlan.objects.filter(nnivel=3).filter(neducat=5)
        #print(context['listado'])
        #print ("este es el id" + str(pk))
        
        return context


class actualizar_asiento(TemplateView):
    model= PgDiario
    template_name = 'adminis/updateAsiento.html'

    def get_context_data(self, **kwargs):
        context = super(actualizar_asiento,self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        asien = self.kwargs.get('asi')
        query = PgDiario.objects.values_list('nasiento','ccod_cue','ndebe','nhaber','cglosa2','cglosa','id','cpernum','pgplan').filter(ccod_user=pk).filter(nasiento=asien).order_by('id')
        queryb = PgDiario.objects.values_list('nasiento','ndebe','nhaber','cglosa','id','pgplan').filter(ccod_user=pk).filter(nasiento=asien).order_by('id')
        queryc = PgDiario.objects.all().filter(ccod_user=pk).filter(nasiento=asien).order_by('id')       

        #for dt in queryc:
        #    print(dt.ndebe)

        context["nombre"] = "Actualizar Asiento"
        context['consulta'] = queryc
        return context


class EditarAsiento(TemplateView):
    def get(self, request,*args, **kwargs):
        cglosa_p = request.GET['glosa']
        monto_p = request.GET['monto'] 
        usua_p = request.GET['usuario']

        id_plan_debe = request.GET['id_plan_debe']
        id_plan_haber = request.GET['id_plan_haber']

        idDebe = request.GET['id_debe']
        idHaber = request.GET['id_haber']

        
        #PgDiario.objects.filter(id=id_haber).update(ccod_cue=haber,ndebe=0 ,nhaber=monto_p,cglosa=cglosa_p,cglosa2=cuenta_has_p,cpernum=bal_haber,pgplan=pk_haber)
        #PgDiario.objects.filter(id=id_debe).update(ccod_cue=debe, ndebe=monto_p,nhaber=0 ,cglosa=cglosa_p,cglosa2=cuenta_des_p,cpernum=bal_debe,pgplan=pk_debe)
        
        PgDiario.objects.filter(id=idHaber).filter(ccod_user=usua_p).update(ndebe=0 ,nhaber=monto_p,cglosa=cglosa_p,pgplan=id_plan_haber)
        PgDiario.objects.filter(id=idDebe).filter(ccod_user=usua_p).update(ndebe=monto_p ,nhaber=0,cglosa=cglosa_p,pgplan=id_plan_debe)
        
        diario = PgDiario.objects.all()
        data = serializers.serialize('json',diario,fields=('nasiento'))
        return HttpResponse(data,content_type='application/json')


############################## nuevo pacioli ##########################
#devoulicion de Jsons

class buscarMasCuentas(TemplateView):
    def get(self, request, *args, **kwargs):
        cuenta = request.GET.get('cuenta')
        usuario = request.GET.get('usuario')
        #cuentabase(cuenta)
        
        lista = []
        '''
        planes = PgPlan.objects.filter(ccod_cue__startswith=cuenta.strip()).filter(nnivel=3).order_by('ccod_cue')
        for pla in planes:
            nuevConsulta = PgPlan.objects.filter(ccod_cue=pla.cdes_d.strip()).filter(nnivel=3).order_by('ccod_cue')
            nuevConsultab = PgPlan.objects.filter(ccod_cue=pla.cdes_h.strip()).filter(nnivel=3).order_by('ccod_cue')

            lista.append({'ccod_cue':pla.ccod_cue.strip(),'cdsc': pla.cdsc.strip(),'nanalisis':str(pla.nanalisis),'cdes_d':pla.cdes_d,'cdes_h':pla.cdes_h, 'planes_d': debe, 'planes_h': haber})
        '''
        lista= buscarCuentaBase(cuenta, usuario)
        return HttpResponse(json.dumps(lista), content_type="application/json")

class registrarPersona(TemplateView):
    def get(self, request, *args, **kwargs):
        ruc_dni = request.GET.get('ruc_dni')
        apPaterno = request.GET.get('apPaterno')
        apMaterno= request.GET.get('apMaterno')
        PrimerNombre= request.GET.get('PrimerNombre')
        SegundoNombre= request.GET.get('SegundoNombre')
        DireccionNatural= request.GET.get('DireccionNatural')
        usuario= request.GET.get('usuario')
        ntipo= request.GET.get('ntipo')
        #print(ccod_asi, monto, 'ccodasi - monto')
        #print(ruc_dni)
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        crazon = apPaterno + ' ' + apMaterno +' '+PrimerNombre +' '+SegundoNombre
        p = PgCliPro(
            ccod_cli=ruc_dni, 
            crazon= crazon,
            cdir=  DireccionNatural,
            cpaterno=apPaterno,
            cmaterno = apMaterno,
            cnombre1 = PrimerNombre,
            cnombre2=SegundoNombre,
            ctel='5000_',
            ntipo=ntipo,
            usuario=usuario)
        p.save()
        lista.append({'guardado':'guardaddo'})
        return HttpResponse(json.dumps(lista), content_type="application/json")

class registrarEmpresa(TemplateView):
    def get(self, request, *args, **kwargs):
        ruc_dni = request.GET.get('ruc_dni')
        RazonSocial = request.GET.get('RazonSocial')
        DireccionSocial= request.GET.get('DireccionSocial')
        usuario= request.GET.get('usuario')
        ntipo= request.GET.get('ntipo')
        #print(ccod_asi, monto, 'ccodasi - monto')
        #print(ruc_dni)
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        p = PgCliPro(
            ccod_cli=ruc_dni, 
            crazon= RazonSocial,
            cdir=  DireccionSocial,
            ctel='5000_',
            ntipo=ntipo,
            usuario=usuario)
        p.save()
        lista.append({'guardado':'guardaddo'})
        return HttpResponse(json.dumps(lista), content_type="application/json")


class buscarMasCuentasTabla(TemplateView):
    def get(self, request, *args, **kwargs):
        cuenta = request.GET.get('cuenta')
        usuario = request.GET.get('usuario')
        #cuentabase(cuenta)
        #print(cuenta, usuario, 'cuenta y usuario')
        lista = []
        planes = PgPlan.objects.filter(ccod_cue__startswith=cuenta.strip()).filter(nnivel=3).filter(usuario=usuario).order_by('ccod_cue')
        for pla in planes:
            #nuevConsulta = PgPlan.objects.filter(ccod_cue=pla.cdes_d.strip()).filter(nnivel=3).order_by('ccod_cue')
            #nuevConsultab = PgPlan.objects.filter(ccod_cue=pla.cdes_h.strip()).filter(nnivel=3).order_by('ccod_cue')
            lista.append({'ccod_cue':pla.ccod_cue.strip(),'cdsc': pla.cdsc.strip(),'nanalisis':str(pla.nanalisis),'cdes_d':pla.cdes_d,'cdes_h':pla.cdes_h,'id':pla.id})        
        #lista= buscarCuentaBase(cuenta)
        return HttpResponse(json.dumps(lista), content_type="application/json")


class buscarCuentasDestinos(TemplateView):
    def get(self, request, *args, **kwargs):
        cuenta = request.GET.get('cuenta')
        lista = []
        planes = PgPlan.objects.filter(ccod_cue=cuenta.strip()).filter(nnivel=3).order_by('ccod_cue')
        lista.append({'ccod_cue':planes.ccod_cue.strip(),'cdsc': planes.cdsc.strip()})
        #for pla in planes:
        #    lista.append({'ccod_cue':pla.ccod_cue.strip(),'cdsc': pla.cdsc.strip(),'nanalisis':str(pla.nanalisis),'cdes_d':pla.cdes_d,'cdes_h':pla.cdes_h})
        return HttpResponse(json.dumps(lista), content_type="application/json")



class buscarDocumento(TemplateView):
    def get(self, request, *args, **kwargs):
        documentos_buscar_valido = request.GET.get('documentos_buscar_valido').upper()
        usuario = request.GET.get('usuario')
        #print(documentos_buscar_valido , '######################################################');
        lista = []

        if documentos_buscar_valido == 'NONE' or documentos_buscar_valido == '':
            documento = PgDoc.objects.all().filter(usuario=usuario).order_by('ccod_doc')
            for x in documento:
                datos = {'ccod_doc':x.ccod_doc, 'cdsc':x.cdsc}
                lista.append(datos)
        else:
            documento = PgDoc.objects.filter(ccod_doc__startswith=documentos_buscar_valido).filter(usuario=usuario).order_by('ccod_doc')
            tam = len(documento)
            cdocumento = ''
            if tam == 1:
                for x in range(len(documento)):
                    cdocumento = documento[x].cdsc
            if tam == 0:
                cdocumento = ''
        #print(tam)
            lista.append({'tamaño':str(tam),'crazon':cdocumento})

        return HttpResponse(json.dumps(lista), content_type="application/json")


class BuscarRuc(TemplateView):
    def get(self, request, *args, **kwargs):
        ruc_dni = request.GET.get('ruc_dni')
        usuario = request.GET.get('usuario')
        #print(ccod_asi, monto, 'ccodasi - monto')
        #print(ruc_dni)
        lista = []
        #lista = datos_listos(ccod_asi, monto)
        cliente = PgCliPro.objects.filter(ccod_cli__startswith=ruc_dni).filter(usuario=usuario).order_by('ccod_cli')
        tam = len(cliente)
        crazon = ''
        cruc = ''
        if tam == 1:
            for x in range(len(cliente)):
                crazon = cliente[x].crazon
                cruc = cliente[x].ccod_cli
        if tam == 0:
            crazon = ''
        #print(tam)
        lista.append({'tamaño':str(tam),'crazon':crazon,'ruc':cruc})

        return HttpResponse(json.dumps(lista), content_type="application/json")


class buscarOrigenes(TemplateView):
    def get(self, request, *args, **kwargs):
        ccod_ori = request.GET.get('subdiario')
        usuario = request.GET.get('usuario')
        if len(ccod_ori) <=0 :
            print(ccod_ori, 'origen vacio');
        else:
            print(ccod_ori, 'origen lleno');
        lista = []
        #cliente = PgCliPro.objects.filter(ccod_cli__startswith=ruc_dni).order_by('ccod_cli')
        ori = PgOrigen.objects.filter(ccod_ori__startswith=ccod_ori).filter(usuario=usuario)
        tam = len(ori)
        cdsc = ''
        ccod_ori = ''
        if tam == 1:
            for x in range(len(ori)):
                cdsc = ori[x].cdsc.strip()
                ccod_ori = ori[x].ccod_ori
        if tam == 0:
            cdsc = ''
        #print(tam)
        lista.append({'tamaño':str(tam),'cdsc':cdsc,'ccod_ori':ccod_ori})

        return HttpResponse(json.dumps(lista), content_type="application/json")
        

class buscarOrigenesTabla(TemplateView):
    def get(self, request, *args, **kwargs):
        ccod_ori = request.GET.get('subdiario')
        if len(ccod_ori) <=0 :
            ori = PgOrigen.objects.all()
        else:
            ori = PgOrigen.objects.filter(ccod_ori__startswith=ccod_ori)
        
        lista = []
        #cliente = PgCliPro.objects.filter(ccod_cli__startswith=ruc_dni).order_by('ccod_cli')
        
        tam = len(ori)
        cdsc = ''
        ccod_ori = ''
        print(ori)
        for x in range(len(ori)):
            cdsc = ori[x].cdsc.strip()
            ccod_ori = ori[x].ccod_ori
        
        #print(tam)
            lista.append({'ccod_ori':ccod_ori, 'cdsc':cdsc})

        return HttpResponse(json.dumps(lista), content_type="application/json")


class buscarAsiento(TemplateView):
    def get(self, request, *args, **kwargs):
        ccod_ori = request.GET.get('ccod_ori')
        campo = request.GET.get('campo')
        usuario = request.GET.get('usuario')

        consulta =  PgOrigen.objects.filter(ccod_ori=ccod_ori)
        espresion = "PgOrigen.objects.values_list('"+campo+"').filter(ccod_ori=ccod_ori).filter(usuario= usuario)"
        animo = eval(espresion)
        cuentaList = 0
        for x in range(len(animo)):
            cuentaList =  animo[x][0]
            print(cuentaList,'mi monto a sumar')


        lista=[]
        lista.append({'nnum': str(cuentaList)})


        return HttpResponse(json.dumps(lista), content_type="application/json")


class buscarSubdiarioUsuario(TemplateView):
    def get(self, request, *args, **kwargs):
        ccod_ori = request.GET.get('ccod_ori')
        usuario = request.GET.get('usuario')
        consulta =  PgDiario.objects.filter(ccod_ori=ccod_ori).filter(ccod_user=usuario)
        longitud = len(consulta)
        #print(longitud,'+++++++++++++')
        lista=[{'lon': str(longitud)}]
        return HttpResponse(json.dumps(lista), content_type="application/json")


class buscarExisteSubDiario(TemplateView):
    def get(self, request, *args, **kwargs):
        ccod_ori = request.GET.get('ccod_ori')
        usuario = request.GET.get('usuario')
        consulta =  PgOrigen.objects.filter(ccod_ori=ccod_ori).filter(usuario=usuario)
        longitud = len(consulta)
        lista=[{'lon': str(longitud)}]
        return HttpResponse(json.dumps(lista), content_type="application/json")


class buscarAsientoExiste(TemplateView):
    def get(self, request, *args, **kwargs):
        ccod_ori = request.GET.get('ccod_ori')
        usuario = request.GET.get('usuario')
        nasiento = request.GET.get('nasiento')
        consulta =  PgDiario.objects.filter(ccod_user = usuario).filter(ccod_ori=ccod_ori).filter(nasiento=nasiento)
        longitud = len(consulta)
        lista=[{'lon': str(longitud)}]
        return HttpResponse(json.dumps(lista), content_type="application/json")



class eliminarasientosVarios(TemplateView):
    def get(self, request, *args, **kwargs):
        #data:{'ccod_ori':ccod_ori,'aInicial': aInicial,'aFinal':aFinal,'usuario':usuario},
        ccod_ori = request.GET.get('ccod_ori')
        cmes = request.GET.get('cmes')
        usuario = request.GET.get('usuario')
        aInicial = request.GET.get('aInicial')
        aFinal = request.GET.get('aFinal')
        if not aFinal:
            print('no final')
            actualizarOrigenEliminar(cmes, usuario, ccod_ori, aInicial)
            consultaAsiento =  PgDiario.objects.filter(ccod_user=usuario).filter(nasiento=aInicial).filter(ccod_ori=ccod_ori)
            for con in consultaAsiento:
                actualizarValoresPlan(usuario,con.ccod_cue, con.cmes, con.ndebe, con.nhaber, 'e')
                print(usuario,con.ccod_cue, con.cmes, con.ndebe, con.nhaber,'$-$')

            PgDiario.objects.filter(ccod_user=usuario).filter(nasiento=aInicial).filter(ccod_ori=ccod_ori).delete()
            
            #validar si el asiento es el mayor y conduerda con el origen
        else:
            print('si final')
            for n in range(int(aInicial),int(aFinal)+1):
                actualizarOrigenEliminar(cmes, usuario, ccod_ori, n)
                consultaAsientoBloque =  PgDiario.objects.filter(ccod_user=usuario).filter(nasiento=n).filter(ccod_ori=ccod_ori)

                for cn in consultaAsientoBloque:
                    actualizarValoresPlan(usuario,cn.ccod_cue, cn.cmes, cn.ndebe, cn.nhaber, 'e')
                    print(usuario,cn.ccod_cue, cn.cmes, cn.ndebe, cn.nhaber, '$-en bloque-$')

                PgDiario.objects.filter(ccod_user=usuario).filter(nasiento=n).filter(ccod_ori=ccod_ori).delete()
            
                #diario = PgDiario.objects.filter(ccod_cli=usuario_id).filter(nasiento=asiento_id).exists()
                #if diario:
        #consulta =  PgDiario.objects.filter(ccod_user = usuario).filter(ccod_ori=ccod_ori).filter(nasiento=nasiento)
        #longitud = len(consulta)
        #print(ccod_ori, usuario, aInicial, aFinal, 'datos recividoaas ++++++++')
        lista=[{'lon': str('hola')}]
        return HttpResponse(json.dumps(lista), content_type="application/json")



class eliminarasientosFull(TemplateView):
    def get(self, request, *args, **kwargs):
        
        usuario = request.GET.get('usuario')
        PgDiario.objects.filter(ccod_user=usuario).delete()
        PgOrigen.objects.filter(usuario=usuario).delete()
        insertOrigenes(usuario)
        actualizarPlanes(usuario)
        
        lista=[{'lon': str('hola')}]
        return HttpResponse(json.dumps(lista), content_type="application/json")


def actualizarPlanes(usuario):
    PgPlan.objects.filter(usuario=usuario).update(ndebe0 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber0 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe1 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber1 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe2 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber2 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe3 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber3 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe4 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber4 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe5 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber5 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe6 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber6 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe7 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber7 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe8 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber8 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe9 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber9 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe10 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber10 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe11 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber11 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe12 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber12 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe13 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber13 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe14 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber14 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebe15 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaber15 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed0 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd0 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed1 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd1 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed2 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd2 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed3 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd3 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed4 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd4 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed5 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd5 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed6 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd6 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed7 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd7 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed8 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd8 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed9 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd9 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed10 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd10 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed11 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd11 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed12 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd12 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed13 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd13 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed14 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd14 = 0)
    PgPlan.objects.filter(usuario=usuario).update(ndebed15 = 0)
    PgPlan.objects.filter(usuario=usuario).update(nhaberd15 = 0)
    


def actualizarOrigenEliminar(cmes, usuario, subdiario, posibleAsiento):
    if cmes[0:1] == '0':
        el_mes = 'nnum'+ str(cmes[1:])
    else:
        el_mes = 'nnum'+ str(cmes)


    espresion = "PgOrigen.objects.values_list("+"'"+el_mes+"'"+").filter(ccod_ori=subdiario).filter(usuario=usuario)"
    print(espresion)
    origen = eval(espresion)
    if int(origen[0][0]) == int(posibleAsiento):
        print(origen[0][0], posibleAsiento, 'iguales')
        buscar_diario = PgDiario.objects.filter(ccod_user=usuario).filter(cmes=cmes).filter(ccod_ori=subdiario).order_by('-nasiento')
        dato = 0
        asientos = []

        if len(buscar_diario) >2:
            for x in buscar_diario:
                if x.nasiento !=origen[0][0]:
                    asientos.append(x.nasiento)
                    asientos.sort(reverse = True)
            espresionb = "PgOrigen.objects.filter(ccod_ori=subdiario).filter(usuario=usuario).update("+el_mes+" = "+str(asientos[0])+")"
            eval(espresionb)
            print('mas de un solo asuieto')
        elif len(buscar_diario) ==2:
            espresionc = "PgOrigen.objects.filter(ccod_ori=subdiario).filter(usuario=usuario).update("+el_mes+" = 0)"
            eval(espresionc)
            print('un solo asuieto')

        

        print(asientos)    
        print(dato, origen[0][0],'jjjjjjjjjjjjjjjj')


    else:
        print(origen[0][0], posibleAsiento, 'diferentes')

    #print(origen, 'mio origennnnnnnnnnnnnn')


def cuentas(ccod_asi):
    planes = PgAsicab.objects.filter(ccod_asi=ccod_asi).order_by('nitem')

    lista = []
    for dato in planes:
        #lista.append({'id':smart_str(row.product_id), 'name':smart_str(row.product_name), 'price':smart_str(row.retail_price)})
        #nombred = PgPlan.objects.values_list('cdsc', flat=True).filter(ccod_cue=dato.cdebe)
        #nombreh = PgPlan.objects.values_list('cdsc', flat=True).filter(ccod_cue=dato.chaber)        
        nombred = PgPlan.objects.all().filter(ccod_cue=dato.cdebe)
        nombreh = PgPlan.objects.all().filter(ccod_cue=dato.chaber)        
        for m in nombreh:
            lista.append({'cdsc':m.cdsc.strip(),'nanalisis':str(m.nanalisis),'cdsc_asicab':dato.cdsc,'nubic':str(dato.nubic).strip(),'cid':dato.cid ,'ccod_cue_debe':dato.cdebe,'ccod_cue_haber':dato.chaber,'formula':dato.cformula,'nitem':str(dato.nitem),'tipo':'h','monto':0,'montoS':0.00,'montoD':0.00,'lrango':dato.lrango,'ldespliega':dato.ldespliega,'movimiento':0,'lcancela':dato.lcancela,'lcomp':'','rangolos':'','formulario':'null'})
        for x in nombred:
            lista.append({'cdsc':x.cdsc.strip(),'nanalisis':str(x.nanalisis),'cdsc_asicab':dato.cdsc,'nubic':str(dato.nubic).strip(),'cid':dato.cid ,'ccod_cue_debe':dato.cdebe,'ccod_cue_haber':dato.chaber,'formula':dato.cformula,'nitem':str(dato.nitem),'tipo':'d','monto':0,'montoS':0.00,'montoD':0.00,'lrango':dato.lrango,'ldespliega':dato.ldespliega,'movimiento':0,'lcancela':dato.lcancela,'lcomp':'','rangolos':'','formulario':'null'})
    
    
    
    #for h in lista:
    #    print(h)


    return lista


def cuentabase(cuenta):
    planes = PgPlan.objects.filter(ccod_cue__startswith=cuenta.strip()).filter(nnivel=3).order_by('ccod_cue')
    lista=[]
    litab=[]

    for pla in planes:
        lista.append({'ccod_cue':pla.ccod_cue.strip(),'cdsc': pla.cdsc.strip(),'nanalisis':str(pla.nanalisis),'cdes_d':pla.cdes_d,'cdes_h':pla.cdes_h,'destino':''})

        #listab.append(pla.cdes_d)
        #listab.append(pla.cdes_h)

    nombred = ''
    nombreh = ''
    elf = {'nombred':'','nombreh':''}
    li= []
    for x in lista:
        if x['cdes_d'].strip() != '':
            nombred = PgPlan.objects.filter(ccod_cue=x['cdes_d'])
            nombreh = PgPlan.objects.filter(ccod_cue=x['cdes_h'])
            li.append({'nombred':str(nombred[0].cdsc),'nombreh':str(nombreh[0].cdsc)})
        else:
            li.append({'nombred':'','nombreh':''})
            #print(x['cdes_d'],nombred[0].cdsc,'-------------------')
            #print(x['cdes_h'],nombreh[0].cdsc,'-------------------')
    #cuentaDescripcion(lista, listab)
        
        #elf['nombred']= str(nombred[0].cdsc)
        #elf['nombreh']=str(nombreh[0].cdsc)
        #print(elf)
        

        x['destino'] = li

    print(len(lista), 'tamaño lista base')

    return lista


def buscarCuentaBase(cuenta, usuario):
    #print(cuenta, usuario ,'esta es la cuenta que recibe busca cuenta base')
    planes = PgPlan.objects.filter(ccod_cue__startswith=cuenta.strip()).filter(nnivel=3).filter(usuario=usuario).order_by('ccod_cue')
    #print(planes, 'listaDEBE de planes ')
    tam =len(planes)
    lista=[]
    li = []
    cuenta = ''
    cdsc = ''
    cdes_d = ''
    cdes_h = ''
    if tam == 1:
        for x in range(len(planes)):
            cuenta = planes[x].ccod_cue
            cdsc = planes[x].cdsc
            cdes_d = planes[x].cdes_d
            cdes_h = planes[x].cdes_h 
            nanalisis = planes[x].nanalisis
            idp = planes[x].id

        lista.append({'tam': str(tam), 'cuenta': cuenta, 'cdsc': cdsc, 'cdes_d':cdes_d,'cdes_h':cdes_h,'nanalisis':str(nanalisis), 'destino':'', 'id':idp})

        if len(cdes_d.strip()) >= 1:
            for x in lista:
                nombred = PgPlan.objects.filter(ccod_cue=x['cdes_d'])
                nombreh = PgPlan.objects.filter(ccod_cue=x['cdes_h'])
                li.append({'cuentabased': x['cdes_d'].strip(),'cbnombred':nombred[0].cdsc.strip(),'idestino_d':str(nombred[0].id),'cuentabaseh': x['cdes_h'].strip(),'cbnombreh':nombreh[0].cdsc.strip() , 'idestino_h':str(nombreh[0].id)})
                #li.append({'cuentabase': x['cdes_h'].strip(),'cbnombre':nombreh[0].cdsc.strip()})
                x['destino'] = li
        else:
            for x in lista:
                x['destino'] = None   
    else:
        lista.append({'tam': 0, 'cuenta': '', 'cdsc': '', 'cdes_d':'','cdes_h':'', 'destino':''})

    #print(lista)

    return lista


class jsonPrueba(TemplateView):
    def get(self, request, *args, **kwargs):
        npigv = 18.0
        false = False
        true = True
        null = "null"

        cuenta = eval(request.GET.get('cuenta'))
        print('++++++++++++++++++++++++++')
        for x in cuenta:
            print(x)
        print('++++++++++++++++++++++++++')
        usuario = cuenta[0]['usuario']
        monto = cuenta[0]['totalDebe']
        subdiario = cuenta[0]['SubDiario']
        posibleAsiento = cuenta[0]['posibleAsiento']
        posibleGlosa = cuenta[0]['posibleGlosa']
        #monto tipo cambio
        tipomoneda_valido = cuenta[0]['tipomoneda_valido']
        #nombreCambio
        monedaTipo_valido = cuenta[0]['monedaTipo_valido']
        subdiario = cuenta[0]['SubDiario']
        cmes = cuenta[0]['cmes']
        fechaOperacion = cuenta[0]['ffechareg']

        #elimina el asiento si existe
        #subdiario = ['01','02','03','04','05','06','07','08','09','10','11','12']
        nombre = ''

        if cmes == '00':
            nombre = 'A'
        elif cmes == '14' or cmes == '15':
            nombre = 'C'
        else:
            nombre='M'
        


        existencia = PgDiario.objects.filter(ccod_user=usuario).filter(cmes=cmes).filter(ccod_ori=subdiario).filter(nasiento=posibleAsiento)
        if len(existencia) >= 1:
            #print('si este este asiento al modificar #################')
            existencia.delete()
        else:
            print('no existe este asiento al modificar')

        #para actualizar origenes activar depues
        actualizarOrigenesUsuario(cmes, usuario, subdiario, posibleAsiento)

        print(cuenta[0], 'datos cabecera')

        lista = []
        juntos =  juntararrays(cuenta[1:], lista,monedaTipo_valido, tipomoneda_valido,monto)
        extras = {'nina':0.00, 'nexo':0.00, 'nnet':0.00 ,'nimp':0.00 ,'ntot':0.00, 'ninad':0.00, 'nexod':0.00 ,'nnetd':0.00 ,'nimpd':0.00 ,'ntotd':0.00,'nbase2':0.00,'nigv2':0.00,'nbase3':0.00,'nigv3':0.00,'nisc':0.00,'nbase2d':0.00,'nigv2d':0.00,'nbase3d':0.00,'nigv3d':0.00,'niscd':0.00}

        indice = 1
        for j in juntos:
            #print(j, 'los datos juntos')

            #function for actualizar el valor de lso ndebe in the table pg_plan
            d = float(j['montoSD'])
            h = float(j['montoSH'])
            actualizarValoresPlan(usuario, j['cuenta'].strip(), cmes,d,h,'N')


            if j['datepicker_v'] =='null':
                j['datepicker_v'] = None

            pacoa = PgPlan(
                id = j['id'].strip()
            )
            
            p = PgDiario(
                cglosa2='5000_',
                ccod_cue= j['cuenta'].strip(),
                cmes= cmes,
                nasiento=posibleAsiento,
                ccod_ori=subdiario,
                cglosa=posibleGlosa,
                ndebe=j['montoSD'],
                nhaber=j['montoSH'],
                cregis=j['cregis'],
                ffecha=datetime.now(),
                ffechadoc= j['datepicker_d'],
                cnumero=j['serie_Numero'],
                ffechaven=j['datepicker_v'],
                ccod_doc=j['documentos_buscar_valido'],
                ccod_cli=j['ruc_dni'],
                #nina=extras['nina'],
                #nexo=extras['nexo'],
                #nnet=extras['nnet'],
                #nimp=extras['nimp'],
                #ntot=extras['ntot'],
                nina=j['nina'],
                nexo=j['nexo'],
                nnet=j['nnet'],
                nimp=j['nimp'],
                ntot=j['ntot'],
                ccod_user=usuario,
                #ffechareg=datetime.now(),
                ffechareg =fechaOperacion,
                cmoneda=monedaTipo_valido,
                ntc=tipomoneda_valido,
                ndebed=j['montoDD'],
                nhaberd=j['montoDH'],
                #ninad=extras['ninad'],
                #nexod=extras['nexod'],
                #nnetd=extras['nnetd'],
                #nimpd=extras['nimpd'],
                #ntotd=extras['ntotd'],
                ninad=j['ninad'],
                nexod=j['nexod'],
                nnetd=j['nnetd'],
                nimpd=j['nimpd'],
                ntotd=j['ntotd'],
                lcomp=j['lcomp'],
                ccod_asi=None,
                ccod_flujo=j['flujo_efectivo'],
                ccod_pagsu=j['MedPago_valido'],
                ccod_camp1=j['patri_tributario'],
                ccod_camp2=j['patri_cnv'],

                nbase2=j['nbase2'],
                nigv2=j['nigv2'],
                nbase3=j['nbase3'],
                nigv3=j['nigv3'],
                nisc=j['nisc'],

                nbase2d=extras['nbase2d'],
                nigv2d=extras['nigv2d'],
                nbase3d=extras['nbase3d'],
                nigv3d=extras['nigv3d'],
                niscd=extras['niscd'],
                t2008_8=j['t2008_8'],
                cdestino=j['cdestino'],
                cdes=j['cdes'].strip(),
                npigv=0.00,
                ccorre40=nombre + str(indice),
                pgplan = pacoa
                )
            p.save()
            indice +=1
        lista=[{'origen':'ori'}]
        return HttpResponse(json.dumps(lista), content_type="application/json")





def juntararrays( cuenta, listat,nombreCambio,valorCambio , montoT):
    #print(nombreCambio, valorCambio, '++++++++++++++++++++++++++++')
    elefante = {'cuenta':'',
    'item':'',
    'ruc_dni':'',
    'documentos_buscar_valido':'',
    'serie_Numero':'',
    'datepicker_d':None,
    'datepicker_v':None,
    'flujo_efectivo':'',
    'MedPago_valido':'',
    'nanalisis':'',
    'nina':float(0),
    'nexo':float(0),
    'nnet':float(0),
    'nimp':float(0),
    'ntot':float(0),
    'ninad':float(0),
    'nexod':float(0),
    'nnetd':float(0),
    'nimpd':float(0),
    'ntotd':float(0)
    };
    #print(cuenta)
    '''
    for l in listat:
        for c in cuenta:
            if l['nitem']==c['nitem']:
                if c['formulario']=='null':
                    c['formulario'] = elefante
                l['formulario'] = c['formulario']
                #reemplaza la nueva cuenta de ldespliega a formulario['cuenta']
                if l['tipo']=='d':
                    l['formulario']['cuenta'] = c['ccod_cue_debe']
                elif l['tipo']=='h':
                    l['formulario']['cuenta'] = c['ccod_cue_haber']

            #print('list', l['nitem'],'cuenta',c['nitem'])
        #print(l['nitem'])
    #reemplazar los nanalisis del formulario a la plantilla origilal
    for l in listat:
        #print(l['nanalisis'],'|',l['formulario']['nanalisis'], '|',l['ldespliega'])
        if l['ldespliega'] == True:
            if l['formulario']['nanalisis'] != '':
                l['nanalisis'] = l['formulario']['nanalisis']

        if l['nanalisis']=='2' or l['nanalisis']=='4':
            l['lcomp'] = True

        else:
            l['lcomp'] = False
            l['formulario']['datepicker_v'] =None
        #print(l)
        #print('8888888888888888888888888')

    #manejos de los montos para soles y dolares
    '''
    comparacion = True
    cue = [{'62':'', '41':''}]
    for m in cuenta:
        u= {'62':False, '42': False}
        if m['cuenta'][:2] == '62' and m['debe']>m['haber']:
            m['62'] = True
        if m['cuenta'][:2] == '41' and m['haber']>m['debe']:
            m['42'] = True
        cue.append(m)
    print(cue[0], cue, 'kkkkkkkkkkkkkkkkkkk')


    t2008_8= ''
    for ma in cuenta:
        if ma['nanalisis']=='IGV' and ma['cregis']=='C':
            #l['t2008_8']  = '08'
            t2008_8='08'
            break
        elif ma['nanalisis']=='IGV' and ma['cregis']=='V':
            #l['t2008_8']  = '14'
            t2008_8='14'
            break
        elif ma['cuenta'][0:2] == '10':
            #l['t2008_8']  = '01'
            t2008_8='01'
            break
        elif ma['cuenta'][0:2] == '33':
            #l['t2008_8']  = '07'
            t2008_8='07'
            break
        elif cue[0]['62'] == True and cue[0]['41'] == True:
            #l['t2008_8']  = '31'
            t2008_8='31'
            break
        else:
            #l['t2008_8']  = '05'
            t2008_8='05'
            break
            




    for l in cuenta:
        #monto en soles y dolares
        monto = 0.0
        if float(l['debe']) > float(l['haber']):
            monto = float(l['debe'])

            #SD  soles Debe
            #SH  soles haber
            if nombreCambio == 'S':
                l['montoSD'] = monto
                l['montoSH'] = 0.00
                l['montoDD'] = str(round((monto * float(valorCambio)),2))
                l['montoDH'] = 0.00
            elif nombreCambio == 'D':
                l['montoSD'] = str(round((monto / float(valorCambio)),2))
                l['montoSH'] = 0.00
                l['montoDD'] = monto
                l['montoDH'] = 0.00
        else:
            monto = float(l['haber'])
            if nombreCambio == 'S':
                l['montoSD'] = 0.00
                l['montoSH'] = monto
                l['montoDD'] = 0.00
                l['montoDH'] = str(round((monto * float(valorCambio)),2))
            elif nombreCambio == 'D':
                l['montoSD'] = 0.00
                l['montoSH'] = str(round((monto / float(valorCambio)),2))
                l['montoDD'] = 0.00
                l['montoDH'] = monto
        #cregis solo si es IGV
        '''
        if l['nanalisis'] =='IGV':
            l['cregis'] = 'C'
        else:
            l['cregis'] = ''
        '''

        if l['nanalisis']=='2' or l['nanalisis']=='4':
            l['lcomp'] = True
        else:
            l['lcomp'] = False
        l['t2008_8']  = t2008_8    


    for mac in cuenta:
        if mac['nanalisis'] == 'IGV':
            if mac['cregis'] == 'C':
                mac['nina']=float(str(mac['igv5_input']))
                mac['nexo']=float(str(mac['igv4_input']))
                mac['nnet']=float(str(mac['igv3_input']))
                mac['nimp']=float(str(mac['igv2_input']))
                mac['ntot']=float(str(mac['igv1_input']))

                mac['nisc']=float(str(mac['igv10_input']))    
                mac['nigv3']=float(str(mac['igv9_input']))
                mac['nbase3']=float(str(mac['igv8_input']))
                mac['nigv2']=float(str(mac['igv7_input']))
                mac['nbase2']=float(str(mac['igv6_input']))

                mac['ninad']=float(0)
                mac['nexod']=float(0)
                mac['nnetd']=float(0)
                mac['nimpd']=float(0)
                mac['ntotd']=float(0)
            elif mac['cregis'] == 'V':
                mac['nina']=float(mac['igv2_input'])
                mac['nexo']=float(mac['igv7_input'])
                mac['nnet']=float(mac['igv5_input'])
                mac['nimp']=float(mac['igv3_input'])
                mac['ntot']=float(mac['igv1_input'])

                mac['nisc']=float(mac['igv4_input'])    
                mac['nigv3']=float(0)
                mac['nbase3']=float(mac['igv6_input'])
                mac['nigv2']=float(0)
                mac['nbase2']=float(mac['igv8_input'])

                mac['ninad']=float(0)
                mac['nexod']=float(0)
                mac['nnetd']=float(0)
                mac['nimpd']=float(0)
                mac['ntotd']=float(0)
            else:
                mac['nina']=float(0)
                mac['nexo']=float(0)
                mac['nnet']=float(0)
                mac['nimp']=float(0)
                mac['ntot']=float(0)

                mac['nisc']=float(0)
                mac['nigv3']=float(0)
                mac['nbase3']=float(0)
                mac['nigv2']=float(0)
                mac['nbase2']=float(0)

                mac['ninad']=float(0)
                mac['nexod']=float(0)
                mac['nnetd']=float(0)
                mac['nimpd']=float(0)
                mac['ntotd']=float(0)
        else:
            mac['nina']=float(0)
            mac['nexo']=float(0)
            mac['nnet']=float(0)
            mac['nimp']=float(0)
            mac['ntot']=float(0)

            mac['nisc']=float(0)
            mac['nigv3']=float(0)
            mac['nbase3']=float(0)
            mac['nigv2']=float(0)
            mac['nbase2']=float(0)
            
            mac['ninad']=float(0)
            mac['nexod']=float(0)
            mac['nnetd']=float(0)
            mac['nimpd']=float(0)
            mac['ntotd']=float(0)

    return cuenta



##crear las cuentas base de origenes

class eliminarCuenta(TemplateView):
     def get(self, request, *args, **kwargs):
        npigv = 18.0
        false = False
        true = True
        null = "null"

        cuenta = eval(request.GET.get('cuenta'))
        usuario = cuenta[0]['usuario']
        monto = cuenta[0]['totalDebe']
        subdiario = cuenta[0]['SubDiario']
        posibleAsiento = cuenta[0]['posibleAsiento']
        posibleGlosa = cuenta[0]['posibleGlosa']
        posibleGlosa = cuenta[0]['posibleGlosa']
        tipomoneda_valido = cuenta[0]['tipomoneda_valido']
        subdiario = cuenta[0]['SubDiario']
        cmes = cuenta[0]['cmes']

        existencia = PgDiario.objects.filter(ccod_user=usuario).filter(cmes=cmes).filter(ccod_ori=subdiario).filter(nasiento=posibleAsiento)
        existencia.delete()

        lista=[{'origen':'ori'}]
        return HttpResponse(json.dumps(lista), content_type="application/json")

class crearOrigenes(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        origenes = PgOrigen.objects.filter(usuario=usuario)

        tam = len(origenes)
        if tam == 0:
            insertOrigenes(usuario)
        else:
            print('si tiene origenes')
        lista=[{'origen':str(usuario),'tam': str(tam)}]
        #rellenarorigenesVacios()
        return HttpResponse(json.dumps(lista), content_type="application/json")


class crearPlanes(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        plan = PgPlan.objects.filter(usuario=usuario)
        crearListaCous(usuario)

        m = replicarDatos(usuario)
        m.everyThing()

        tam = len(plan)
        if tam == 0:
            insertarPlanes(usuario)
            print('no tiene planes')
        else:
            print('si tiene origenes')

        lista=[{'origen':str(usuario),'tam': str(tam)}]
        #rellenarorigenesVacios()
        return HttpResponse(json.dumps(lista), content_type="application/json")

class buscarCuentaIGV(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        #print(usuario, 'usuario llegada')
        #origenes = PgOrigen.objects.filter(usuario=usuario)
        cuentaIGV = PgPath.objects.filter(usuario=usuario)
        cuentaGLOBAL = PgPath.objects.filter(usuario=0)

        tam = len(cuentaIGV)
        cuenta = ''
        if tam == 0:
            print(cuentaGLOBAL[0].cctaigv, 'no tiene cuenta IGV ++++++++++++++++++++++')
            cuenta = cuentaGLOBAL[0].cctaigv
        else:
            print(cuentaIGV[0].cctaigv, 'si tiene cuenta IGV ++++++++++++++++++++++')
            cuenta = cuentaIGV[0].cctaigv
            

        #lista=[{'cuentaIGV':cuenta,'tam': str(tam)}]
        lista=[{'cuentaIGV':cuenta,'tam': str(tam)}]
        #rellenarorigenesVacios()
        return HttpResponse(json.dumps(lista), content_type="application/json")

class registrarCuentaIGV(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        cuentaIgv = request.GET.get('cuentaIGV')
        print(usuario, 'usuario llegada', cuentaIgv , 'IGV a grabar')
        #origenes = PgOrigen.objects.filter(usuario=usuario)
        PgPathG = PgPath(
            usuario = usuario,
            nyear = datetime.now().year,
            ccod_emp = '0'+usuario,
            cctaigv = cuentaIgv
        )
        PgPathG.save()
        #lista=[{'cuentaIGV':cuenta,'tam': str(tam)}]
        lista=[{'proceso':'registro listo'}]
        #rellenarorigenesVacios()
        return HttpResponse(json.dumps(lista), content_type="application/json")

class ListarSubDiario(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        subdiarios = PgOrigen.objects.filter(usuario=usuario).order_by('ccod_ori')
        subdiario = []
        for sd in subdiarios:
            m = {'ccod_ori': sd.ccod_ori, 'cdsc': sd.cdsc}
            subdiario.append(m)


        return HttpResponse(json.dumps(subdiario), content_type="application/json")

class buscarCuo(TemplateView):
    def get(self, request, *args, **kwargs):
        usuario = request.GET.get('usuario')
        mes = request.GET.get('mes')
        SubDiario = request.GET.get('SubDiario')
        asiento = request.GET.get('asiento')

        if len(asiento) == 0:
            asiento = 0
        else:
            asiento = asiento
        #print('usuario:', usuario, 'cmes:', mes, 'subdiario:', SubDiario, 'nasiento:', asiento)
        cuo = PgDiario.objects.filter(ccod_user=usuario).filter(cmes=mes).filter(ccod_ori=SubDiario).filter(nasiento=asiento)
        if len(cuo) >= 1:
            lista=[]
            for cuos in range(len(cuo)):
                nArray={'ccod_cue': cuo[cuos].ccod_cue,
                 'ndebe': str(cuo[cuos].ndebe),
                 'nhaber': str(cuo[cuos].nhaber),
                 'cglosa': cuo[cuos].cglosa,
                 'cdestino': cuo[cuos].cdestino,
                 'cdes':cuo[cuos].cdes,
                 'pgplan': str(cuo[cuos].pgplan_id),
                 'pgplan__cdsc':cuo[cuos].pgplan.cdsc,
                 'ffechaDoc':str(cuo[cuos].ffechadoc),
                 'SN': cuo[cuos].cnumero,
                 'ffechaven':str(cuo[cuos].ffechaven),
                 'ccod_cli':cuo[cuos].ccod_cli,
                 'ccod_doc':cuo[cuos].ccod_doc,
                 'flujo_efectivo':cuo[cuos].ccod_flujo,
                 'MedPago_valido':cuo[cuos].ccod_pagsu,
                 'patri_tributario':cuo[cuos].ccod_camp1,
                 'patri_cnv':cuo[cuos].ccod_camp2,
                 'cregis':cuo[cuos].cregis,
                 'pgplan__nanalisis':str(cuo[cuos].pgplan.nanalisis),
                 'pgplan__nanalisis':str(cuo[cuos].pgplan.nanalisis),
                 'nina':str(cuo[cuos].nina),
                 'nexo':str(cuo[cuos].nexo),
                 'nnet':str(cuo[cuos].nnet),
                 'nimp':str(cuo[cuos].nimp),
                 'ntot':str(cuo[cuos].ntot),
                 'nbase2':str(cuo[cuos].nbase2),
                 'nigv2':str(cuo[cuos].nigv2),
                 'nbase3':str(cuo[cuos].nbase3),
                 'nigv3':str(cuo[cuos].nigv3),
                 'nisc':str(cuo[cuos].nisc)
                 }
                #print(cuo[cuos].nasiento, cuo[cuos].ccod_ori, cuo[cuos].cmes)
                #print(cuo[cuos], 'devolver datos cuos pgplan__cdsc')
                #print(nArray, '$$ nArray')
                lista.append(nArray)
            return HttpResponse(json.dumps(lista), content_type="application/json")
        else:
            lista=[{'ccod_cue':'nada'}]
            return HttpResponse(json.dumps(lista), content_type="application/json")        

class buscarFlujo(TemplateView):
    def get(self, request, *args, **kwargs):
        flujo_efectivo = request.GET.get('flujo_efectivo').upper()
        usuario = request.GET.get('usuario')
        print(flujo_efectivo, usuario, 'usuario__isnull vacios flujo')
        lista = []
        if flujo_efectivo == 'NONE' or flujo_efectivo == '':
            #documento = PgDoc.objects.all().order_by('ccod_doc')
            flujo = PgFlujo.objects.all().filter(usuario=usuario).order_by('ccod_flujo')
            for x in flujo:
                datos = {'ccod_flujo':x.ccod_flujo, 'cdsc':x.cdsc, 'usuario': x.usuario}
                lista.append(datos)
        else:
            flujo = PgFlujo.objects.filter(ccod_flujo__startswith=flujo_efectivo).filter(usuario=usuario).order_by('ccod_flujo')
            tam = len(flujo)
            cdocumento = ''
            if tam == 1:
                for x in range(len(flujo)):
                    cdocumento = flujo[x].cdsc
            if tam == 0:
                cdocumento = ''
            lista.append({'tamaño':str(tam),'crazon':cdocumento})

        return HttpResponse(json.dumps(lista), content_type="application/json")

class buscarmedio(TemplateView):
    def get(self, request, *args, **kwargs):
        medPago_valido = request.GET.get('medPago_valido').upper()
        usuario = request.GET.get('usuario')
        #print(medPago_valido, 'estes es el medio de pago para buscar ++++++++++++')
        lista = []
        if medPago_valido == 'NONE' or medPago_valido == '':
            medio = PgPagsunat.objects.all().filter(usuario=usuario).order_by('ccod_pagsu')
            for x in medio:
                datos = {'ccod_pagsu':x.ccod_pagsu, 'cdsc':x.cdsc}
                lista.append(datos)
        else:
            medio = PgPagsunat.objects.filter(ccod_pagsu__startswith=medPago_valido).filter(usuario=usuario).order_by('ccod_pagsu')
            tam = len(medio)
            cdocumento = ''
            if tam == 1:
                for x in range(len(medio)):
                    cdocumento = medio[x].cdsc
            if tam == 0:
                cdocumento = ''
            lista.append({'tamaño':str(tam),'crazon':cdocumento})

        return HttpResponse(json.dumps(lista), content_type="application/json")


class buscarPatriTribu(TemplateView):
    def get(self, request, *args, **kwargs):
        patrimonio_valido = request.GET.get('patrimonio_valido').upper()
        usuario = request.GET.get('usuario')
        #print(medPago_valido, 'estes es el medio de pago para buscar ++++++++++++')
        lista = []
        if patrimonio_valido == 'NONE' or patrimonio_valido == '':
            patri = PgCampat1.objects.all().filter(usuario=usuario).order_by('ccod_camp')
            for x in patri:
                datos = {'ccod_camp':x.ccod_camp, 'cdsc':x.cdsc}
                lista.append(datos)
        else:
            patri = PgCampat1.objects.filter(ccod_camp__startswith=patrimonio_valido).filter(usuario=usuario).order_by('ccod_camp')
            tam = len(patri)
            cdocumento = ''
            if tam == 1:
                for x in range(len(patri)):
                    cdocumento = patri[x].cdsc
            if tam == 0:
                cdocumento = ''
            lista.append({'tamaño':str(tam),'crazon':cdocumento})

        return HttpResponse(json.dumps(lista), content_type="application/json")

class buscarPatriCnv(TemplateView):
    def get(self, request, *args, **kwargs):
        patrimonioCnvvalido = request.GET.get('patrimonioCnvvalido').upper()
        usuario = request.GET.get('usuario')
        #print(medPago_valido, 'estes es el medio de pago para buscar ++++++++++++')
        lista = []
        if patrimonioCnvvalido == 'NONE' or patrimonioCnvvalido == '':
            patri = PgCampat2.objects.all().filter(usuario=usuario).order_by('ccod_camp')
            for x in patri:
                datos = {'ccod_camp':x.ccod_camp, 'cdsc':x.cdsc}
                lista.append(datos)
        else:
            patri = PgCampat2.objects.filter(ccod_camp__startswith=patrimonioCnvvalido).filter(usuario=usuario).order_by('ccod_camp')
            tam = len(patri)
            cdocumento = ''
            if tam == 1:
                for x in range(len(patri)):
                    cdocumento = patri[x].cdsc
            if tam == 0:
                cdocumento = ''
            lista.append({'tamaño':str(tam),'crazon':cdocumento})

        return HttpResponse(json.dumps(lista), content_type="application/json")


def insertOrigenes(usuario):
    li = rellenarorigenesVacios()
    for x in li:
        asientos =  PgOrigen.objects.values_list('id', flat=True)

        alto = max(asientos)+1
        p = PgOrigen(
        ccod_ori  = x['ccod_ori'],
        cdsc = x['cdsc'],
        nnum0 = 0, 
        nnum1 = 0,
        nnum2 = 0,
        nnum3 = 0,
        nnum4 = 0,
        nnum5 = 0,
        nnum6 = 0,
        nnum7 = 0,
        nnum8 = 0,
        nnum9 = 0,
        nnum10= 0,
        nnum11= 0,
        nnum12= 0,
        nnum13= 0,
        nnum14= 0,
        nnum15= 0,
        lmodi = False,
        usuario = usuario,
        id=alto
        )
        p.save()
       
#def resetPlanes(usuario):

def rellenarorigenesVacios():
    listaOrigenes = [
    {'ccod_ori': '00', 'cdsc': 'APERTURA'},
    {'ccod_ori': '03', 'cdsc': 'INGRESOS CAJA-BANCOS'},
    {'ccod_ori': '04', 'cdsc': 'EGRESOS CAJA-BANCOS'},
    {'ccod_ori': '05', 'cdsc': 'AJUSTES'}, 
    {'ccod_ori': '06', 'cdsc': 'OTRAS OPERACIONES'}, 
    {'ccod_ori': '08', 'cdsc': 'PROVISIONES DE ACTIV'}, 
    {'ccod_ori': '10', 'cdsc': 'INGRESOS INVENTARIO'}, 
    {'ccod_ori': '11', 'cdsc': 'SALIDAS DE INVENTARI'}, 
    {'ccod_ori': '12', 'cdsc': 'NO TRIBUTARIO'}, 
    {'ccod_ori': '20', 'cdsc': 'ASIENTOS DE CIERRE'}, 
    {'ccod_ori': '21', 'cdsc': 'ASIENTOS DE REVERSION'}, 
    {'ccod_ori': '91', 'cdsc': 'OPERACIONES ODOO'}, 
    {'ccod_ori': '92', 'cdsc': 'AJUSTE POR REDONDEO'}, 
    {'ccod_ori': '98', 'cdsc': 'PERIODOS ANTERIORES SIN REGISTRAR'}, 
    {'ccod_ori': '99', 'cdsc': 'PERIODOS ANTERIORES REGISTRADOS'}, 
    {'ccod_ori': 'X1', 'cdsc': 'Importación desde PLE'}, 
    {'ccod_ori': 'CA', 'cdsc': 'CONTROL ANALITICO'}, 
    {'ccod_ori': '07', 'cdsc': 'PLANILLAS'}, 
    {'ccod_ori': '01', 'cdsc': 'COMPRAS Y GASTOS'}, 
    {'ccod_ori': '02', 'cdsc': 'VENTAS E INGRESOS'}]

    return listaOrigenes


def insertarPlanes(usuario):
    li = buscarPlanesInsertar()
    for l in li:
        valorid  = PgPlan.objects.values_list('id', flat=True)
        alto = max(valorid)+1
        p= PgPlan(
        id = alto,
        ccod_cue = l['ccod_cue'],
        cdsc= l['cdsc'],
        nnivel = l['nnivel'],
        ntipo = l['ntipo'],
        nanalisis = l['nanalisis'],
        ccod_bal =  l['ccod_bal'],
        cdes_d = l['cdes_d'],
        cdes_h = l['cdes_h'],
        ccod_baln = l['ccod_baln'],
        ccod_bal2 = l['ccod_bal2'],
        ccod_baln2 = l['ccod_baln2'],
        neducat = l['neducat'],
        npresup = l['npresup'],
        ccod_balr = l['ccod_balr'],
        ccod_cueci = l['ccod_cueci'],
        usuario= usuario
        )
        p.save()
    

def buscarPlanesInsertar():
    planes = PgPlan.objects.filter(usuario__isnull=True)
    listaPlanes=[]

    for pla in planes:
        formato = {'ccod_cue':pla.ccod_cue,'cdsc':pla.cdsc,'nnivel':pla.nnivel,'ntipo':pla.ntipo,'nanalisis':pla.nanalisis,'ccod_bal':pla.ccod_bal,'cdes_d':pla.cdes_d,'cdes_h':pla.cdes_h,'ccod_baln':pla.ccod_baln,'ccod_bal2':pla.ccod_bal2,'ccod_baln2':pla.ccod_baln2,'neducat':pla.neducat,'npresup':pla.npresup,'ccod_balr':pla.ccod_balr,'ccod_cueci':pla.ccod_cueci}
        listaPlanes.append(formato)

    return listaPlanes



def actualizarOrigenesUsuario(cmes, usuario, subdiario, posibleAsiento):
    #PgDiario.objects.values_list('nasiento', flat=True).filter(ccod_user=usua_p)
    if cmes[0:1] == '0':
        el_mes = 'nnum'+ str(cmes[1:])
    else:
        el_mes = 'nnum'+ str(cmes)
    #consulta = "PgOrigen.objects.values_list("+el_mes+", flat=True).filter(ccod_ori=subdiario).filter(usuario=usuario)"
    consultab = PgOrigen.objects.values_list(""+el_mes+"", flat=True).filter(ccod_ori=subdiario).filter(usuario=usuario)
    dato = consultab[0]
    
    #dato =  eval(consulta)
    #print(dato, posibleAsiento ,'el asiento actual ----------------------------')
    if (int(posibleAsiento) < int(dato)):
        pass
    elif (int(posibleAsiento) == int(dato)):
        pass
    elif (int(posibleAsiento) > int(dato)):
        #print('mayor que la consulta')
        espresion = "PgOrigen.objects.filter(ccod_ori=subdiario).filter(usuario=usuario).update("+el_mes+" = posibleAsiento)"
        #PgOrigen.objects.filter(ccod_ori=subdiario).filter(usuario=usuario).update("+el_mes+" = posibleAsiento)
        #print(espresion)
        eval(espresion)
    

def actualizarValoresPlan(usuario, cuenta, cmes, ndebe, nhaber, condicion):
    #buscando el valñor en la tabla pg_plan
    #condicion = n si es nuevo  , e si es eliminar

    mi_consulta  = PgPlan.objects.all().filter(ccod_cue=cuenta.strip()).filter(usuario= usuario)



    ndebea = float(0)
    nhabera = float(0)
    valordebe = ''
    valorhaber = ''
    if cmes == '00':
        #print(cmes,'555555555555555')
        valordebe = 'x.ndebe0'
        valorhaber = 'x.nhaber0'
        elmesdebe = 'ndebe0'
        elmeshaber = 'nhaber0'
    elif cmes[:1]=='0':
        #print(cmes,'33333333333333333')
        valordebe = 'x.ndebe'+ str(cmes[1:])
        valorhaber = 'x.nhaber'+str(cmes[1:])
        elmesdebe = 'ndebe'+str(cmes[1:])
        elmeshaber = 'nhaber'+str(cmes[1:])
        print(cmes,valordebe, valorhaber, elmesdebe, elmeshaber)
    else:
        #print(cmes,'444444444444')
        valordebe= 'x.ndebe'+ str(cmes)
        valorhaber = 'x.nhaber' + str(cmes)
        elmesdebe = 'ndebe'+str(cmes)
        elmeshaber = 'nhaber'+str(cmes)

    for x in mi_consulta:
        debe = eval(valordebe)
        haber = eval(valorhaber)
        #m = {'ndebe':x.ndebe+cmes, 'nhaber': x.nhaber+cmes}
        #print(m)


    if condicion == 'N':
        ndebea = float(ndebe) + float(debe)
        nhabera = float(nhaber) + float(haber)
        print(debe, haber,'4444',ndebea, nhabera)
        
    elif condicion == 'e':
        #buscar los datos para eliminar y restar la cuentas
        ndebea = float(debe) - float(ndebe)
        nhabera = float(haber) - float(nhaber)
        print(debe, haber,'555',ndebea, nhabera)
    else:
        #para cunado sea modificar
        pass

    
    
    espresion = "PgPlan.objects.filter(usuario=usuario).filter(ccod_cue=cuenta).update("+elmesdebe+" = ndebea, "+elmeshaber+" = nhabera)"
    #print(espresion)
    eval(espresion)

    



