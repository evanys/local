from django.shortcuts import render
from rest_framework import serializers, filters, viewsets, generics
from rest_framework.response import Response
from apps.api.serializers import *
from django.db.models import Q, Max

class pasientoViewSet(viewsets.ModelViewSet):
    queryset = PgPasiento.objects.all()
    serializer_class = pgpasientoSerializer

    def listaPlanes(self, repuest, *args, **kwargs):
        pasiento = PgPasiento.objects.all()
        serializer = pgpasientoshortSerializer(pasiento, many=True)
        return Response(serializer.data)

