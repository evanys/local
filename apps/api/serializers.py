from django.contrib.auth.models import User
from rest_framework import serializers

from apps.pacioli.models import *



class pgpasientoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PgPasiento
        fields = ('ccod_asi','cdsc')

class pgpasientoshortSerializer(serializers.ModelSerializer):
    class Meta:
        model = PgPasiento
        fields = ('ccod_asi','cdsc')
