from django.contrib import admin
from django.urls import path,include
from django.conf.urls import url, include
from rest_framework import routers
from apps.api import views
#from apps.pacioliApp.views import PurchaseList
from apps.api.views import *

#from apps.pacioli.views import *
router = routers.DefaultRouter()
#router.register(r'pais',views.listaPais)
router.register(r'pasiento',views.pasientoViewSet,basename='listapasiento')

urlpatterns = [
	url(r'api',include(router.urls)),
]
