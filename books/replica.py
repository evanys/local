from apps.pacioli.models import *
from django.contrib.auth.models import User

class replicarDatos:
    def __init__(self, usuario):
        self.usuario = usuario
    def everyThing(self):
        self.pgActivo()
        print("pgActivo Listo")
        self.PgAdicion()
        print('PgAdicion Listo')
        self.PgAsicab()
        print('PgAsicab Listo')
        self.PgAsidet()
        print('PgAsidet Listo')
        self.PgAsiento()
        print('PgAsiento Listo')
        self.PgAsimodel()
        print('PgAsimodel Listo')
        self.PgCampat1()
        print('PgCampat1 Listo')
        self.PgCampat2()
        print('PgCampat2 Listo')
        self.PgCatesfin()
        print('PgCatesfin Listo')
        self.PgClabieser()
        print('PgClabieser Listo')
        self.PgConveniodt()
        print('PgConveniodt Listo')
        self.PgCostos()
        print('PgCostos Listo')
        self.PgCostos2()
        print('PgCostos2 Listo')
        self.PgCtrlple()
        print('PgCtrlple Listo')
        self.PgDoc()
        print('PgDoc Listo')
        self.PgEnfinan()
        print('PgEnfinan Listo')
        self.PgFactor()
        print('PgFactor Listo')
        self.PgFlujo()
        print('PgFlujo Listo')
        self.PgFormato()
        print('PgFormato Listo')
        self.PgFormato2()
        print('PgFormato2 Listo')
        self.PgMoneda()
        print('PgMoneda Listo')
        self.PgOrigenfi()
        print('PgOrigenfi Listo')
        self.PgPagsunat()
        print('PgPagsunat Listo')
        self.PgPais()
        print('PgPais Listo')
        self.PgPath()
        print('PgPath Listo')
        self.PgPorcen()
        print('PgPorcen Listo')
        self.PgSunat05()
        print('PgSunat05 Listo')
        self.PgTabdetra()
        print('PgTabdetra Listo')
        self.PgTipo()
        print('PgTipo Listo')
        self.PgTipod()
        print('PgTipod Listo')
        self.PgTiprenta()
        print('PgTiprenta Listo')


    def pgActivo(self):
        buscarConsulta = PgActivo.objects.filter(usuario= self.usuario)
        Message_me = PgActivo.objects.filter(usuario=self.usuario).count()
        if Message_me <=0:
            pg = PgActivo.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgActivo.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgActivo(
                        id=alto,
                        ccod_acti = x.ccod_acti,
                        cdsc = x.cdsc,
                        ffecha_ad = x.ffecha_ad,
                        lestado = x.lestado,
                        ffecha_ba = x.ffecha_ba,
                        cubicacion = x.cubicacion,
                        ccod_costo = x.ccod_costo,
                        nmeses = x.nmeses,
                        ntasadep = x.ntasadep,
                        nvalor = x.nvalor,
                        nresidual = x.nresidual,
                        ndepre = x.ndepre,
                        ccod_cue = x.ccod_cue,
                        ccue_dep = x.ccue_dep,
                        ccue_gas = x.ccue_gas,
                        ndep1 = x.ndep1,
                        ndep2 = x.ndep2,
                        ndep3 = x.ndep3,
                        ndep4 = x.ndep4,
                        ndep5 = x.ndep5,
                        ndep6 = x.ndep6,
                        ndep7 = x.ndep7,
                        ndep8 = x.ndep8,
                        ndep9 = x.ndep9,
                        ndep10 = x.ndep10,
                        ndep11 = x.ndep11,
                        ndep12 = x.ndep12,
                        najus1 = x.najus1,
                        najus2 = x.najus2,
                        ldepre = x.ldepre,
                        mdsc = x.mdsc,
                        ccod_presu = x.ccod_presu,
                        usuario = self.usuario
                        )
                    p.save()
    
    def PgAdicion(self):
        buscarConsulta = PgAdicion.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgAdicion.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgAdicion.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgAdicion(
                        id=alto,
                        ncod = x.ncod,
                        cdsc = x.cdsc,
                        npot1 = x.npot1,
                        cdetalle = x.cdetalle,
                        nextraer = x.nextraer,
                        nfiltrado = x.nfiltrado,
                        ncontable = x.ncontable,
                        ncalculo = x.ncalculo,
                        ntributa = x.ntributa,
                        nadicion = x.nadicion,
                        npot2 = x.npot2,
                        usuario = self.usuario
                        )
                    p.save()
        
    '''
    def PgAsicab(self):
        buscarConsulta = PgAsicab.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgAsicab.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgAsicab.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgAsicab(
                        id=alto,
                        ccod_asi = x.ccod_asi,
                        cid = x.cid,
                        cdsc = x.cdsc,
                        cdebe = x.cdebe,
                        chaber = x.chaber,
                        lrango = x.lrango,
                        nubic = x.nubic,
                        lpedir = x.lpedir,
                        nitem = x.nitem,
                        cformula = x.cformula,
                        lcancela = x.lcancela,
                        lcaja = x.lcaja,
                        ntipocam = x.ntipocam,
                        ldifcam = x.ldifcam,
                        ccod_doc = x.ccod_doc,
                        cnumero = x.cnumero,
                        ccod_cli = x.ccod_cli,
                        ccod_costo = x.ccod_costo,
                        cglosa = x.cglosa,
                        ffechadoc = x.ffechadoc,
                        ffechaven = x.ffechaven,
                        lautoinc = x.lautoinc,
                        cserinc = x.cserinc,
                        cdocinc = x.cdocinc,
                        ccod_flujo = x.ccod_flujo,
                        ldespliega = x.ldespliega,
                        cdatregesp = x.cdatregesp,
                        cdatdoc = x.cdatdoc,
                        lresp = x.lresp,
                        lcobesp = x.lcobesp,
                        cdatcosto = x.cdatcosto,
                        ccod_pagsu = x.ccod_pagsu,
                        ccod_camp1 = x.ccod_camp1,
                        ccod_camp2 = x.ccod_camp2,
                        ccod_presu = x.ccod_presu,
                        lmostrar = x.lmostrar,
                        ccod_vend = x.ccod_vend,
                        ccod_cos2 = x.ccod_cos2,
                        ccod_clabs = x.ccod_clabs,
                        usuario = self.usuario
                        )
                    p.save()    
    '''
    def PgAsicab(self):
        buscarNulo = PgAsicab.objects.filter(usuario="A").count()
        buscarConsulta = PgAsicab.objects.filter(usuario= self.usuario).count()
        lista = []
        if buscarNulo != buscarConsulta:
            print('valores distintos')
            for record in PgAsicab.objects.filter(usuario='A'):
                usuario = self.usuario
                lista.append(PgAsicab(usuario=usuario,ccod_asi = record.ccod_asi))
            PgAsicab.objects.bulk_create(lista)
        else:
            print('valores iguales')

    def PgOrigen(self):
        buscarNulo = PgOrigen.objects.filter(usuario__isnull=True).count()
        buscarConsulta = PgOrigen.objects.filter(usuario= self.usuario).count()
        lista = []
        mensaje = ''
        if buscarNulo != buscarConsulta:
            for record in PgOrigen.objects.filter(usuario__isnull=True):
                usuario = self.usuario
                lista.append(PgOrigen(
                    ccod_ori = record.ccod_ori,
                    cdsc = record.cdsc,
                    nnum0 = record.nnum0,
                    nnum1 = record.nnum1,
                    nnum2 = record.nnum2,
                    nnum3 = record.nnum3,
                    nnum4 = record.nnum4,
                    nnum5 = record.nnum5,
                    nnum6 = record.nnum6,
                    nnum7 = record.nnum7,
                    nnum8 =  record.nnum8,
                    nnum9 = record.nnum9,
                    nnum10 = record.nnum10,
                    nnum11 =  record.nnum11,
                    nnum12 = record.nnum12,
                    nnum13 = record.nnum13,
                    nnum14 = record.nnum14,
                    nnum15 = record.nnum15,
                    lmodi = False,
                    usuario = usuario)
                    )
            PgOrigen.objects.bulk_create(lista)
            mensaje = 'origen creado ahora'
        else:
            mensaje = 'origen creado anteriormente'
        return mensaje


    def PgAsidet(self):
        buscarConsulta = PgAsidet.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgAsidet.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgAsidet.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgAsidet(
                        id=alto,
                        cid = x.cid,
                        npor = x.npor,
                        ccod_asi = x.ccod_asi,
                        ccod_cue = x.ccod_cue,
                        ccod_costo = x.ccod_costo,
                        cformula = x.cformula,
                        ccod_presu = x.ccod_presu,
                        ccod_cos2 = x.ccod_cos2,
                        usuario = self.usuario
                        )
                    p.save()
        

    def PgAsiento(self):
        buscarConsulta = PgAsiento.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgAsiento.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgAsiento.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgAsiento(
                        id=alto,
                        ccod_asi = x.ccod_asi,
                        cdsc = x.cdsc,
                        ccod_ori = x.ccod_ori,
                        cglosa = x.cglosa,
                        cregis = x.cregis,
                        limp = x.limp,
                        ccod_rep = x.ccod_rep,
                        mdsc = x.mdsc,
                        cdesenc = x.cdesenc,
                        t2008_8 = x.t2008_8,
                        usuario = self.usuario
                        )
                    p.save()
        

    def PgAsimodel(self):
        buscarConsulta = PgAsimodel.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgAsimodel.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgAsimodel.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgAsimodel(
                        id=alto,
                        cmes = x.cmes,
                        nasiento = x.nasiento,
                        ccod_cue = x.ccod_cue,
                        ndebe = x.ndebe,
                        nhaber = x.nhaber,
                        cglosa = x.cglosa,
                        ccod_ori = x.ccod_ori,
                        cregis = x.cregis,
                        ffecha = x.ffecha,
                        ffechadoc = x.ffechadoc,
                        cnumero = x.cnumero,
                        ffechaven = x.ffechaven,
                        ccod_doc = x.ccod_doc,
                        ccod_cli = x.ccod_cli,
                        nina = x.nina,
                        nexo = x.nexo,
                        nnet = x.nnet,
                        nimp = x.nimp,
                        ntot = x.ntot,
                        ccod_user = x.ccod_user,
                        ffechareg = x.ffechareg,
                        cmesc = x.cmesc,
                        cdestino = x.cdestino,
                        cdes = x.cdes,
                        ccod_costo = x.ccod_costo,
                        cmoneda = x.cmoneda,
                        ntc = x.ntc,
                        ndebed = x.ndebed,
                        nhaberd = x.nhaberd,
                        ninad = x.ninad,
                        nexod = x.nexod,
                        nnetd = x.nnetd,
                        nimpd = x.nimpd,
                        ntotd = x.ntotd,
                        cmreg = x.cmreg,
                        lcomp = x.lcomp,
                        ntcc = x.ntcc,
                        ncajas = x.ncajas,
                        ncajad = x.ncajad,
                        lajusdif = x.lajusdif,
                        lcaja = x.lcaja,
                        cid = x.cid,
                        ccod_asi = x.ccod_asi,
                        ccadpdt = x.ccadpdt,
                        ccod_flujo = x.ccod_flujo,
                        nbase2 = x.nbase2,
                        nigv2 = x.nigv2,
                        nbase3 = x.nbase3,
                        nigv3 = x.nigv3,
                        nisc = x.nisc,
                        nbase2d = x.nbase2d,
                        nigv2d = x.nigv2d,
                        nbase3d = x.nbase3d,
                        nigv3d = x.nigv3d,
                        niscd = x.niscd,
                        lresp = x.lresp,
                        nresp = x.nresp,
                        nporresp = x.nporresp,
                        cserresp = x.cserresp,
                        cnumresp = x.cnumresp,
                        fguiaresp = x.fguiaresp,
                        cserguia = x.cserguia,
                        cnumguia = x.cnumguia,
                        nrets = x.nrets,
                        nretd = x.nretd,
                        cdocf = x.cdocf,
                        cnumf = x.cnumf,
                        ffecf = x.ffecf,
                        fdocdet = x.fdocdet,
                        cdoc_ndom = x.cdoc_ndom,
                        cdoc_refnc = x.cdoc_refnc,
                        fpagimpret = x.fpagimpret,
                        fpagimport = x.fpagimport,
                        ldiferido = x.ldiferido,
                        ffechadif = x.ffechadif,
                        ccod_pagsu = x.ccod_pagsu,
                        ccod_camp1 = x.ccod_camp1,
                        ccod_camp2 = x.ccod_camp2,
                        t2008_11 = x.t2008_11,
                        ffecdua = x.ffecdua,
                        t2008_8 = x.t2008_8,
                        ccod_presu = x.ccod_presu,
                        cdocmodi = x.cdocmodi,
                        cserext = x.cserext,
                        cnumext = x.cnumext,
                        ncoddes = x.ncoddes,
                        nnumdes = x.nnumdes,
                        cnumdet = x.cnumdet,
                        cexttpo = x.cexttpo,
                        cextser = x.cextser,
                        cextnum = x.cextnum,
                        fextfec = x.fextfec,
                        nextbas = x.nextbas,
                        nextigv = x.nextigv,
                        cperser = x.cperser,
                        cpernum = x.cpernum,
                        ntpcv = x.ntpcv,
                        dua_año = x.dua_año,
                        dua_num = x.dua_num,
                        dua_fregu = x.dua_fregu,
                        dua_valor = x.dua_valor,
                        ccod_vend = x.ccod_vend,
                        cdocc = x.cdocc,
                        cnumc = x.cnumc,
                        ccod_user1 = x.ccod_user1,
                        ffechareg1 = x.ffechareg1,
                        nestado = x.nestado,
                        ccod_cos2 = x.ccod_cos2,
                        cmesple = x.cmesple,
                        coriple = x.coriple,
                        nasiple = x.nasiple,
                        ccodruc = x.ccodruc,
                        npigv = x.npigv,
                        cglosa2 = x.cglosa2,
                        cregpdb = x.cregpdb,
                        ctpcv = x.ctpcv,
                        ccoddes = x.ccoddes,
                        cnumdes = x.cnumdes,
                        cinddet = x.cinddet,
                        ccoddet = x.ccoddet,
                        cindret = x.cindret,
                        cperind = x.cperind,
                        cpercod = x.cpercod,
                        cref = x.cref,
                        cmle = x.cmle,
                        cale = x.cale,
                        ncorrelat = x.ncorrelat,
                        cnumero2 = x.cnumero2,
                        ncorrecb = x.ncorrecb,
                        cmle2 = x.cmle2,
                        cale2 = x.cale2,
                        lperden = x.lperden,
                        nmonperbas = x.nmonperbas,
                        ccorre40 = x.ccorre40,
                        nrentanet = x.nrentanet,
                        nimpreten = x.nimpreten,
                        ndeduc = x.ndeduc,
                        ntasreten = x.ntasreten,
                        ccod_renta = x.ccod_renta,
                        ccod_mone = x.ccod_mone,
                        ntcamsnd =  x.ntcamsnd,
                        usuario = self.usuario
                        )
                    p.save()
        

    def PgCampat1(self):
        buscarConsulta = PgCampat1.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgCampat1.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgCampat1.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgCampat1(
                        id=alto,
                        ccod_camp =  x.ccod_camp,
                        cdsc = x.cdsc,
                        ccod_csev =  x.ccod_csev,
                        usuario = self.usuario
                        )
                    p.save()

    def PgCampat2(self):
        buscarConsulta = PgCampat2.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgCampat2.objects.filter(usuario__isnull=True)
            for x in pg:
                valorid  = PgCampat2.objects.values_list('id', flat=True)
                alto = max(valorid)+1
                p= PgCampat2(
                id=alto,
                ccod_camp = x.ccod_camp,
                cdsc = x.cdsc,
                ccod_csev = x.ccod_csev,
                usuario = self.usuario
                )
                p.save()

    def PgCatesfin(self):
        buscarConsulta = PgCatesfin.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgCatesfin.objects.filter(usuario__isnull=True)
            for x in pg:
                valorid  = PgCatesfin.objects.values_list('id', flat=True)
                alto = max(valorid)+1
                p= PgCatesfin(
                id=alto,
                ccod_catef = x.ccod_catef,
                cdsc = x.cdsc,
                usuario = self.usuario
                )
                p.save()

    def PgClabieser(self):
        buscarConsulta = PgClabieser.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgClabieser.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgClabieser.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgClabieser(
                        id=alto,
                        ccod_clabs = x.ccod_clabs,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()

    def PgConveniodt(self):
        buscarConsulta = PgConveniodt.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgConveniodt.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgConveniodt.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgConveniodt(
                        id=alto,
                        ccod_conve = x.ccod_conve,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()

    def PgCostos(self):
        buscarConsulta = PgCostos.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgCostos.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgCostos.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgCostos(
                        id=alto,
                        ccod_costo = x.ccod_costo,
                        cdsc = x.cdsc,
                        cdebe = x.cdebe,
                        chaber = x.chaber,
                        lbloquea = x.lbloquea,
                        usuario = self.usuario
                        )
                    p.save()

    def PgCostos2(self):
        buscarConsulta = PgCostos2.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgCostos2.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgCostos2.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgCostos2(
                        id=alto,
                        ccod_costo = x.ccod_costo,
                        cdsc = x.cdsc,
                        cdebe = x.cdebe,
                        chaber = x.chaber,
                        lbloquea = x.lbloquea,
                        usuario = self.usuario
                        )
                    p.save()

    def PgCtrlple(self):
        buscarConsulta = PgCtrlple.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgCtrlple.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgCtrlple.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgCtrlple(
                        id=alto,
                        ncod = x.ncod,
                        cdsc = x.cdsc,
                        l1 = x.l1,
                        l2 = x.l2,
                        l3 = x.l3,
                        l4 = x.l4,
                        l5 = x.l5,
                        l6 = x.l6,
                        l7 = x.l7,
                        l8 = x.l8,
                        l9 = x.l9,
                        l10 = x.l10,
                        l11 = x.l11,
                        l12 = x.l12,
                        cmesple30 = x.cmesple30,
                        cperple30 = x.cperple30,
                        usuario = self.usuario
                        )
                    p.save()

    def PgDoc(self):
        buscarConsulta = PgDoc.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgDoc.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgDoc.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgDoc(
                        id= alto,
                        ccod_doc = x.ccod_doc,
                        cdsc = x.cdsc,
                        ligvc = x.ligvc,
                        ligvv = x.ligvv,
                        lcv = x.lcv,
                        lexp = x.lexp,
                        ccom_habc = x.ccom_habc,
                        ccom_habr = x.ccom_habr,
                        cven_debc = x.cven_debc,
                        cven_debr = x.cven_debr,
                        cven_hab = x.cven_hab,
                        nie = x.nie,
                        lneg = x.lneg,
                        limp = x.limp,
                        nser = x.nser,
                        nnum = x.nnum,
                        mpath = x.mpath,
                        ndocint = x.ndocint,
                        usuario = self.usuario
                        )
                    p.save()

    def PgEnfinan(self):
        buscarConsulta = PgEnfinan.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgEnfinan.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgEnfinan.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgEnfinan(
                        id=alto,
                        ccod_enfin = x.ccod_enfin,
                        cdsc = x.cdsc,
                        mcuenta = x.mcuenta,
                        usuario = self.usuario
                        )
                    p.save()

    def PgFactor(self):
        buscarConsulta = PgFactor.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgFactor.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgFactor.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgFactor(
                        id=alto,
                        nfac0 = x.nfac0,
                        nfac1 = x.nfac1,
                        nfac2 = x.nfac2,
                        nfac3 = x.nfac3,
                        nfac4 = x.nfac4,
                        nfac5 = x.nfac5,
                        nfac6 = x.nfac6,
                        nfac7 = x.nfac7,
                        nfac8 = x.nfac8,
                        nfac9 = x.nfac9,
                        nfac10 = x.nfac10,
                        nfac11 = x.nfac11,
                        nfac12 = x.nfac12,
                        ccod_cue = x.ccod_cue,
                        ccod_ori = x.ccod_ori,
                        cori_dep = x.cori_dep,
                        usuario = self.usuario
                        )
                    p.save()

    def PgFlujo(self):
        buscarConsulta = PgFlujo.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgFlujo.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgFlujo.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgFlujo(
                        id=alto,
                        ccod_flujo = x.ccod_flujo,
                        cdsc = x.cdsc,
                        ndebe0 = x.ndebe0,
                        nhaber0 = x.nhaber0,
                        ndebe1 = x.ndebe1,
                        nhaber1 = x.nhaber1,
                        ndebe2 = x.ndebe2,
                        nhaber2 = x.nhaber2,
                        ndebe3 = x.ndebe3,
                        nhaber3 = x.nhaber3,
                        ndebe4 = x.ndebe4,
                        nhaber4 = x.nhaber4,
                        ndebe5 = x.ndebe5,
                        nhaber5 = x.nhaber5,
                        ndebe6 = x.ndebe6,
                        nhaber6 = x.nhaber6,
                        ndebe7 = x.ndebe7,
                        nhaber7 = x.nhaber7,
                        ndebe8 = x.ndebe8,
                        nhaber8 = x.nhaber8,
                        ndebe9 = x.ndebe9,
                        nhaber9 = x.nhaber9,
                        ndebe10 = x.ndebe10,
                        nhaber10 = x.nhaber10,
                        ndebe11 = x.ndebe11,
                        nhaber11 = x.nhaber11,
                        ndebe12 = x.ndebe12,
                        nhaber12 = x.nhaber12,
                        ndebe13 = x.ndebe13,
                        nhaber13 = x.nhaber13,
                        ndebe14 = x.ndebe14,
                        nhaber14 = x.nhaber14,
                        ndebe15 = x.ndebe15,
                        nhaber15 = x.nhaber15,
                        ndebed0 = x.ndebed0,
                        nhaberd0 = x.nhaberd0,
                        ndebed1 = x.ndebed1,
                        nhaberd1 = x.nhaberd1,
                        ndebed2 = x.ndebed2,
                        nhaberd2 = x.nhaberd2,
                        ndebed3 = x.ndebed3,
                        nhaberd3 = x.nhaberd3,
                        ndebed4 = x.ndebed4,
                        nhaberd4 = x.nhaberd4,
                        ndebed5 = x.ndebed5,
                        nhaberd5 = x.nhaberd5,
                        ndebed6 = x.ndebed6,
                        nhaberd6 = x.nhaberd6,
                        ndebed7 = x.ndebed7,
                        nhaberd7 = x.nhaberd7,
                        ndebed8 = x.ndebed8,
                        nhaberd8 = x.nhaberd8,
                        ndebed9 = x.ndebed9,
                        nhaberd9 = x.nhaberd9,
                        ndebed10 = x.ndebed10,
                        nhaberd10 = x.nhaberd10,
                        ndebed11 = x.ndebed11,
                        nhaberd11 = x.nhaberd11,
                        ndebed12 = x.ndebed12,
                        nhaberd12 = x.nhaberd12,
                        ndebed13 = x.ndebed13,
                        nhaberd13 = x.nhaberd13,
                        ndebed14 = x.ndebed14,
                        nhaberd14 = x.nhaberd14,
                        ndebed15 = x.ndebed15,
                        nhaberd15 = x.nhaberd15,
                        cformula = x.cformula,
                        ccod_csev = x.ccod_csev,
                        usuario = self.usuario
                        )
                    p.save()

    def PgFormato(self):
        buscarConsulta = PgFormato.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgFormato.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgFormato.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgFormato(
                        id=alto,
                        ccod_bal = x.ccod_bal,
                        cdsc = x.cdsc,
                        ntipo = x.ntipo,
                        cformula = x.cformula,
                        nimporte = x.nimporte,
                        nimported = x.nimported,
                        notas = x.notas,
                        cnotac1 = x.cnotac1,
                        cnotac2 = x.cnotac2,
                        ccod_csev = x.ccod_csev,
                        lconfig = x.lconfig,
                        ccodpdt = x.ccodpdt,
                        usuario = self.usuario
                        )
                    p.save()

    def PgFormato2(self):
        buscarConsulta = PgFormato2.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgFormato2.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgFormato2.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgFormato2(
                        id=alto,
                        ccod_bal = x.ccod_bal,
                        cdsc = x.cdsc,
                        ntipo = x.ntipo,
                        cformula = x.cformula,
                        nimporte = x.nimporte,
                        nimported = x.nimported,
                        notas = x.notas,
                        cnotac1 = x.cnotac1,
                        cnotac2 = x.cnotac2,
                        ccod_csev = x.ccod_csev,
                        lconfig = x.lconfig,
                        ccodpdt = x.ccodpdt,
                        usuario = self.usuario
                        )
                    p.save()

    def PgMoneda(self):
        buscarConsulta = PgMoneda.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgMoneda.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgMoneda.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgMoneda(
                        id=alto,
                        ccod_mone = x.ccod_mone,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()    

    def PgOrigenfi(self):
        buscarConsulta = PgOrigenfi.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgOrigenfi.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgOrigenfi.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgOrigenfi(
                        id=alto,
                        ccod_ac = x.ccod_ac,
                        cdsc = x.cdsc,
                        ccuenta = x.ccuenta,
                        usuario = self.usuario
                        )
                    p.save()    

    def PgPagsunat(self):
        buscarConsulta = PgPagsunat.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgPagsunat.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgPagsunat.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgPagsunat(
                        id=alto,
                        ccod_pagsu = x.ccod_pagsu,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()

    def PgPais(self):
        buscarConsulta = PgPais.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgPais.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgPais.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgPais(
                        id = alto,
                        ccod_pais = x.ccod_pais,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()

    def PgPath(self):
        buscarConsulta = PgPath.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgPath.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgPath.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgPath(
                        id=alto,
                        nyear = x.nyear,
                        ccod_emp = x.ccod_emp,
                        lred = x.lred,
                        cctaigv = x.cctaigv,
                        ccta4ta = x.ccta4ta,
                        cctaresu = x.cctaresu,
                        lc1 = x.lc1,
                        lc2 = x.lc2,
                        lc3 = x.lc3,
                        lc4 = x.lc4,
                        lc5 = x.lc5,
                        lc6 = x.lc6,
                        lc7 = x.lc7,
                        lc8 = x.lc8,
                        lc9 = x.lc9,
                        lc10 = x.lc10,
                        lc11 = x.lc11,
                        lc12 = x.lc12,
                        lm1 = x.lm1,
                        lm2 = x.lm2,
                        lm3 = x.lm3,
                        lm4 = x.lm4,
                        lm5 = x.lm5,
                        lm6 = x.lm6,
                        lm7 = x.lm7,
                        lm8 = x.lm8,
                        lm9 = x.lm9,
                        lm10 = x.lm10,
                        lm11 = x.lm11,
                        lm12 = x.lm12,
                        cfiltro = x.cfiltro,
                        cdocexp = x.cdocexp,
                        nigv = x.nigv,
                        cctagan = x.cctagan,
                        cctaper = x.cctaper,
                        csubajus = x.csubajus,
                        lasicosto = x.lasicosto,
                        csubcom = x.csubcom,
                        csubven = x.csubven,
                        lc0 = x.lc0,
                        lc13 = x.lc13,
                        lc14 = x.lc14,
                        lc15 = x.lc15,
                        nvaluacion = x.nvaluacion,
                        lcostol = x.lcostol,
                        lflujoefec = x.lflujoefec,
                        ccolc1 = x.ccolc1,
                        ccolc2 = x.ccolc2,
                        ccolc3 = x.ccolc3,
                        ccolc4 = x.ccolc4,
                        ccolc5 = x.ccolc5,
                        ccolc6 = x.ccolc6,
                        ccolc7 = x.ccolc7,
                        ccolc8 = x.ccolc8,
                        ccolc9 = x.ccolc9,
                        ccolc10 = x.ccolc10,
                        ccolv1 = x.ccolv1,
                        ccolv2 = x.ccolv2,
                        ccolv3 = x.ccolv3,
                        ccolv4 = x.ccolv4,
                        ccolv5 = x.ccolv5,
                        ccolv6 = x.ccolv6,
                        ccolv7 = x.ccolv7,
                        ccolv8 = x.ccolv8,
                        ccolv9 = x.ccolv9,
                        ccolv10 = x.ccolv10,
                        ccueigv2c = x.ccueigv2c,
                        ccueigv3c = x.ccueigv3c,
                        ccueigv2v = x.ccueigv2v,
                        ccueigv3v = x.ccueigv3v,
                        ctadet = x.ctadet,
                        ctaper = x.ctaper,
                        ctaret = x.ctaret,
                        ctaren = x.ctaren,
                        lejec9c = x.lejec9c,
                        lejec9s = x.lejec9s,
                        lpatri = x.lpatri,
                        lpreaut = x.lpreaut,
                        ntc = x.ntc,
                        lkardex = x.lkardex,
                        lkarjun = x.lkarjun,
                        lkarcom = x.lkarcom,
                        ltipocam = x.ltipocam,
                        lvaldoc = x.lvaldoc,
                        lpfludoc = x.lpfludoc,
                        lmarcahora = x.lmarcahora,
                        lliquidac = x.lliquidac,
                        cplancue = x.cplancue,
                        luser = x.luser,
                        lusers = x.lusers,
                        lejec9p = x.lejec9p,
                        lejec9a = x.lejec9a,
                        csubcoma = x.csubcoma,
                        csubvena = x.csubvena,
                        nqueniif = x.nqueniif,
                        csubcc = x.csubcc,
                        csubcv = x.csubcv,
                        lcredito = x.lcredito,
                        lglosa2 = x.lglosa2,
                        lrellena = x.lrellena,
                        lcostos2 = x.lcostos2,
                        ncatalogo = x.ncatalogo,
                        lhoradefa = x.lhoradefa,
                        lhoremp = x.lhoremp,
                        lnumfinal = x.lnumfinal,
                        lpercep = x.lpercep,
                        lverped = x.lverped,
                        cceoserver = x.cceoserver,
                        cceocodusu = x.cceocodusu,
                        cceoclaves = x.cceoclaves,
                        cceocodemp = x.cceocodemp,
                        nregtrib = x.nregtrib,
                        nmontouit = x.nmontouit,
                        nageret = x.nageret,
                        nporret = x.nporret,
                        nmonret = x.nmonret,
                        lcatalogo = x.lcatalogo,
                        cctaigvp = x.cctaigvp,
                        nmonplas = x.nmonplas,
                        usuario = self.usuario
                        )
                    p.save()

    def PgPorcen(self):
        buscarConsulta = PgPorcen.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgPorcen.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgPorcen.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgPorcen(
                        id=alto,
                        ccod_por = x.ccod_por,
                        cdsc = x.cdsc,
                        npor = x.npor,
                        usuario = self.usuario
                        )
                    p.save()

    def PgSunat05(self):
        buscarConsulta = PgSunat05.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgSunat05.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgSunat05.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgSunat05(
                        id=alto,
                        ccod_t05 = x.ccod_t05,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()

    def PgTabdetra(self):
        buscarConsulta = PgTabdetra.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgTabdetra.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgTabdetra.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgTabdetra(
                        id=alto,
                        ccod_detra = x.ccod_detra,
                        nporcen = x.nporcen,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()        

    def PgTipo(self):
        buscarConsulta = PgTipo.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgTipo.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgTipo.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgTipo(
                        id=alto,
                        ccod_tip = x.ccod_tip,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()

    def PgTipod(self):
        buscarConsulta = PgTipod.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgTipod.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgTipod.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgTipod(
                        id=alto,
                        ccod_tipd = x.ccod_tipd,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()

    def PgTiprenta(self):
        buscarConsulta = PgTiprenta.objects.filter(usuario= self.usuario)
        if len(buscarConsulta) <=0:
            pg = PgTiprenta.objects.filter(usuario__isnull=True)
            for x in pg:
                    valorid  = PgTiprenta.objects.values_list('id', flat=True)
                    alto = max(valorid)+1
                    p= PgTiprenta(
                        id = alto,
                        ccod_renta = x.ccod_renta,
                        cdsc = x.cdsc,
                        usuario = self.usuario
                        )
                    p.save()
