from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

from threading import Thread

from datetime import datetime

from decimal import Decimal
import json, traceback, os, csv, random, string
import sys
import requests


def group_required(*group_names):
	"""Requires user membership in at least one of the groups passed in."""
	def in_groups(u):
		if u.is_authenticated():
			if bool(u.groups.filter(name__in=group_names)) | bool(u.groups.filter(name = settings.ROLES['ADMINISTRATION'])) or not group_names:
				return True
		return False
	return user_passes_test(in_groups, login_url='/login/')



def logear(string):
	coneccion = open("log.txt", 'a')
	coneccion.write((string + "\n"))
	coneccion.close()

def send_message(from_email, to, subject, text, html):
	t = Thread(target = Thread_send_emails, args = (from_email, to, subject, text, html))
	t.start()

def Thread_send_emails(from_email, to, subject, text, html):
	try:
		msg = EmailMultiAlternatives(subject, text, from_email, to)
		msg.attach_alternative(html, "text/html")
		msg.send()
		return True
	except Exception:
		print ("error")
	return False





from itertools import islice, chain

class QuerySetChain(object):
	"""
	Chains multiple subquerysets (possibly of different models) and behaves as
	one queryset.  Supports minimal methods needed for use with
	django.core.paginator.
	"""

	def __init__(self, *subquerysets):
		self.querysets = subquerysets

	def count(self):
		"""
		Performs a .count() for all subquerysets and returns the number of
		records as an integer.
		"""
		return sum(qs.count() for qs in self.querysets)

	def _clone(self):
		"Returns a clone of this queryset chain"
		return self.__class__(*self.querysets)

	def _all(self):
		"Iterates records in all subquerysets"
		return chain(*self.querysets)

	def __getitem__(self, ndx):
		"""
		Retrieves an item or slice from the chained set of results from all
		subquerysets.
		"""
		if type(ndx) is slice:
			return list(islice(self._all(), ndx.start, ndx.stop, ndx.step or 1))
		else:
			return islice(self._all(), ndx, ndx+1).next()

def password_generator(size = 6, chars = string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for x in range(size))

def parse_to_round(number, digits):
	cad = "%%0.%sf"%(digits)
	return cad %(round(number, digits))

def get_safe_number(number):
	mask = '*' * (len(number) - 4)
	return '{0}{1}'.format(mask, number[-4:])

def parse_validate_phone(phone):
	do_Return = ""
	split_phone = phone.split("/")
	for x in split_phone:
		y = x.strip()
		if not y or len(y) < 9:
			continue
		y = "".join(y.replace("(","").replace(")","").replace(".","").replace(",","").replace("-","").replace("_","").replace("+","").split())
		if y[:3]=="001":
			y = y[2:]
		try:
			z = int(y)
		except:
			continue
		if len(y) == 11 and y[0] == 1:
			do_Return = y
			break
		elif len(y) == 10:
			do_Return = "1%s"%(y)
			break
		else:
			continue
	if not do_Return:
		one_phone = "".join(phone.replace("(","").replace(")","").replace(".","").replace(",","").replace("-","").replace("_","").replace("+","").replace("/","").split())
		if one_phone[:3]=="001":
			one_phone = one_phone[2:]
		try:
			z = int(one_phone)
		except:
			pass
		if len(one_phone) == 11 and y[0] == 1:
			do_Return = one_phone
		elif len(one_phone) in [10]:
			do_Return = "1%s"%(one_phone)
	return "+" + do_Return

def shorturl(urltoshorten):
	try:
		r = requests.post("https://www.googleapis.com/urlshortener/v1/url",
			params={"key": settings.API_KEY_GOOGLE_API},
			data='{"longUrl": "' + urltoshorten +'"}',
			headers={"Content-Type": "application/json"})
		shorturl = r.json()["id"]
	except:
		shorturl = urltoshorten
	return shorturl

def clean_zero_right(number):
	if Decimal(number) == int(Decimal(number)):
		return str(int(Decimal(number)))
	else:
		return number.rstrip("0")
