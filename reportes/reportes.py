from apps.pacioli.models import *
from django.contrib.auth.models import User

from django.http import HttpResponse, JsonResponse
from django.views.generic import TemplateView,ListView, UpdateView


from datetime import datetime,  timedelta
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl import load_workbook
from openpyxl.styles import Font, Fill ,Border, Side, PatternFill, GradientFill, Alignment,Border

#ruta_origen = "C:/Users/Evanys/Documents/Documentos"
#ruta_origen = "C:/Users/server/Documents/Documentos"
#ruta_origen = "/home/hernancapcha/webapps/demo_static/files"
ruta_origen = "C:/Users/Evanys/Documents/Documentos"
#ruta_origen = "C:/Users/server/Documents/Documentos"
#ruta_origen = "C:/Users/pc07/Documents/Documentos"
#ruta_origen = "/home/hernancapcha/webapps/demo_static/files"


class reporte:
    def __init__(self, usuario):
        self.usuario = usuario

    def libroDiario(self, cmes, proposito):
        if len(cmes)==1:
                cmes = '0'+str(cmes)
        else:
            cmes = cmes
        now = datetime.now()
        datetime_str = ''+cmes+'/28/'+str(now.year)[2:]+' 13:55:26'
        #datetime_str = '09/19/18 13:55:26'
        datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
        last_month = datetime_object - timedelta(days=0)            
        #usuario_id = pk
        usuario_id = self.usuario
        if proposito == 0:
            asientos =  PgDiario.objects.filter(ccod_user=usuario_id).filter(ffechareg__month=cmes).order_by('-nasiento')
        else:
            asientos =  PgDiario.objects.filter(ccod_user=usuario_id).filter(ffechareg__lte=last_month).order_by('-nasiento')
        #asientos =  PgDiario.objects.filter(ccod_user=usuario_id).order_by('nasiento')
        lista = []
        for asiento in asientos:
            cuo = str(asiento.cmes).strip()+str(asiento.ccod_ori).strip()+str(asiento.nasiento).strip()
            m = {'cuo':cuo, 'cmes':asiento.cmes,'ccod_ori':asiento.ccod_ori,'nasiento':str(asiento.nasiento).strip(), 'ffechareg': asiento.ffechareg,'cglosa':asiento.cglosa.strip(),'cnumero':asiento.cnumero,'ccod_cue':asiento.ccod_cue.strip(),'cdsc':asiento.pgplan.cdsc.strip(),'ndebe':asiento.ndebe,'nhaber':asiento.nhaber, 'ccorre40':asiento.ccorre40,'t2008_8': asiento.t2008_8}
            lista.append(m)
        #listaCous= crearListaCous(pk)
        listaCous= crearListaCous(self.usuario)
        for x in listaCous:
            d = []
            for a in lista:
                if x['cuo'] == a['cuo']:
                    datos = {'cmes':a['cmes'], 'ccod_ori': a['ccod_ori'],'nasiento':a['nasiento'],'ffechareg':a['ffechareg'],'cglosa':a['cglosa'],'cnumero':a['cnumero'],'ccod_cue':a['ccod_cue'],'cdsc':a['cdsc'],'ndebe':a['ndebe'],'nhaber':a['nhaber'],'ccorre40':a['ccorre40'],'t2008_8':a['t2008_8']}
                    d.append(datos)
            x['datos'] = d
        nameFile = '/LIBRO DIARIO.xlsx'
        miruta = ruta_origen+nameFile
        wb = load_workbook(miruta)
        ws = wb.active
        
        thin_border = Border(left=Side(style='thin'),
                    right=Side(style='thin'),
                    bottom=Side(style='medium'))

        thin_lat = Border(left=Side(style='thin'),
                    right=Side(style='thin')
                    )

        cont=11
        tDEBE = 11
        tHABER = 11

        ttdebe = float(0)
        tthaber = float(0)
        for dato in listaCous:
            tdebe = float(0)
            thaber = float(0)
            for d in dato['datos']:
                tdebe = tdebe + float(d['ndebe'])
                thaber = thaber + float(d['nhaber'])

                ws['B'+str(cont)] = d['cmes']
                ws['B'+str(cont)].border = thin_lat
                ws['B'+str(cont+2)].border = thin_lat
                ws['B'+str(cont+1)].border = thin_lat

                ws['C'+str(cont)] = d['ccod_ori']
                ws['C'+str(cont)].border = thin_lat
                ws['C'+str(cont+2)].border = thin_lat
                ws['C'+str(cont+1)].border = thin_lat

                ws['D'+str(cont)] = d['nasiento']
                ws['D'+str(cont)].border = thin_lat
                ws['D'+str(cont+2)].border = thin_lat
                ws['D'+str(cont+1)].border = thin_lat

                #ws['E'+str(cont)] = d['ffechareg'].strftime('%d/%m/%y')
                ws['E'+str(cont)] = funcionFecha(d['ffechareg'])
                ws['E'+str(cont)].border = thin_lat
                ws['E'+str(cont+2)].border = thin_lat
                ws['E'+str(cont+1)].border = thin_lat

                ws['F'+str(cont)] = d['cglosa']
                ws['F'+str(cont)].border = thin_lat
                ws['F'+str(cont+2)].border = thin_lat
                ws['F'+str(cont+1)].border = thin_lat

                ws['G'+str(cont)] = d['t2008_8']
                ws['G'+str(cont)].border = thin_lat
                ws['G'+str(cont+2)].border = thin_lat
                ws['G'+str(cont+1)].border = thin_lat

                ws['H'+str(cont)] = d['ccorre40']
                ws['H'+str(cont)].border = thin_lat
                ws['H'+str(cont+2)].border = thin_lat
                ws['H'+str(cont+1)].border = thin_lat

                ws['I'+str(cont)] = d['cnumero']
                ws['I'+str(cont)].border = thin_lat
                ws['I'+str(cont+2)].border = thin_lat
                ws['I'+str(cont+1)].border = thin_lat

                ws['J'+str(cont)] = d['ccod_cue']
                ws['J'+str(cont)].border = thin_lat
                ws['J'+str(cont+2)].border = thin_lat
                ws['J'+str(cont+1)].border = thin_lat

                ws['K'+str(cont)] = d['cdsc']
                ws['K'+str(cont)].border = thin_lat
                ws['K'+str(cont+2)].border = thin_lat
                ws['K'+str(cont+1)].border = thin_lat

                ws['L'+str(cont)] = d['ndebe']
                ws['L'+str(cont)].border = thin_lat
                ws['L'+str(cont+2)].border = thin_lat
                ws['L'+str(cont+1)].border = thin_lat

                ws['M'+str(cont)] = d['nhaber']
                ws['M'+str(cont)].border = thin_lat
                ws['M'+str(cont+2)].border = thin_lat
                ws['M'+str(cont+1)].border = thin_lat

                ws['L'+str(cont+1)] = tdebe
                ws['L'+str(cont)].border = thin_lat
                ws['L'+str(cont+2)].border = thin_lat
                

                ws['M'+str(cont+1)] = thaber
                ws['M'+str(cont)].border = thin_lat
                cont +=1 
            ws['L'+str(cont-1)].border = thin_border
            ws['M'+str(cont-1)].border = thin_border
            ttdebe = ttdebe + tdebe
            tthaber = tthaber + thaber
            cont += 2
        cont = cont
        li = ['B','C','D','E','F','G','H','I','J','K','L','M']

        for x in li:
            ws[x + str(cont-2)].border = thin_lat
            ws[x + str(cont-1)].border = thin_lat
            ws[x + str(cont)].border = thin_border
        x = datetime.now()
        pie = 'Generado automáticamente por Gestión Contable Financiero Plus – Premium 20.00 - NewContaSis - Versión Educativa el ' + funcionFecha(x)
        ws['B' + str(cont+1)]  = pie
        ws['K' + str(cont)]  = 'TOTALES'
        ws['L' + str(cont)]  = ttdebe
        ws['M' + str(cont)]  = tthaber

        nombre_archivo ="LibroDiario.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido

        wb.save(response)
        return response




class ReporteLibroHojaTrabajoExcel(TemplateView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        cmes = self.kwargs.get('cmes')
        proposito = self.kwargs.get('proposito')

        if len(cmes)==1:
            cmes = '0'+str(cmes)
        else:
            cmes = cmes

        now = datetime.now()
        datetime_str = ''+cmes+'/28/'+str(now.year)[2:]+' 13:55:26'
        #datetime_str = '09/19/18 13:55:26'
        datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M:%S')
        last_month = datetime_object - timedelta(days=0)

        if proposito == 0:
            minuevaconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc','pgplan__ntipo').filter(ffechareg__month=cmes).filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
            miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc').filter(ffechareg__month=cmes).filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        else:
            minuevaconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc','pgplan__ntipo').filter(ffechareg__lte=last_month).filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
            miconsulta = PgDiario.objects.values_list('pgplan__ccod_cue','pgplan__cdsc').filter(ffechareg__lte=last_month).filter(ccod_user=pk).annotate(sum_debe=Sum('ndebe')).annotate(sum_haber=Sum('nhaber')).order_by('pgplan__ccod_cue')
        listaNueva=[]
        listaVieja=[]
        for x in minuevaconsulta:
            l = {'ccod_cue': x[0],'cdsc':x[1].strip(),'ntipo':x[2],'sum_debe':x[3],'sum_haber':x[4]}
            listaNueva.append(l)

        for m in miconsulta:
            ll = {'ccod_cue': m[0],'cdsc':m[1].strip(),'sum_debe':m[2],'sum_haber':m[3]}
            listaVieja.append(ll)

        #Creamos el libro de trabajo
        wb = Workbook()
        
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        ws.column_dimensions['C'].width = 35
        ws.column_dimensions['D'].width = 14
        ws.column_dimensions['E'].width = 14
        ws.column_dimensions['F'].width = 14
        ws.column_dimensions['G'].width = 14
        ws.column_dimensions['H'].width = 14
        ws.column_dimensions['I'].width = 14
        ws.column_dimensions['J'].width = 20
        ws.column_dimensions['K'].width = 20
        ws.column_dimensions['L'].width = 20
        ws.column_dimensions['M'].width = 20
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'HOJA DE TRABAJO'
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:E1')
        #Creamos los encabezados desde la celda B3 hasta la E3
        ws['B3'] = 'CUENTA'
        c = ws['B3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['C3'] = 'NOMBRE'
        c = ws['C3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['D3'] = 'DEBITO'
        c = ws['D3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['E3'] = 'CREDITO'
        c = ws['E3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['F3'] = 'S. DEUDOR'
        c = ws['F3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['G3'] = 'S. ACREEDOR'
        c = ws['G3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['H3'] = 'ACTIVO'
        c = ws['H3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['I3'] = 'PASIVO'
        c = ws['I3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['J3'] = 'PERDIDAS NAT'
        c = ws['J3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['K3'] = 'GANANCIAS NAT'
        c = ws['K3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['L3'] = 'PERDIDAS FUN'
        c = ws['L3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        ws['M3'] = 'GANANCIAS FUN'
        c = ws['M3']
        c.font = Font(size=12)
        c.font= Font(bold=True)
        c.fill = PatternFill("solid", fgColor="DDDDDD")
        
        cont=4
        conta=4
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        deu = []
        acre =[]
        activos = []
        pasivos = []
        perdidas_a = []
        ganancias_a = []
        perdidas_f = []
        ganancias_f = []
        acti = 0
        per = 0

        for listado in range(len(listaNueva)):
            resta = listaNueva[listado]['sum_debe'] - listaNueva[listado]['sum_haber']
            #print(listaNueva[listado]['sum_debe'], listaNueva[listado]['sum_haber'], resta)
            if(resta > 0):
                #nuevoJson = { 'deudor' : str(abs(resta)), 'acreedor': str(0)}
                listaNueva[listado]['deudor'] = str(abs(resta))
                listaNueva[listado]['acreedor'] = str(0.00)
            else:
                #nuevoJson = {'deudor' : 0, 'acreedor': str(abs(resta))}
                listaNueva[listado]['deudor'] = str(0.00)
                listaNueva[listado]['acreedor'] = str(abs(resta))


            if listaNueva[listado]['ntipo'] == 1:
                listaNueva[listado]['activo'] = str(abs(resta))
                listaNueva[listado]['pasivo'] = str(0.00)
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0)
                listaNueva[listado]['GanFun'] = float(0)

            if listaNueva[listado]['ntipo'] == 2:
                listaNueva[listado]['activo'] = str(0.00)
                listaNueva[listado]['pasivo'] = str(abs(resta))
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0)
                listaNueva[listado]['GanFun'] = float(0)

            if listaNueva[listado]['ntipo'] == 3:
                if(float(listaNueva[listado]['deudor']) <= 0):
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = abs(resta)
                    '''
                    listaNueva[listado]['perNat'] = float(5000)
                    listaNueva[listado]['GanNat'] = float(50001)
                    listaNueva[listado]['perFun'] = float(50002)
                    listaNueva[listado]['GanFun'] = float(50003)
                    '''
                    listaNueva[listado]['perNat'] = float(listaNueva[listado]['deudor'])
                    listaNueva[listado]['GanNat'] = float(listaNueva[listado]['acreedor'])
                    listaNueva[listado]['perFun'] = float(listaNueva[listado]['activo'])
                    listaNueva[listado]['GanFun'] = float(listaNueva[listado]['pasivo'])
                    
                else:
                    listaNueva[listado]['activo'] = abs(resta)
                    listaNueva[listado]['pasivo'] = str(0.00)
                    '''
                    listaNueva[listado]['perNat'] = float(5000)
                    listaNueva[listado]['GanNat'] = float(50001)
                    listaNueva[listado]['perFun'] = float(50002)
                    listaNueva[listado]['GanFun'] = float(50003)
                    '''
                    listaNueva[listado]['perNat'] = float(listaNueva[listado]['deudor'])
                    listaNueva[listado]['GanNat'] = float(listaNueva[listado]['acreedor'])
                    listaNueva[listado]['perFun'] = float(listaNueva[listado]['activo'])
                    listaNueva[listado]['GanFun'] = float(listaNueva[listado]['pasivo'])
                    
               
            if listaNueva[listado]['ntipo'] == 4:
                if(float(listaNueva[listado]['deudor']) <= 0):
                    #print(listaNueva[listado]['ccod_cue'], "4 esta en pasivo")
                    listaNueva[listado]['activo'] = str(abs(resta))
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(0)
                    listaNueva[listado]['GanNat'] = float(abs(resta))
                    listaNueva[listado]['perFun'] = float(0.00)
                    listaNueva[listado]['GanFun'] = float(0.00)
                else:
                    #print(listaNueva[listado]['ccod_cue'], "4 esta en activo")
                    listaNueva[listado]['activo'] = str(abs(resta))
                    listaNueva[listado]['pasivo'] = str(0.00)
                    listaNueva[listado]['perNat'] = float(abs(resta))
                    listaNueva[listado]['GanNat'] = float(0)
                    listaNueva[listado]['perFun'] = float(0.00)
                    listaNueva[listado]['GanFun'] = float(0.00)

            if listaNueva[listado]['ntipo'] == 5:
                if(float(listaNueva[listado]['deudor']) <= 0):
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(abs(resta))
                    listaNueva[listado]['perNat'] = float(0.00)
                    listaNueva[listado]['GanNat'] = float(0.00)
                    listaNueva[listado]['perFun'] = float(0.00)
                    listaNueva[listado]['GanFun'] = float(abs(resta))
                else:
                    listaNueva[listado]['activo'] = str(0.00)
                    listaNueva[listado]['pasivo'] = str(abs(resta))
                    listaNueva[listado]['perNat'] = float(0.00)
                    listaNueva[listado]['GanNat'] = float(0.00)
                    listaNueva[listado]['perFun'] = float(abs(resta))
                    listaNueva[listado]['GanFun'] = float(0.00)

                
            
            if listaNueva[listado]['ntipo'] == 6:
                listaNueva[listado]['activo'] = str(0.00)
                listaNueva[listado]['pasivo'] = str(abs(resta))
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0.00)
                listaNueva[listado]['GanFun'] = float(0.00)

            if listaNueva[listado]['ntipo'] == 7:
                listaNueva[listado]['activo'] = str(0.00)
                listaNueva[listado]['pasivo'] = str(abs(resta))
                listaNueva[listado]['perNat'] = str(0.00)
                listaNueva[listado]['GanNat'] = str(0.00)
                listaNueva[listado]['perFun'] = float(0.00)
                listaNueva[listado]['GanFun'] = float(0.00)


        for n in miconsulta:
            r = n[2]-n[3]
            a = n[0][:1]
            if r > 0:
                deu.append(abs(r))
                acre.append(0)
            else:
                acre.append(abs(r))
                deu.append(0)
            if a=='1' or a == '2' or a=='3' or a=='4' or a=='5':
                acti= acti + 1
            else:
                per = per + 1

        for n in range(len(deu)):
            if acti > n:
                activos.append(deu[n])
                pasivos.append(acre[n])
                perdidas_a.append(float(listaNueva[n]['perNat']))
                ganancias_a.append(float(listaNueva[n]['GanNat']))

                perdidas_f.append(float(listaNueva[n]['perFun']))
                ganancias_f.append(float(listaNueva[n]['GanFun']))
                #perdidas_a.append(0)
                #ganancias_a.append(0)
            else:
                activos.append(0)
                pasivos.append(0)
                perdidas_a.append(float(listaNueva[n]['perNat']))
                ganancias_a.append(float(listaNueva[n]['GanNat']))

                perdidas_f.append(float(listaNueva[n]['perFun']))
                ganancias_f.append(float(listaNueva[n]['GanFun']))
                #tod[val].append(listaNueva[val]['perFun'])
                #tod[val].append(listaNueva[val]['GanFun'])
                #perdidas_a.append(deu[n])
                #ganancias_a.append(acre[n])

        for asiento in miconsulta:
            ws.cell(row=cont,column=2).value = asiento[0]
            _cell = ws.cell(row=cont, column = 2)
            _cell.number_format = "@"
            ws.cell(row=cont,column=3).value = asiento[1]
            _cell = ws.cell(row=cont, column = 3)
            _cell.number_format = "@"
            
            ws.cell(row=cont,column=4).value = asiento[2]
            _cell = ws.cell(row=cont, column = 4)
            _cell.number_format = '#,##0.00'
            ws.cell(row=cont,column=5).value = asiento[3]
            _cell = ws.cell(row=cont, column = 5)
            _cell.number_format = '#,##0.00'
            cont = cont + 1

        for x in range(len(deu)):
            ws.cell(row=conta,column=6).value = deu[x]
            _cell = ws.cell(row=conta, column = 6)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=7).value = acre[x]
            _cell = ws.cell(row=conta, column = 7)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=8).value = activos[x]
            _cell = ws.cell(row=conta, column = 8)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=9).value = pasivos[x]
            _cell = ws.cell(row=conta, column = 9)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=10).value = perdidas_a[x]
            _cell = ws.cell(row=conta, column = 10)
            _cell.number_format = '#,##0.00'
            ws.cell(row=conta,column=11).value = ganancias_a[x]
            _cell = ws.cell(row=conta, column = 11)
            _cell.number_format = '#,##0.00'

            ws.cell(row=conta,column=12).value = perdidas_f[x]
            _cell = ws.cell(row=conta, column = 12)
            _cell.number_format = '#,##0.00'

            ws.cell(row=conta,column=13).value = ganancias_f[x]
            _cell = ws.cell(row=conta, column = 13)
            _cell.number_format = '#,##0.00'

            conta = conta + 1
        #5 es la fila
        fl = len(deu)+5
        flb= fl+1
        flc=fl+2

        #sumas de columnas totales
        tDEBITO = 0
        for x in miconsulta:
            tDEBITO = tDEBITO + x[2]
        tCREDITO =0
        for x in miconsulta:
            tCREDITO = tCREDITO + x[3]
        tDEUDOR = sum(deu)
        tACREEDOR = sum(acre)
        tACTIVO = sum(activos)
        tPASIVO = sum(pasivos)
        tPERDIDAS = sum(perdidas_a)
        tGANANCIAS = sum(ganancias_a)

        tfPERDIDAS = sum(perdidas_f)
        tfGANANCIAS = sum(ganancias_f)

        tACTIVtPASIVO = tACTIVO - tPASIVO
        tPERDIDAStGANANCIA = tPERDIDAS-tGANANCIAS

        tfPERDIDAStGANANCIA = tfPERDIDAS-tfGANANCIAS
        
        sIGUALESTOTALACTIVO = 0
        sIGUALESTOTALPASIVO = 0
        sIGUALESTOTALPERDIDAS = 0
        sIGUALESTOTALGANANCIAS = 0

        sFIGUALESTOTALPERDIDAS = 0
        sFIGUALESTOTALGANANCIAS = 0

        ws['B'+str(fl)] = 'TOTALES'
        ws.merge_cells('B'+str(fl) +':'+'C'+str(fl))
        


        ws['D'+str(fl)] = tDEBITO
        _cell = ws['D'+str(fl)]
        #_cell.number_format = '#,##0.00'

        ws['E'+str(fl)] = tCREDITO
        _cell = ws['E'+str(fl)]
        #_cell.number_format = '#,##0.00'

        ws['F'+str(fl)] = tDEUDOR
        _cell = ws['F'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['G'+str(fl)] = tACREEDOR
        _cell = ws['G'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['H'+str(fl)] = tACTIVO
        _cell = ws['H'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['I'+str(fl)] = tPASIVO
        _cell = ws['I'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['J'+str(fl)] = tPERDIDAS
        _cell = ws['J'+str(fl)]
        #_cell.number_format = '#,##0.00'
        
        ws['K'+str(fl)] = tGANANCIAS
        _cell = ws['K'+str(fl)]

        ws['L'+str(fl)] = tfPERDIDAS
        _cell = ws['L'+str(fl)]

        ws['M'+str(fl)] = tfGANANCIAS
        _cell = ws['M'+str(fl)]
        #_cell.number_format = '#,##0.00'

        letrasnumeros = ['D','E','F','G','H','I','J','K','L','M']
        for m in letrasnumeros:
            h = m+ str(fl)
            celda  = ws[h]
            celda.number_format = '#,##0.00'
        

        ws['B'+str(flb)] = 'MENSAJES'

        ws.merge_cells('B'+str(flb) +':'+'C'+str(flb))
        if tACTIVtPASIVO <0:
            ws['B'+str(flb)] = 'PERDIDA DEL EJERCICIO'
            ws.merge_cells('B'+str(flb) +':'+'C'+str(flb))

            ws['H'+str(flb)] =  abs(tACTIVtPASIVO)
            _cell = ws['H'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['I'+str(flb)] =  0
            _cell = ws['I'+str(flb)]
            _cell.number_format = '#,##0.00'

            sIGUALESTOTALACTIVO = abs(tACTIVO) + abs(tACTIVtPASIVO)
            sIGUALESTOTALPASIVO = tPASIVO + 0
        else:
            ws['B'+str(flb)] = 'GANANCIA DEL EJERCICIO'
            ws.merge_cells('B'+str(flb) +':'+'C'+str(flb))
            ws['H'+str(flb)] =  0
            _cell = ws['H'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['I'+str(flb)] =  abs(tACTIVtPASIVO)
            _cell = ws['I'+str(flb)]
            _cell.number_format = '#,##0.00'

            sIGUALESTOTALACTIVO = tACTIVO + 0
            sIGUALESTOTALPASIVO = abs(tPASIVO) + abs(tACTIVtPASIVO)
        
        if tPERDIDAStGANANCIA <0:
            ws['J'+str(flb)] =  abs(tPERDIDAStGANANCIA)
            _cell = ws['J'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['K'+str(flb)] =  0
            _cell = ws['K'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['L'+str(flb)] =  abs(tfPERDIDAStGANANCIA)
            _cell = ws['L'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['M'+str(flb)] =  0
            _cell = ws['M'+str(flb)]
            _cell.number_format = '#,##0.00'

            sIGUALESTOTALPERDIDAS = abs(tPERDIDAS) + abs(tPERDIDAStGANANCIA)
            sIGUALESTOTALGANANCIAS = tGANANCIAS + 0

            sFIGUALESTOTALPERDIDAS = abs(tfPERDIDAS) + abs(tfPERDIDAStGANANCIA)
            sFIGUALESTOTALGANANCIAS = tfGANANCIAS + 0
        else:
            ws['J'+str(flb)] =  0
            _cell = ws['J'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['K'+str(flb)] =  abs(tPERDIDAStGANANCIA)
            _cell = ws['K'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['L'+str(flb)] =  0
            _cell = ws['L'+str(flb)]
            _cell.number_format = '#,##0.00'

            ws['M'+str(flb)] =  abs(tfPERDIDAStGANANCIA)
            _cell = ws['M'+str(flb)]
            _cell.number_format = '#,##0.00'

            sIGUALESTOTALPERDIDAS = tPERDIDAS + 0
            sIGUALESTOTALGANANCIAS = abs(tGANANCIAS) + abs(tPERDIDAStGANANCIA)

            sFIGUALESTOTALPERDIDAS = tfPERDIDAS + 0
            sFIGUALESTOTALGANANCIAS = abs(tfGANANCIAS) + abs(tfPERDIDAStGANANCIA)


        ws['B'+str(flc)] = 'SUMAS IGUALES'
        ws.merge_cells('B'+str(flc) +':'+'C'+str(flc))
        ws['H'+str(flc)] = abs(sIGUALESTOTALACTIVO) 
        _cell = ws['H'+str(flc)]
        _cell.number_format = '#,##0.00'
        
        
        ws['I'+str(flc)] = abs(sIGUALESTOTALPASIVO)
        _cell = ws['I'+str(flc)]
        _cell.number_format = '#,##0.00'
        
        ws['J'+str(flc)] = abs(sIGUALESTOTALPERDIDAS)
        _cell = ws['J'+str(flc)]
        _cell.number_format = '#,##0.00'
        
        ws['K'+str(flc)] = abs(sIGUALESTOTALGANANCIAS)
        _cell = ws['K'+str(flc)]
        _cell.number_format = '#,##0.00'

        ws['L'+str(flc)] = abs(sFIGUALESTOTALGANANCIAS)
        _cell = ws['L'+str(flc)]
        _cell.number_format = '#,##0.00'

        ws['M'+str(flc)] = abs(sFIGUALESTOTALGANANCIAS)
        _cell = ws['M'+str(flc)]
        _cell.number_format = '#,##0.00'
        

        miarr = ['B','C', 'D','E','F','G','H','I','J','K','L','M']
        mitroarr = [fl,flb,flc]
        for n in miarr:
            for m in mitroarr:
                h=str(n)+str(m)
                c = ws[h]
                c.font = Font(size=12)
                c.font= Font(bold=True)
                c.fill = PatternFill("solid", fgColor="DDDDDD")

        
        #Establecemos el nombre del archivo
        nombre_archivo ="HojadeTrabajo.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response


def crearListaCous(usuario):
    asientos =  PgDiario.objects.filter(ccod_user=usuario).order_by('nasiento')
    listaAsiento=[]
    for asiento in asientos:
        #m = {'cmes':asiento.cmes,'ccod_ori':asiento.ccod_ori,'nasiento':asiento.nasiento}
        cuo = str(asiento.cmes).strip()+str(asiento.ccod_ori).strip()+str(asiento.nasiento).strip()
        res = next((sub for sub in listaAsiento if sub['cuo'] == cuo), None)
        if res == None:
            listaAsiento.append({'cuo': cuo,'datos':''})
    return listaAsiento

def funcionFecha(x):
    #x = datetime.now()
    day = str(x.day)
    month = str(x.month)
    if len(day) == 1:
        day = '0' + day
    else:
        day =  day
    if len(month) == 1:
        month = '0' +  month
    else:
        month =  month
    ff = str(day) +'/'+ str(month) +'/'+ str(x.year)
    return ff