
import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'demosecond.settings')

application = get_wsgi_application()
'''
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = "demosecond.settings"
activate_this = os.path.expanduser("~/.virtualenvs/django23/bin/activate_this.py")

exec(open(activate_this).read(),{'__file__':activate_this})

from django.core.wsgi import get_wsgi_application

#os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'demo.settings')

application = get_wsgi_application()
'''