from django.contrib import admin
from django.urls import path , include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('pacioli/', include('apps.pacioli.urls'),name='pacioli'),
    path('experto/', include('apps.experto.urls'), name='experto'),
    path('expertoPlus/', include('apps.expertoPlus.urls'), name='expertoPlus'),
    path('', include('apps.api.urls'),name='api'),
    path('', include('apps.logeo.urls')),
]
